<?php
@include 'header.php';
?>
<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="#">Home</a>
                <a href="#" class="active">Video</a>
            </div>
        </div>
    </div>
</nav>
<!--- video player ---------->
<article class="video-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class=" mb-20 xs-text-center">
                    <h1 class="uppercase video-head">videos</h1>
                    <i class="arrows"><img src="assets/img/right-bar.png"></i>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="videos-bg">
                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 float-left videos-bg-full">
                    <a href="#" class="video-link">
                        <img src="assets/img/video-top-icon.png" class="img-fluid video-top">
                        <div class="player">
                            <img src="assets/img/video-play.jpg" class="img-fluid" alt="video-player" title="player">
                            <h4 class="video-title">Watch: Team ‘Golmaal Again’ does some golmaal on the sets of ‘The Drama Company’
                                <small class="text-muted fs-14 d-block">October 21, 2017</small></h4>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-12 float-left">
                    <div class="grid-videos row">
                        <div class="md-col-50">
                            <div class="card">
                                <a href="#">
                                    <div class="card-link">
                                        <div class="video-content">
                                            <img src="assets/img/video-side-link.jpg" class="img-fluid full-wide" >
                                        </div>
                                        <div class="video-sub">
                                            <p>Deepika Padukone on learning folk dance for..</p>
                                            <small class="text-muted">October 21, 2017</small>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="md-col-50">
                            <div class="card">
                                <a href="#">
                                    <div class="card-link">
                                        <div class="video-content">
                                            <img src="assets/img/video-side-link2.jpg" class="img-fluid full-wide" >
                                        </div>
                                        <div class="video-sub">
                                            <p>Irrfan Khan & Parvathy reveal everything about Their ..</p>
                                            <small class="text-muted">October 21, 2017</small>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>
<!--- end video player ----------->

<!------ most popular------>
<section>
    <div class="container">
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="xs-text-center">
                    <h2 class="box uppercase title d-inline-b">most popular
                    </h2>
                    <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                </div>
                <div class="row" data-plugin="matchHeight" data-by-row="true">
                    <div class="col-md-6 col-xs-12">
                        <div class="card">
                            <a href="#">
                                <div class="card-link">
                                    <div class="video-content">
                                        <img src="assets/img/videos-link1.jpg" alt="video-link" class="img-fluid full-wide">
                                        <label class="duration">15:22</label>
                                    </div>
                                    <div class="card-title">
                                        <h3>Find out why would Farhan Akhtar wants to disco dance...</h3>
                                        <small class="text-muted">October 21, 2017</small>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="card">
                            <a href="#">
                                <div class="card-link">
                                    <div class="video-content width-40">
                                        <img src="assets/img/alia-video.jpg" alt="video-link" class="img-fluid full-wide">
                                        <label class="duration">15:22</label>
                                    </div>
                                    <div class="card-title width-60">
                                        <h3>Raazi is a very different film and it's awesome...</h3>
                                        <small class="text-muted">October 21, 2017</small>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row" data-plugin="matchHeight" data-by-row="true">
                    <div class="col-md-6 col-xs-12 float-left">
                        <div class="card">
                            <a href="#">
                                <div class="card-link">
                                    <div class="video-content width-40">
                                        <img src="assets/img/vidya-video.jpg" alt="video-link" class="img-fluid full-wide">
                                        <label class="duration">15:22</label>
                                    </div>
                                    <div class="card-title width-60">
                                        <h3>Check Out The Fantastic Behind The Scenes of 'Hawa Hawai 2.0' Song...</h3>
                                        <small class="text-muted">October 21, 2017</small>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12 float-left">
                        <div class="card">
                            <a href="#">
                                <div class="card-link">
                                    <div class="video-content width-40">
                                        <img src="assets/img/video-link2.jpg" alt="video-link" class="img-fluid full-wide">
                                        <label class="duration">15:22</label>
                                    </div>
                                    <div class="card-title width-60">
                                        <h3>Kriti Kharbanda's CANDID Rapid Fire On Deepika..</h3>
                                        <small class="text-muted">October 21, 2017</small>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row" data-plugin="matchHeight" data-by-row="true">
                    <div class="col-md-6 col-xs-12 float-left">
                        <div class="card">
                            <a href="#">
                                <div class="card-link">
                                    <div class="video-content width-40">
                                        <img src="assets/img/video-link3.jpg" alt="video-link" class="img-fluid full-wide">
                                        <label class="duration">15:22</label>
                                    </div>
                                    <div class="card-title width-60">
                                        <h3>Check Out The Fantastic Behind The Scenes of 'Hawa Hawai 2.0' Song...</h3>
                                        <small class="text-muted">October 21, 2017</small>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12 float-left">
                        <div class="card">
                            <a href="#">
                                <div class="card-link">
                                    <div class="video-content width-40">
                                        <img src="assets/img/video-link4.jpg" alt="video-link" class="img-fluid full-wide">
                                        <label class="duration">15:22</label>
                                    </div>
                                    <div class="card-title width-60">
                                        <h3>Check Out The Fantastic Behind The Scenes of 'Hawa Hawai 2.0' Song...</h3>
                                        <small class="text-muted">October 21, 2017</small>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="button-center">
                    <a href="#" class="btn btn-default">view all</a>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="bg-white border-primary mt-3">
                    <div class="text-center">
                        <h2 class="artist uppercase sideTitle">most viewed</h2>
                    </div>
                    <div class="video-list">
                        <a href="#">
                            <div class="card-link mb-20">
                                <div class="video-content">
                                    <img src="assets/img/most-viewed-1.jpg" class="img-fluid full-wide" alt="video">
                                    <label class="duration">15:22</label>
                                </div>
                                <h3>Raees</h3>
                            </div>
                        </a>
                        <a href="#">
                            <div class="card-link mb-20">
                                <div class="video-content">
                                    <img src="assets/img/most-viewed-2.jpg" class="img-fluid full-wide" alt="video">
                                    <label class="duration">15:22</label>
                                </div>
                                <h3>Kaabil</h3>
                            </div>
                        </a>
                        <a href="#">
                            <div class="card-link mb-20">
                                <div class="video-content">
                                    <img src="assets/img/most-viewed-3.jpg" class="img-fluid full-wide" alt="video">
                                    <label class="duration">15:22</label>
                                </div>
                                <h3>Padmawati</h3>
                            </div>
                        </a>
                        <a href="#">
                            <div class="card-link mb-20">
                                <div class="video-content">
                                    <img src="assets/img/most-viewed-1.jpg" class="img-fluid full-wide" alt="video">
                                    <label class="duration">15:22</label>
                                </div>
                                <h3>Gentleman</h3>
                            </div>
                        </a>

                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!------end most popular------>

<!---- BB Special ---------->
<section class="bg-pink">
    <div class="container">
        <div class="col-md-12 border-primary bg-white">
            <div class="row">
                <div class="full-wide text-center">
                    <h2 class="artist bg-light-red sideTitle special uppercase">bb special</h2>
                </div>
                <div class="row">
                    <div class="col-md-3 col-xs-12" id="order2">
                        <div class="left-side">
                            <div class="videos">
                                <a href="#">
                                    <div class="video-content width-40">
                                        <img src="assets/img/bb-special-1.jpg" alt="BB Special" class="img-fluid full-wide">
                                        <label class="duration">15:22</label>
                                    </div>
                                    <div class="width-60">
                                        <h3>Abhishek Bachchan saves Aishwarya Rai from an OOPS..</h3>
                                        <small class="text-muted">October 21, 2017</small>
                                    </div>
                                </a>
                            </div>
                            <div class="videos">
                                <a href="#">
                                    <div class="video-content width-40">
                                        <img src="assets/img/bb-special-2.jpg" alt="BB Special" class="img-fluid full-wide">
                                        <label class="duration">15:22</label>
                                    </div>
                                    <div class="width-60">
                                        <h3>Kareena adds an edge to the traditional look...</h3>
                                        <small class="text-muted">October 21, 2017</small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12" id="order1">
                        <div class="main-special">
                            <div class="videos">
                                <a href="#">
                                    <div class="video-content">
                                        <img src="assets/img/bb-special-main.jpg" alt="main-video"  class="img-fluid full-wide">
                                        <label class="duration">15:22</label>
                                    </div>
                                    <h2 class="font-wt-400">Deepika Padukone and Farah Khan spotted at the airport!</h2>
                                    <small class="text-muted">October 21, 2017</small>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-12" id="order3">
                        <div class="right-side">
                            <div class="videos">
                                <a href="#">
                                    <div class="video-content width-40">
                                        <img src="assets/img/bb-special-3.jpg" alt="BB Special" class="img-fluid full-wide">
                                        <label class="duration">15:22</label>
                                    </div>
                                    <div class="width-60">
                                        <h3>Ranbir Kapoor, Aadar Jain get clicked during the usual...</h3>
                                        <small class="text-muted">October 21, 2017</small>
                                    </div>
                                </a>
                            </div>
                            <div class="videos">
                                <a href="#">
                                    <div class="video-content width-40">
                                        <img src="assets/img/bb-special-4.jpg" alt="BB Special" class="img-fluid full-wide">
                                        <label class="duration">15:22</label>
                                    </div>
                                    <div class="width-60">
                                        <h3>Bollywood celeb strolls: What were they up to, this week?</h3>
                                        <small class="text-muted">October 21, 2017</small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="button-center">
                    <a href="#" class="btn btn-default">view all</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!---- end BB Special ---------->
<!------ exclusives ------>
<section>
    <div class="container">
        <div class="row">
            <div class="full-wide text-center mb-40">
                <i class="arrowsSub"><img src="assets/img/left-bar.png"></i>
                <h2 class="d-inline title uppercase">exclusives</h2>
                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
            </div>
            <div class="col-md-8 col-xs-12">
                <div class="exclusive">
                    <div class="videos">
                        <a href="#">
                            <div class="card-link">
                                <div class="video-content">
                                    <img src="assets/img/exclusive-main.jpg" alt="main-video"  class="img-fluid full-wide">
                                    <label class="duration">15:22</label>
                                </div>
                                <h2 class="font-wt-400">Swag Se Swagat: Salman-Katrina up the temperature in this instant HIT from TZH</h2>
                                <small class="text-muted">October 21, 2017</small>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="exclusive-r">
                    <div class="videos">
                        <a href="#">
                            <div class="card-link">
                                <div class="video-content width-40">
                                    <img src="assets/img/exclusive-link1.jpg" alt="BB Special" class="img-fluid full-wide">
                                    <label class="duration">15:22</label>
                                </div>
                                <div class="width-60">
                                    <h3>Shah Rukh Khan joins Farhan Akhtar at the #Lalkaar concert</h3>
                                    <small class="text-muted">October 21, 2017</small>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="videos">
                        <a href="#">
                            <div class="card-link">
                                <div class="video-content width-40">
                                    <img src="assets/img/exclusive-link2.jpg" alt="BB Special" class="img-fluid full-wide">
                                    <label class="duration">15:22</label>
                                </div>
                                <div class="width-60">
                                    <h3>Watch: Aishwarya Rai Bachchan gets emotional and cries in front of media</h3>
                                    <small class="text-muted">October 21, 2017</small>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="button-center">
            <a href="#" class="btn btn-default">view all</a>
        </div>
    </div>
</section>
<!------end exclusives ------->
<!---------- events ------>
<section class="bg-grey">
    <div class="container">
        <div class="row">
            <div class="full-wide text-center mb-40">
                <i class="arrowsSub"><img src="assets/img/left-bar.png"></i>
                <h2 class="d-inline title text-center uppercase">events</h2>
                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div class="events">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <a href="#">
                        <div class="card-link videos">
                            <div class="video-content">
                                <img src="assets/img/events1.jpg" alt="BB Special" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <div>
                                <h3>Dhadak: Janhvi and Ishaan soak in moments...</h3>
                                <small class="text-muted">October 21, 2017</small>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <a href="#">
                        <div class="card-link videos">
                            <div class="video-content width-40">
                                <img src="assets/img/events2.jpg" alt="BB Special" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <div class="width-60">
                                <h3>5 unknown facts about Janhvi Kapoor that her Instagram...</h3>
                                <small class="text-muted">October 21, 2017</small>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <a href="#">
                        <div class="card-link videos">
                            <div class="video-content width-40">
                                <img src="assets/img/events3.jpg" alt="BB Special" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <div class="width-60">
                                <h3>Padmavati: Deepika Padukone's 'ghoomar' act...</h3>
                                <small class="text-muted">October 21, 2017</small>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <a href="#">
                        <div class="card-link videos">
                            <div class="video-content width-40">
                                <img src="assets/img/event4.jpg" alt="BB Special" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <div class="width-60">
                                <h3>THIS movie would have been Chitrangda's debut instead...</h3>
                                <small class="text-muted">October 21, 2017</small>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="button-center">
            <a href="#" class="btn btn-default">view all</a>
        </div>
    </div>
</section>
<!----------end  events ------->
<!---------- interviews -------->
<section class="interview-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12 xs-text-center">
                <h2 class="box uppercase title d-inline-b">interviews
                </h2>
                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <div class="col-md-4 col-sm-4 col-xs-12 mb-30">
                <div class="card">
                    <a href="#">
                        <div class="card-link">
                            <div class="video-content">
                                <img src="assets/img/interview-.jpg" alt="video-link" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <div class="card-title text-center">
                                <h3>WOW! Zaira Wasim receives National Child Award from President...</h3>
                                <small class="text-muted">October 21, 2017</small>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 mb-30">               
                <div class="card">
                    <a href="#">
                        <div class="card-link">
                            <div class="video-content">
                                <img src="assets/img/interview-1.jpg" alt="video-link" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <div class="card-title text-center">
                                <h3>Arjun Kapoor and Parineeti Chopra start shooting Sandeep Aur Pinky Faraar in New Delhi!</h3>
                                <small class="text-muted">October 21, 2017</small>
                            </div>
                        </div>
                    </a>
                </div>               
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mb-30">
                <div class="card">
                    <a href="#">
                        <div class="card-link">
                            <div class="video-content">
                                <img src="assets/img/interview-2.jpg" alt="video-link" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <div class="card-title text-center">
                                <h3>Who is Amir Khan? The boxing world champ hoping to knock out the...</h3>
                                <small class="text-muted">October 21, 2017</small>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="row " data-plugin="matchHeight" data-by-row="true">
            <div class="col-md-4 col-sm-4 col-xs-12 mb-30">
                <div class="card">
                    <a href="#">
                        <div class="card-link">
                            <div class="video-content">
                                <img src="assets/img/interview-3.jpg" alt="video-link" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <div class="card-title text-center">
                                <h3>Sushant Singh Rajput will make you blush with his charm</h3>
                                <small class="text-muted">October 21, 2017</small>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 mb-30">
                <div class="card">
                    <a href="#">
                        <div class="card-link">
                            <div class="video-content">
                                <img src="assets/img/interview-5-.jpg" alt="video-link" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <div class="card-title text-center">
                                <h3>Irrfan Khan plays a Bengali writer inspired by Humayun Ahmed in Mostofa Farooki’s next</h3>
                                <small class="text-muted">October 21, 2017</small>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 mb-30">
                <div class="card">
                    <a href="#">
                        <div class="card-link">
                            <div class="video-content">
                                <img src="assets/img/interview-6.jpg" alt="video-link" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <div class="card-title text-center">
                                <h3>Aditya Roy Kapur, Shraddha Kapoor, Gurmeet Choudhary spotted after </h3>
                                <small class="text-muted">October 21, 2017</small>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="button-center">
            <a href="#" class="btn btn-default">view all</a>
        </div>
    </div>
</section>
<!-------- end interviews ------->


<?php
@include 'footer.php';
?>

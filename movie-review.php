<?php @include 'header.php'; ?>

<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="#">Movies</a>
                <a href="#" class="active">Golmal Again</a>
            </div>
        </div>
    </div>
</nav>
<article>
    <div class="ad-verticle-160 ad-left-fix">
        <a href="#">
            <img src="assets/img/ad-160-01.jpg" alt="ads" title="">
        </a>
    </div>
    <div class="container">
        <div class="row">
            <div class="width-600">
                <div class="full-wide">
                    <h2 class="title2">movie review</h2>
                    <a href="#" class="btn btn-primary btn-round float-right">movie page</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="width-600">
                <div class="full-wide">
                    <img src="assets/img/golmal-review.jpg" alt="Golmal Poster" title="Golmal Again" class="img-fluid full-wide">
                </div>
                <div class="movie-details">
                    <h1>Golmaal Again</h1>
                    <p><span>U/A</span>Hindi</p>
                    <p><span><i class="ion-ios-calendar-outline"></i></span>19th May</p>
                    <p><span><i class="ion-clock"></i></span>2 hr 50 mins</p>
                    <p><span><i class="icon"><img src="assets/img/genre-icon.png" class="img-fluid" alt="genre-icon"></i></span>Comedy drama</p>
                </div>
                <div class="rating border">
                    <div class="star-ratings-css">
                        <label>Bollywood Bubble Rating</label>
                        <div class="star-ratings-sprite">
                            <span style="width:68%" class="star-ratings-sprite-rating"></span>
                        </div>
                    </div>
                </div>
                <div class="share-buttons">
                    <div class="icon-box">
                        <ul class="nav navbar-nav">
                            <li class="dropdown mega-dropdown">
                                <i class="ion-android-share-alt fs-2"></i><span>Share</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="review-des">
                    <h3>laugh and be merry</h3>
                    <p>Golmaal is the screwball comedy franchise that has been kept alive for the last eleven years. Reruns on satellite channels always make you chuckle, no matter from which point you catch the film. Like its previous instalments, this one too has moments of unadulterated fun. The gags tnate alternate between being truly funny and averagely routine but you find yourself laughing 
                        uncontrollably because each of the actors here is a delight to watch. Whether it's the lisping Lakshman (Shreyas) or the bully Gopal, who is petrified of ghosts, everyone is so proficient, you can't help but smile at their antics. The dialogue is pedestrian but witty. </p>
                    <p>Of course then you have the additional horror angle thrown in. Drawing inspiration from the South film   Kanchana, where a ghost is out to seek revenge, these parts too are treated 
                        lightheartedly making you guffaw at situations that you may have seen many times over. However, in the second half, when the actual plot is introduced, proceedings get slightly dull.</p>
                    <p>Here one must say, it is to Rohit Shetty's credit that he never lets you get too fidgety. Gags, fights, songs, giggles, ghosts, here is a buffet you can overdose on. At times, you actually 
                        admonish yourself for being stupid but you can't help but guffaw. As far as performances go, Tabu as the 'exorcist' lends credence to a part, which could have otherwise looked ridiculous. The add-on members — Vasuli Bhai (Mukesh) and Pappi (Johny Lever) have their moments too but Shreyas is the scene-stealer. Parineeti (Khushi/Damini) is just </p>
                    <p>The add-on members — Vasuli Bhai (Mukesh) and Pappi (Johny Lever) have their moments too but Shreyas is the scene-stealer. Parineeti (Khushi/Damini) is just </p>
                    <div class="read-more">
                        <label>Also Read :</label> <br />
                        <a href="#">'Golmaal Again' box-office collection Day 2</a>
                    </div>
                </div>
            </div>
            <div class="right-bar-fix-width-300 mt-30">
                <div class="ads mb-40">
                    <a href="#">
                        <img src="assets/img/mmt-ads.jpg" alt="mmt-ads" class="img-fluid">
                    </a>
                </div>
                <div class="bubble-tv-box p-0">
                    <div class="text-center">
                        <h2 class="uppercase sideTitle text-center">bubble tv<i class="bubble-tv"><img src="assets/img/youtube-icon.png"></i></h2>
                    </div>
                    <div class="video-list">
                        <a href="#">
                            <div class="card-link">
                                <div class="video-content">
                                    <img src="assets/img/kalki.jpg" class="img-fluid full-wide" alt="video">
                                </div>
                                <div class="video-sub">
                                    <h5>Kalki Koechlin visited Bollywood Bubble and this is how we welcomed her!</h5>
                                    <small class="text-muted">October 21, 2017</small>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="video-list">
                        <a href="#">
                            <div class="card-link">
                                <div class="video-content">
                                    <img src="assets/img/salman-big.jpg" class="img-fluid full-wide" alt="video">
                                </div>
                                <div class="video-sub">
                                    <h5>Salman Khan sings his heart out as he takes the center stage at
                                        Birmingham..</h5>
                                    <small class="text-muted">October 21, 2017</small>
                                </div>
                            </div>
                        </a>
                        <a href="#">
                            <div class="card-link">
                                <div class="video-content">
                                    <img src="assets/img/maliaka.jpg" class="img-fluid full-wide" alt="video">
                                </div>
                                <div class="video-sub">
                                    <h5>Video alert! Learn to slay the monochrome look from Malaika Arora..</h5>
                                    <small class="text-muted">October 21, 2017</small>
                                </div>
                            </div>
                        </a>
                        <a href="#">
                            <div class="card-link">
                                <div class="video-content">
                                    <img src="assets/img/bb-video-3.jpg" class="img-fluid full-wide" alt="video">
                                </div>
                                <div class="video-sub">
                                    <h5>Watch: Sonakshi, Jacqueline head home after some ‘Da Bangg’iyat in UK</h5>
                                    <small class="text-muted">October 21, 2017</small>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ad-verticle-160 ad-right-fix">
        <a href="#">
            <img src="assets/img/ad-160-02.jpg" alt="ads" title="">
        </a>
    </div>
</article>

<?php @include 'footer.php'; ?>


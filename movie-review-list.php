<?php @include 'header.php'; ?>

<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="#">Movies</a>
                <a href="#" class="active">Movie reviews</a>
            </div>
        </div>
    </div>
</nav>
<article class="review-lists-bg">
    <div class="container">
        <div class="row">
            <div class="mb-30 xs-text-center">
                <h1 class="d-inline title uppercase">Movie reviews</h1> 
                <i class="arrows"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <div class="col-md-6 col-xs-12">
                <div class="review-list">
                    <div>
                        <img src="assets/img/review-1.jpg" alt="Movie review" title="Review" class="img-fluid">
                    </div>
                    <div class="review-details">
                        <h5>secret superstar</h5>
                        <div class="star-ratings-sprite">
                            <span style="width:68%" class="star-ratings-sprite-rating"></span>
                        </div>
                        <p><span class="pr-3"><i class="ion-ios-calendar-outline"></i>19th May 2017</span>
                            <span class="pr-3"><i class="icon"><img src="assets/img/genre-icon.png" class="img-fluid" alt="genre-icon"></i>comedy, drama</span></p>
                        <p><span>Star Cast </span><label>Shah Rukh Khan, Mahira Khan, Nawazuddin Siddiqui</label></p>
                        <p class="font-wt-400">Raees is a paisa-wasool entertainer for massy Bollywood fans! It is a complete package of action, romance...</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="review-list">
                    <div>
                        <img src="assets/img/review-2.jpg" alt="Movie review" title="Review" class="img-fluid">
                    </div>
                    <div class="review-details">
                        <h5>juduwaa 2</h5>
                        <div class="star-ratings-sprite">
                            <span style="width:68%" class="star-ratings-sprite-rating"></span>
                        </div>
                        <p><span><i class="ion-ios-calendar-outline"></i>19th May 2017</span>
                            <span><i class="icon"><img src="assets/img/genre-icon.png" class="img-fluid" alt="genre-icon"></i>comedy, drama</span></p>
                        <p><span>Star Cast </span><label>Shah Rukh Khan, Mahira Khan, Nawazuddin Siddiqui</label></p>
                        <p class="font-wt-400">Raees is a paisa-wasool entertainer for massy Bollywood fans! It is a complete package of action, romance...</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <div class="col-md-6 col-xs-12">
                <div class="review-list">
                    <div>
                        <img src="assets/img/review-3.jpg" alt="Movie review" title="Review" class="img-fluid">
                    </div>
                    <div class="review-details">
                        <h5>toilet</h5>
                        <div class="star-ratings-sprite">
                            <span style="width:68%" class="star-ratings-sprite-rating"></span>
                        </div>
                        <p><span><i class="ion-ios-calendar-outline"></i>19th May 2017</span>
                            <span><i class="icon"><img src="assets/img/genre-icon.png" class="img-fluid" alt="genre-icon"></i>comedy, drama</span></p>
                        <p><span>Star Cast </span><label>Shah Rukh Khan, Mahira Khan, Nawazuddin Siddiqui</label></p>
                        <p class="font-wt-400">Raees is a paisa-wasool entertainer for massy Bollywood fans! It is a complete package of action, romance...</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="review-list">
                    <div>
                        <img src="assets/img/review-4.jpg" alt="Movie review" title="Review" class="img-fluid">
                    </div>
                    <div class="review-details">
                        <h5>Newton</h5>
                        <div class="star-ratings-sprite">
                            <span style="width:68%" class="star-ratings-sprite-rating"></span>
                        </div>
                        <p><span><i class="ion-ios-calendar-outline"></i>19th May 2017</span>
                            <span><i class="icon"><img src="assets/img/genre-icon.png" class="img-fluid" alt="genre-icon"></i>comedy, drama</span></p>
                        <p><span>Star Cast </span><label>Shah Rukh Khan, Mahira Khan, Nawazuddin Siddiqui</label></p>
                        <p class="font-wt-400">Raees is a paisa-wasool entertainer for massy Bollywood fans! It is a complete package of action, romance...</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>

<?php @include 'footer.php'; ?>


<?php @include 'header.php' ?>

<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="#">Home</a>
                <a href="#" class="active">Box Office Report</a>
            </div>
        </div>
    </div>
</nav>

<article class="box-office-bg">
    <div class="container p-0">
        <div class="mb-30 xs-text-center">
            <h1 class="d-inline title uppercase color-white">Top grossing movies</h1> 
            <i class="arrows"><img src="assets/img/right-bar.png"></i>
        </div>
        <div class="box-office-navs">
            <div class="tabs-main col-xs-12 col-lg-9 mb-30" id="">
                <ul class="nav nav-tabs" id="tablist" role="tablist">
                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#thisWeek" role="tab" aria-controls="thisWeek" aria-expanded="true">This Week</a> </li>
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#thisMonth" role="tab" aria-controls="thisMonth" aria-expanded="false">This Month</a> </li>
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#threeMonth" role="tab" aria-controls="threeMonth" aria-expanded="false">Last 3 Months</a> </li>
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#sixMonth" role="tab" aria-controls="sixMonth" aria-expanded="false">Last 6 Months</a> </li>
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#thisYear" role="tab" aria-controls="thisYear" aria-expanded="false">This Year</a> </li>
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#lastYear" role="tab" aria-controls="lastYear" aria-expanded="false">2016</a> </li>
                </ul>
            </div>
            <div class="col-lg-3 col-xs-12">
                <div class="box-button">
                    <a href="#" class="font-wt-500 btn-round text-capitalize pl-3 pr-3 btn-primary">View Earning Summary</a>
                </div>
            </div>
        </div>
        <div class="tab-content box-office">   
            <div class="tab-pane active" id="thisWeek" role="tabpanel">
                <div class="owl-carousel" id="moviesLists1">
                    <div class="col-xs-12 item">
                        <div class="xs-fit-content boxOffice">
                            <a href="#" class="full-wide"> <img src="assets/img/boxoffice-1.jpg" alt="poster">
                                <label class="d-inline-b hit"><span><b>141 cr</b></span>Hit</label>
                            </a>
                            <h5 class="mt-2">Judwaa 2</h5>
                            <p>Comedy</p>
                            <div class="rating">
                                <div class="star-ratings-css">
                                    <div class="star-ratings-sprite">
                                        <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 item">
                        <div class="xs-fit-content boxOffice">
                            <a href="#" class="full-wide"> <img src="assets/img/boxoffice-2.jpg" alt="poster">
                                <label class="d-inline-b superHit"><span><b>141 cr</b></span>Super Hit</label>
                            </a>
                            <h5 class="mt-2">Toilet ek prem katha</h5>
                            <p>Drama</p>
                            <div class="rating">
                                <div class="star-ratings-css">
                                    <div class="star-ratings-sprite">
                                        <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 item">
                        <div class="xs-fit-content boxOffice">
                            <a href="#" class="full-wide"> <img src="assets/img/boxoffice-3.jpg" alt="poster">
                                <label class="d-inline-b average"><span><b>141 cr</b></span>Average</label>
                            </a>
                            <h5 class="mt-2">Newton</h5>
                            <p>Thriller</p>
                            <div class="rating">
                                <div class="star-ratings-css">
                                    <div class="star-ratings-sprite">
                                        <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 item">
                        <div class="xs-fit-content boxOffice">
                            <a href="#" class="full-wide"> <img src="assets/img/boxoffice-4.jpg" alt="poster">
                                <label class="d-inline-b average"><span><b>141 cr</b></span>Average</label>
                            </a>
                            <h5 class="mt-2">Newton</h5>
                            <p>Thriller</p>
                            <div class="rating">
                                <div class="star-ratings-css">
                                    <div class="star-ratings-sprite">
                                        <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="thisMonth" role="tabpanel">
                <div class="owl-carousel" id="moviesLists2">
                    <div class="col-xs-12 item">
                        <div class="xs-fit-content boxOffice">
                            <a href="#" class="full-wide"> <img src="assets/img/boxoffice-2.jpg" alt="Movie-list">
                                <label class="d-inline-b average"><span><b>141 cr</b></span>Average</label>
                            </a>
                            <h5 class="mt-2">Newton</h5>
                            <p>Thriller</p>
                            <div class="rating">
                                <div class="star-ratings-css">
                                    <div class="star-ratings-sprite">
                                        <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 item">
                        <div class="xs-fit-content boxOffice">
                            <a href="#" class="full-wide"> <img src="assets/img/boxoffice-1.jpg" alt="Movie-list">
                                <label class="d-inline-b average"><span><b>141 cr</b></span>Average</label>
                            </a>
                            <h5 class="mt-2">Newton</h5>
                            <p>Thriller</p>
                            <div class="rating">
                                <div class="star-ratings-css">
                                    <div class="star-ratings-sprite">
                                        <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 item">
                        <div class="xs-fit-content boxOffice">
                            <a href="#" class="full-wide"> <img src="assets/img/boxoffice-4.jpg" alt="Movie-list">
                                <label class="d-inline-b average"><span><b>141 cr</b></span>Average</label>
                            </a>
                            <h5 class="mt-2">Newton</h5>
                            <p>Thriller</p>
                            <div class="rating">
                                <div class="star-ratings-css">
                                    <div class="star-ratings-sprite">
                                        <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="threeMonth" role="tabpanel">
                <div class="owl-carousel" id="moviesLists3">
                    <div class="col-xs-12 item">
                        <div class="xs-fit-content boxOffice">
                            <a href="#" class="full-wide"> <img src="assets/img/boxoffice-4.jpg" alt="Movie-list">
                                <label class="d-inline-b average"><span><b>141 cr</b></span>Average</label>
                            </a>
                            <h5 class="mt-2">Newton</h5>
                            <p>Thriller</p>
                            <div class="rating">
                                <div class="star-ratings-css">
                                    <div class="star-ratings-sprite">
                                        <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 item">
                        <div class="xs-fit-content boxOffice">
                            <a href="#" class="full-wide"> <img src="assets/img/boxoffice-2.jpg" alt="Movie-list">
                                <label class="d-inline-b average"><span><b>141 cr</b></span>Average</label>
                            </a>
                            <h5 class="mt-2">Newton</h5>
                            <p>Thriller</p>
                            <div class="rating">
                                <div class="star-ratings-css">
                                    <div class="star-ratings-sprite">
                                        <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 item">
                        <div class="xs-fit-content boxOffice">
                            <a href="#" class="full-wide"> <img src="assets/img/boxoffice-3.jpg" alt="poster">
                                <label class="d-inline-b average"><span><b>141 cr</b></span>Average</label>
                            </a>
                            <h5 class="mt-2">Newton</h5>
                            <p>Thriller</p>
                            <div class="rating">
                                <div class="star-ratings-css">
                                    <div class="star-ratings-sprite">
                                        <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="sixMonth" role="tabpanel">
                <div class="owl-carousel" id="moviesLists4">
                    <div class="col-xs-12 item">
                        <div class="xs-fit-content boxOffice">
                            <a href="#" class="full-wide"> <img src="assets/img/boxoffice-2.jpg" alt="poster">
                                <label class="d-inline-b average"><span><b>141 cr</b></span>Average</label>
                            </a>
                            <h5 class="mt-2">Newton</h5>
                            <p>Thriller</p>
                            <div class="rating">
                                <div class="star-ratings-css">
                                    <div class="star-ratings-sprite">
                                        <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 item">
                        <div class="xs-fit-content boxOffice">
                            <a href="#" class="full-wide"> <img src="assets/img/boxoffice-4.jpg" alt="poster">
                                <label class="d-inline-b average"><span><b>141 cr</b></span>Average</label>
                            </a>
                            <h5 class="mt-2">Newton</h5>
                            <p>Thriller</p>
                            <div class="rating">
                                <div class="star-ratings-css">
                                    <div class="star-ratings-sprite">
                                        <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="thisYear" role="tabpanel">
                <div class="owl-carousel" id="moviesLists5">
                    <div class="col-xs-12 item">
                        <div class="xs-fit-content boxOffice">
                            <a href="#" class="full-wide"> <img src="assets/img/boxoffice-1.jpg" alt="poster">
                                <label class="d-inline-b average"><span><b>141 cr</b></span>Average</label>
                            </a>
                            <h5 class="mt-2">Newton</h5>
                            <p>Thriller</p>
                            <div class="rating">
                                <div class="star-ratings-css">
                                    <div class="star-ratings-sprite">
                                        <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 item">
                        <div class="xs-fit-content boxOffice">
                            <a href="#" class="full-wide"> <img src="assets/img/boxoffice-3.jpg" alt="poster">
                                <label class="d-inline-b average"><span><b>141 cr</b></span>Average</label>
                            </a>
                            <h5 class="mt-2">Newton</h5>
                            <p>Thriller</p>
                            <div class="rating">
                                <div class="star-ratings-css">
                                    <div class="star-ratings-sprite">
                                        <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="lastYear" role="tabpanel">
                <div class="owl-carousel" id="moviesLists6">
                    <div class="col-xs-12 item">
                        <div class="xs-fit-content boxOffice">
                            <a href="#" class="full-wide"> <img src="assets/img/boxoffice-1.jpg" alt="poster">
                                <label class="d-inline-b average"><span><b>141 cr</b></span>Average</label>
                            </a>
                            <h5 class="mt-2">Newton</h5>
                            <p>Thriller</p>
                            <div class="rating">
                                <div class="star-ratings-css">
                                    <div class="star-ratings-sprite">
                                        <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 item">
                        <div class="xs-fit-content boxOffice">
                            <a href="#" class="full-wide"> <img src="assets/img/boxoffice-4.jpg" alt="poster">
                                <label class="d-inline-b superHit"><span><b>141 cr</b></span>Super Hit</label>
                            </a>
                            <h5 class="mt-2">Toilet ek prem katha</h5>
                            <p>Drama</p>
                            <div class="rating">
                                <div class="star-ratings-css">
                                    <div class="star-ratings-sprite">
                                        <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</article>
<section>
    <div class="container">
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="xs-text-center">
                    <i class="arrowsSub xl-hidden"><img src="assets/img/left-bar.png"></i>
                    <h2 class="box uppercase title d-inline-b">featured
                    </h2>
                    <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                </div>
                <div class="row" data-plugin="matchHeight" data-by-row="true">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 float-left">
                        <div class="card">
                            <div class="feature-list">
                                <a href="#" class="d-block">
                                    <div class="photo-link">
                                        <img src="assets/img/videos-link1.jpg" alt="video-link" class="img-fluid full-wide">
                                        <button>features</button>
                                    </div>
                                    <div class="p-3">
                                        <h3 class="font-wt-300">Find out why would Farhan Akhtar wants to disco dance...</h3>
                                        <small>October 21, 2017</small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 float-left">
                        <div class="card">
                            <div class="feature-list">
                                <a href="#" class="d-block">
                                    <div class="photo-link">
                                        <img src="assets/img/alia-video.jpg" alt="video-link" class="img-fluid full-wide">
                                        <button>features</button>
                                    </div>
                                    <div class="p-3">
                                        <h3 class="font-wt-300">Raazi is a very different film and it's awesome...</h3>
                                        <small>October 21, 2017</small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" data-plugin="matchHeight" data-by-row="true">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 float-left">
                        <div class="card">
                            <div class="feature-list">
                                <a href="#" class="d-block">
                                    <div class="photo-link">
                                        <img src="assets/img/vidya-video.jpg" alt="video-link" class="img-fluid full-wide">
                                        <button>features</button>
                                    </div>
                                    <div class="p-3">
                                        <h3 class="font-wt-300">Check Out The Fantastic Behind The Scenes of 'Hawa Hawai...</h3>
                                        <small>October 21, 2017</small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 float-left">
                        <div class="card">
                            <div class="feature-list">
                                <a href="#" class="d-block">
                                    <div class="photo-link">
                                        <img src="assets/img/video-link2.jpg" alt="video-link" class="img-fluid full-wide">
                                        <button>features</button>
                                    </div>
                                    <div class="p-3">
                                        <h3 class="font-wt-300">Kriti Kharbanda's CANDID Rapid Fire On Deepika..</h3>
                                        <small>October 21, 2017</small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" data-plugin="matchHeight" data-by-row="true">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 float-left">
                        <div class="card">
                            <div class="feature-list">
                                <a href="#" class="d-block">
                                    <div class="photo-link">
                                        <img src="assets/img/video-link3.jpg" alt="video-link" class="img-fluid full-wide">
                                        <button>features</button>
                                    </div>
                                    <div class="p-3">
                                        <h3 class="font-wt-300">Check Out The Fantastic Behind The Scenes of 'Hawa Hawai...</h3>
                                        <small>October 21, 2017</small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 float-left">
                        <div class="card">
                            <div class="feature-list">
                                <a href="#" class="d-block">
                                    <div class="photo-link">
                                        <img src="assets/img/video-link4.jpg" alt="video-link" class="img-fluid full-wide">
                                        <button>features</button>
                                    </div>
                                    <div class="p-3">
                                        <h3 class="font-wt-300">Check Out The Fantastic Behind The Scenes of 'Hawa Hawai...</h3>
                                        <small>October 21, 2017</small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="button-center">
                    <a href="#" class="btn btn-default">Load More</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="bg-white border-primary mt-3 mb-40">
                    <div class="text-center">
                        <h2 class="artist uppercase sideTitle">reviews</h2>
                    </div>
                    <div class="video-list">
                        <div class="reviews-list">
                            <a href="#">
                                <div class="card-link mb-20">
                                    <div class="video-content">
                                        <img src="assets/img/most-viewed-1.jpg" class="img-fluid full-wide" alt="video">
                                        <label class="duration">15:22</label>
                                    </div>
                                    <div class="details">
                                        <h3>Raees</h3>
                                        <h6>Raees is a paisa-wasool entertainer for massy Bollywood fans! It is a complete package of action, romance.</h6>
                                        <small>October 21, 2017</small>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="reviews-list">
                            <a href="#">
                                <div class="card-link mb-20">
                                    <div class="video-content">
                                        <img src="assets/img/most-viewed-2.jpg" class="img-fluid full-wide" alt="video">
                                        <label class="duration">15:22</label>
                                    </div>
                                    <div class="details">
                                        <h3>Kaabil</h3>
                                        <h6>Kaabil is no where near perfect! For die-hard Hrithik fans who want to wash off Mohenjo Daro memories..</h6>
                                        <small>October 21, 2017</small>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="reviews-list">
                            <a href="#">
                                <div class="card-link mb-20">
                                    <div class="video-content">
                                        <img src="assets/img/most-viewed-3.jpg" class="img-fluid full-wide" alt="video">
                                        <label class="duration">15:22</label>
                                    </div>
                                    <div class="details">
                                        <h3>Gentleman</h3>
                                        <h6>This lazily titled film is an improvement on th leave-your-brains at home genre that we have been lazily lapping</h6>
                                        <small>October 21, 2017</small>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<?php @include 'footer.php' ?>
<script>
    $('#moviesLists1, #moviesLists2, #moviesLists3, #moviesLists4, #moviesLists5, #moviesLists6').owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        dots: false,
        //autoplay: true,
        autoplayTimeout: 3000,

        responsive: {
            100: {
                items: 1
            },
            576: {
                items: 2
            },
            769: {
                items: 3
            },
            990: {
                items: 4
            }
        }
    });
</script>


<?php
@include 'header.php';
?>
<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="#">Home</a>
                <a href="#" class="active">Photos</a>
            </div>
        </div>
    </div>
</nav>
<article>
    <div class="container">
        <div class="row">
            <div class="news mb-30">
                <h1 class="d-inline title text-center uppercase">Photos</h1> 
                <i class="arrows"><img src="assets/img/right-bar.png"></i>
            </div>
            <div class="col-md-12">
                <div class="row"  data-plugin="matchHeight" data-by-row="true">
                    <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12">
                        <div class="photo-list">
                            <a href="#" class="d-block">
                                <div class="photo-link">
                                    <img src="assets/img/photos-gallery.jpg" class="img-fluid full-wide">
                                    <label for="">11</label>
                                    <button>spotted</button>
                                </div>
                                <h5 class="pt-2 title">Deepika, Alia, Sidharth, Karan, Farhan,Farah return from SRK's birthday bash.</h5>
                            </a>
                            <small>October 21, 2017</small>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                        <div class="photo-list">
                            <a href="#" class="d-block">
                                <div class="photo-link">
                                    <img src="assets/img/photos-1.jpg" class="img-fluid full-wide">
                                    <label for="">11</label>
                                    <button>stories</button>
                                </div>
                                <h5 class="pt-2 font-22">Kajol and other celebs at the prayer meet in memory of Rani's father..</h5>
                            </a>
                            <small>October 21, 2017</small>
                        </div>
                        <div class="photo-list">
                            <a href="#" class="d-block">
                                <div class="photo-link">
                                    <img src="assets/img/photos-2.jpg" class="img-fluid full-wide">
                                    <label for="">11</label>
                                    <button>stories</button>
                                </div>
                                <h5 class="pt-2 font-22">Aishwarya Rai Bachchan has a spiritual birthday..See pics</h5>
                            </a>
                            <small>October 21, 2017</small>
                        </div>
                    </div>
                    <div class="button-center">
                        <a href="#" class="btn btn-default">View all</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>

<section class="bg-grey">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-30">
                <i class="arrowsSub xl-hidden"><img src="assets/img/left-bar.png"></i>
                <h2 class="d-inline title uppercase font-32">spotted</h2>
                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div class="row"  data-plugin="matchHeight" data-by-row="true">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 float-left">
                <div class="card">
                    <a href="#" class="p-relative">
                        <img src="assets/img/spotted-1.jpg" alt="video-link" class="img-fluid">
                        <label for="" class="numbers">10</label>
                    </a>
                    <div class="card-title text-center">
                        <h3 class="font-22 font-wt-400 font-black mb-10">Photo: Sushant Singh Rajput will make you blush with his charm</h3>
                        <small class="font-light font-wt-400">October 21, 2017</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 float-left">
                <div class="card photo">
                    <a href="#" class="p-relative">
                        <img src="assets/img/spotted-2.jpg" alt="video-link" class="img-fluid">
                        <label for="" class="numbers">10</label>
                    </a>
                    <div class="card-title text-center">
                        <h3 class="font-22 font-wt-400 font-black mb-10">Arjun Kapoor and Parineeti Chopra start shooting...</h3>
                        <small class="font-light font-wt-400">October 21, 2017</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 float-left">
                <div class="card photo">
                    <a href="#" class="p-relative">
                        <img src="assets/img/spotted-3.jpg" alt="video-link" class="img-fluid">
                        <label for="" class="numbers">10</label>
                    </a>
                    <div class="card-title text-center">
                        <h3 class="font-22 font-wt-400 font-black mb-10">Who is Amir Khan? The boxing world champ... </h3>
                        <small class="font-light font-wt-400">October 21, 2017</small>
                    </div>
                </div>
            </div>
        </div>

        <div class="row xs-hidden"  data-plugin="matchHeight" data-by-row="true">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 float-left">
                <div class="card photo">
                    <a href="#" class="p-relative">
                        <img src="assets/img/spotted-4.jpg" alt="video-link" class="img-fluid">
                        <label for="" class="numbers">10</label>
                    </a>
                    <div class="card-title text-center">
                        <h3 class="font-22 font-wt-400 font-black mb-10">WOW! Zaira Wasim receives National Child Award from President...</h3>
                        <small class="font-light font-wt-400">October 21, 2017</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 float-left">
                <div class="card photo">
                    <a href="#" class="p-relative">
                        <img src="assets/img/spotted-5.jpg" alt="video-link" class="img-fluid">
                        <label for="" class="numbers">10</label>
                    </a>
                    <div class="card-title text-center">
                        <h3 class="font-22 font-wt-400 font-black mb-10">Irrfan Khan plays a Bengali writer inspired by Humayun Ahmed in Mostofa Farooki’s next</h3>
                        <small class="font-light font-wt-400">October 21, 2017</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 float-left">
                <div class="card photo">
                    <a href="#" class="p-relative">
                        <img src="assets/img/spotted-6.jpg" alt="video-link" class="img-fluid">
                        <label for="" class="numbers">10</label>
                    </a>
                    <div class="card-title text-center">
                        <h3 class="font-22 font-wt-400 font-black mb-10">Aditya Roy Kapur, Shraddha Kapoor, Gurmeet Choudhary spotted after </h3>
                        <small class="font-light font-wt-400">October 21, 2017</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="button-center">
            <a href="#" class="btn btn-default">View all bb specials</a>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="col-md-12 border-primary bg-white">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="artist bg-light-red sideTitle uppercase font-32 mb-20">events</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-12 col-xs-6" id="order2">
                    <div class="photo-list">
                        <a href="#" class="d-block">
                            <div class="photo-link">
                                <img src="assets/img/special-1.jpg" alt="BB Special" class="img-fluid">
                                <label for="" class="numbers">10</label>
                                <button>features</button>
                            </div>
                            <h3 class="text-left">Abhishek Bachchan saves Aishwarya Rai from...</h3>
                        </a>
                        <small>October 21, 2017</small>
                    </div>
                    <div class="photo-list">
                        <a href="#" class="d-block">
                            <div class="photo-link">
                                <img src="assets/img/special-2.jpg" alt="BB Special" class="img-fluid">
                                <label for="" class="numbers">10</label>
                                <button>events</button>
                            </div>
                            <h3 class="text-left">Kareena adds an edge to the traditional look...</h3>
                        </a>
                        <small>October 21, 2017</small>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12" id="order1">
                    <div class="photo-list xs-text-center">
                        <a href="#" class="d-block">
                            <div class="photo-link">
                                <img src="assets/img/special-main.jpg" alt="main-video" class="img-fluid">
                                <label for="" class="numbers">10</label>
                                <button>spotted</button>
                            </div>
                            <h2 class="font-wt-300 pt-3">Deepika Padukone and Farah Khan spotted at the airport!</h2>
                        </a>
                        <small>October 21, 2017</small>
                        <h6 class="xs-hidden">Shraddha Kapoor will next be seen in Apoorva Lakhia’s ‘Haseena Parkar’ which is slated to hit the screens on September 22, 2017. Recently, a special screening of the film took place which was attended by Shraddha...</h6>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-6" id="order3">
                    <div class="right-side">
                        <div class="photo-list">
                            <a href="#" class="d-block">
                                <div class="photo-link">
                                    <img src="assets/img/special-3.jpg" alt="BB Special" class="img-fluid">
                                    <label for="" class="numbers">10</label>
                                    <button>spotted</button>
                                </div>
                                <h3 class="text-left">Ranbir Kapoor, Aadar Jain get clicked during...</h3>
                            </a>
                            <small>October 21, 2017</small>
                        </div>
                        <div class="photo-list">
                            <a href="#" class="d-block">
                                <div class="photo-link">
                                    <img src="assets/img/special-4.jpg" alt="BB Special" class="img-fluid">
                                    <label for="" class="numbers">10</label>
                                    <button>spotted</button>
                                </div>
                                <h3 class="text-left">Ranbir Kapoor, Aadar Jain get clicked during...</h3>
                            </a>
                            <small>October 21, 2017</small>
                        </div>
                    </div>
                </div>
                <div class="events xs-hidden"  data-plugin="matchHeight" data-by-row="true" id="order4">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="photo-list sm-mb-3 mb-20">
                            <a href="#" class="d-block">
                                <div class="photo-link">
                                    <img src="assets/img/events1.jpg" alt="BB Special" class="img-fluid">
                                    <label for="" class="numbers">10</label>
                                    <button>features</button>
                                </div>
                                <h3 class="text-left">Dhadak: Janhvi and Ishaan soak in moments...</h3>
                            </a>
                            <small>October 21, 2017</small>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="photo-list sm-mb-3 mb-20">
                            <a href="#" class="d-block">
                                <div class="photo-link">
                                    <img src="assets/img/events2.jpg" alt="BB Special" class="img-fluid">
                                    <label for="" class="numbers">10</label>
                                    <button>features</button>
                                </div>
                                <h3 class="text-left ">5 unknown facts about Janhvi Kapoor that her...</h3>
                            </a>
                            <small>October 21, 2017</small>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="photo-list sm-mb-3 mb-20">
                            <a href="#" class="d-block">
                                <div class="photo-link">
                                    <img src="assets/img/events3.jpg" alt="BB Special" class="img-fluid">
                                    <label for="" class="numbers">10</label>
                                    <button>features</button>
                                </div>
                                <h3 class="text-left">Padmavati: Deepika Padukone’s ‘ghoomar’ act...</h3>
                            </a>
                            <small>October 21, 2017</small>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="photo-list sm-mb-3 mb-20">
                            <a href="#" class="d-block">
                                <div class="photo-link">
                                    <img src="assets/img/event4.jpg" alt="BB Special" class="img-fluid">
                                    <label for="" class="numbers">10</label>
                                    <button>features</button>
                                </div>
                                <h3 class="text-left">THIS movie would have been Chitrangda’s debut...</h3>
                            </a>
                            <small>October 21, 2017</small>
                        </div>
                    </div>
                </div>
                <div class="button-center" id="order5">
                    <a href="#" class="btn btn-default">View all</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="bg-light-red">
    <div class="container">
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <div class="width-600">
                <div class="text-center border-primary bg-white full-h">
                    <h2 class="artist bg-light-red sideTitle uppercase font-32">stories</h2>
                    <div class="events">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 sm-mb-3">
                            <div class="photo-story">
                                <a href="#" class="d-block">
                                    <div class="photo-link">
                                        <img src="assets/img/stories-1.jpg" alt="BB Special" class="img-fluid">
                                        <button>guess who</button>
                                    </div>
                                    <h4>Guess who got drunk in kareena kapoors birthday bash?</h4>
                                </a>
                                <small>October 21, 2017</small>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 sm-mb-3">
                            <div class="photo-story">
                                <a href="#" class="d-block">
                                    <div class="photo-link">
                                        <img src="assets/img/stories-2.jpg" alt="BB Special" class="img-fluid">
                                        <button>news</button>
                                    </div>
                                    <h4>Guess which movie has these two actors?</h4>
                                </a>
                                <small>October 21, 2017</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="right-bar-fix-width-300">
                <div class="text-center border-primary bg-white full-h">
                    <h2 class="artist bg-light-red sideTitle uppercase font-32">first look</h2>
                    <div class="first-look">
                        <a href="#" class="full-wide"> <img src="assets/img/newton-poster2.jpg" class="img-fluid" alt="poster">
                        </a>
                        <div class="card-footer">
                            <p>Newton</p>
                            <label>Romance/comedy</label>
                            <h6>Releasing: 12 Nov 2017</h6> 
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php
@include 'footer.php';
?>

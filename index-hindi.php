<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Bollywood Bubble</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="assets/img/bubble-logo.png" type="image/gif" sizes="16x16">
        <link rel="manifest" href="site.webmanifest">
        <link rel="apple-touch-icon" href="icon.png">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/main.css">
        <link rel="stylesheet" href="assets/plugins/css/select2.min.css">
        <link rel="stylesheet" href="assets/css/custom.css">
        <link rel="stylesheet" href="assets/plugins/css/ionicons.min.css">
        <link href="https://fonts.googleapis.com/css?family=Passion+One:400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Alegreya+Sans:300,400,500,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:300,400,700" rel="stylesheet">
        <link rel="stylesheet" href="assets/plugins/css/owl.carousel.min.css">
        <link rel="stylesheet" href="assets/plugins/css/owl.theme.default.min.css">
        <link rel="stylesheet" href="assets/css/responsive.css">
        <!-- Data Table -->
        <link rel="stylesheet" href="assets/plugins/css/dataTables.bootstrap4.min.css">
    </head>
    <body>
       <!-- PRELOADER -->
       <div class="loader-wrapper">
           <div class="loader"><img src="assets/img/loader.png" class="spinn-right" alt=""></div>
       </div>
       <!-- END: PRELOADER -->
        <header>
            <nav class="navbar navbar-inverse fixed-top">
                <div class="container">                    
                    <div class="navbar-header">
                        <div class="button  hidden-md-down">
                            <a class="btn-open" href="#"></a>
                        </div>
                        <a href="#" class="navbar-toggle" data-toggle="collapse"  onclick="openNav()">
                            <span class="sr-only">Toggle navigation</span>
                            <i class="ion-drag"></i>
                        </a>
                        <a class="navbar-brand" href="index.php"><img src="assets/img/logo-white.png" alt="Bollywood Bubble" class="Bollywood Bubble"></a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav hidden-md-down">
                            <li><a href="index.php">होम</a></li>
                            <li><a href="article.php">समाचार</a></li>
                            <li><a href="#">हस्तियाँ</a></li>
                            <li><a href="#">चलचित्र</a></li>
                            <li><a href="photo-landing.php">तस्वीरें</a></li>
                            <li><a href="video.php">वीडियो</a></li>
                            <li><a href="video.php">जीवनशैली</a></li>
                            <li><a href="video.php">देखा गया</a></li>
                        </ul>
                        <ul class="float-right right-nav">
                            <li class="nav-item float-left">
                                <form class="form-inline">
                                    <a class="search-bar-lg" data-toggle="modal" href="#searchModal" data-toggle="modal" data-target="#searchModal">
                                        <span class="icon"><i class="ion-ios-search"></i></span>
                                    </a>
                                </form>
                            </li>
                            <li class="nav-item float-left border-l-r">
                                <a href="index-hindi.php">
                                    <span class="p-10"><img src="assets/img/language-icon.png" class="img-fluid" title="Change Language" alt="Language"></span>
                                </a>
                            </li>
                            <li class="nav-item float-left mr-10 hidden-md-down">
                                <a href="#"  data-toggle="modal" data-target="#login">
                                    <span class="icon">
                                        <i class="ion-ios-person-outline fs"></i>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div><!-- /.nav-collapse -->
                </div>
            </nav>
        </header>

        <div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="wrap">
                            <ul class="wrap-nav">
                                <li><span>समाचार</span>
                                    <ul>
                                        <li><a href="#">Gossip</a></li>
                                        <li><a href="#">Bubble Diaries</a>
                                            <ul>
                                                <li><a href="#">Bubble Speaks</a></li>
                                                <li><a href="#">Nostalgia</a></li>
                                                <li><a href="#">Time Travel</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Guest Blogger</a></li>
                                    </ul>
                                </li>
                                <li><span>हस्तियाँ</span>
                                    <ul>
                                        <li><a href="#">Interviews</a></li>
                                        <li><a href="#">Lifestyle</a></li>
                                        <li><a href="#">Top Trending</a></li>
                                    </ul>
                                </li>
                                <li><span>फिल्में</span>
                                    <ul>
                                        <li><a href="#">Reviews</a></li>
                                        <li><a href="#">Box Office</a></li>
                                        <li><a href="#">Upcoming Movies</a></li>
                                        <li><a href="#">Top Trending</a></li>
                                    </ul>
                                </li>
                                <li><span>तस्वीरें</span>
                                    <ul>
                                        <li><a href="#">कैमरे पर गलती करते हुए पकड़ना</a></li>
                                        <li><a href="#">विशेषताएं</a></li>
                                        <li><a href="#">आयोजन</a></li>
                                        <li><a href="#">जिम में देखा</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="wrap-nav">
                                <li><span>बुलबुला टीवी</span>
                                    <ul>
                                        <li><a href="#">बी बी स्पेशल</a></li>
                                        <li><a href="#">मूल्य का टैग</a></li>
                                        <li><a href="#">लघु टॉक</a></li>
                                        <li><a href="#">विराम समय</a></li>
                                        <li><a href="#">बी-टाउन Backpackers</a></li>
                                        <li><a href="#">बुलबुला स्नूप</a></li>
                                        <li><a href="#">बॉलीवुड सास</a></li>
                                    </ul>
                                </li>
                                <li><span class="no-bg">&nbsp;</span>
                                    <ul>
                                        <li><a href="#">बॉलीवुड खोलना</a></li>
                                        <li><a href="#">बुलबुला बाइट्स</a></li>
                                        <li><a href="#">Petstagram</a></li>
                                        <li><a href="#">स्वास्थ्य के लिए सड़क</a></li>
                                        <li><a href="#">डाक पता</a></li>
                                        <li><a href="#">समय यात्रा</a></li>
                                        <li><a href="#">बॉलीवुड परिवार जाम</a></li>
                                    </ul>
                                </li>
                                <li><span>जीवन शैली</span>
                                    <ul>
                                        <li><a href="#">Fitness</a>
                                            <ul>
                                                <li><a href="#">Road to Fitness</a></li>
                                                <li><a href="#">Gym Spottings</a></li>
                                                <li><a href="#">Gym Videos</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">B-Town Juniors</a></li>
                                        <li><a href="#">Postal Address</a></li>
                                        <li><a href="#">Sportstainment</a></li>
                                        <li><a href="#">Exclusives</a></li>
                                        <li><a href="#">Couple Goals</a></li>
                                    </ul>
                                </li>
                                <li><span class="no-bg">&nbsp;</span>
                                    <ul>
                                        <li><a href="#">Bollywood FAM JAM</a></li>
                                        <li><a href="#">OOTD</a></li>
                                        <li><a href="#">Wonders & Blunders</a></li>
                                        <li><a href="#">Trendsetters</a></li>
                                        <li><a href="#">Price Tag</a></li>
                                        <li><a href="#">Clone the Look</a></li>
                                        <li><a href="#">B-Town Backpackers</a></li>
                                        <li><a href="#">Petstagram</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<div class="container-fluid single-moview-timer">
   <div class="container">        
       <div id="timer" class="xs-text-center">
           <div class="title">Releasing this Week</div>
           <div class=""><span class="days"></span></div>
           <div class="smalltext">Days</div>
           <div class=""><span class="hours"></span></div>
           <div class="smalltext">Hr</div>
           <div class=""><span class="minutes"></span></div>
           <div class="smalltext">Min</div>
           <div class=""><span class="seconds"></span></div>
           <div class="smalltext">Sec</div>
       </div>
   </div>    
</div>
<div class="container">
    <div class="row" data-plugin="matchHeight" data-by-row="true">
        <!--------- toP bar ---------------->
        <div class="col-md-3 col-sm-3 col-lg-2 col-xs-12 xs-hidden">
            <p class="release">releasing this week</p>
        </div>
        <div class="col-md-6 col-sm-12 col-lg-6 col-xs-12" id="order2">
            <div class="row">
                <div class="owl-carousel" id="carousel1">
                    <div class="item">                    
                        <h5 class="d-inline-b">गोलमाल अगेन</h5><span>अक्टूबर 21,2017</span>
                    </div>
                    <div class="item">
                        <h5 class="d-inline-b">टाइगर जिंदा है</h5><span>अक्टूबर 21,2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-12 col-lg-4 col-xs-12" id="order1">
            <div id="clockdiv" class="xs-text-center">
                <div class="calendar-icon"> <span class="days"></span> </div>
                <div class="smalltext">दिन</div>
                <div class="calendar-icon"> <span class="hours"></span> </div>
                <div class="smalltext">घंटा</div>
                <div class="calendar-icon"> <span class="minutes"></span> </div>
                <div class="smalltext">मिनट</div>
                <div class="calendar-icon"> <span class="seconds"></span> </div>
                <div class="smalltext">सेकंड</div>
            </div>
        </div>
    </div>
</div>
<!---------- End top bar -------------->
<!---------- ads 970 width -------------->
<div class="ad-horizontal-970 pb-10">
    <a href="#">
        <img src="assets/img/ad-970-01.jpg" alt="ads" title="" >
    </a>
</div>
<!---------- end ads 970 width -------------->
<div class="container-fluid">
    <div class="row" data-plugin="matchHeight" data-by-row="true">
        <div class="owl-carousel" id="slider">
            <div class="slider-content item">
                <div class="poster">
                    <a href="#"> <img src="assets/img/slider-1.jpg" class="img-fluid"> </a> <a href="#" class="content-link">
                        बॉक्स ऑफिस
                    </a> </div>
                <div class="description mt-1">
                    <a href="#">
                        <p>मिलिए कैटरीना, 'टाइगर ज़िदा हाई' के नए अभी भी से उग्र बाघिन</p>
                    </a> <small>अक्टूबर 21, 2017</small> </div>
            </div>
            <div class="slider-content item">
                <div class="poster">
                    <a href="#"> <img src="assets/img/slider-2.jpg" class="img-fluid"> </a> <a href="#" class="content-link">
                        बॉक्स ऑफिस
                    </a> </div>
                <div class="description mt-1">
                    <a href="#">
                        <p>पद्मावती गीत घूमर अब बाहर! दीपिका पादुकोण अपनी दुनिया गोल जाना कर देगा</p>
                    </a> <small>अक्टूबर 21, 2017</small> </div>
            </div>
            <div class="slider-content item">
                <div class="poster">
                    <a href="#"> <img src="assets/img/slider-3.jpg" class="img-fluid"> </a> <a href="#" class="content-link">
                        नया क्या है
                    </a> </div>
                <div class="description mt-1">
                    <a href="#">
                        <p>शाहरुख खान ने अपने 52 वें जन्मदिन आज मनाता है और हम आपको बता क्यों नहीं कभी कर सकते हैं ..</p>
                    </a> <small>अक्टूबर 21, 2017</small> </div>
            </div>
            <div class="slider-content item">
                <div class="poster">
                    <a href="#"> <img src="assets/img/slider-4.jpg" class="img-fluid"> </a>
                </div>
                <div class="description mt-1">
                    <h2>शनि और रविवार 7 बजे</h2>
                </div>
            </div>
            <div class="slider-content item">
                <div class="poster">
                    <a href="#"> <img src="assets/img/slider-5.jpg" class="img-fluid"> </a> <a href="#" class="content-link">
                        देखा गया
                    </a> </div>
                <div class="description mt-1">
                    <a href="#">
                        <p>मिलिए कैटरीना, 'टाइगर ज़िदा हाई' के नए अभी भी से उग्र बाघिन</p>
                    </a> <small>अक्टूबर 21, 2017</small> </div>
            </div>
            <div class="slider-content item">
                <div class="poster">
                    <a href="#"> <img src="assets/img/slider-1.jpg" class="img-fluid"> </a> <a href="#" class="content-link">
                        बॉक्स ऑफिस
                    </a> </div>
                <div class="description mt-1">
                    <a href="#">
                        <p>मिलिए कैटरीना, 'टाइगर ज़िदा हाई' के नए अभी भी से उग्र बाघिन</p>
                    </a> <small>अक्टूबर 21, 2017</small> </div>
            </div>
        </div>
    </div>
</div>
<!---------End SLider ------->
<!---------- ads 970 width -------------->
<div class="ad-horizontal-970 pt-30">
    <a href="#">
        <img src="assets/img/ad-970-02.jpg" alt="ads" title="" >
    </a>
</div>
<!---------- end ads 970 width -------------->
<!--------- Photos ------------>
<section>
    <!---------- ads 160 width -------------->
    <div class="ad-verticle-160 ad-left-fix">
        <a href="#">
            <img src="assets/img/ad-160-01.jpg" alt="ads" title="" >
        </a>
    </div>
    <!---------- end ads 160 width -------------->

    <div class="container">
        <div class="row photos">
            <div class="full-wide text-center mb-30"> <i class="arrows"><img src="assets/img/left-bar.png"></i>
                <h1 class="d-inline title text-center uppercase">तस्वीरें</h1> <i class="arrows"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div class="row " data-plugin="matchHeight" data-by-row="true">
            <div class="photo-list-container">
                <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
                    <div class="photo-list">
                        <a href="#">
                            <div class="photo-link"> 
                                <img src="assets/img/photos-gallery.jpg" class="img-fluid full-wide">
                                <label for="">11</label>
                                <button>देखा गया</button>
                            </div>
                            <h5 class="pt-2 title">दीपिका, आलिया, सिद्धार्थ, करण, फरहान, शाहरुख खान के जन्मदिन की पार्टी से फराह वापसी।</h5>
                        </a>
                        <small>अक्टूबर 21, 2017</small>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="photo-list">
                        <div class="ads">
                            <img src="assets/img/mc-d-ads.jpg" class="img-fluid full-wide" alt="MacD ads">
                        </div>
                    </div>
                    <div class="photo-list">
                        <a href="#">
                            <div class="photo-link"> 
                                <img src="assets/img/photos-2.jpg" class="img-fluid full-wide">
                                <label for="">11</label>
                                <button>कहानियों</button>
                            </div>
                            <h5 class="pt-2 font-22">ऐश्वर्या राय बच्चन एक आध्यात्मिक जन्मदिन.. तस्वीरें देखें</h5>
                        </a>
                        <small>अक्टूबर 21, 2017</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="photo-list">
                    <a href="#">
                        <div class="photo-link"> 
                            <img src="assets/img/photos-3.jpg" class="img-fluid full-wide">
                            <label for="">11</label>
                            <button>बताओ कौन</button>
                        </div>
                        <h5 class="pt-2 font-22">कैटरीना कैफ अभिषेक बच्चन आश्चर्य, 'एल्विस बोमन ईरानी को देखकर इमारत छोड़ देता है'</h5>
                    </a>
                    <small>October 21, 2017</small>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="photo-list">
                    <a href="#">
                        <div class="photo-link"> 
                            <img src="assets/img/photos-4.jpg" class="img-fluid full-wide">
                            <label for="">11</label>
                            <button>नया क्या है</button>
                        </div>
                        <h5 class="pt-2 font-22">पद्मावती की 3 डी ट्रेलर लॉन्च पर दीपिका पादुकोण की काली पोशाक हम से नीचे एक अंगूठे हो जाता है</h5>
                    </a>
                    <small>अक्टूबर 21, 2017</small>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="photo-list">
                    <a href="#" class="d-block">
                        <div class="photo-link"> 
                            <img src="assets/img/photos-1.jpg" class="img-fluid full-wide">
                            <label for="">11</label>
                            <button>stories</button>
                        </div>
                        <h5 class="pt-2 font-22">काजोल और प्रार्थना में अन्य हस्तियां रानी के पिता की स्मृति में पूरा ..</h5>
                    </a>
                    <small>अक्टूबर 21, 2017</small>
                </div>
            </div>
            <div class="button-center">
                <a href="#" class="btn btn-default">सभी को देखें</a>
            </div>
        </div>
    </div>
    <!---------- ads 160 width -------------->
    <div class="ad-verticle-160 ad-right-fix">
        <a href="#">
            <img src="assets/img/ad-160-02.jpg" alt="ads" title="" >
        </a>
    </div>
    <!---------- end ads 160 width -------------->
</section>
<!----------- End Photos --------------->
<!----------- Videos ------------------>
<section class="pt-0 videos">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="full-wide text-center mb-2"> <i class="arrows"><img src="assets/img/left-bar.png"></i>
                    <h1 class="d-inline title text-center uppercase  ">वीडियो</h1> <i class="arrows"><img src="assets/img/right-bar.png"></i> </div>
                <div class="full-wide d-inline-flex">
                    <ul class="nav nav-tabs" id="videos" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#mostPopular" role="tab" aria-controls="mostPopular">सबसे लोकप्रिय</a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#trailers" role="tab" aria-controls="trailers">ट्रेलरों</a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#bbSpecial" role="tab" aria-controls="bbSpecial">बी बी विशेष</a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#exclusives" role="tab" aria-controls="exclusives">अनन्य</a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#events" role="tab" aria-controls="events">आयोजन</a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#interviews" role="tab" aria-controls="interviews">साक्षात्कार</a> </li>
                    </ul>
                </div>
                <div class="videos-bg">
                    <div class="tab-content d-inline-b p-relative">
                        <div class="tab-pane active" id="mostPopular" role="tabpanel">
                            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 float-left mb-5 videos-bg-full">
                                <a href="#" class="video-link">
                                    <img src="assets/img/video-top-icon.png" class="img-fluid video-top">
                                    <div class="col-md-12 col-lg-12 xs-p-0 float-left ">
                                        <img src="assets/img/video-play.jpg" class="img-fluid" alt="video-player" title="player">
                                    </div>
                                    <h4 class="font-wt-300 text-center bg-white video-title">घड़ी: टीम 'गोलमाल फिर' करता है 'नाटक कंपनी' के सेट पर कुछ गोलमाल 
                                        <small class="text-muted fs-14 d-block">अक्टूबर 21, 2017</small>
                                    </h4>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 float-left md-p-0">
                                <div class="grid-videos row">
                                    <div class="md-col-50">
                                        <div class="card">
                                            <a href="#">
                                                <div class="card-link">
                                                    <div class="video-content">
                                                        <img src="assets/img/video-side-link.jpg" class="img-fluid full-wide" >
                                                    </div>
                                                    <div class="video-sub">
                                                        <p>दीपिका पादुकोण 'घूमर' के लिए लोक नृत्य सीखने पर</p>
                                                        <small class="text-muted">अक्टूबर 21, 2017</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="md-col-50">
                                        <div class="card">
                                            <a href="#">
                                                <div class="card-link">
                                                    <div class="video-content">
                                                        <img src="assets/img/video-side-link2.jpg" class="img-fluid full-wide" >
                                                    </div>
                                                    <div class="video-sub">
                                                        <p>इरफान खान और पार्वती उनके बारे में सब कुछ पता चलता है ..</p>
                                                        <small class="text-muted">अक्टूबर 21, 2017</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="trailers" role="tabpanel">
                            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 float-left mb-5 videos-bg-full">
                                <a href="#" class="video-link">
                                    <img src="assets/img/video-top-icon.png" class="img-fluid video-top">
                                    <div class="col-md-12 col-lg-12 xs-p-0 float-left ">
                                        <img src="assets/img/video-play.jpg" class="img-fluid" alt="video-player" title="player">
                                    </div>
                                    <h4 class="font-wt-300 text-center bg-white video-title">घड़ी: टीम 'गोलमाल फिर' करता है 'नाटक कंपनी' के सेट पर कुछ गोलमाल 
                                        <small class="text-muted fs-14 d-block">अक्टूबर 21, 2017</small>
                                    </h4>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 float-left md-p-0">
                                <div class="grid-videos row">
                                    <div class="md-col-50">
                                        <div class="card">
                                            <a href="#">
                                                <div class="card-link">
                                                    <div class="video-content">
                                                        <img src="assets/img/video-side-link.jpg" class="img-fluid full-wide" >
                                                    </div>
                                                    <div class="video-sub">
                                                        <p>दीपिका पादुकोण 'घूमर' के लिए लोक नृत्य सीखने पर</p>
                                                        <small class="text-muted">अक्टूबर 21, 2017</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="md-col-50">
                                        <div class="card">
                                            <a href="#">
                                                <div class="card-link">
                                                    <div class="video-content">
                                                        <img src="assets/img/video-side-link2.jpg" class="img-fluid full-wide" >
                                                    </div>
                                                    <div class="video-sub">
                                                        <p>इरफान खान और पार्वती उनके बारे में सब कुछ पता चलता है ..</p>
                                                        <small class="text-muted">अक्टूबर 21, 2017</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="bbSpecial" role="tabpanel">
                            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 float-left mb-5 videos-bg-full">
                                <a href="#" class="video-link">
                                    <img src="assets/img/video-top-icon.png" class="img-fluid video-top">
                                    <div class="col-md-12 col-lg-12 xs-p-0 float-left ">
                                        <img src="assets/img/video-play.jpg" class="img-fluid" alt="video-player" title="player">
                                    </div>
                                    <h4 class="font-wt-300 text-center bg-white video-title">घड़ी: टीम 'गोलमाल फिर' करता है 'नाटक कंपनी' के सेट पर कुछ गोलमाल 
                                        <small class="text-muted fs-14 d-block">अक्टूबर 21, 2017</small>
                                    </h4>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 float-left md-p-0">
                                <div class="grid-videos row">
                                    <div class="md-col-50">
                                        <div class="card">
                                            <a href="#">
                                                <div class="card-link">
                                                    <div class="video-content">
                                                        <img src="assets/img/video-side-link.jpg" class="img-fluid full-wide" >
                                                    </div>
                                                    <div class="video-sub">
                                                        <p>दीपिका पादुकोण 'घूमर' के लिए लोक नृत्य सीखने पर</p>
                                                        <small class="text-muted">अक्टूबर 21, 2017</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="md-col-50">
                                        <div class="card">
                                            <a href="#">
                                                <div class="card-link">
                                                    <div class="video-content">
                                                        <img src="assets/img/video-side-link2.jpg" class="img-fluid full-wide" >
                                                    </div>
                                                    <div class="video-sub">
                                                        <p>इरफान खान और पार्वती उनके बारे में सब कुछ पता चलता है ..</p>
                                                        <small class="text-muted">अक्टूबर 21, 2017</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="exclusives" role="tabpanel">
                            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 float-left mb-5 videos-bg-full">
                                <a href="#" class="video-link">
                                    <img src="assets/img/video-top-icon.png" class="img-fluid video-top">
                                    <div class="col-md-12 col-lg-12 xs-p-0 float-left ">
                                        <img src="assets/img/video-play.jpg" class="img-fluid" alt="video-player" title="player">
                                    </div>
                                    <h4 class="font-wt-300 text-center bg-white video-title">घड़ी: टीम 'गोलमाल फिर' करता है 'नाटक कंपनी' के सेट पर कुछ गोलमाल 
                                        <small class="text-muted fs-14 d-block">अक्टूबर 21, 2017</small>
                                    </h4>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 float-left md-p-0">
                                <div class="grid-videos row">
                                    <div class="md-col-50">
                                        <div class="card">
                                            <a href="#">
                                                <div class="card-link">
                                                    <div class="video-content">
                                                        <img src="assets/img/video-side-link.jpg" class="img-fluid full-wide" >
                                                    </div>
                                                    <div class="video-sub">
                                                        <p>दीपिका पादुकोण 'घूमर' के लिए लोक नृत्य सीखने पर</p>
                                                        <small class="text-muted">अक्टूबर 21, 2017</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="md-col-50">
                                        <div class="card">
                                            <a href="#">
                                                <div class="card-link">
                                                    <div class="video-content">
                                                        <img src="assets/img/video-side-link2.jpg" class="img-fluid full-wide" >
                                                    </div>
                                                    <div class="video-sub">
                                                        <p>इरफान खान और पार्वती उनके बारे में सब कुछ पता चलता है ..</p>
                                                        <small class="text-muted">अक्टूबर 21, 2017</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="events" role="tabpanel">
                            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 float-left mb-5 videos-bg-full">
                                <a href="#" class="video-link">
                                    <img src="assets/img/video-top-icon.png" class="img-fluid video-top">
                                    <div class="col-md-12 col-lg-12 xs-p-0 float-left ">
                                        <img src="assets/img/video-play.jpg" class="img-fluid" alt="video-player" title="player">
                                    </div>
                                    <h4 class="font-wt-300 text-center bg-white video-title">घड़ी: टीम 'गोलमाल फिर' करता है 'नाटक कंपनी' के सेट पर कुछ गोलमाल 
                                        <small class="text-muted fs-14 d-block">अक्टूबर 21, 2017</small>
                                    </h4>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 float-left md-p-0">
                                <div class="grid-videos row">
                                    <div class="md-col-50">
                                        <div class="card">
                                            <a href="#">
                                                <div class="card-link">
                                                    <div class="video-content">
                                                        <img src="assets/img/video-side-link.jpg" class="img-fluid full-wide" >
                                                    </div>
                                                    <div class="video-sub">
                                                        <p>दीपिका पादुकोण 'घूमर' के लिए लोक नृत्य सीखने पर</p>
                                                        <small class="text-muted">अक्टूबर 21, 2017</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="md-col-50">
                                        <div class="card">
                                            <a href="#">
                                                <div class="card-link">
                                                    <div class="video-content">
                                                        <img src="assets/img/video-side-link2.jpg" class="img-fluid full-wide" >
                                                    </div>
                                                    <div class="video-sub">
                                                        <p>इरफान खान और पार्वती उनके बारे में सब कुछ पता चलता है ..</p>
                                                        <small class="text-muted">अक्टूबर 21, 2017</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="interviews" role="tabpanel">
                            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 float-left mb-5 videos-bg-full">
                                <a href="#" class="video-link">
                                    <img src="assets/img/video-top-icon.png" class="img-fluid video-top">
                                    <div class="col-md-12 col-lg-12 xs-p-0 float-left ">
                                        <img src="assets/img/video-play.jpg" class="img-fluid" alt="video-player" title="player">
                                    </div>
                                    <h4 class="font-wt-300 text-center bg-white video-title">घड़ी: टीम 'गोलमाल फिर' करता है 'नाटक कंपनी' के सेट पर कुछ गोलमाल 
                                        <small class="text-muted fs-14 d-block">अक्टूबर 21, 2017</small>
                                    </h4>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 float-left md-p-0">
                                <div class="grid-videos row">
                                    <div class="md-col-50">
                                        <div class="card">
                                            <a href="#">
                                                <div class="card-link">
                                                    <div class="video-content">
                                                        <img src="assets/img/video-side-link.jpg" class="img-fluid full-wide" >
                                                    </div>
                                                    <div class="video-sub">
                                                        <p>दीपिका पादुकोण 'घूमर' के लिए लोक नृत्य सीखने पर</p>
                                                        <small class="text-muted">अक्टूबर 21, 2017</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="md-col-50">
                                        <div class="card">
                                            <a href="#">
                                                <div class="card-link">
                                                    <div class="video-content">
                                                        <img src="assets/img/video-side-link2.jpg" class="img-fluid full-wide" >
                                                    </div>
                                                    <div class="video-sub">
                                                        <p>इरफान खान और पार्वती उनके बारे में सब कुछ पता चलता है ..</p>
                                                        <small class="text-muted">अक्टूबर 21, 2017</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--------- end Videos -------->

<!---------- ads 970 width -------------->
<div class="ad-horizontal-970 pb-60">
    <a href="#">
        <img src="assets/img/ad-970-03.jpg" alt="ads" title="" >
    </a>
</div>
<!---------- end ads 970 width -------------->
<!--------- Bollywood life -------->
<section class="bg-pink lifestyle">
    <div class="backgroung-icon left"> </div>
    <div class="container back-image">
        <div class="row xs-m-0 Bollywood-life">
            <div class="full-wide text-center mb-30">
                <i class="arrows xs-hidden"><img src="assets/img/left-bar.png"></i>
                <h1 class="d-inline title text-center uppercase">बॉलीवुड लाइफस्टाइल</h1>
                <i class="arrows xs-hidden"><img src="assets/img/right-bar.png"></i> 
            </div>
            <div class="bollywood-life bolly-wide" data-plugin="matchHeight" data-by-row="true">
                <div class="col-md-6 col-xs-12">
                    <div class="card no-border">
                        <a href="#">
                            <img src="assets/img/bolly-life.jpg" class="img-fluid full-wide" alt="bollywood-life">
                            <div class="button-link">
                                <button class="font-wt-700">फैशन</button>
                            </div>
                        </a>
                        <p class="lead">5 बार जब दीपिका पादुकोण साड़ी के प्रति अपने प्यार को साबित कर दिया, गंभीरता से जातीय समूहों के प्रति अपने प्यार का दिखावा करने के लिए जारी!</p> <small>30 अक्टूबर, 2017</small> 
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="card no-border">
                        <a href="#"> 
                            <img src="assets/img/bolly-life2.jpg"  class="img-fluid full-wide" alt="bollywood-life">
                            <div class="button-link">
                                <button class="font-wt-700">fashion</button>
                            </div>
                        </a>
                        <p class="lead">हमें छोड़ दो! गौरी खान एक हेलोवीन पार्टी कल रात जो जो बॉलीवुड की कौन है ने भाग लिया आयोजित की और वे अपने में तैयार किया गया ...</p> <small>30 अक्टूबर, 2017</small> 
                    </div>
                </div>
                <div class="button-center">
                    <a href="#" class="btn btn-default">सभी को देखें</a>
                </div>
            </div>
        </div>
    </div>
    <div class="backgroung-icon right"> </div>
</section>
<!------- end Bollywood life ----------->
<!------ News section ---------->
<section>
    <div class="container">
        <div class="row news" data-plugin="matchHeight" data-by-row="true">
            <div class="full-wide mb-30 text-center"> 
                <i class="arrows"><img src="assets/img/left-bar.png"></i>
                <h1 class="d-inline title uppercase">समाचार</h1> 
                <i class="arrows"><img src="assets/img/right-bar.png"></i> </div>
            <div class="col-md-4 col-xs-12 md-m-auto">
                <div class="news-gal xs-text-center">
                    <a href="#"> <img src="assets/img/news-1.jpg" alt="News" class="img-fluid">
                        <button class="content-link font-wt-700">चित्रित किया</button>
                    </a>
                    <div class="description">
                        <p class="news-details"> पुष्टि: सिद्धार्थ मल्होत्रा कारगिल शहीद कैप्टन विक्रम बत्रा खेलने के लिए </p>
                        <small>अक्टूबर 21, 2017</small> 
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12 md-m-auto">
                <div class="news-gal xs-text-center">
                    <a href="#"> <img src="assets/img/news-2.jpg" alt="News" class="img-fluid">
                        <button class="content-link font-wt-700">गपशप</button>
                    </a>
                    <div class="description">
                        <p class="news-details"> एक नशे में संजय दत्त करण जौहर के जन्मदिन की पार्टी में कुछ अप्रत्याशित किया</p>
                        <small>अक्टूबर 21, 2017</small> 
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12 md-m-auto">
                <div class="news-gal xs-text-center">
                    <a href="#"> <img src="assets/img/news-3.jpg" alt="News" class="img-fluid">
                        <button class="content-link font-wt-700">विषाद</button>
                    </a>
                    <div class="description">
                        <p class="news-details">क्या तुम्हें पता था? अमिताभ शूटिंग के सिर्फ 10 दिनों के बाद जया के साथ अपनी पहली फिल्म से हटा दिया गया</p>
                        <small>अक्टूबर 21, 2017</small> 
                    </div>
                </div>
            </div>
            <div class="button-center">
                <a href="#" class="btn btn-default">सभी को देखें</a>
            </div>
        </div>
    </div>
</section>
<!-----------End News --------->
<!---------- ads 970 width -------------->
<div class="ad-horizontal-970 pb-60">
    <a href="#">
        <img src="assets/img/ad-970-04.jpg" alt="ads" title="" >
    </a>
</div>
<!---------- end ads 970 width -------------->
<!-------- Upcoming movies -------->
<section class="bg-grey upcoming">
    <div class="full-wide text-center mb-3"> <i class="arrows"><img src="assets/img/left-bar.png"></i>
        <h1 class="d-inline title text-center uppercase">आगामी फिल्म</h1> <i class="arrows"><img src="assets/img/right-bar.png"></i> </div>
    <div class="container">
        <div class="row upcoming-movie" data-plugin="matchHeight" data-by-row="true">
            <div class="col-md-12 owl-carousel" id="upcomingMovies">
                <div class="col-xs-12 item">
                    <div class="row">
                        <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-1.jpg" alt="News" class="img-fluid"> </a>
                        <div class="card-footer">
                            <p>करीब करीब सिंगल</p>
                            <label>रोमांस / कॉमेडी</label>
                            <h6>Releasing: 12 Nov 2017</h6> </div>
                    </div>
                </div>
                <div class="col-xs-12 item">
                    <div class="row">
                        <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-2.jpg" alt="News" class="img-fluid"> </a>
                        <div class="card-footer">
                            <p>शादी में जरूर आना</p>
                            <label>रोमांस / कॉमेडी</label>
                            <h6>जारी: 12 नवंबर 2017</h6> </div>
                    </div>
                </div>
                <div class="col-xs-12 item">
                    <div class="row">
                        <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-3.jpg" alt="News" class="img-fluid"> </a>
                        <div class="card-footer">
                            <p>तुम्हारी सुलु</p>
                            <label>रोमांस / कॉमेडी</label>
                            <h6>जारी: 12 नवंबर 2017</h6> </div>
                    </div>
                </div>
                <div class="col-xs-12 item ">
                    <div class="row">
                        <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-4.jpg" alt="News" class="img-fluid"> </a>
                        <div class="card-footer">
                            <p>मंटो</p>
                            <label>रोमांस / कॉमेडी</label>
                            <h6>जारी: 12 नवंबर 2017</h6> </div>
                    </div>
                </div>
                <div class="col-xs-12 item">
                    <div class="row">
                        <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-5.jpg" alt="News" class="img-fluid"> </a>
                        <div class="card-footer">
                            <p>हाउस नेक्स्ट डोर</p>
                            <label>सभी को देखें</label>
                            <h6>जारी: 12 नवंबर 2017</h6> </div>
                    </div>
                </div>
            </div>
            <div class="button-center">
                <a href="#" class="btn btn-default">सभी को देखें</a>
            </div>
        </div>
    </div>
</section>
<!---------- end Upcoming movies------->
<!-------- Reviews ---------->
<section>
    <div class="container">
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <div class="width-600 full">
                <div class="border-primary pb-3 full-h">
                    <div class="text-center">
                        <h1  class="artist bg-light-red uppercase">चलचित्र समीक्षा</h1>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-12 col-xs-12">
                            <div class="youtube_link">
                                <a href="https://www.youtube.com/watch?v=J_yb8HORges" target="_blank">
                                    <img src="assets/img/reviews-images.jpg" class="img-fluid full-wide">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12 float-left content-right">
                            <div class="rating-view">
                                <div class="rating">
                                    <div class="star-ratings-css">
                                        <label>Bollywood Bubble Rating</label>
                                        <div class="star-ratings-sprite">
                                            <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                        </div>
                                    </div>
                                </div>
                                <p class="uppercase fs-12">बॉलीवुड बुलबुला रेटिंग</p> 
                                <h4 class="font-wt-400 mt-30">गुप्त सुपरस्टार फिल्म समीक्षा: अपनी माँ इस एक के लिए साथ ले लो। एक देखना चाहिए</h4>
                                <small>October 30, 2017</small>
                                <p class="pt-3 pb-3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. </p>
                                <!--<p class="pb-3">In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. </p>-->
                                <a href="#" class="primary-color"><u>पूर्ण समीक्षा पढ़ें</u></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="right-bar-fix-width-300">
                <div class="text-center border-primary bg-white full-h">
                    <h1 class="artist bg-light-red uppercase quiz">बॉलीवुड प्रश्नोत्तरी</h1>
                    <div class="col- quiz">
                        <div class="full-wide">
                            <img src="assets/img/quiz-img.jpg" class="img-fluid" alt="quiz">
                        </div>
                        <h5>सलमान खान, शाहरुख खान या अक्षय कुमार जो सुपरस्टार अपने प्रिय टीवी होस्ट है?</h5>
                        <div class="radio-group quiz">
                            <div class="md-radio inline-radio">
                                <input type="radio" class="form-control" name="quiz" id="option-1">
                                <label for="option-1">सलमान खान</label>
                            </div>
                            <div class="md-radio inline-radio">
                                <input type="radio" class="form-control" name="quiz" id="option-2">
                                <label for="option-2">शाहरुख खान</label>
                            </div>
                            <div class="md-radio inline-radio">
                                <input type="radio" class="form-control" name="quiz" id="option-3">
                                <label for="option-3">अक्षय कुमार</label>
                            </div>
                        </div>
                        <div class="inline-button sm-inline-b"> 
                            <a href="#" class="font-wt-500 fs-14 btn btn-default primary-color float-left">वोट करें</a> 
                            <a href="#" class="links">देखें परिणाम</a> 
                            <a href="#" class="links">एक और प्रश्नोत्तरी लें</a> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-------- End Reviews ---------->

<!---------- ads 970 width -------------->
<div class="ad-horizontal-970 pb-60">
    <a href="#">
        <img src="assets/img/ad-970-05.jpg" alt="ads" title="" >
    </a>
</div>
<!---------- end ads 970 width -------------->

<!-------- Box Office ------------>
<article class="bg-light-red">
    <div class="container">
        <div class="row">
            <div class="width-600 p-static">
                <h1 class="uppercase text-center full-wide">बॉक्स ऑफिस</h1> 
                <div data-plugin="matchHeight" data-by-row="true">
                    <div class="owl-carousel" id="boxoffice">
                        <div class="col-xs-12 p-static item">
                            <div class="xs-fit-content boxOffice">
                                <a href="#" class="full-wide"> <img src="assets/img/judua-poster.jpg" alt="poster">
                                    <label class="d-inline-b hit"><span><b>141 करोड़</b></span>हिट</label>
                                </a>
                                <h5 class="mt-2">जुड़वा 2</h5>
                                <span>कॉमेडी</span>
                                <div class="rating">
                                    <div class="star-ratings-css">
                                        <div class="star-ratings-sprite">
                                            <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 p-static item">
                            <div class="xs-fit-content boxOffice">
                                <a href="#" class="full-wide"> <img src="assets/img/toilet-poster.jpg" alt="poster">
                                    <label class="d-inline-b superHit"><span><b>141 करोड़</b></span>सुपर हिट</label>
                                </a>
                                <h5 class="mt-2">शौचालय एक प्रेम कथा</h5>
                                <span>नाटक</span>
                                <div class="rating">
                                    <div class="star-ratings-css">
                                        <div class="star-ratings-sprite">
                                            <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 p-static item">
                            <div class="xs-fit-content boxOffice">
                                <a href="#" class="full-wide"> <img src="assets/img/newton-poster.jpg" alt="poster">
                                    <label class="d-inline-b average"><span><b>141 करोड़</b></span>औसत</label>
                                </a>
                                <h5 class="mt-2">न्यूटन</h5>
                                <span>थ्रिलर</span>
                                <div class="rating">
                                    <div class="star-ratings-css">
                                        <div class="star-ratings-sprite">
                                            <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="button-center">
                    <a href="box-office.php" class="btn btn-default">सभी को देखें</a>
                </div>
            </div>
            <div class="right-bar-fix-width-300">
                <div class="full-wide text-center border-primary bg-white">
                    <h2 class="artist bg-light-red uppercase quiz sideTitle">इस महीने के कलाकार</h2>
                    <div class="artist-month pt-0 full-h"> <img src="assets/img/artist-month.jpg" alt="artist of month">
                        <p>ऐश्वर्या राय के ये 9 चित्र साबित वह नहीं कहा की सबसे खूबसूरत महिला कुछ भी नहीं है के लिए!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>
<!-------- End Box Office ----------->


<article class="pb-0 celebs">
    <div class="container-fluid">
        <div class="row">
            <div class="full-wide text-center bg-white"> 
                <div class="bollyTitle">
                    <i class="arrows"><img src="assets/img/left-bar.png"></i>
                    <h1 class="d-inline title text-center uppercase">बॉलीवुड हस्तियां</h1> 
                    <i class="arrows"><img src="assets/img/right-bar.png"></i> 
                </div>
            </div>
            <div class="bubble-bg">
                <ul class="celeb-list">
                    <li>
                        <div class="wide"><a href="celebs.php"><img src="assets/img/celeb-01.jpg" alt=""><span>सलमान खान</span></a></div>
                        <div class="small"><a href="celebs.php"><img src="assets/img/celeb-02.jpg" alt=""><span>रणवीर सिंह</span></a></div>
                        <div class="small"><a href="celebs.php"><img src="assets/img/celeb-03.jpg" alt=""><span>प्रियंका चोपड़ा</span></a></div>
                    </li>
                    <li>
                        <div class="wide"><a href="celebs.php"><img src="assets/img/celeb-04.jpg" alt=""><span>आलिया भट्ट</span></a></div>
                        <div class="wide"><a href="celebs.php"><img src="assets/img/celeb-05.jpg" alt=""><span>रणबीर कपूर</span></a></div>
                    </li>
                    <li>
                        <div class="small"><a href="celebs.php"><img src="assets/img/celeb-06.jpg" alt=""><span>आमिर खान</span></a></div>
                        <div class="small"><a href="celebs.php"><img src="assets/img/celeb-07.jpg" alt=""><span>अनुष्का शर्मा</span></a></div>
                        <div class="wide"><a href="celebs.php"><img src="assets/img/celeb-08.jpg" alt=""><span>शाहरुख खान</span></a></div>
                    </li>
                    <li>
                        <div class="wide"><a href="celebs.php"><img src="assets/img/celeb-09.jpg" alt=""><span>अक्षय कुमार</span></a></div>
                        <div class="wide"><a href="celebs.php"><img src="assets/img/celeb-10.jpg" alt=""><span>दीपिका पादुकोण</span></a></div>
                    </li>
                    <li>
                        <div class="small"><a href="celebs.php"><img src="assets/img/celeb-11.jpg" alt=""><span>कंगना रानौत</span></a></div>
                        <div class="small"><a href="celebs.php"><img src="assets/img/celeb-12.jpg" alt=""><span>कटरीना कैफ</span></a></div>
                        <div class="wide"><a href="celebs.php"><img src="assets/img/celeb-13.jpg" alt=""><span>करीना कपूर</span></a></div>
                    </li>
                </ul>
                <div class="button-bottom">
                    <a href="#" class="font-wt-500 btn-round text-capitalize pl-3 pr-3 btn-default">सभी को देखें</a>
                </div>
            </div>
        </div>
    </div>
</article>

<footer>
    <div class="container">
        <a href="#" id="scrollTop"> <img src="assets/img/scroll-top.png" alt="Scroll-icon"> </a>
        <div class="row top-footer">
            <div class="col-md-4">
                <h5 class="font-wt-700">हमारी उपस्थिति</h5>
                <div class="icon-box">
                    <ul>
                        <li class="bg-fb">
                            <a href="#"> <i class="ion-social-facebook text-white"></i> </a>
                        </li>
                        <li class="bg-twitter">
                            <a href="#"> <i class="ion-social-twitter text-white"></i> </a>
                        </li>
                        <li class="bg-primary">
                            <a href="#"> <i class="ion-social-instagram text-white"></i> </a>
                        </li>
                        <li class="bg-youtube">
                            <a href="#"> <i class="ion-social-youtube text-white"></i> </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 mb-20">
                <div class="row">
                    <div class="col-md-12">
                        <h5 class="font-wt-700">साइटमैप</h5>
                    </div>
                    <div class="col-xs-6">
                        <ul>
                            <li> <a href="#">समाचार</a></li>
                            <li> <a href="#">हस्तियाँ</a></li>
                            <li> <a href="#">चलचित्र</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-6">
                        <ul>
                            <li> <a href="#">तस्वीरें</a> </li>
                            <li> <a href="#">वीडियो</a> </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12">
                        <h5 class="font-wt-700">अन्य</h5>
                    </div>
                    <div class="col-xs-6">
                        <ul>
                            <li> <a href="#">हमारे बारे में</a> </li>
                            <li> <a href="#">गोपनीयता नीति</a> </li>
                        </ul>
                    </div>
                    <div class="col-xs-6">
                        <ul>
                            <li> <a href="#">नियम एवं शर्तें</a> </li>
                            <li> <a href="#">हमसे संपर्क करें</a> </li>
                        </ul>
                    </div>
                </div>
            </div>            
        </div>
    </div>
    <hr>
    <div class="container">
        <div class="row bottom-footer">
            <div class="col-md-12 col-xs-12 text-center">
                <ul class="d-inline-b">
                    <li>कॉपीराइट @ २०१७<span class="pipe2">|</span> </li>
                    <li>www.bollywoodbubble.com<span class="pipe2">|</span> </li>
                    <li>सर्वाधिकार सुरक्षित</li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<div id="overlay"></div>
<div id="mySidenav" class="sidenav">
    <div class="inner">
        <div class="nav-head"><a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><img src="assets/img/close-icon.png" alt=""></a>
            <div class="profile">
                <div class="profile-img"><img src="assets/img/profile-pic.png" alt=""></div>
                <p>Hello Guest!</p>
                <a href="#" data-toggle="modal" data-target="#login" class="font-wt-500 btn-round link-red">Login</a>
            </div>
        </div>
        <ul class="nav flex-column">            
            <li class="nav-item"><a class="nav-link" href="ind">Home</a></li>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#news" data-toggle="collapse" data-target="#news">News</a>
                <div class="collapse" id="news" aria-expanded="false">
                    <ul class="flex-column pl-2 nav">
                        <li class="nav-item"><a class="nav-link" href="">Gossip</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Bubble Diaries</a></li>
                        <li class="nav-item">
                            <a class="nav-link collapsed" href="#submenu1sub1" data-toggle="collapse" data-target="#submenu1sub1">Customers</a>
                            <div class="collapse small" id="submenu1sub1" aria-expanded="false">
                                <ul class="flex-column nav pl-4">
                                    <li class="nav-item">
                                        <a class="nav-link p-0" href="">
                                            <i class="fa fa-fw fa-clock-o"></i> Bubble Speaks
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link p-0" href="">
                                            <i class="fa fa-fw fa-dashboard"></i> Nostalgia
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link p-0" href="">
                                            <i class="fa fa-fw fa-bar-chart"></i> Time Travel
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="">Guest Blogger</a></li>
                    </ul>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#celeb" data-toggle="collapse" data-target="#celeb">Celebrities</a>
                <div class="collapse" id="celeb" aria-expanded="false">
                    <ul class="flex-column pl-2 nav">
                        <li class="nav-item"><a class="nav-link" href="">Interviews</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Lifestyle</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Top Trending</a></li>
                    </ul>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#movies" data-toggle="collapse" data-target="#movies">Movies</a>
                <div class="collapse" id="movies" aria-expanded="false">
                    <ul class="flex-column pl-2 nav">
                        <li class="nav-item"><a class="nav-link" href="">Reviews</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Box Office</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Upcoming Movies</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Top Trending</a></li>
                    </ul>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#photos" data-toggle="collapse" data-target="#photos">Photos</a>
                <div class="collapse" id="photos" aria-expanded="false">
                    <ul class="flex-column pl-2 nav">
                        <li class="nav-item"><a class="nav-link" href="">Caught on Camera</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Features</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Events</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Gym Spottings</a></li>
                    </ul>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#video" data-toggle="collapse" data-target="#video">Bubble TV</a>
                <div class="collapse" id="video" aria-expanded="false">
                    <ul class="flex-column pl-2 nav">
                        <li class="nav-item"><a class="nav-link" href="">BB Specials</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Price Tag</a></li>
                        <li class="nav-item"><a class="nav-link" href="">SHort Talk</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Break Time</a></li>
                        <li class="nav-item"><a class="nav-link" href="">B-Town Backpackers</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Bubble Snoop</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Bollywood Sass</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Bollywood Spotting </a></li>
                        <li class="nav-item"><a class="nav-link" href="">Bubble Bytes</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Petstagram</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Road to Fitness</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Postal Address</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Time Travel</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Bollywood FAM JAM</a></li>
                    </ul>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#video" data-toggle="collapse" data-target="#video">Lifestyle</a>
                <div class="collapse" id="video" aria-expanded="false">
                    <ul class="flex-column pl-2 nav">
                        <li class="nav-item">
                            <a class="nav-link collapsed py-0" href="#fitness" data-toggle="collapse" data-target="#fitness">Fitness</a>
                            <div class="collapse small" id="fitness" aria-expanded="false">
                                <ul class="flex-column nav pl-4">
                                    <li class="nav-item">
                                        <a class="nav-link p-0" href="">
                                            <i class="fa fa-fw fa-clock-o"></i> Road to Fitness
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link p-0" href="">
                                            <i class="fa fa-fw fa-dashboard"></i> Gym Spottings
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link p-0" href="">
                                            <i class="fa fa-fw fa-bar-chart"></i> Gym Videos
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="">B-Town Juniors</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Postal Address</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Sportstainment</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Exclusives</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Couple Goals</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Bollywood FAM JAM</a></li>
                        <li class="nav-item"><a class="nav-link" href="">OOTD</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Wonders & Blunders</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Trendsetters</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Price Tag</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Clone the Look</a></li>
                        <li class="nav-item"><a class="nav-link" href="">B-Town Backpackers</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Petstagram</a></li>
                    </ul>
                </div>
            </li>            
            <!--<li class="nav-item"><a class="nav-link" href="#" data-toggle="modal" data-target="#login">Login</a></li>-->      
        </ul>
        <div class="icon-box pt-20">
            <ul>
                <li class="bg-fb">
                    <a href="#">
                        <i class="ion-social-facebook text-white"></i>
                    </a>
                </li>
                <li class="bg-twitter">
                    <a href="#">
                        <i class="ion-social-twitter text-white"></i>
                    </a>
                </li>
                <li class="bg-google">
                    <a href="#">
                        <i class="ion-social-googleplus text-white"></i>
                    </a>
                </li>
                <li class="bg-youtube">
                    <a href="#">
                        <i class="ion-social-youtube text-white"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>

<!-- LOGIN/SIGNUP -->
<div class="modal fade" tabindex="-1" id="login" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"> &nbsp;
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <img src="assets/img/close-icon.png" alt=""> </button>
            </div>
            <div class="modal-body">
                <div class="tab-pane active" id="login-tab" role="tabpanel">
                    <h3>Login with</h3>
                    <ul class="login-social">
                        <li><a href="#" class="btn fb-btn"><i class="ion-social-facebook"></i> Facebook</a></li>
                        <li><a href="#" class="btn google-btn"><i class="ion-social-googleplus"></i> Google+</a></li>
                    </ul>                    
                </div>
            </div>
        </div>
    </div>
</div>

<!-- MOBILE FLOATING ELEMENT -->
<div class="floating-nav">
    <div class="nav1"><a href="#">समाचार</a></div>
    <div class="nav2"><a href="#">हस्तियाँ</a></div>
    <div class="nav3"><a href="#">फिल्में</a></div>
    <div class="nav4"><a href="#">तस्वीरें</a></div>
    <div class="mask"><img src="assets/img/floating-icon.png" alt=""></div>  
</div>

<!---------- Search modal -------------->
<div class="modal fade search-modal" role="dialog" aria-labelledby="searchModal" aria-hidden="true" id="searchModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content ">
            <div class="row bg-bubble">
                <div class="modal-header container">
                    <div class="dropdown filter-box">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">All</button>
                        <ul class="dropdown-menu">
                            <li class="news"><a href="#"><span></span>समाचार</a></li>
                            <li class="celeb"><a href="#"><span></span>हस्तियाँ</a></li>
                            <li class="movies"><a href="#"><span></span>फिल्में</a></li>
                            <li class="photo"><a href="#"><span></span>तस्वीरें</a></li>
                            <li class="video"><a href="#"><span></span>चलचित्र</a></li>
                        </ul>
                    </div>
                    <div class="search-box">
                        <input type="search" placeholder="Search news, videos, photos, movies, celebrities" class="form-control">
                        <span class="icon"><i class="ion-ios-search"></i></span>
                    </div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">    
                    <i class="ion-ios-close-empty"></i>
                </button>
            </div>
            <div class="modal-body">
                <section class="recent-search">
                    <div class="container">
                        <div class="">
                            <h3>Your Recent Searches</h3>
                            <div class="alert alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert"><i class="ion-ios-close-empty"></i></button>
                              Tiger zinda hai
                            </div>
                            <div class="alert alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert"><i class="ion-ios-close-empty"></i></button>
                              Recent Articles 
                            </div>
                            <div class="alert alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert"><i class="ion-ios-close-empty"></i></button>
                              Shahrukh Khan
                            </div>
                            <div class="alert alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert"><i class="ion-ios-close-empty"></i></button>
                              Virat & Anushka Wedding
                            </div>
                            <button type="button" class="clear-btn">Clear all</button>
                        </div>
                    </div>
                </section>
                <section class="bg-grey">
                    <div class="container">
                        <div class="row">
                            <div class="xs-text-center col-md-12">
                                <i class="arrowsSub xl-hidden"><img src="assets/img/left-bar.png"></i>
                                <h2 class="box uppercase title d-inline-b">popular
                                </h2>
                                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                            </div>
                            <div class="events">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <a href="#" class="d-block">
                                        <div class="popular-link">
                                            <div class="photo">
                                                <img src="assets/img/events1.jpg" alt="BB Special" class="img-fluid full-wide">
                                            </div>
                                            <h3>Dhadak: Janhvi and Ishaan soak in moments...</h3>
                                            <small class="text-muted">October 21, 2017</small>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <a href="#" class="d-block">
                                        <div class="popular-link">
                                            <div class="photo">
                                                <img src="assets/img/events2.jpg" alt="BB Special" class="img-fluid full-wide">
                                            </div>
                                            <h3>5 unknown facts about Janhvi Kapoor that her Instagram...</h3>
                                            <small class="text-muted">October 21, 2017</small>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <a href="#" class="d-block">
                                        <div class="popular-link">
                                            <div class="photo">
                                                <img src="assets/img/events3.jpg" alt="BB Special" class="img-fluid full-wide">
                                            </div>
                                            <h3>Padmavati: Deepika Padukone’s ‘ghoomar’ act...</h3>
                                            <small class="text-muted">October 21, 2017</small>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <a href="#" class="d-block">
                                        <div class="popular-link">
                                            <div class="photo">
                                                <img src="assets/img/event4.jpg" alt="BB Special" class="img-fluid full-wide">
                                            </div>
                                            <h3>THIS movie would have been Chitrangda’s debut instead...</h3>
                                            <small class="text-muted">October 21, 2017</small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="button-center">
                                <a href="#" class="btn btn-default">View all Popular News</a>
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    <div class="container">
                        <div class="row">
                            <div class="xs-text-center col-md-12">
                                <i class="arrowsSub xl-hidden"><img src="assets/img/left-bar.png"></i>
                                <h2 class="box uppercase title d-inline-b">trending
                                </h2>
                                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                            </div>
                            <div class="events">
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 sm-mb-3">
                                    <a href="#" class="d-block">
                                        <div class="card-link">
                                            <div class="video-content">
                                                <img src="assets/img/events1.jpg" alt="BB Special" class="img-fluid full-wide">
                                                <label class="duration">15:22</label>
                                            </div>
                                            <h3>Dhadak: Janhvi and Ishaan soak in moments...</h3>
                                            <small class="text-muted">October 21, 2017</small>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 sm-mb-3">
                                    <a href="#" class="d-block">
                                        <div class="card-link">
                                            <div class="video-content">
                                                <img src="assets/img/events2.jpg" alt="BB Special" class="img-fluid full-wide">
                                                <label class="duration">15:22</label>
                                            </div>
                                            <h3>5 unknown facts about Janhvi Kapoor that her Instagram...</h3>
                                            <small class="text-muted">October 21, 2017</small>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 sm-mb-3">
                                    <a href="#" class="d-block">
                                        <div class="card-link">
                                            <div class="video-content">
                                                <img src="assets/img/events3.jpg" alt="BB Special" class="img-fluid full-wide">
                                                <label class="duration">15:22</label>
                                            </div>
                                            <h3>Padmavati: Deepika Padukone’s ‘ghoomar’ act...</h3>
                                            <small class="text-muted">October 21, 2017</small>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 sm-mb-3">
                                    <a href="#" class="d-block">
                                        <div class="card-link">
                                            <div class="video-content">
                                                <img src="assets/img/event4.jpg" alt="BB Special" class="img-fluid full-wide">
                                                <label class="duration">15:22</label>
                                            </div>
                                            <h3>THIS movie would have been Chitrangda’s debut instead...</h3>
                                            <small class="text-muted">October 21, 2017</small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="button-center">
                                <a href="#" class="btn btn-default">View all trending videos</a>
                            </div>
                        </div>
                       
                        <div class="row mt-30">
                            <div class="col-md-3 col-xs-12">
                                <div class="photo-block">
                                    <a href="#" class="p-relative">
                                        <img src="assets/img/interview-.jpg" alt="video-link" class="img-fluid full-wide">
                                        <label for="" class="numbers">10</label>
                                    </a>
                                    <h3 class="font-22 font-wt-300 font-black mb-10" id="title-limit1">Photo: Sushant Singh Rajput will make you blush with his cha</h3>
                                    <small class="font-light font-wt-400">October 21, 2017</small>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <div class="photo-block">
                                    <a href="#" class="p-relative">
                                        <img src="assets/img/interview-1.jpg" alt="video-link" class="img-fluid full-wide">
                                        <label for="" class="numbers">10</label>
                                    </a>
                                    <h3 class="font-22 font-wt-300 font-black mb-10" id="title-limit1">Photo: Sushant Singh Rajput will make you blush with his cha</h3>
                                    <small class="font-light font-wt-400">October 21, 2017</small>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <div class="photo-block">
                                    <a href="#" class="p-relative">
                                        <img src="assets/img/interview-3.jpg" alt="video-link" class="img-fluid full-wide">
                                        <label for="" class="numbers">10</label>
                                    </a>
                                    <h3 class="font-22 font-wt-300 font-black mb-10" id="title-limit1">Photo: Sushant Singh Rajput will make you blush with his cha</h3>
                                    <small class="font-light font-wt-400">October 21, 2017</small>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <div class="photo-block">
                                    <a href="#" class="p-relative">
                                        <img src="assets/img/interview-2.jpg" alt="video-link" class="img-fluid full-wide">
                                        <label for="" class="numbers">10</label>
                                    </a>
                                    <h3 class="font-22 font-wt-300 font-black mb-10" id="title-limit1">Photo: Sushant Singh Rajput will make you blush with his cha</h3>
                                    <small class="font-light font-wt-400">October 21, 2017</small>
                                </div>
                            </div>
                        </div>
                        <div class="button-center">
                            <a href="#" class="btn btn-default">View all trending photos</a>
                        </div>
                    </div>                    
                </section>                
            </div>

        </div>
    </div>
</div>

<script src="assets/js/jquery-3.2.1.min.js"></script>
<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
<script src="assets/plugins/js/owl.carousel.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/plugins/js/select2.min.js"></script>
<script src="assets/plugins/js/jquery.rateyo.js"></script>
<script src="assets/plugins/js/jquery.matchHeight.js" type="text/javascript"></script>
<script src="assets/js/main.js"></script>
<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
<script src="https://npmcdn.com/bootstrap@4.0.0-alpha.5/dist/js/bootstrap.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<!-- Data Table -->
<script src="assets/plugins/js/jquery.dataTables.min.js"></script>

<script>
            window.ga = function () {
                ga.q.push(arguments)
            };
            ga.q = [];
            ga.l = +new Date;
            ga('create', 'UA-XXXXX-Y', 'auto');
            ga('send', 'pageview')
</script>
<script src="https://www.google-analytics.com/analytics.js" async defer></script>

<script>
            function openNav() {
                document.getElementById("mySidenav").style.width = "300px";
                document.getElementById("overlay").style.display = "block";
                //document.getElementById("main").style.marginLeft = "300px";
                //document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
                document.body.style.overflow = "hidden"
            }

            function closeNav() {
                document.getElementById("mySidenav").style.width = "0";
                //document.getElementById("main").style.marginLeft= "0";
                //document.body.style.backgroundColor = "white";
                document.body.style.overflow = "scroll"
                document.getElementById("overlay").style.display = "none";
            }

            $(document).ready(function () {
                $(".dropdown").hover(
                        function () {
                            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true, true).slideDown("1500");
                            $(this).toggleClass('open');
                        },
                        function () {

                            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true, true).slideUp("1500");
                            $(this).toggleClass('open');
                        }
                );
            });
            function scrollUp() {
                $('#scrollTop').click(function () {
                    $("html, body").animate({scrollTop: 1}, 1500);
                    return true;
                });
            }
            scrollUp();

            $('#boxoffice').owlCarousel({
                loop: true,
                margin: 0,
                nav: false,
                dots: false,
                //autoplay: true,
                autoplayTimeout: 3000,

                responsive: {
                    100: {
                        items: 1
                    },
                    576: {
                        items: 1
                    },
                    768: {
                        items: 2
                    },
                    990: {
                        items: 3
                    }
                }
            });

            $("ul.filter-list").on("click", ".init", function () {
                $(this).closest("ul.filter-list").children('li:not(.init)').slideDown();
            });

            var allOptions = $("ul.filter-list").children('li:not(.init)');
            $("ul.filter-list").on("click", "li:not(.init)", function () {
                allOptions.removeClass('selected');
                $(this).addClass('selected');
                $("ul.filter-list").children('.init').html($(this).html());
                allOptions.slideUp();
            });
</script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#DataTable').DataTable();
    });
</script>
<script>
    $(document).ready(function () {
        var headerTop = $('header').offset().top;
        var headerBottom = headerTop + 120; // Sub-menu should appear after this distance from top.
        $(window).scroll(function () {
            var scrollTop = $(window).scrollTop(); // Current vertical scroll position from the top
            if (scrollTop > headerBottom) { // Check to see if we have scrolled more than headerBottom
                if (($("#share-article").is(":visible") === false)) {
                    $('#share-article').fadeIn('slow');
                }
            } else {
                if ($("#share-article").is(":visible")) {
                    $('#share-article').hide();
                }
            }
        });
    });
</script>
<script>
    $(document).ready(function () {

        var active1 = false;
        var active2 = false;
        var active3 = false;
        var active4 = false;

        $('.floating-nav').on('hover touchstart', function () {

            if (!active1)
                $(this).find('.nav1').css({'transform': 'translate(14px,-96px)'});
            else
                $(this).find('.nav1').css({'transform': 'none'});
            if (!active2)
                $(this).find('.nav2').css({'transform': 'translate(-43px,-83px)'});
            else
                $(this).find('.nav2').css({'transform': 'none'});
            if (!active3)
                $(this).find('.nav3').css({'transform': 'translate(-85px,-43px)'});
            else
                $(this).find('.nav3').css({'transform': 'none'});
            if (!active4)
                $(this).find('.nav4').css({'transform': 'translate(-102px,13px)'});
            else
                $(this).find('.nav4').css({'transform': 'none'});
            active1 = !active1;
            active2 = !active2;
            active3 = !active3;
            active4 = !active4;

        });
    });
</script>
<script>
    $(document).ready(function () {
        $(".button a").click(function () {
            $(".overlay").fadeToggle(200);
            $(this).toggleClass('btn-open').toggleClass('btn-close');
        });
    });
    $('.overlay').on('click', function () {
        $(".overlay").fadeToggle(200);
        $(".button a").toggleClass('btn-open').toggleClass('btn-close');
        open = false;
    });
</script>


</body>
</html>

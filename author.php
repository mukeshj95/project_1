<?php @include 'header.php'; ?>

<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="index.php">Home</a>
                <a href="author.php" class="active">My account</a>
            </div>
        </div>
    </div>
</nav>

<div class="author-ac">
    <div class="container-fluid">
        <div class="row">
            <div class="account-info">
                <div class="blog-imgae">
                    <img src="assets/img/author-pic.jpg" alt="Author" title="Rohit Sharma">
                </div>
                <h2>Rohit Sharma</h2>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="about-me">
                <div class="card">
                    <div class="full-wide text-center mb-10">
                        <i class="arrowsSub"><img src="assets/img/left-bar.png"></i>
                        <h2 class="d-inline title text-center uppercase">about me</h2>
                        <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                    </div>
                    <div class="description">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt blandit gravida. Phasellus eleifend lorem a lorem maximus dictum. Curabitur vehicula egestas nibh, id mollis diam ullamcorper ac. Sed sollicitudin augue id massa laoreet, eu fermentum massa volutpat. Donec nec quam interdum, consequat nulla vitae, tempor elit. Donec urna enim, aliquam quis hendrerit sed, pulvinar fringilla odio. Sed blandit varius tempus. </p>
                    </div>
                    <div class="button-center">
                        <a href="#" class="btn btn-default">follow me</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="bg-light-red pb-0">
    <div class="author-list">
        <div class="container">
            <div class="row">
                <div class="full-wide text-center mb-30">
                    <i class="arrowsSub"><img src="assets/img/left-bar.png"></i>
                    <h2 class="d-inline title text-center uppercase">blogs <span>(12)</span></h2>
                    <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                </div>
            </div>
            <div class="row" data-plugin="matchHeight" data-by-row="true">
                <div class="col-md-4 col-xs-12">
                    <div class="guest-blog">
                        <div class="card">
                            <a href="#"> 
                                <img src="assets/img/priyanka.jpg" class="img-fluid full-wide" alt="bollywood-life">
                                <label class="duration"></label>  
                            </a>
                            <div class="card-footer">
                                <h5>When Bhumi Pednekar stunned us with her simple yet significant style play for Shubh Mangal </h5> 
                                <small>October 30, 2017</small> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="guest-blog">
                        <div class="card">
                            <a href="#"> 
                                <img src="assets/img/jenifer.jpg" class="img-fluid full-wide" alt="bollywood-life">
                                <label class="duration"></label>  
                            </a>
                            <div class="card-footer">
                                <h5>Jennifer Winget Gets A Brigitte Bardot Makeover And It's The Best House Party Look For The Season</h5> 
                                <small>October 30, 2017</small> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="guest-blog">
                        <div class="card">
                            <a href="#"> 
                                <img src="assets/img/alia-bolly.jpg" class="img-fluid full-wide" alt="bollywood-life">
                                <label class="duration"></label>  
                            </a>
                            <div class="card-footer">
                                <h5>Alia Bhatt's Ravishing Black Gown At The International Film Festival Of India 2017 Costs Rs 82,000</h5> 
                                <small>October 30, 2017</small> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" data-plugin="matchHeight" data-by-row="true">
                <div class="col-md-4 col-xs-12">
                    <div class="guest-blog">
                        <div class="card">
                            <a href="#"> 
                                <img src="assets/img/priyanka.jpg" class="img-fluid full-wide" alt="bollywood-life">
                                <label class="duration"></label>  
                            </a>
                            <div class="card-footer">
                                <h5>When Bhumi Pednekar stunned us with her simple yet significant style play for Shubh Mangal </h5> 
                                <small>October 30, 2017</small> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="guest-blog">
                        <div class="card">
                            <a href="#"> 
                                <img src="assets/img/jenifer.jpg" class="img-fluid full-wide" alt="bollywood-life">
                                <label class="duration"></label>  
                            </a>
                            <div class="card-footer">
                                <h5>Jennifer Winget Gets A Brigitte Bardot Makeover And It's The Best House Party Look For The Season</h5> 
                                <small>October 30, 2017</small> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="guest-blog">
                        <div class="card">
                            <a href="#"> 
                                <img src="assets/img/alia-bolly.jpg" class="img-fluid full-wide" alt="bollywood-life">
                                <label class="duration"></label>  
                            </a>
                            <div class="card-footer">
                                <h5>Alia Bhatt's Ravishing Black Gown At The International Film Festival Of India 2017 Costs Rs 82,000</h5> 
                                <small>October 30, 2017</small> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="button-center">
                    <a href="#" class="btn btn-default">Load More</a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php @include 'footer.php'; ?>


<?php
@include 'header.php';
?>
<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="#">Home</a>
                <a href="#" class="active">Photos</a>
            </div>
        </div>
    </div>
</nav>
<article>
    <div class="container">
        <div class="row">
            <div class="width-600">
                <div class="row">
                    <div class="title">
                        <h1 class="subtitle photo">Check Out: Flight fashion: Best and worst dressed celebs this week!</h1>
                    </div>
                    <div class="tags full-wide pt-2">
                        <ul>
                            <li>
                                <a href="#">Lifestyle</a>
                            </li>
                            <li>
                                <a href="#">Fashion</a>
                            </li>
                            <li>
                                <a href="#">Spotted</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-50 float-left">
                        <p class="text-muted mt-1">By <span class="primary-color">Prajakta Ajgaonkar </span>on October 27 2017<i class="ion-chatbubble-working"><span class="text-muted">12</span></i></p>
                    </div>
                    <div class="col-50 editor p-0 float-right">
                        <div class="icon-box p-0 float-right xl-hidden">
                            <ul>
                                <li class="pr-3">
                                    <i class="ion-android-share-alt fs-2"></i>
                                </li>
                            </ul>
                        </div>
                        <div class="icon-box p-0 float-right xs-hidden">
                            <ul>
                                <li class="bg-fb">
                                    <a href="#">
                                        <i class="ion-social-facebook text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-twitter">
                                    <a href="#">
                                        <i class="ion-social-twitter text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-google">
                                    <a href="#">
                                        <i class="ion-social-googleplus text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-youtube">
                                    <a href="#" class="heart">
                                        <i class="ion-ios-heart-outline text-white"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="bg-black mt-20 p-10">
                    <div class="photo-block mb-20 red-border p-10">
                        <div class="photo-lists">
                            <img src="assets/img/photo-detail-01.jpg" class="img-fluid full-wide m-auto" alt="source">
                            <label class="count">1/4</label>
                        </div>
                        <div class="full-wide">
                            <h5>Deepika Padukone spotted at a restaurant.</h5>
                            <ul class="nav navbar-nav float-right d-inline-b p-2">
                                <li class="dropdown mega-dropdown share-button">
                                    <img src="assets/img/share-icon.png" alt="share-button">
                                </li>
                            </ul>
                        </div>
                        <p class="font-17 border-top-1">We all know that thanks to the all the controversy, Sanjay Leela Bhansali’s ‘Padmavati’ has been postponed. This goes without saying that the actors of the film are upset. Recently, our shutterbugs captured Deepika Padukone and Ranveer Singh in the city, of course at different venues. While Deepika was clicked jetting out of a restaurant, Ranveer was spotted in suburban Mumbai at a football ground. Well, the actress clearly was in no mood to smile for the shutterbugs and Ranveer, who is always in a jovial mood, didn’t mind
                            obliging his fans.</p>
                        <label class="fs-14 font-light float-right font-wt-400 m-0">Image Source - Yogesh Shah</label>
                        <div class="clear"></div>
                    </div>
                    <div class="photo-block mb-20 red-border p-10">
                        <div class="photo-lists">
                            <img src="assets/img/photo-detail-02.jpg" class="img-fluid full-wide m-auto" alt="source">
                            <label class="count">1/4</label>
                        </div>
                        <div class="full-wide">
                            <h5>The actress looked quite sad.</h5>
                            <ul class="nav navbar-nav float-right p-2">
                                <li class="dropdown mega-dropdown share-button">
                                    <img src="assets/img/share-icon.png" alt="share-button">
                                </li>
                            </ul>
                        </div>
                        <label class="fs-14 font-light float-right font-wt-400 m-0">Image Source - Yogesh Shah</label>
                        <div class="clear"></div>
                    </div>
                    <div class="photo-block mb-20 red-border p-10">
                        <div class="photo-lists">
                            <img src="assets/img/photo-detail-03.jpg" class="img-fluid full-wide m-auto" alt="source">
                            <label class="count">1/4</label>
                        </div>
                        <div class="full-wide">
                            <h5>Ranveer Singh was clicked in suburban Mumbai.</h5>
                            <ul class="nav navbar-nav float-right p-2">
                                <li class="dropdown mega-dropdown share-button">
                                    <img src="assets/img/share-icon.png" alt="share-button">
                                </li>
                            </ul>
                        </div>
                        <label class="fs-14 font-light float-right font-wt-400 m-0">Image Source - Yogesh Shah</label>
                        <div class="clear"></div>
                    </div>
                    <div class="photo-block mb-20 red-border p-10">
                        <div class="photo-lists">
                            <img src="assets/img/photo-detail-04.jpg" class="img-fluid full-wide m-auto" alt="source">
                            <label class="count">1/4</label>
                        </div>
                        <div class="full-wide">
                            <h5>Ranveer Singh was clicked in suburban Mumbai.</h5>
                            <ul class="nav navbar-nav float-right p-2">
                                <li class="dropdown mega-dropdown share-button">
                                    <img src="assets/img/share-icon.png" alt="share-button">
                                </li>
                            </ul>
                        </div>
                        <label class="fs-14 font-light float-right font-wt-400 m-0">Image Source - Yogesh Shah</label>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="flipkart-ads mt-30 mb-30">
                    <a href="https://www.flipkart.com/" target=�?_blank�?>
                        <img src="assets/img/flipkart-ads.jpg" class="" alt="Flipkart Ad">
                    </a>
                </div>
                <div class="comment-box mt-3">
                    <h2 class="fs-2 font-wt-800 uppercase">comments</h2>
                    <textarea class="form-control" placeholder="Add your comment" rows="4"></textarea>
                    <a href="#" class="btn btn-md btn-primary btn-round pl-5 pr-5">Post</a>
                    <a href="#" class="primary-color float-right underline">See All Comments</a>
                </div>
            </div>
            <div class="right-bar-fix-width-300">
                <div class="mb-40">
                    <a href="#">
                        <img src="assets/img/mmt-ads.jpg" alt="mmt-ads" class="">
                    </a>
                </div>
                <div class="text-center border-primary bg-white mb-40">
                    <h2 class="artist bg-light-red uppercase quiz sideTitle">bollywood quiz</h2>
                    <div class="col- quiz">
                        <div class="full-wide">
                            <img src="assets/img/quiz-img.jpg" class="img-fluid" alt="quiz">
                        </div>
                        <h5>Salman Khan, Shah Rukh Khan or Akshay Kumar  which superstar is your fave TV host?</h5>
                        <div class="radio-group quiz">
                            <div class="md-radio inline-radio">
                                <input class="form-control" name="quiz" id="option-1" type="radio">
                                <label for="option-1">Salman Khan</label>
                            </div>
                            <div class="md-radio inline-radio">
                                <input class="form-control" name="quiz" id="option-2" type="radio">
                                <label for="option-2">Shahrukh Khan</label>
                            </div>
                            <div class="md-radio inline-radio">
                                <input class="form-control" name="quiz" id="option-3" type="radio">
                                <label for="option-3">Akshay Kumar</label>
                            </div>
                        </div>
                        <div class="inline-button mt-10 sm-inline-b"> <a href="#" class="btn btn-default btn-round font-wt-500 float-left">Vote Now</a> <a href="#" class="font-wt-500 primary-color fs-14 p-1 float-left">View Results</a> <a href="#" class="font-wt-500 primary-color fs-14 p-1 float-left">Take Another quiz</a> </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="full-wide text-center border-primary bg-white mb-30">
                    <h2 class="artist bg-light-red uppercase quiz sideTitle">Artist of Month</h2>
                    <div class="artist-month pt-0 full-h"> <img src="assets/img/artist-month.jpg" alt="artist of month">
                        <p class="font-wt-300 font-22">These 9 Pictures Of Aishwarya Rai Prove She's Not Called The Most Beautiful Woman For Nothing!</p>
                    </div>
                </div>
                <div class="mb-30">
                    <a href="#">
                        <img src="assets/img/mc-d-ads.jpg" alt="mmt-ads" class="">
                    </a>
                </div>
                <div class="mb-30">
                    <a href="#">
                        <img src="assets/img/plural-ads.jpg" alt="mmt-ads" class="">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-30 mb-30 border-icon"></div>
    <div class="container">
        <div class="row">
            <div class="width-600">
                <div class="title">
                    <h2 class="m-0 font-wt-300">PICS: Shraddha Kapoor brightens up the second day of IFFI in yellow</h2>
                </div>
                <div class="tags full-wide pt-2">
                    <ul class="d-block">
                        <li class="float-left mr-2">
                            <a href="#" class="font-wt-500 p-1 primary-color d-inline-b bg-light-red">Lifestyle</a>
                        </li>
                        <li class="float-left mr-2">
                            <a href="#" class="font-wt-500 p-1 primary-color d-inline-b bg-light-red">Fashion</a>
                        </li>
                        <li class="float-left mr-2">
                            <a href="#" class="font-wt-500 p-1 primary-color d-inline-b bg-light-red">Spotted</a>
                        </li>
                    </ul>
                </div>
                <div class="b-block">
                    <p class="d-inline-b text-muted mt-1">By Prajakta Ajgaonkar on October 17, 2017</p>
                    <div class="icon-box p-0 float-right">
                        <ul>
                            <li class="bg-fb">
                                <a href="#">
                                    <i class="ion-social-facebook text-white"></i>
                                </a>
                            </li>
                            <li class="bg-twitter">
                                <a href="#">
                                    <i class="ion-social-twitter text-white"></i>
                                </a>
                            </li>
                            <li class="bg-google">
                                <a href="#">
                                    <i class="ion-social-googleplus text-white"></i>
                                </a>
                            </li>
                            <li class="bg-youtube">
                                <a href="#"  class="heart">
                                    <i class="ion-ios-heart-outline text-white"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="bg-black mt-20 p-10">
                    <div class="photo-block mb-20 red-border p-10">
                        <div class="photo-lists">
                            <img src="assets/img/photo-detail-05.jpg" class="img-fluid full-wide m-auto" alt="source">
                            <label class="count">1/4</label>
                        </div>
                        <div class="full-wide">
                            <h5 class="fs-22 mt-2 d-inline-b font-wt-500">Shraddha Kapoor at IFFI 2017.</h5>
                            <ul class="nav navbar-nav float-right d-inline-b p-2">
                                <li class="dropdown mega-dropdown share-button">
                                    <img src="assets/img/share-icon.png" alt="share-button">
                                </li>
                            </ul>
                        </div>
                        <p class="font-17 border-top-1">We all know that thanks to the all the controversy, Sanjay Leela Bhansali’s ‘Padmavati’ has been postponed. This goes without saying that the actors of the film are upset. Recently, our shutterbugs captured Deepika Padukone and Ranveer Singh in the city, of course at different venues. While Deepika was clicked jetting out of a restaurant, Ranveer was spotted in suburban Mumbai at a football ground. Well, the actress clearly was in no mood to smile for the shutterbugs and Ranveer, who is always in a jovial mood, didn’t mind
                            obliging his fans.</p>

                        <label class="fs-14 font-light float-right font-wt-400 m-0">Image Source - Yogesh Shah</label>
                        <div class="clear"></div>
                    </div>
                    <div class="photo-block mb-20 red-border p-10">
                        <div class="photo-lists">
                            <img src="assets/img/photo-detail-06.jpg" class="img-fluid full-wide m-auto" alt="source">
                            <label class="count">2/4</label>
                        </div>
                        <div class="full-wide">
                            <h5>Ranveer Singh was clicked in suburban Mumbai.</h5>
                            <ul class="nav navbar-nav float-right p-2">
                                <li class="dropdown mega-dropdown share-button">
                                    <img src="assets/img/share-icon.png" alt="share-button">
                                </li>
                            </ul>
                        </div>
                        <label class="fs-14 font-light float-right font-wt-400 m-0">Image Source - Yogesh Shah</label>
                        <div class="clear"></div>
                    </div>
                    <div class="photo-block mb-20 red-border p-10">
                        <div class="photo-lists">
                            <img src="assets/img/photo-detail-07.jpg" class="img-fluid full-wide m-auto" alt="source">
                            <label class="count">3/4</label>
                        </div>
                        <div class="full-wide">
                            <h5>Ranveer Singh was clicked in suburban Mumbai.</h5>
                            <ul class="nav navbar-nav float-right p-2">
                                <li class="dropdown mega-dropdown share-button">
                                    <img src="assets/img/share-icon.png" alt="share-button">
                                </li>
                            </ul>
                        </div>
                        <label class="fs-14 font-light float-right font-wt-400 m-0">Image Source - Yogesh Shah</label>
                        <div class="clear"></div>
                    </div>
                    <div class="photo-block mb-20 red-border p-10">
                        <div class="photo-lists">
                            <img src="assets/img/photo-detail-08.jpg" class="img-fluid full-wide m-auto" alt="source">
                            <label class="count">4/4</label>
                        </div>
                        <div class="full-wide">
                            <h5>Ranveer Singh was clicked in suburban Mumbai.</h5>
                            <ul class="nav navbar-nav float-right p-2">
                                <li class="dropdown mega-dropdown share-button">
                                    <img src="assets/img/share-icon.png" alt="share-button">
                                </li>
                            </ul>
                        </div>
                        <label class="fs-14 font-light float-right font-wt-400 m-0">Image Source - Yogesh Shah</label>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="flipkart-ads mt-30 mb-30">
                    <a href="https://www.flipkart.com/" target=�?_blank�?>
                        <img src="assets/img/loreal-ads.jpg" class="" alt="Flipkart Ad">
                    </a>
                </div>
                <div class="comment-box mt-3">
                    <h2 class="fs-2 font-wt-800 uppercase">comments</h2>
                    <textarea class="form-control" placeholder="Add your comment" rows="4"></textarea>
                    <a href="#" class="btn btn-md btn-primary btn-round pl-5 pr-5">Post</a>
                    <a href="#" class="primary-color float-right underline">See All Comments</a>
                </div>
            </div>
            <div class="right-bar-fix-width-300">
                <div class="mb-30">
                    <a href="#">
                        <img src="assets/img/mc-d-ads.jpg" alt="mmt-ads" class="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</article>

<?php
@include 'footer.php';
?>

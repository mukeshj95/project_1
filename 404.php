<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Page Not Found</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="assets/img/bubble-logo.png" type="image/gif" sizes="16x16">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
    <style>
        html {
            color: #888;
            display: table;
            font-family: sans-serif;
            height: 100%;
            text-align: center;
            width: 100%;
        }

        body {
            display: table-cell;
            vertical-align: middle;
            margin: 2em auto;
        }
    </style>
</head>
<body>
    <section class="not-found">
        <div class="container">
            <img src="assets/img/404-bg.png" alt="">
            <h1>Ailaa! Ooi maaa! Page not found!</h1>
            <p>The page you are looking for no longer exists.</p>
            <div class="button-center">
                <a href="index.php" class="btn btn-primary">Go to Homepage</a>
            </div>
        </div>
    </section>
</body>
</html>
<!-- IE needs 512+ bytes: https://blogs.msdn.microsoft.com/ieinternals/2010/08/18/friendly-http-error-pages/ -->

<?php @include 'header.php'; ?>


<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="index.php">Home</a>
                <a href="celebs-landing.php" class="active">Celebrities</a>
            </div>
        </div>
    </div>
</nav>

<article class="bg-grey">
    <div class="container-fluid">
        <div class="row">
            <div class="full-wide text-center"> 
                <div class="height-0">
                    <i class="arrows"><img src="assets/img/left-bar.png"></i>
                    <h1 class="d-inline title text-center uppercase">Top Celebs this Week</h1> 
                    <i class="arrows"><img src="assets/img/right-bar.png"></i> 
                </div>
            </div>
            <div class="bubble-bg main">
                <ul class="celeb-list">
                    <li>
                        <div class="wide"><a href="celebs.php"><img src="assets/img/celeb-01.jpg" alt=""><span>Salman Khan</span></a></div>
                        <div class="small"><a href="celebs.php"><img src="assets/img/celeb-02.jpg" alt=""><span>Ranveer Singh</span></a></div>
                        <div class="small"><a href="celebs.php"><img src="assets/img/celeb-03.jpg" alt=""><span>Priyanka Chopra</span></a></div>
                    </li>
                    <li>
                        <div class="wide"><a href="celebs.php"><img src="assets/img/celeb-04.jpg" alt=""><span>Aliya Bhatt</span></a></div>
                        <div class="wide"><a href="celebs.php"><img src="assets/img/celeb-05.jpg" alt=""><span>Ranbeer Kapoor</span></a></div>
                    </li>
                    <li>
                        <div class="small"><a href="celebs.php"><img src="assets/img/celeb-06.jpg" alt=""><span>Amir Khan</span></a></div>
                        <div class="small"><a href="celebs.php"><img src="assets/img/celeb-07.jpg" alt=""><span>Anushka Sharma</span></a></div>
                        <div class="wide"><a href="celebs.php"><img src="assets/img/celeb-08.jpg" alt=""><span>Shah Rukh Khan</span></a></div>
                    </li>
                    <li>
                        <div class="wide"><a href="celebs.php"><img src="assets/img/celeb-09.jpg" alt=""><span>Akishay Kumar</span></a></div>
                        <div class="wide"><a href="celebs.php"><img src="assets/img/celeb-10.jpg" alt=""><span>Dipeka Padukone</span></a></div>
                    </li>
                    <li>
                        <div class="small"><a href="celebs.php"><img src="assets/img/celeb-11.jpg" alt=""><span>Kangna Ranaut</span></a></div>
                        <div class="small"><a href="celebs.php"><img src="assets/img/celeb-12.jpg" alt=""><span>Katrina Kaif</span></a></div>
                        <div class="wide"><a href="celebs.php"><img src="assets/img/celeb-13.jpg" alt=""><span>Kareena Kapoor</span></a></div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <article>
        <div class="container">
            <div class="row">
                <div class="col-md-12 mb-30 xs-center">
                    <h2 class="d-inline title uppercase font-32">Artist of the month</h2>
                    <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-xs-12">
                    <div class="celebs-img">
                        <img src="assets/img/Sharukh-celeb.jpg" alt="Shahrukh Khan" title="Shahrukh Khan" class="img-fluid full-wide">
                    </div>
                    <div class="icon-box">
                        <ul class="m-auto">
                            <li class="bg-fb">
                                <a href="#">
                                    <i class="ion-social-facebook text-white"></i>
                                </a>
                            </li>
                            <li class="bg-twitter">
                                <a href="#">
                                    <i class="ion-social-twitter text-white"></i>
                                </a>
                            </li>
                            <li class="bg-google">
                                <a href="#">
                                    <i class="ion-social-googleplus text-white"></i>
                                </a>
                            </li>
                            <li class="bg-youtube">
                                <a href="#">
                                    <i class="ion-ios-heart-outline text-white"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9 col-xs-12">
                    <div class="celeb-des">
                        <div>
                            <h1>Shah Rukh Khan</h1>
                            <p>Shahrukh Khan also known as SRK, is an Indian film actor, producer and television personality. Referred to in the media as the "Baadshah of Bollywood", "King of Bollywood" or "King Khan", he has appeared in more than 80 Bollywood films, and earned numerous accolades, including 14 Filmfare Awards. Khan has a significant following in Asia and the Indian diaspora worldwide. In terms of audience size and income, he has been described as one of the most successful film stars in the world.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi et felis vitae tortor eleifend varius hendrerit quis justo. In et pulvinar dui, mattis tempus arcu. Morbi pretium est vel leo gravida vulputate. Sed rutrum rutrum suscipit. Donec at ornare nibh. Nullam laoreet dictum libero, sed ultricies nisl sodales. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</article>
<section>
    <div class="container">
        <div class="row">
            <div class="full-wide text-center mb-40">
                <i class="arrowsSub"><img src="assets/img/left-bar.png"></i>
                <h2 class="d-inline title text-center uppercase">Celebrity Directory</h2>
                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div class="celebs-dir">
            <div class="pagination-box">
                <nav aria-label="Page navigation">
                    <ul class="pagination justify-content-center">
                        <li class="page-item active"><a class="page-link" href="#">All</a></li>
                        <li class="page-item"><a class="page-link" href="#">a</a></li>
                        <li class="page-item"><a class="page-link" href="#">b</a></li>
                        <li class="page-item"><a class="page-link" href="#">c</a></li>
                        <li class="page-item"><a class="page-link" href="#">d</a></li>
                        <li class="page-item"><a class="page-link" href="#">e</a></li>
                        <li class="page-item"><a class="page-link" href="#">f</a></li>
                        <li class="page-item"><a class="page-link" href="#">g</a></li>
                        <li class="page-item"><a class="page-link" href="#">h</a></li>
                        <li class="page-item"><a class="page-link" href="#">i</a></li>
                        <li class="page-item"><a class="page-link" href="#">j</a></li>
                        <li class="page-item"><a class="page-link" href="#">k</a></li>
                        <li class="page-item"><a class="page-link" href="#">l</a></li>
                        <li class="page-item"><a class="page-link" href="#">m</a></li>
                        <li class="page-item"><a class="page-link" href="#">n</a></li>
                        <li class="page-item"><a class="page-link" href="#">o</a></li>
                        <li class="page-item"><a class="page-link" href="#">p</a></li>
                        <li class="page-item"><a class="page-link" href="#">q</a></li>
                        <li class="page-item"><a class="page-link" href="#">r</a></li>
                        <li class="page-item"><a class="page-link" href="#">s</a></li>
                        <li class="page-item"><a class="page-link" href="#">t</a></li>
                        <li class="page-item"><a class="page-link" href="#">u</a></li>
                        <li class="page-item"><a class="page-link" href="#">v</a></li>
                        <li class="page-item"><a class="page-link" href="#">w</a></li>
                        <li class="page-item"><a class="page-link" href="#">x</a></li>
                        <li class="page-item"><a class="page-link" href="#">y</a></li>
                        <li class="page-item"><a class="page-link" href="#">z</a></li>
                    </ul>
                </nav>
            </div>

            <div class="celebs-list-view">
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="celeb-list">
                            <div class="img-celeb">
                                <img src="assets/img/Amir-celeb.jpg" class="img-fluid" alt="Amir Khan" title="Amir Khan">
                            </div>
                            <div class="celeb-title">
                                <h3>Amir Khan</h3>
                                <p>Actor, Producer</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="celeb-list">
                            <div class="img-celeb">
                                <img src="assets/img/Amir-celeb.jpg" class="img-fluid" alt="Amir Khan" title="Amir Khan">
                            </div>
                            <div class="celeb-title">
                                <h3>Amir Khan</h3>
                                <p>Actor, Producer</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="celeb-list">
                            <div class="img-celeb">
                                <img src="assets/img/Amir-celeb.jpg" class="img-fluid" alt="Amir Khan" title="Amir Khan">
                            </div>
                            <div class="celeb-title">
                                <h3>Amir Khan</h3>
                                <p>Actor, Producer</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="celebs-list-view">
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="celeb-list">
                            <div class="img-celeb">
                                <img src="assets/img/Amir-celeb.jpg" class="img-fluid" alt="Amir Khan" title="Amir Khan">
                            </div>
                            <div class="celeb-title">
                                <h3>Amir Khan</h3>
                                <p>Actor, Producer</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="celeb-list">
                            <div class="img-celeb">
                                <img src="assets/img/Amir-celeb.jpg" class="img-fluid" alt="Amir Khan" title="Amir Khan">
                            </div>
                            <div class="celeb-title">
                                <h3>Amir Khan</h3>
                                <p>Actor, Producer</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="celeb-list">
                            <div class="img-celeb">
                                <img src="assets/img/Amir-celeb.jpg" class="img-fluid" alt="Amir Khan" title="Amir Khan">
                            </div>
                            <div class="celeb-title">
                                <h3>Amir Khan</h3>
                                <p>Actor, Producer</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="celebs-list-view">
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="celeb-list">
                            <div class="img-celeb">
                                <img src="assets/img/Amir-celeb.jpg" class="img-fluid" alt="Amir Khan" title="Amir Khan">
                            </div>
                            <div class="celeb-title">
                                <h3>Amir Khan</h3>
                                <p>Actor, Producer</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="celeb-list">
                            <div class="img-celeb">
                                <img src="assets/img/Amir-celeb.jpg" class="img-fluid" alt="Amir Khan" title="Amir Khan">
                            </div>
                            <div class="celeb-title">
                                <h3>Amir Khan</h3>
                                <p>Actor, Producer</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="celeb-list">
                            <div class="img-celeb">
                                <img src="assets/img/Amir-celeb.jpg" class="img-fluid" alt="Amir Khan" title="Amir Khan">
                            </div>
                            <div class="celeb-title">
                                <h3>Amir Khan</h3>
                                <p>Actor, Producer</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="button-center pb-3">
                <a href="#" class="btn btn-default pr-5 pl-5">Load more</a>
            </div>
        </div>
    </div>
</section>
<?php @include 'footer.php'; ?>


// PAGE LOADER
$(document).ready(function () {
    if ($(".loader-wrapper").length != 0) {
        $(".loader").fadeOut(200);
        $(".loader-wrapper").delay(100).fadeOut(400);
    }
});

$("select").select2();

var select_language = $('select.form-control');
$(select_language).select2({
    minimumResultsForSearch: -1
});

$('.search-bar').on('click', function () {
    //alert("hello");
    $('#search-box').slideToggle('display');
});
$('.search-bar-lg').on('click', function () {
    //alert("hello");
    $('#search-box-lg').slideToggle('display');
});

var title1 = $('#title-limit1');
title1.text(title1.text().substring(0,55)+'..');

var title2 = $('#title-limit2');
title2.text(title2.text().substring(0,55)+'..');

var title3 = $('#title-limit3');
title3.text(title3.text().substring(0,55)+'..');

var title4 = $('#title-limit4');
title4.text(title4.text().substring(0,55)+'..');

var title5 = $('#title-limit5');
title5.text(title5.text().substring(0,55)+'..');

var title6 = $('#title-limit6');
title6.text(title6.text().substring(0,55)+'..');

/********** TOP MOVIES SLIDER **********/
$('#top-movies-slider').owlCarousel({
    loop:true,
    margin:0,
    nav:true,
    dots: false,
    //autoplay: true,
    autoplayTimeout: 2000,
    animateOut: 'fadeOut',

    responsive:{
        100: {
            items: 1
        },
        576:{
            items:2
        },
        769:{
            items:3
        },
        990:{
            items:5
        }
    }
});

/********** count down timer **********/

function getTimeRemaining(endtime) {
    var t = Date.parse(endtime) - Date.parse(new Date());
    var seconds = Math.floor((t / 1000) % 60);
    var minutes = Math.floor((t / 1000 / 60) % 60);
    var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
    var days = Math.floor(t / (1000 * 60 * 60 * 24));
    return {
        'total': t,
        'days': days,
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
    };
}

function initializeClock(id, endtime) {
    var clock = document.getElementById(id);
    var daysSpan = clock.querySelector('.days');
    var hoursSpan = clock.querySelector('.hours');
    var minutesSpan = clock.querySelector('.minutes');
    var secondsSpan = clock.querySelector('.seconds');

    function updateClock() {
        var t = getTimeRemaining(endtime);

        daysSpan.innerHTML = t.days;
        hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
        minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
        if(secondsSpan!=undefined) secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

        if (t.total <= 0) {
            clearInterval(timeinterval);
        }
    }

    updateClock();
    var timeinterval = setInterval(updateClock, 1000);
}

var deadline = new Date(Date.parse(new Date()) + 15 * 24 * 60 * 60 * 1000);
initializeClock('clockdiv', deadline);
initializeClock('timer', new Date(Date.parse(new Date()) + 15 * 24 * 60 * 60 * 1000));

/********* upcoming movies ************/




/************Multiple carousal ***********/



$('#carousel1').owlCarousel({
    loop:true,
    margin:0,
    nav:false,
    dots: false,
    autoplay: true,
    autoplayTimeout: 3000,
    animateOut: 'fadeOut',
    autoplayHoverPause: true,

    responsive:{
        100: {
            items: 1
        },
        576:{
            items:1
        },
        770:{
            items:2
        },
        990:{
            items:2
        }
    }
});

$('#slider').owlCarousel({
    loop:true,
    margin:0,
    nav:true,
    dots: false,
    autoplay: true,
    autoplayTimeout: 2000,
    animateOut: 'fadeOut',
    autoplayHoverPause: false,

    responsive:{
        100: {
            items: 1
        },
        576:{
            items:2
        },
        769:{
            items:3
        },
        990:{
            items:5
        }
    }
});
$('#upcomingMovies').owlCarousel({
    loop:true,
    margin:0,
    nav:true,
    dots: false,
    //autoplay: true,
    autoplayTimeout: 2000,
    autoplayHoverPause: true,

    responsive:{
        100: {
            items: 1
        },
        576:{
            items:2
        },
        769:{
            items:3
        },
        990:{
            items:4
        }
    }
});



$(window).resize(function(){
    if ($(window).width() <= 600) {
        $('#news-section').owlCarousel({
            loop:true,
            margin:0,
            nav:true,
            dots: false,
            //autoplay: true,
            autoplayTimeout: 2000,

            responsive:{
                100: {
                    items: 1
                },
                576:{
                    items:2
                },
                769:{
                    items:3
                },
                990:{
                    items:4
                }
            }
        });
    }
});


$('[data-plugin="matchHeight"]').each(function() {
    var $this = $(this),
        options = $.extend(true, {}, null, $this.data()),
        matchSelector = $this.data('matchSelector');

    if (matchSelector) {
        $this.find(matchSelector).matchHeight(options);
    } else {
        $this.children().matchHeight(options);
    }

});

//var logo = $('.navbar-brand img');
if ($(window).width() <= 768) {
    $('.logo').attr('src', 'assets/img/bubble-logo.png');
}

$('#videos a').click(function (e) {
    e.preventDefault();
    $(this).tab('show')
});

window.onload = function () {

    var options = {
        animationEnabled: true,
        title: {
            text: "Monthly Sales - 2017"
        },
        axisX: {
            valueFormatString: "MMM"
        },
        axisY: {
            title: "Sales (in USD)",
            prefix: "$",
            includeZero: false
        },
        data: [{
            yValueFormatString: "$#,###",
            xValueFormatString: "MMMM",
            type: "spline",
            dataPoints: [
                {x: new Date(2017, 0), y: 25060},
                {x: new Date(2017, 1), y: 27980},
                {x: new Date(2017, 2), y: 33800},
                {x: new Date(2017, 3), y: 49400},
                {x: new Date(2017, 4), y: 40260},
                {x: new Date(2017, 5), y: 33900},
                {x: new Date(2017, 6), y: 48000},
                {x: new Date(2017, 7), y: 31500},
                {x: new Date(2017, 8), y: 32300},
                {x: new Date(2017, 9), y: 42000},
                {x: new Date(2017, 10), y: 52160},
                {x: new Date(2017, 11), y: 49400}
            ]
        }]
    };
    $(document).ready(function () {
        var my_posts = $("[rel=tooltip]");

        var size = $(window).width();
        for (i = 0; i < my_posts.length; i++) {
            the_post = $(my_posts[i]);

            if (the_post.hasClass('invert') && size >= 767) {
                the_post.tooltip({placement: 'left'});
                the_post.css("cursor", "pointer");
            } else {
                the_post.tooltip({placement: 'rigth'});
                the_post.css("cursor", "pointer");
            }
        }
    });
}

$('[data-toggle="tooltip"]').tooltip('show');


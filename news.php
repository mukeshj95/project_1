<?php @include 'header.php'; ?>

<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="#">Home</a>
                <a href="#" class="active">News</a>
            </div>
        </div>
    </div>
</nav>

<article>
    <div class="container">
        <div class="row">
            <div class="news mb-30 xs-text-center">
                <h1 class="d-inline title text-center uppercase">news</h1> 
                <i class="arrows"><img src="assets/img/right-bar.png"></i>
            </div>
            <div class="col-md-12">
                <div class="row" data-plugin="matchHeight" data-by-row="true">
                    <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12">
                        <div class="news-list">
                            <a href="#" class="d-block">
                                <div class="photo-link">
                                    <img src="assets/img/virat-anuska.jpg" class="img-fluid full-wide">
                                    <button>spotted</button>
                                </div>
                                <h2>Here is a look at Anushka Sharma-Virat Kohli's reception invitation</h2>
                            </a>
                            <small>October 21, 2017</small>
                            <p>Dear Sanjay Leela Bhansali, <br />In the last two decades you devoted to the Indian film industry, you have made some outstanding films with concepts many filmmakers would not even like to touch. You made ‘Khamoshi: The Musical’ in which two...</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                        <div class="news-list">
                            <a href="#" class="d-block">
                                <div class="photo-link">
                                    <img src="assets/img/hema.jpg" class="img-fluid full-wide">
                                    <button>spotted</button>
                                </div>
                                <h5>Before Deepika Padukone, THIS actress already played the role of Rani Padmavati</h5>
                            </a>
                            <small>October 21, 2017</small>
                        </div>
                        <div class="news-list">
                            <a href="#" class="d-block">
                                <div class="photo-link">
                                    <img src="assets/img/hritik.jpg" class="img-fluid full-wide">
                                    <button>blog-list</button>
                                </div>
                                <h5>When Hrithik Roshan almost drowned while shooting for ‘Kaho Naa… Pyaar Hai’</h5>
                            </a>
                            <small>October 21, 2017</small>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</article>
<article class="bg-grey">
    <div class="container">
        <div class="row">
            <div class="full-wide text-center mb-40">
                <i class="arrowsSub"><img src="assets/img/left-bar.png"></i>
                <h2 class="d-inline title text-center uppercase">gossips</h2>
                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
            </div>
            <div class="bollywood-life sub-cat airport" data-plugin="matchHeight" data-by-row="true">
                <div class="col-md-4 col-xs-12">
                    <div class="card no-border">
                        <a href="#"> 
                            <img src="assets/img/gossips-1.jpg" class="img-fluid full-wide" alt="bollywood-life">
                            <div class="button-link">
                                <button class="font-wt-700">Gossips</button>
                            </div>
                        </a>
                        <p class="lead">Kangana Ranaut wants ‘MENTAL’ but will Salman Khan oblige?</p> <small>October 30, 2017</small> 
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="card no-border">
                        <a href="#"> 
                            <img src="assets/img/gossips-2.jpg" class="img-fluid full-wide" alt="bollywood-life">
                            <div class="button-link">
                                <button class="font-wt-700">Gossips</button>
                            </div>
                        </a>
                        <p class="lead">SAY WHAT! Will Aamir Khan work only on one project for next ten years?</p> <small>October 30, 2017</small> 
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="card no-border">
                        <a href="#"> 
                            <img src="assets/img/gossips-3.jpg" class="img-fluid full-wide" alt="bollywood-life">
                            <div class="button-link">
                                <button class="font-wt-700">Gossips</button>
                            </div>
                        </a>
                        <p class="lead">Priyanka to get a whopping amount to perform at an award function? </p> <small>October 30, 2017</small> 
                    </div>
                </div>
            </div>
            <div class="button-center">
                    <a href="#" class="btn btn-default">View all gossips</a>
            </div>
        </div>
    </div>
</article>
<article>
    <div class="container">
        <div class="row">
            <div class="full-wide text-center mb-40">
                <i class="arrowsSub"><img src="assets/img/left-bar.png"></i>
                <h2 class="d-inline title text-center uppercase">features</h2>
                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <div class="col-md-6 col-xs-12">
                <div class="news-list">
                    <a href="#" class="d-block">
                        <div class="photo-link">
                            <img src="assets/img/virat-anuska-2.jpg" class="img-fluid full-wide">
                            <button>featured</button>
                        </div>
                        <h5>2017: Anushka, Sagarika and others who made up for the prettiest brides</h5>
                    </a>
                    <small>October 21, 2017</small>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="news-list">
                    <a href="#" class="d-block">
                        <div class="photo-link">
                            <img src="assets/img/features-2.jpg" class="img-fluid full-wide">
                            <button>featured</button>
                        </div>
                        <h5>Sridevi’s ‘MOM’ takes Russia by STORM</h5>
                    </a>
                    <small>October 21, 2017</small>
                </div>
            </div>
        </div>
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <div class="col-md-4 col-xs-12">
                <div class="news-list">
                    <a href="#" class="d-block">
                        <div class="photo-link">
                            <img src="assets/img/john.jpg" class="img-fluid full-wide">
                            <button>featured</button>
                        </div>
                        <h5>Sridevi’s ‘MOM’ takes Russia by STORM</h5>
                    </a>
                    <small>October 21, 2017</small>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="news-list">
                    <a href="#" class="d-block">
                        <div class="photo-link">
                            <img src="assets/img/ajay.jpg" class="img-fluid full-wide">
                            <button>featured</button>
                        </div>
                        <h5>Sridevi’s ‘MOM’ takes Russia by STORM</h5>
                    </a>
                    <small>October 21, 2017</small>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="news-list">
                    <a href="#" class="d-block">
                        <div class="photo-link">
                            <img src="assets/img/features-3.jpg" class="img-fluid full-wide">
                            <button>featured</button>
                        </div>
                        <h5>Sridevi’s ‘MOM’ takes Russia by STORM</h5>
                    </a>
                    <small>October 21, 2017</small>
                </div>
            </div>
        </div>
        <div class="button-center">
            <a href="#" class="btn btn-default">Load More</a>
        </div>
    </div>
</article>
<section class="bb-dairies-bg">
    <div class="container">
        <div class="col-md-12 border-primary bg-white">
            <div class="row">
                <div class="full-wide text-center">
                    <h2 class="artist bg-light-red sideTitle special uppercase">bubble dairies</h2>
                </div>
            </div>
            <div class="row" data-plugin="matchHeight" data-by-row="true">
                <div class="col-md-4 col-xs-12">
                    <div class="bb-dairies">
                        <a href="#" class="d-block">
                            <div class="photo-link">
                                <img src="assets/img/john.jpg" class="img-fluid full-wide">
                                <button>featured</button>
                            </div>
                            <h5>Inside Kareena Kapoor's Grand Birthday Party </h5>
                        </a>
                        <small>October 21, 2017</small>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="bb-dairies">
                        <a href="#" class="d-block">
                            <div class="photo-link">
                                <img src="assets/img/ajay.jpg" class="img-fluid full-wide">
                                <button>stories</button>
                            </div>
                            <h5>Things that brought Salman-Shah Rukh Khan together</h5>
                        </a>
                        <small>October 21, 2017</small>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="bb-dairies">
                        <a href="#" class="d-block">
                            <div class="photo-link">
                                <img src="assets/img/features-3.jpg" class="img-fluid full-wide">
                                <button>featured</button>
                            </div>
                            <h5>Ayushmann Khurana And The Art Of Effortless Charm</h5>
                        </a>
                        <small>October 21, 2017</small>
                    </div>
                </div>
            </div>
            <div class="button-center">
                <a href="#" class="btn btn-default">View All</a>
            </div>
        </div>
    </div>
</section>
<article>
    <div class="container">
        <div class="row">
            <div class="full-wide text-center mb-30">
                <i class="arrowsSub"><img src="assets/img/left-bar.png"></i>
                <h2 class="d-inline title text-center uppercase">NOSTALGIA</h2>
                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div class="nostalgia-border">
            <div class="border-1-grey">
                <div class="row" data-plugin="matchHeight" data-by-row="true">
                    <div class="col-md-6 col-xs-12">
                        <div class="nostalgia-list">
                            <div>
                                <img src="assets/img/nostalgia-1.jpg" alt="Nostalgia" title="Nostalgia" class="img-fluid full-wide">
                            </div>
                            <div class="card">
                                <h5>When a little Kareena would hide and watch her sister Karisma cry</h5>
                                <small>October 21, 2017</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="nostalgia-list">
                            <div>
                                <img src="assets/img/nostalgia-2.jpg" alt="Nostalgia" title="Nostalgia" class="img-fluid full-wide">
                            </div>
                            <div class="card">
                                <h5>Salman, Madhuri and Shah Rukh Khan: A casting coup that could have been ‘Pardes’</h5>
                                <small>October 21, 2017</small>
                            </div>
                        </div>
                    </div>
                    <div class="button-center">
                        <a href="#" class="btn btn-default">View All</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>
<section class="bg-light-black">
    <div class="container">
        <div class="row">
            <div class="full-wide text-center mb-30">
                <i class="arrowsSub"><img src="assets/img/left-bar.png"></i>
                <h2 class="d-inline title text-center uppercase">guest blog</h2>
                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <div class="col-md-4 col-xs-12">
                <div class="guest-blog">
                    <div class="card">
                        <a href="#"> 
                            <img src="assets/img/priyanka.jpg" class="img-fluid full-wide" alt="bollywood-life">
                            <label class="duration"></label>
                            <div class="img-celeb">
                                <img src="assets/img/Amir-celeb.jpg" class="img-fluid" alt="Amir Khan" title="Amir Khan">
                            </div>
                        </a>
                        <div class="card-footer">
                            <label>Akshay Jain</label>
                            <h5>When Bhumi Pednekar stunned us with her simple yet significant style play for Shubh Mangal </h5> 
                            <small>October 30, 2017</small> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="guest-blog">
                    <div class="card">
                        <a href="#"> 
                            <img src="assets/img/jenifer.jpg" class="img-fluid full-wide" alt="bollywood-life">
                            <label class="duration"></label>
                            <div class="img-celeb">
                                <img src="assets/img/Amir-celeb.jpg" class="img-fluid" alt="Amir Khan" title="Amir Khan">
                            </div>
                        </a>
                        <div class="card-footer">
                            <label>Akshay Jain</label>
                            <h5>Jennifer Winget gets a Brigitte Bardot makeover and it's the best house party look for the season </h5> 
                            <small>October 30, 2017</small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="guest-blog">
                    <div class="card">
                        <a href="#">
                            <img src="assets/img/alia-bolly.jpg" class="img-fluid full-wide" alt="bollywood-life">
                            <label class="duration"></label>
                            <div class="img-celeb">
                                <img src="assets/img/Amir-celeb.jpg" class="img-fluid" alt="Amir Khan" title="Amir Khan">
                            </div>
                        </a>
                        <div class="card-footer">
                            <label>Akshay Jain</label>
                            <h5>Alia Bhatt's ravishing black gown at the International Film Festival of India 2017  costs Rs 82,000 </h5> 
                            <small>October 30, 2017</small> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="button-center">
                <a href="#" class="btn btn-default">Load More</a>
            </div>
        </div>
    </div>
</section>
<?php @include 'footer.php'; ?>


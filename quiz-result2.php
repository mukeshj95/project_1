<?php @include 'header.php' ?>
<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="#">Home</a>
                <a href="photo-quiz.php" class="active">Photo Quiz</a>
            </div>
        </div>
    </div>
</nav>
<section>
    <div class="container">
        <div class="row">
            <div class="mb-30 xs-text-center">
                <h1 class="d-inline title text-center uppercase">guess who</h1>
                <i class="arrows"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="result_quiz shadow">
                    <br /><br />
                    <div class="trophy">
                        <i class="ion-thumbsup"></i>
                    </div>
                    <div class="text-center">
                        <h1>Better luck next time !</h1>
                        <label>you scored 01/05</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php @include 'footer.php' ?>

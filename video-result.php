<?php @include 'header.php' ?>
<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="#">Home</a>
                <a href="photo-quiz.php" class="active">Photo Quiz</a>
            </div>
        </div>
    </div>
</nav>
<article class="bg-quiz">
    <div class="container">
        <div class="row">
            <div class="mb-30 xs-text-center">
                <h1 class="d-inline title text-center uppercase">take a quiz</h1>
                <i class="arrows"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div class="bg-white shadow">
            <div class="quiz-content result primary">
                <h2>Result Sheet</h2>
            </div>
            <div class="col-sm-12">
                <ol class="quiz-result">
                    <li>
                        <p>Can you guess this bollywood movie from the picture?</p>
                        <div class="incorrect">
                            <label>Your answer &nbsp<span>Incorrect</span></label><br />
                            <span>Hum Kisi se kam nhi</span><i class="ion-thumbsdown"></i>
                        </div>
                        <div class="correct">
                            <label>Correct answer</label><br />
                            <span>Hum aapke hai kon</span><i class="ion-thumbsup"></i>
                        </div>
                    </li>
                    <li>
                        <p>Can you guess this bollywood movie from the picture?</p>
                        <div class="incorrect">
                            <label>Your answer &nbsp<span>Incorrect</span></label><br />
                            <span>Hum Kisi se kam nhi</span><i class="ion-thumbsdown"></i>
                        </div>
                        <div class="correct">
                            <label>Correct answer</label><br />
                            <span>Hum aapke hai kon</span><i class="ion-thumbsup"></i>
                        </div>
                    </li>
                    <li>
                        <p>Can you guess this bollywood movie from the picture?</p>
                        <div class="correct">
                            <label>Correct answer</label><br />
                            <span>Hum aapke hai kon</span><i class="ion-thumbsup"></i>
                        </div>
                    </li>
                    <li>
                        <p>Can you guess this bollywood movie from the picture?</p>
                        <div class="incorrect">
                            <label>Your answer &nbsp<span>Incorrect</span></label><br />
                            <span>Hum Kisi se kam nhi</span><i class="ion-thumbsdown"></i>
                        </div>
                        <div class="correct">
                            <label>Correct answer</label><br />
                            <span>Hum aapke hai kon</span><i class="ion-thumbsup"></i>
                        </div>
                    </li>
                    <li>
                        <p>Can you guess this bollywood movie from the picture?</p>

                        <div class="correct">
                            <label>Correct answer</label><br />
                            <span>Hum aapke hai kon</span><i class="ion-thumbsup"></i>
                        </div>
                    </li>
                </ol>
            </div>
        </div>
    </div>
</article>
<?php @include 'footer.php' ?>

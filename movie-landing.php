<?php
/**
 * Created by PhpStorm.
 * User: HIRA
 * Date: 12/6/2017
 * Time: 10:45 AM
 */
@include 'header.php';
?>
<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="index.php">Home</a>
                <a href="#" class="active">Movies</a>
            </div>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="news mb-20 mt-20 xs-text-center">
            <h1 class="d-inline title text-center uppercase">movies</h1> 
            <i class="arrows"><img src="assets/img/right-bar.png"></i>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row" data-plugin="matchHeight" data-by-row="true">
        <div class="loop owl-carousel owl-theme" id="movie-slider">
            <div class="slider-content item shadow">
                <div class="poster">
                    <a href="#"> <img src="assets/img/movie-slider.jpg" class="img-fluid"> </a>
                    <label class="duration">15:22</label>
                </div>
            </div>
            <div class="slider-content item shadow">
                <div class="poster">
                    <a href="#"> <img src="assets/img/movie-slider.jpg" class="img-fluid"> </a>
                    <label class="duration">15:22</label>
                </div>
            </div>
            <div class="slider-content item shadow">
                <div class="poster">
                    <a href="#"> <img src="assets/img/movie-slider.jpg" class="img-fluid"> </a>
                    <label class="duration">15:22</label>
                </div>
            </div>
        </div>
    </div>
</div>

<section>
    <div class="container">
        <div class="border-primary">
            <div class="text-center">
                <h2 class="rightTitle">latest movie review</h2>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <a href="#">
                            <div class="card-link h-300">
                                <div class="video-content">
                                    <img src="assets/img/movie-poster.jpg" alt="" class="img-fluid full-wide">
                                    <label class="duration">15:22</label>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="movie-detail">
                            <div class="rating mb-20">
                                <div class="star-ratings-css">
                                    <label>Bollywood Bubble Rating</label>
                                    <div class="star-ratings-sprite">
                                        <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="full-wide">
                                <h4 class="font-wt-300">Introducing the worst Bank Chor EVER</h4>
                                <small>October 21, 2017</small>
                                <p class="font-17">Champak Chandrakant Chiplunkar, a simple Marathi manoos played by Riteish Deshmukh who picks the worst day possible to rob a bank. To make matters worse, he recruits 2 idiots from Delhi who've never...</p>
                                <div class="button-center text-left">
                                    <a href="movie-review-list.php" class="btn btn-default">Read full review</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="md-col-20 xs-col-50">
                        <div class="card movie-list">
                            <a href="#" class="full-wide"> <img src="assets/img/top-movie-06.jpg" alt="Movie-list">
                                <div>
                                    <h3>Dhisoom</h3>
                                    <span>Comedy</span>
                                    <div class="star-ratings-css">
                                        <div class="star-ratings-sprite">
                                            <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="md-col-20 xs-col-50">
                        <div class="card movie-list">
                            <a href="#" class="full-wide"> <img src="assets/img/top-movie-05.jpg" alt="Movie-list">
                                <div>
                                    <h3>Noor</h3>
                                    <span>Drama</span>
                                    <div class="star-ratings-css">
                                        <div class="star-ratings-sprite">
                                            <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="md-col-20 xs-col-50">
                        <div class="card movie-list">
                            <a href="#" class="full-wide"> <img src="assets/img/top-movie-02.jpg" alt="Movie-list">
                                <div>
                                    <h3>Toilet</h3>
                                    <span>Drama</span>
                                    <div class="star-ratings-css">
                                        <div class="star-ratings-sprite">
                                            <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="md-col-20 xs-col-50">
                        <div class="card movie-list">
                            <a href="#" class="full-wide"> <img src="assets/img/top-movie-01.jpg" alt="Movie-list">
                                <div>
                                    <h3>Juduwaa 2</h3>
                                    <span>Comedy</span>
                                    <div class="star-ratings-css">
                                        <div class="star-ratings-sprite">
                                            <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="md-col-20 xs-col-50 xs-hidden">
                        <div class="card movie-list">
                            <a href="#" class="full-wide"> <img src="assets/img/top-movie-10.jpg" alt="Movie-list">
                                <div>
                                    <h3>Kahaani 2</h3>
                                    <span>Comedy</span>
                                    <div class="star-ratings-css">
                                        <div class="star-ratings-sprite">
                                            <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="bg-grey">
    <div class="full-wide text-center mb-3"> <i class="arrows"><img src="assets/img/left-bar.png"></i>
        <h2 class="d-inline title text-center uppercase">Upcoming Movies</h2> <i class="arrows"><img src="assets/img/right-bar.png"></i> </div>
    <div class="container">
        <div class="row upcoming-movie" data-plugin="matchHeight" data-by-row="true">
            <div class="col-md-12">
                <div class="owl-carousel" id="upcomingMovies2">

                    <div class="col-xs-12 item">
                        <div class="row">
                            <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-1.jpg" alt="News" class="img-fluid">
                                <div class="card-footer">
                                    <p>Qarib Qarib Singlle</p>
                                    <label>Romance/comedy</label>
                                    <h6>Releasing: 12 Nov 2017</h6> 
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12 item">
                        <div class="row">
                            <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-2.jpg" alt="News" class="img-fluid">
                                <div class="card-footer">
                                    <p>Shaadi Mein Zaroor Aana</p>
                                    <label>Romance/comedy</label>
                                    <h6>Releasing: 12 Nov 2017</h6> 
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12 item">
                        <div class="row">
                            <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-3.jpg" alt="News" class="img-fluid">
                                <div class="card-footer">
                                    <p>Tumhari Sulu</p>
                                    <label>Romance/comedy</label>
                                    <h6>Releasing: 12 Nov 2017</h6> 
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12 item ">
                        <div class="row">
                            <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-4.jpg" alt="News" class="img-fluid">
                                <div class="card-footer">
                                    <p>Manto</p>
                                    <label>Romance/comedy</label>
                                    <h6>Releasing: 12 Nov 2017</h6> 
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12 item">
                        <div class="row">
                            <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-5.jpg" alt="News" class="img-fluid">
                                <div class="card-footer">
                                    <p>The House Next Door</p>
                                    <label>Romance/comedy</label>
                                    <h6>Releasing: 12 Nov 2017</h6> 
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="button-center">
            <a href="upcoming-movies.php" class="btn btn-default">View All</a>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <div class="width-600">
                <div class="text-center border-primary full-h">
                    <h2 class="leftTitle uppercase">trailers</h2>
                    <div class="events">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 sm-mb-3">
                            <div class="card-link mb-20">
                                <div class="video-content">
                                    <img src="assets/img/most-viewed-1.jpg" class="img-fluid full-wide" alt="video">
                                    <label class="duration">15:22</label>
                                </div>
                                <h3>Raees</h3>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 sm-mb-3">
                            <div class="card-link mb-20">
                                <div class="video-content">
                                    <img src="assets/img/trailer-2.jpg" class="img-fluid full-wide" alt="video">
                                    <label class="duration">15:22</label>
                                </div>
                                <h3>Juduwaa 2</h3>
                            </div>
                        </div>
                    </div>
                    <div class="events">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 sm-mb-3">
                            <div class="card-link mb-20">
                                <div class="video-content">
                                    <img src="assets/img/most-viewed-2.jpg" class="img-fluid full-wide" alt="video">
                                    <label class="duration">15:22</label>
                                </div>
                                <h3>Kaabil</h3>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 sm-mb-3">
                            <div class="card-link mb-20">
                                <div class="video-content">
                                    <img src="assets/img/trailer-4.jpg" class="img-fluid full-wide" alt="video">
                                    <label class="duration">15:22</label>
                                </div>
                                <h3>Fan</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="right-bar-fix-width-300 top-50">
                <div class="border-primary text-center full-h">
                    <h2 class="rightTitle">trending movie</h2>
                    <div class="video-list text-left  boxOffice">
                        <a href="#" class="full-wide"> <img src="assets/img/newton-poster.jpg" alt="poster" class="img-fluid full-wide">
                            <label class="d-inline-b superHit"><small>Super hit</small></label>
                        </a>
                        <h5>Newton <span>141 Cr</span></h5>
                        <div class="rating">
                            <div class="star-ratings-css">
                                <div class="star-ratings-sprite">
                                    <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-center">
                        <a href="top-movies.php" class="btn btn-default">View All top 50</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="bg-light-black">
    <div class="container">
        <div class="row">
            <div class="mb-30 text-center full-wide">
                <i class="arrowsSub"><img src="assets/img/left-bar.png"></i>
                <h2 class="d-inline title text-center">news & updates</h2> 
                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <div class="news-gal">
                    <a href="#"> 
                        <img src="assets/img/news-update-1.jpg" class="img-fluid full-wide" alt="bollywood-life">
                        <div class="button-link">
                            <button class="font-wt-700">Gossips</button>
                        </div>
                    </a>
                    <div class="description">
                        <p class="lead">Kangana Ranaut wants ‘MENTAL’ but will Salman Khan oblige?</p> <small>October 30, 2017</small> 
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="news-gal">
                    <a href="#"> 
                        <img src="assets/img/news-update-2.jpg" class="img-fluid full-wide" alt="bollywood-life">
                        <div class="button-link">
                            <button class="font-wt-700">Gossips</button>
                        </div>
                    </a>
                    <div class="description">
                        <p class="lead">Kangana Ranaut wants ‘MENTAL’ but will Salman Khan oblige?</p> <small>October 30, 2017</small> 
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="news-gal">
                    <a href="#"> 
                        <img src="assets/img/news-update-3.jpg" class="img-fluid full-wide" alt="bollywood-life">
                        <div class="button-link">
                            <button class="font-wt-700">Gossips</button>
                        </div>
                    </a>
                    <div class="description">
                        <p class="lead">Kangana Ranaut wants ‘MENTAL’ but will Salman Khan oblige?</p> <small>October 30, 2017</small> 
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <div class="news-gal">
                    <a href="#"> 
                        <img src="assets/img/news-update-4.jpg" class="img-fluid full-wide" alt="bollywood-life">
                        <div class="button-link">
                            <button class="font-wt-700">Gossips</button>
                        </div>
                    </a>
                    <div class="description">
                        <p class="lead">Kangana Ranaut wants ‘MENTAL’ but will Salman Khan oblige?</p> <small>October 30, 2017</small> 
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="news-gal">
                    <a href="#"> 
                        <img src="assets/img/news-update-5.jpg" class="img-fluid full-wide" alt="bollywood-life">
                        <div class="button-link">
                            <button class="font-wt-700">Gossips</button>
                        </div>
                    </a>
                    <div class="description">
                        <p class="lead">Kangana Ranaut wants ‘MENTAL’ but will Salman Khan oblige?</p> <small>October 30, 2017</small> 
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="news-gal">
                    <a href="#"> 
                        <img src="assets/img/news-update-6.jpg" class="img-fluid full-wide" alt="bollywood-life">
                        <div class="button-link">
                            <button class="font-wt-700">Gossips</button>
                        </div>
                    </a>
                    <div class="description">
                        <p class="lead">Kangana Ranaut wants ‘MENTAL’ but will Salman Khan oblige?</p> <small>October 30, 2017</small> 
                    </div>
                </div>
            </div>
        </div>
        <div class="button-center">
            <a href="top-movies.php" class="btn btn-default">View All</a>
        </div>
    </div>
</section>


<?php
@include 'footer.php';
?>
<script>
    $('#movie-slider').owlCarousel({
        center: true,
        items: 2,
        loop: true,
        lazyLoad: true,
        nav: false,
        responsive: {
            600: {
                items: 2
            },
            200: {
                items: 1
            }
        }
    });
    $('#upcomingMovies2').owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        dots: false,
        //autoplay: true,
        autoplayTimeout: 2000,

        responsive: {
            100: {
                items: 1
            },
            576: {
                items: 1
            },
            769: {
                items: 3
            },
            990: {
                items: 4
            }
        }
    });
</script>


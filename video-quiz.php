<?php @include 'header.php' ?>
<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="#">Home</a>
                <a href="photo-quiz.php" class="active">Video Quiz</a>
            </div>
        </div>
    </div>
</nav>
<article class="bg-quiz">
    <div class="container">
        <div class="row">
            <div class="mb-30 xs-text-center">
                <h1 class="d-inline title text-center uppercase">take a quiz</h1>
                <i class="arrows"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div class="quiz-content primary" id="quiz2">
            <div class="row">
                <div class="col-md-7 col-sm-12">
                    <div class="border-right">
                        <a href="#">
                            <div class="video-content">
                                <img src="assets/img/quiz.jpg" alt="video-link">
                                <label class="duration">15:22</label>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-5 col-sm-12">
                    <h4>1 of 5</h4>
                    <h3>Can you guess this bollywood movie from one picture?</h3>
                    <div class="radio-group quiz">
                        <div class="md-radio inline-radio">
                            <input type="radio" class="form-control" name="photoQuiz" id="option-1" checked>
                            <label for="option-1">Dabangg</label>
                        </div>
                        <div class="md-radio inline-radio">
                            <input type="radio" class="form-control" name="photoQuiz" id="option-2">
                            <label for="option-2">Hum aapke hai kaun</label>
                        </div>
                        <div class="md-radio inline-radio">
                            <input type="radio" class="form-control" name="photoQuiz" id="option-3">
                            <label for="option-3">Tubelight</label>
                        </div>
                    </div>
                    <a href="quiz-result.php" class="btn btn-default btn-round" id="nextQuiz">Next</a>
                </div>
            </div>
        </div>
    </div>
</article>

<?php @include 'footer.php' ?>
<script>
    $('#nextQuiz').on('click', function () {
        $('#resultQuiz').show();
        $('#quiz2').hide();
    })
</script>

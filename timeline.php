<?php @include 'header.php';?>

<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="index.php">Home</a>
                <a href="timeline.php" class="active">Timeline</a>
            </div>
        </div>
    </div>
</nav>

<article>
    <div class="container">
        <div class="row">
            <div class="xs-text-center">
                <h1 class="d-inline title text-center uppercase">highlight of the day</h1>
                <i class="arrows"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
    </div>
</article>
<section class="bg-pink">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="timeline-sort">
                    <label class="pr-2 fs-18">Sort By :</label>
                    <select class="form-control">
                        <option selected> All</option>
                        <option>Stories</option>
                        <option>Photos</option>
                        <option>Videos</option>
                        <option>News</option>
                        <option>Events</option>
                    </select>
                </div>
                <ul class="timeline">
                    <h1 class="title float-right">17th January</h1>
                    <li>
                        <div class="timeline-panel shadow">
                            <p>16:52 pm</p>
                            <div class="photo-list">
                                <a href="#" class="d-block">
                                    <div class="photo-link">
                                        <img src="assets/img/highlight-1.jpg" class="img-fluid">
                                        <label for="">11</label>
                                        <button>stories</button>
                                    </div>
                                    <h3>Bigg Boss 11: Salman Khan parties hard with the contestants.</h3>
                                    <p>It was a night to remember for television actress Shilpa Shinde and all contestants of Bigg Boss 11…</p>
                                </a>
                            </div>
                        </div>
                    </li>
                    <li  class="timeline-inverted">

                        <div class="timeline-panel shadow">
                            <p>16:52 pm</p>
                            <div class="news-list">
                                <a href="#" class="d-block">
                                    <div class="photo-link">
                                        <img src="assets/img/highlight-2.jpg" class="img-fluid">
                                        <button>stories</button>
                                    </div>
                                    <h3>Akshay Kumar’s Pad Man yet to get censor nod, big traffic for certification</h3>
                                    <p>R. Balki’s eagerly-awaited Akshay Kumar starrer PadMan which is scheduled for release on January 25</p>
                                </a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-panel shadow">
                            <p>16:52 pm</p>
                            <div class="news-list">
                                <a href="#" class="d-block">
                                    <div class="photo-link">
                                        <img src="assets/img/highlight-3.jpg" class="img-fluid">
                                        <button>Events</button>
                                    </div>
                                    <h3>Kamal Haasan to pitch Vishwaroopam against Rajinikanth’s 2.0 on April 27?</h3>
                                    <p>Even as Rajinikanth and Kamal Haasan prepare for Phase 2 of their age-old rivalry by combating in…</p>
                                </a>
                            </div>
                        </div>
                    </li>
                    <li  class="timeline-inverted">
                        <div class="timeline-panel shadow">
                            <p>16:52 pm</p>
                            <div class="card-link">
                                <a href="#">
                                    <div class="video-content">
                                        <img src="assets/img/highlight-4.jpg" alt="video-link" class="img-fluid full-wide">
                                        <label class="duration">15:22</label>
                                    </div>
                                    <h3>Akshay Kumar’s Pad Man yet to get censor nod, big traffic for certification</h3>
                                    <p>Even as Rajinikanth and Kamal Haasan prepare for Phase 2 of their age-old rivalry by combating in…</p>
                                </a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-panel shadow">
                            <p>16:52 pm</p>
                            <div class="news-list">
                                <a href="#" class="d-block">
                                    <div class="photo-link">
                                        <img src="assets/img/highlight-5.jpg" class="img-fluid">
                                        <button>Events</button>
                                    </div>
                                    <h3>Pooja Hegde spotted at Sequel Bistro & Juice Bar</h3>
                                    <p>It was a night to remember for television actress Shilpa Shinde and all contestants of Bigg Boss 11…</p>
                                </a>
                            </div>
                        </div>
                    </li>
                    <li  class="timeline-inverted">
                        <div class="timeline-panel shadow">
                            <p>16:52 pm</p>
                            <div class="news-list">
                                <a href="#" class="d-block">
                                    <div class="photo-link">
                                        <img src="assets/img/highlight-6.jpg" class="img-fluid">
                                        <button>News</button>
                                    </div>
                                    <h3>Happy Birthday, Sidharth Malhotra! Here’s why we cannot stop gushing</h3>
                                    <p>Handsome, suave and sexy – Sidharth Malhotra graduated from being a Student of the Year to doing…</p>
                                </a>
                            </div>
                        </div>
                    </li>
                    <h1 class="title float-right">16th January</h1>
                    <li>
                        <div class="timeline-panel shadow">
                            <p>16:52 pm</p>
                            <div class="photo-list">
                                <a href="#" class="d-block">
                                    <div class="photo-link">
                                        <img src="assets/img/highlight-1.jpg" class="img-fluid">
                                        <label for="">11</label>
                                        <button>stories</button>
                                    </div>
                                    <h3>Bigg Boss 11: Salman Khan parties hard with the contestants.</h3>
                                    <p>It was a night to remember for television actress Shilpa Shinde and all contestants of Bigg Boss 11…</p>
                                </a>
                            </div>
                        </div>
                    </li>
                    <li  class="timeline-inverted">
                        <div class="timeline-panel shadow">
                            <p>16:52 pm</p>
                            <div class="news-list">
                                <a href="#" class="d-block">
                                    <div class="photo-link">
                                        <img src="assets/img/highlight-2.jpg" class="img-fluid">
                                        <button>stories</button>
                                    </div>
                                    <h3>Akshay Kumar’s Pad Man yet to get censor nod, big traffic for certification</h3>
                                    <p>R. Balki’s eagerly-awaited Akshay Kumar starrer PadMan which is scheduled for release on January 25</p>
                                </a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-panel shadow">
                            <p>16:52 pm</p>
                            <div class="news-list">
                                <a href="#" class="d-block">
                                    <div class="photo-link">
                                        <img src="assets/img/highlight-3.jpg" class="img-fluid">
                                        <button>Events</button>
                                    </div>
                                    <h3>Kamal Haasan to pitch Vishwaroopam against Rajinikanth’s 2.0 on April 27?</h3>
                                    <p>Even as Rajinikanth and Kamal Haasan prepare for Phase 2 of their age-old rivalry by combating in…</p>
                                </a>
                            </div>
                        </div>
                    </li>
                    <li class="clearfix" style="float: none;"></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<?php @include 'footer.php' ?>

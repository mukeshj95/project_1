<?php @include 'header.php'; ?>
<div class="shadow" id="share-article">
    <div class="container">
        <div class="row">
            <nav class="navbar navbar-inverse " id="fixTop">          
                <div class="navbar-header d-flex">
                    <div class="button  hidden-md-down">
                        <a class="btn-open" href="#"></a>
                    </div>
                    <a href="#" class="navbar-toggle" data-toggle="collapse"  onclick="openNav()">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="ion-drag"></i>
                    </a>
                    <div class="d-flex full-wide">
                        <a class="navbar-brand" href="index.php"><img src="assets/img/logo-red.png" alt="Bollywood Bubble" class="Bollywood Bubble"></a>
                        <a href="#" class="title">Wow! It's 28 million followers for Shah Rukh Khan on Twitter</a>
                        <div  class="icon-box m-auto">
                            <ul>
                                <li class="bg-fb">
                                    <a href="#">
                                        <i class="ion-social-facebook text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-twitter">
                                    <a href="#">
                                        <i class="ion-social-twitter text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-google">
                                    <a href="#">
                                        <i class="ion-social-googleplus text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-youtube">
                                    <a href="#" class="heart">
                                        <i class="ion-ios-heart-outline text-white"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div> 
<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="index.php">Home</a>
                <a href="news.php">News</a>
                <a href="bloger-detail.php" class="active xs-hidden">Wow! It's 28 million followers for Shah Rukh Khan on Twitter</a>
            </div>
        </div>
    </div>
</nav>
<article>
    <div class="container">
        <div class="row">
            <div class="width-600">
                <div class="row">
                    <div class="title">
                        <h1 class="subtitle">The SRK Interview - Part 1: 'As Long As India Makes Films, People Will Watch Shah Rukh Khan'</h1>
                    </div>
                    <div class="tags full-wide pt-2">
                        <ul>
                            <li>
                                <a href="#">Lifestyle</a>
                            </li>
                            <li>
                                <a href="#">Fashion</a>
                            </li>
                            <li>
                                <a href="#">Spotted</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-50 float-left">
                        <p class="text-muted mt-1">By <span class="primary-color">Prajakta Ajgaonkar </span>on October 27 2017<i class="ion-chatbubble-working"><span class="text-muted">12</span></i></p>
                    </div>
                    <div class="col-50 editor p-0 float-right">
                        <div class="icon-box p-0 float-right xl-hidden">
                            <ul>
                                <li class="pr-3">
                                    <i class="ion-android-share-alt fs-2"></i>
                                </li>
                            </ul>
                        </div>
                        <div class="icon-box p-0 float-right xs-hidden">
                            <ul>
                                <li class="bg-fb">
                                    <a href="#">
                                        <i class="ion-social-facebook text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-twitter">
                                    <a href="#">
                                        <i class="ion-social-twitter text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-google">
                                    <a href="#">
                                        <i class="ion-social-googleplus text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-youtube">
                                    <a href="#" class="heart">
                                        <i class="ion-ios-heart-outline text-white"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="mt-3">
                    <img src="assets/img/sarukh-blog.jpg" class="img-fluid full-wide m-auto" alt="source">
                    <h5 class="mt-2 font-wt-500">Jacqueline Fernandez at Airport</h5>
                    <hr>
                    <p class="source"><label>Image Source - Yogesh Shah</label></p>
                </div>
                <div class="interview">
                    <p>Movie promotions, brand endorsements and vacays! Bollywood celebs are always on their toes and trying to catch up with the pace of their buzzing glamour life. They have a brigade of fashion stylists at their disposal even when they are shuttling between airport terminals, so it’s not just their choice of ensembles but also travel essentials like hats, backpacks, shades and shoes that make us go ‘wowie’.</p>
                    <p>As fashion aficionados, we bring to you some of the week’s best and worst-dressed celebs whose breezy travel wear features high on our travel wish list. This week, we’ve spotted most celebrities giving preference to comfort over style; few others preferred style. While Kangana Ranaut, Kareena Kapoor Khan, Anushka Sharma. Ranveer Singh upped the much-needed travel quo, we were disappointed with Vidya Balan</p>
                </div>
                <div class="ads-toolbar">
                    <div>
                        <label>Also Read :</label>
                        <a href="#"><span class="uppercase">#ootd: </span>Bebo adds the word ‘Vulgar’ to her ah-mazing airport</a>
                    </div>
                </div>
                <div class="interview">
                    <p>Let’s see what these stars wore while jet zooming in and out of the city!</p>
                </div>
                <h2>Jacqueline Fernandez</h2>
                <div>
                    <img src="assets/img/sarukh-blog-2.jpg" class="img-fluid full-wide" alt="article-image">
                </div>
                <h5 class="mt-2 font-wt-500">Jacqueline Fernandez at Airport</h5>
                <hr>
                <p class="source"><label>Image Source - Yogesh Shah</label></p>
                <div class="interview">
                    <p>Jacqueline squeezed out some time from the ‘Judwaa 2’ promotions and joined Salman Khan for the Da Bangg tour. She was spotted at the Mumbai airport in this oversized orange tracksuit and white Hermès sneakers. We do admit that this outfit is a cross between a sleep-time pajama and fashion blunder, but does the actress give a f**k? Well, it’s got that pop of lovely bright orange colour and looks so comfy! Wouldn’t it be awesome if we all could wear our loose jammies all day long?</p>
                </div>
                <div class="twitter-post mt-3">
                    <div class="p-2">
                        <i class="ion-social-twitter p-absolute fs-2 twitter"></i> <p class="font-wt-500 d-block pl-4 fs-18">Loving the rains...& all the 28 Million of you! Thnx for the love, suggesstions, hugs & ur tenacity. Be Happy All. <a href="#" class="primary-color">pic.twitter.com/2G1P35edtc</a><br />
                            <small class="font-italic">- Shah Rukh Khan (@iamsrk) <span>September 17, 2017</span></small></p>
                    </div>
                </div>
                <hr>
                <div class="interview">
                    <p>As fashion aficionados, we bring to you some of the week’s best and worst-dressed celebs whose breezy travel wear features high on our travel wish list. This week, we’ve spotted most celebrities giving preference to comfort over style; few others preferred style. While Kangana Ranaut, Kareena Kapoor Khan, Anushka Sharma. Ranveer Singh upped the much-needed travel quo, we were disappointed with Vidya Balan, Shraddha Kapoor, Kriti Sanon and looks.</p>
                </div>
                <div class="ad-horizontal-674">
                    <a href="https://www.flipkart.com/" target=�?_blank�?>
                        <img src="assets/img/flipkart-ads.jpg" class="img-fluid" alt="Flipkart Ad">
                    </a>
                </div>
                <div class="comment-box">
                    <h2 class="fs-2 font-wt-800 uppercase">comments</h2>
                    <textarea class="form-control" placeholder="Add your comment" rows="4"></textarea>
                    <a href="#" class="btn btn-md btn-primary btn-round pl-5 pr-5">Post</a>
                    <a href="#" class="primary-color float-right underline">See All Comments</a>
                </div>
            </div>
            <div class="right-bar-fix-width-300 mt-3">
                <div class="bloger-detail shadow">
                    <div class="blog-imgae">
                        <img src="assets/img/bloger-pic.jpg" alt="Blogger" title="Mina Kumari">
                        <h3>Meena Kumari</h3>
                    </div>
                    <div class="description">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <div class="button-center">
                            <a href="#" class="btn btn-default">more stories</a>
                        </div>
                        <div class="icon-box">
                            <ul class="m-auto">
                                <li class="bg-fb">
                                    <a href="#">
                                        <i class="ion-social-facebook text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-twitter">
                                    <a href="#">
                                        <i class="ion-social-twitter text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-google">
                                    <a href="#">
                                        <i class="ion-social-googleplus text-white"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="bg-white blogs border-primary mb-40">
                    <div class="text-center">
                        <h2 class="sideTitle uppercase">M.K's Blogs</h2>
                    </div>
                    <div class="video-list">
                        <a href="#">
                            <div class="card-link">
                                <div class="video-content">
                                    <img src="assets/img/salman-big.jpg" class="img-fluid full-wide" alt="video">
                                </div>
                                <div class="video-sub">
                                    <h5>Salman Khan sings his heart out as he takes the center stage at
                                        Birmingham..</h5>
                                    <small class="text-muted">October 21, 2017</small>
                                </div>
                            </div>
                        </a>
                        <a href="#">
                            <div class="card-link">
                                <div class="video-content">
                                    <img src="assets/img/maliaka.jpg" class="img-fluid full-wide" alt="video">
                                </div>
                                <div class="video-sub">
                                    <h5>Video alert! Learn to slay the monochrome look from Malaika Arora..</h5>
                                    <small class="text-muted">October 21, 2017</small>
                                </div>
                            </div>
                        </a>
                        <a href="#">
                            <div class="card-link">
                                <div class="video-content">
                                    <img src="assets/img/bb-video-3.jpg" class="img-fluid full-wide" alt="video">
                                </div>
                                <div class="video-sub">
                                    <h5>Watch: Sonakshi, Jacqueline head home after some ‘Da Bangg’iyat in UK</h5>
                                    <small class="text-muted">October 21, 2017</small>
                                </div>
                            </div>
                        </a>
                        <a href="#">
                            <div class="card-link">
                                <div class="video-content">
                                    <img src="assets/img/bb-video-4.jpg" class="img-fluid full-wide" alt="video">
                                </div>
                                <div class="video-sub">
                                    <h5>Dialogue promo: Don’t dare to hurt Haseena Parkar’s family...</h5>
                                    <small class="text-muted">October 21, 2017</small>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="ads mb-50">
                    <a href="#">
                        <img src="assets/img/plural-ads.jpg" alt="mmt-ads" class="img-fluid">
                    </a>
                </div> 
            </div>
        </div>
    </div>
</article>
<div class="container-fluid">
    <div class="row border-icon"></div>
</div>
<section>
    <div class="container">
        <div class="row">
            <div class="width-600">
                <div class="row">
                    <div class="title">
                        <h1 class="subtitle">Virat Kohli teaches some bhangra moves to Aamir Khan</h1>
                    </div>
                    <div class="tags full-wide pt-2">
                        <ul>
                            <li>
                                <a href="#">Lifestyle</a>
                            </li>
                            <li>
                                <a href="#">Fashion</a>
                            </li>
                            <li>
                                <a href="#">Spotted</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-50 float-left">
                        <p class="text-muted mt-1">By <span class="primary-color">Prajakta Ajgaonkar </span>on October 27 2017<i class="ion-chatbubble-working"><span class="text-muted">12</span></i></p>
                    </div>
                    <div class="col-50 editor p-0 float-right">
                        <div class="icon-box p-0 float-right">
                            <ul>
                                <li class="bg-fb">
                                    <a href="#">
                                        <i class="ion-social-facebook text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-twitter">
                                    <a href="#">
                                        <i class="ion-social-twitter text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-google">
                                    <a href="#">
                                        <i class="ion-social-googleplus text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-youtube">
                                    <a href="#" class="heart">
                                        <i class="ion-ios-heart-outline text-white"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div>
                    <img src="assets/img/karan-blog.jpg" class="img-fluid full-wide" alt="Article">
                    <h5 class="mt-2">Aamir khan teaming up with Virat Kohli</h5>
                </div>
                <p class="source"><label>Image Source - Yogesh Shah</label></p>
                <div class="interview">
                    <p>This Sunday, we would get to see Mr Perfectionist of Bollywood, Aamir Khan teaming up with Mr Perfectionist of cricket, Virat Kohli for a fun session. The two stars will be seen in a fun session hosted by Aparshakti Khurrana. They recently shot for this special episode that would be aired on Diwali and their pictures too had gone viral. We got a small glimpse of this show wherein we saw Virat showing off his bhangra moves and teaching some to Aamir too.</p>
                    <p>We got a glimpse of this as the television channel uploaded a short teaser of this show. From Virat talking about his ‘love life’ to him teaching Aamir Khan some awesome moves, this Diwali with Virat and Aamir definitely seems to be entertaining.</p>
                </div>
                <div class="ads-toolbar">
                    <div>
                        <label>Also Read :</label>
                        <a href="#"><span class="uppercase">#ootd: </span>Bebo adds the word ‘Vulgar’ to her ah-mazing airport</a>
                    </div>
                </div>
                <div class="comment-box">
                    <h2 class="fs-2 font-wt-800 uppercase">comments</h2>
                    <textarea class="form-control" placeholder="Add your comment" rows="4"></textarea>
                    <a href="#" class="btn btn-md btn-primary btn-round pl-5 pr-5">Post</a>
                    <a href="#" class="primary-color float-right underline">See All Comments</a>
                </div>
            </div>
            <div class="right-bar-fix-width-300 mt-3">
                <div class="ads mb-40">
                    <a href="#">
                        <img src="assets/img/mmt-ads.jpg" alt="mmt-ads" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
@include 'footer.php';
?>


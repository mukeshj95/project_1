<?php
@include 'header.php';
?>
<div class="container-fluid single-moview-timer">
    <div class="container">
        <div id="timer" class="xs-text-center">
            <div class="title">Releasing this Week</div>
            <div class=""><span class="days"></span></div>
            <div class="smalltext">Days</div>
            <div class=""><span class="hours"></span></div>
            <div class="smalltext">Hr</div>
            <div class=""><span class="minutes"></span></div>
            <div class="smalltext">Min</div>
            <div class=""><span class="seconds"></span></div>
            <div class="smalltext">Sec</div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row" data-plugin="matchHeight" data-by-row="true">
        <!--------- toP bar ---------------->
        <div class="col-md-3 col-sm-3 col-lg-2 col-xs-12 xs-hidden">
            <p class="release">releasing this week</p>
        </div>
        <div class="col-md-6 col-sm-12 col-lg-6 col-xs-12" id="order2">
            <div class="row">
                <div class="owl-carousel" id="carousel1">
                    <div class="item">
                        <h5 class="d-inline-b">Golmal Again</h5><span>Oct 21,2017</span>
                    </div>
                    <div class="item">
                        <h5 class="d-inline-b">Tiger Jinda Hai</h5><span>Dec 21,2017</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-12 col-lg-4 col-xs-12" id="order1">
            <div id="clockdiv" class="xs-text-center">
                <div class="calendar-icon"> <span class="days"></span> </div>
                <div class="smalltext">Days</div>
                <div class="calendar-icon"> <span class="hours"></span> </div>
                <div class="smalltext">Hr</div>
                <div class="calendar-icon"> <span class="minutes"></span> </div>
                <div class="smalltext">Min</div>
                <div class="calendar-icon"> <span class="seconds"></span> </div>
                <div class="smalltext">Sec</div>
            </div>
        </div>
    </div>
</div>
<!---------- End top bar -------------->
<!---------- ads 970 width -------------->
<div class="ad-horizontal-970 pb-10">
    <a href="#">
        <img src="assets/img/ad-970-01.jpg" alt="ads" title="" >
    </a>
</div>
<!---------- end ads 970 width -------------->
<div class="container-fluid">
    <div class="row" data-plugin="matchHeight" data-by-row="true">
        <div class="owl-carousel" id="slider">
            <div class="slider-content item">
                <div class="poster">
                    <a href="#"> <img src="assets/img/slider-1.jpg" class="img-fluid"> </a> <a href="#" class="content-link">
                        box office
                    </a> </div>
                <div class="description">
                    <a href="#">
                        <p>Killer! Meet Katrina, the fiery tigress from the new still of 'Tiger Zinda Hai'</p>
                    </a> <small>October 21, 2017</small> </div>
            </div>
            <div class="slider-content item">
                <div class="poster">
                    <a href="#"> <img src="assets/img/slider-2.jpg" class="img-fluid"> </a> <a href="#" class="content-link">
                        box office
                    </a> </div>
                <div class="description">
                    <a href="#">
                        <p>Padmavati song Ghoomar out now! Deepika Padukone will make your world go round</p>
                    </a> <small>October 21, 2017</small> </div>
            </div>
            <div class="slider-content item">
                <div class="poster">
                    <a href="#"> <img src="assets/img/slider-3.jpg" class="img-fluid"> </a> <a href="#" class="content-link">
                        whats new
                    </a> </div>
                <div class="description">
                    <a href="#">
                        <p>Shah Rukh Khan celebrates his 52nd birthday today and we tell you why no can ever..</p>
                    </a> <small>October 21, 2017</small> </div>
            </div>
            <div class="slider-content item">
                <div class="poster">
                    <a href="#"> <img src="assets/img/slider-4.jpg" class="img-fluid"> </a> </div>
                <div class="description">
                    <h2>sat and sun 7 pm</h2>
                </div>
            </div>
            <div class="slider-content item">
                <div class="poster">
                    <a href="#"> <img src="assets/img/slider-5.jpg" class="img-fluid"> </a> <a href="#" class="content-link">
                        spotted
                    </a>
                </div>
                <div class="description">
                    <a href="#">
                        <p>Killer! Meet Katrina, the fiery tigress from the new still of 'Tiger Zinda Hai'</p>
                    </a> <small>October 21, 2017</small> </div>
            </div>
            <div class="slider-content item">
                <div class="poster">
                    <a href="#"> <img src="assets/img/slider-1.jpg" class="img-fluid"> </a> <a href="#" class="content-link">
                        box office
                    </a> </div>
                <div class="description">
                    <a href="#">
                        <p>Killer! Meet Katrina, the fiery tigress from the new still of 'Tiger Zinda Hai'</p>
                    </a> <small>October 21, 2017</small> </div>
            </div>
        </div>
    </div>
</div>
<!---------End SLider ------->
<!---------- ads 970 width -------------->
<div class="ad-horizontal-970 pt-30">
    <a href="#">
        <img src="assets/img/ad-970-02.jpg" alt="ads" title="" >
    </a>
</div>
<!---------- end ads 970 width -------------->
<!--------- Photos ------------>
<section>
    <!---------- ads 160 width -------------->
    <div class="ad-verticle-160 ad-left-fix home-p">
        <a href="#">
            <img src="assets/img/ad-160-01.jpg" alt="ads" title="" >
        </a>
    </div>
    <!---------- end ads 160 width -------------->

    <div class="container">
        <div class="row photos">
            <div class="full-wide text-center mb-30"> <i class="arrows"><img src="assets/img/left-bar.png"></i>
                <h1 class="d-inline title">Photos</h1> <i class="arrows"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div class="row " data-plugin="matchHeight" data-by-row="true">
            <div class="photo-list-container">
                <div class="width-600">
                    <div class="photo-list">
                        <a href="#">
                            <div class="photo-link">
                                <img src="assets/img/photos-gallery.jpg" class="img-fluid full-wide">
                                <label for="">11</label>
                                <button>spotted</button>
                            </div>
                            <h5 class="pt-2 title">Deepika, Alia, Sidharth, Karan, Farhan, Farah return from SRK's birthday bash.</h5>
                        </a>
                        <small>October 21, 2017</small>
                    </div>
                </div>
                <div class="right-bar-fix-width-300">
                    <div class="photo-list">
                        <div class="ads">
                            <img src="assets/img/mc-d-ads.jpg" class="img-fluid full-wide" alt="MacD ads">
                        </div>
                    </div>
                    <div class="photo-list">
                        <a href="#">
                            <div class="photo-link">
                                <img src="assets/img/photos-2.jpg" class="img-fluid full-wide">
                                <label for="">11</label>
                                <button>stories</button>
                            </div>
                            <h5 class="pt-2 font-22">Aishwarya Rai Bachchan has a spiritual birthday..See pics</h5>
                        </a>
                        <small>October 21, 2017</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="photo-list">
                    <a href="#">
                        <div class="photo-link">
                            <img src="assets/img/photos-3.jpg" class="img-fluid full-wide">
                            <label for="">11</label>
                            <button>guess who</button>
                        </div>
                        <h5 class="pt-2 font-22">Katrina Kaif surprises Abhishek Bachchan, 'Elvis leaves the building on seeing Boman Irani'</h5>
                    </a>
                    <small>October 21, 2017</small>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="photo-list">
                    <a href="#">
                        <div class="photo-link">
                            <img src="assets/img/photos-4.jpg" class="img-fluid full-wide">
                            <label for="">11</label>
                            <button>what's new</button>
                        </div>
                        <h5 class="pt-2 font-22">Deepika Padukone's black attire at the 3D trailer launch of Padmavati gets a thumbs down from us</h5>
                    </a>
                    <small>October 21, 2017</small>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="photo-list">
                    <a href="#" class="d-block">
                        <div class="photo-link">
                            <img src="assets/img/photos-1.jpg" class="img-fluid full-wide">
                            <label for="">11</label>
                            <button>stories</button>
                        </div>
                        <h5 class="pt-2 font-22">Kajol and other celebs at the prayer meet in memory of Rani's father..</h5>
                    </a>
                    <small>October 21, 2017</small>
                </div>
            </div>
            <div class="button-center">
                <a href="photo-landing.php" class="btn btn-default">View All</a>
            </div>
        </div>
    </div>
    <!---------- ads 160 width -------------->
    <div class="ad-verticle-160 ad-right-fix home-p">
        <a href="#">
            <img src="assets/img/ad-160-02.jpg" alt="ads" title="" >
        </a>
    </div>
    <!---------- end ads 160 width -------------->
</section>
<!----------- End Photos --------------->
<!----------- Videos ------------------>
<section class="pt-0 videos">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="full-wide text-center mb-2"> <i class="arrows"><img src="assets/img/left-bar.png"></i>
                    <h1 class="d-inline title">Videos</h1> <i class="arrows"><img src="assets/img/right-bar.png"></i> </div>
                <div class="full-wide d-inline-flex">
                    <ul class="nav nav-tabs" id="videos" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#mostPopular" role="tab" aria-controls="mostPopular">Most Popular</a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#trailers" role="tab" aria-controls="trailers">Trailers</a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#bbSpecial" role="tab" aria-controls="bbSpecial">BB Special</a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#exclusives" role="tab" aria-controls="exclusives">Exclusives</a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#events" role="tab" aria-controls="events">Events</a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#interviews" role="tab" aria-controls="interviews">Interviews</a> </li>
                    </ul>
                </div>
                <div class="videos-bg">
                    <div class="tab-content d-inline-b">
                        <div class="tab-pane active" id="mostPopular" role="tabpanel">
                            <div class="col-lg-9 col-md-12 float-left videos-bg-full">
                                <a href="#" class="video-link">
                                    <img src="assets/img/video-top2.png" class="img-fluid video-top">
                                    <div class="player">
                                        <img src="assets/img/video-play.jpg" class="img-fluid" alt="video-player" title="player">
                                    </div>
                                    <h4 class="video-title">Watch: Team 'Golmaal Again' does some golmaal on the sets of 'The Drama Company'
                                        <small class="text-muted fs-14 d-block">October 21, 2017</small>
                                    </h4>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-12 float-left">
                                <div class="grid-videos row" data-plugin="matchHeight" data-by-row="true">
                                    <div class="md-col-50">
                                        <div class="card">
                                            <a href="#">
                                                <div class="card-link">
                                                    <div class="video-content">
                                                        <img src="assets/img/video-side-link.jpg" class="img-fluid full-wide" >
                                                    </div>
                                                    <div class="video-sub">
                                                        <p>Deepika Padukone on learning folk dance for..</p>
                                                        <small class="text-muted">October 21, 2017</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="md-col-50">
                                        <div class="card">
                                            <a href="#">
                                                <div class="card-link">
                                                    <div class="video-content">
                                                        <img src="assets/img/video-side-link2.jpg" class="img-fluid full-wide" >
                                                    </div>
                                                    <div class="video-sub">
                                                        <p>Irrfan Khan & Parvathy reveal everything about Their ..</p>
                                                        <small class="text-muted">October 21, 2017</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="trailers" role="tabpanel">
                            <div class="col-lg-9 col-md-12 float-left videos-bg-full">
                                <a href="#" class="video-link">
                                    <img src="assets/img/video-top2.png" class="img-fluid video-top">
                                    <div class="player">
                                        <img src="assets/img/video-play.jpg" class="img-fluid" alt="video-player" title="player">
                                    </div>
                                    <h4 class="video-title">Watch: Team 'Golmaal Again' does some golmaal on the sets of 'The Drama Company'
                                        <small class="text-muted fs-14 d-block">October 21, 2017</small>
                                    </h4>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-12 float-left">
                                <div class="grid-videos row" data-plugin="matchHeight" data-by-row="true">
                                    <div class="md-col-50">
                                        <div class="card">
                                            <a href="#">
                                                <div class="card-link">
                                                    <div class="video-content">
                                                        <img src="assets/img/video-side-link2.jpg" class="img-fluid full-wide" >
                                                    </div>
                                                    <div class="video-sub">
                                                        <p>Irrfan Khan & Parvathy reveal everything about Their ..</p>
                                                        <small class="text-muted">October 21, 2017</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="md-col-50">
                                        <div class="card">
                                            <a href="#">
                                                <div class="card-link">
                                                    <div class="video-content">
                                                        <img src="assets/img/video-side-link.jpg" class="img-fluid full-wide" >
                                                    </div>
                                                    <div class="video-sub">
                                                        <p>Deepika Padukone on learning folk dance for..</p>
                                                        <small class="text-muted">October 21, 2017</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="bbSpecial" role="tabpanel">
                            <div class="col-lg-9 col-md-12 float-left videos-bg-full">
                                <a href="#" class="video-link">
                                    <img src="assets/img/video-top2.png" class="img-fluid video-top">
                                    <div class="player">
                                        <img src="assets/img/video-play.jpg" class="img-fluid" alt="video-player" title="player">
                                    </div>
                                    <h4 class="video-title">Watch: Team 'Golmaal Again' does some golmaal on the sets of 'The Drama Company'
                                        <small class="text-muted fs-14 d-block">October 21, 2017</small>
                                    </h4>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-12 float-left">
                                <div class="grid-videos row" data-plugin="matchHeight" data-by-row="true">
                                    <div class="md-col-50">
                                        <div class="card">
                                            <a href="#">
                                                <div class="card-link">
                                                    <div class="video-content">
                                                        <img src="assets/img/video-side-link.jpg" class="img-fluid full-wide" >
                                                    </div>
                                                    <div class="video-sub">
                                                        <p>Deepika Padukone on learning folk dance for..</p>
                                                        <small class="text-muted">October 21, 2017</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="md-col-50">
                                        <div class="card">
                                            <a href="#">
                                                <div class="card-link">
                                                    <div class="video-content">
                                                        <img src="assets/img/video-side-link2.jpg" class="img-fluid full-wide" >
                                                    </div>
                                                    <div class="video-sub">
                                                        <p>Irrfan Khan & Parvathy reveal everything about Their ..</p>
                                                        <small class="text-muted">October 21, 2017</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="exclusives" role="tabpanel">
                            <div class="col-lg-9 col-md-12 float-left videos-bg-full">
                                <a href="#" class="video-link">
                                    <img src="assets/img/video-top2.png" class="img-fluid video-top">
                                    <div class="player">
                                        <img src="assets/img/video-play.jpg" class="img-fluid" alt="video-player" title="player">
                                    </div>
                                    <h4 class="video-title">Watch: Team 'Golmaal Again' does some golmaal on the sets of 'The Drama Company'
                                        <small class="text-muted fs-14 d-block">October 21, 2017</small>
                                    </h4>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-12 float-left">
                                <div class="grid-videos row" data-plugin="matchHeight" data-by-row="true">
                                    <div class="md-col-50">
                                        <div class="card">
                                            <a href="#">
                                                <div class="card-link">
                                                    <div class="video-content">
                                                        <img src="assets/img/video-side-link2.jpg" class="img-fluid full-wide" >
                                                    </div>
                                                    <div class="video-sub">
                                                        <p>Irrfan Khan & Parvathy reveal everything about Their ..</p>
                                                        <small class="text-muted">October 21, 2017</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="md-col-50">
                                        <div class="card">
                                            <a href="#">
                                                <div class="card-link">
                                                    <div class="video-content">
                                                        <img src="assets/img/video-side-link.jpg" class="img-fluid full-wide" >
                                                    </div>
                                                    <div class="video-sub">
                                                        <p>Deepika Padukone on learning folk dance for..</p>
                                                        <small class="text-muted">October 21, 2017</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="events" role="tabpanel">
                            <div class="col-lg-9 col-md-12 float-left videos-bg-full">
                                <a href="#" class="video-link">
                                    <img src="assets/img/video-top2.png" class="img-fluid video-top">
                                    <div class="player">
                                        <img src="assets/img/video-play.jpg" class="img-fluid" alt="video-player" title="player">
                                    </div>
                                    <h4 class="video-title">Watch: Team 'Golmaal Again' does some golmaal on the sets of 'The Drama Company'
                                        <small class="text-muted fs-14 d-block">October 21, 2017</small>
                                    </h4>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-12 float-left">
                                <div class="grid-videos row" data-plugin="matchHeight" data-by-row="true">
                                    <div class="md-col-50">
                                        <div class="card">
                                            <a href="#">
                                                <div class="card-link">
                                                    <div class="video-content">
                                                        <img src="assets/img/video-side-link.jpg" class="img-fluid full-wide" >
                                                    </div>
                                                    <div class="video-sub">
                                                        <p>Deepika Padukone on learning folk dance for..</p>
                                                        <small class="text-muted">October 21, 2017</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="md-col-50">
                                        <div class="card">
                                            <a href="#">
                                                <div class="card-link">
                                                    <div class="video-content">
                                                        <img src="assets/img/video-side-link2.jpg" class="img-fluid full-wide" >
                                                    </div>
                                                    <div class="video-sub">
                                                        <p>Irrfan Khan & Parvathy reveal everything about Their ..</p>
                                                        <small class="text-muted">October 21, 2017</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="interviews" role="tabpanel">
                            <div class="col-lg-9 col-md-12 float-left videos-bg-full">
                                <a href="#" class="video-link">
                                    <img src="assets/img/video-top2.png" class="img-fluid video-top">
                                    <div class="player">
                                        <img src="assets/img/video-play.jpg" class="img-fluid" alt="video-player" title="player">
                                    </div>
                                    <h4 class="video-title">Watch: Team 'Golmaal Again' does some golmaal on the sets of 'The Drama Company'
                                        <small class="text-muted fs-14 d-block">October 21, 2017</small>
                                    </h4>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-12 float-left">
                                <div class="grid-videos row" data-plugin="matchHeight" data-by-row="true">
                                    <div class="md-col-50">
                                        <div class="card">
                                            <a href="#">
                                                <div class="card-link">
                                                    <div class="video-content">
                                                        <img src="assets/img/video-side-link2.jpg" class="img-fluid full-wide" >
                                                    </div>
                                                    <div class="video-sub">
                                                        <p>Irrfan Khan & Parvathy reveal everything about Their ..</p>
                                                        <small class="text-muted">October 21, 2017</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="md-col-50">
                                        <div class="card">
                                            <a href="#">
                                                <div class="card-link">
                                                    <div class="video-content">
                                                        <img src="assets/img/video-side-link.jpg" class="img-fluid full-wide" >
                                                    </div>
                                                    <div class="video-sub">
                                                        <p>Deepika Padukone on learning folk dance for..</p>
                                                        <small class="text-muted">October 21, 2017</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--------- end Videos -------->

<!---------- ads 970 width -------------->
<div class="ad-horizontal-970 pb-60">
    <a href="#">
        <img src="assets/img/ad-970-03.jpg" alt="ads" title="" >
    </a>
</div>
<!---------- end ads 970 width -------------->
<!--------- Bollywood life -------->
<section class="bg-pink lifestyle">
    <div class="backgroung-icon left"> </div>
    <div class="container">
        <div class="row Bollywood-life">
            <div class="full-wide text-center mb-30">
                <i class="arrows"><img src="assets/img/left-bar.png"></i>
                <h1 class="d-inline title">Bollywood Lifestyle</h1>
                <i class="arrows"><img src="assets/img/right-bar.png"></i>
            </div>
            <div class="bollywood-life bolly-wide" data-plugin="matchHeight" data-by-row="true">
                <div class="col-md-6 col-xs-12">
                    <div class="card no-border">
                        <a href="#">
                            <img src="assets/img/bolly-life.jpg" class="img-fluid full-wide" alt="bollywood-life">
                            <div class="button-link">
                                <button class="font-wt-700">fashion</button>
                            </div>
                        </a>
                        <p class="lead">5 times when Deepika Padukone proved her love for saree, seriously continue to flaunt her love for ethnic ensembles!</p> <small>October 30, 2017</small>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="card no-border">
                        <a href="#">
                            <img src="assets/img/bolly-life2.jpg"  class="img-fluid full-wide" alt="bollywood-life">
                            <div class="button-link">
                                <button class="font-wt-700">fashion</button>
                            </div>
                        </a>
                        <p class="lead">Spare us! Gauri Khan held a Halloween bash last night which was attended by the who's who of Bollywood and they were dressed in their... </p> <small>October 30, 2017</small>
                    </div>
                </div>
                <div class="button-center">
                    <a href="lifestyle.php" class="btn btn-default">View All</a>
                </div>
            </div>
        </div>
    </div>
    <div class="backgroung-icon right"> </div>
</section>
<!------- end Bollywood life ----------->
<!------ News section ---------->
<section>
    <div class="container">
        <div class="row news" data-plugin="matchHeight" data-by-row="true">
            <div class="full-wide mb-30 text-center">
                <i class="arrows"><img src="assets/img/left-bar.png"></i>
                <h1 class="d-inline title">News</h1>
                <i class="arrows"><img src="assets/img/right-bar.png"></i> </div>
            <div class="col-md-4 col-xs-12">
                <div class="news-gal xs-text-center">
                    <a href="#"> <img src="assets/img/news-1.jpg" alt="News" class="img-fluid">
                        <button class="content-link font-wt-700">featured</button>
                    </a>
                    <div class="description">
                        <p class="news-details"> CONFIRMED: Sidharth Malhotra to play Kargil martyr Captain Vikram Batra </p>
                        <small>October 21, 2017</small>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="news-gal xs-text-center">
                    <a href="#"> <img src="assets/img/news-2.jpg" alt="News" class="img-fluid">
                        <button class="content-link font-wt-700">gossips</button>
                    </a>
                    <div class="description">
                        <p class="news-details"> A drunk Sanjay Dutt did something unexpected at Karan Johar's birthday bash </p>
                        <small>October 21, 2017</small>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="news-gal xs-text-center">
                    <a href="#"> <img src="assets/img/news-3.jpg" alt="News" class="img-fluid">
                        <button class="content-link font-wt-700">nostalgia</button>
                    </a>
                    <div class="description">
                        <p class="news-details"> Did you know? Amitabh was dropped from his first film with Jaya after just 10 days of shooting </p>
                        <small>October 21, 2017</small>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="news-gal xs-text-center">
                    <a href="#"> <img src="assets/img/news-1.jpg" alt="News" class="img-fluid">
                        <button class="content-link font-wt-700">featured</button>
                    </a>
                    <div class="description">
                        <p class="news-details"> CONFIRMED: Sidharth Malhotra to play Kargil martyr Captain Vikram Batra </p>
                        <small>October 21, 2017</small>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="news-gal xs-text-center">
                    <a href="#"> <img src="assets/img/news-2.jpg" alt="News" class="img-fluid">
                        <button class="content-link font-wt-700">gossips</button>
                    </a>
                    <div class="description">
                        <p class="news-details"> A drunk Sanjay Dutt did something unexpected at Karan Johar's birthday bash </p>
                        <small>October 21, 2017</small>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="news-gal xs-text-center">
                    <a href="#"> <img src="assets/img/news-3.jpg" alt="News" class="img-fluid">
                        <button class="content-link font-wt-700">nostalgia</button>
                    </a>
                    <div class="description">
                        <p class="news-details"> Did you know? Amitabh was dropped from his first film with Jaya after just 10 days of shooting </p>
                        <small>October 21, 2017</small>
                    </div>
                </div>
            </div>
            <div class="button-center">
                <a href="news.php" class="btn btn-default">View All</a>
            </div>
        </div>
    </div>
</section>
<!-----------End News --------->
<!---------- ads 970 width -------------->
<div class="ad-horizontal-970 pb-60">
    <a href="#">
        <img src="assets/img/ad-970-04.jpg" alt="ads" title="" >
    </a>
</div>
<!---------- end ads 970 width -------------->
<!-------- Upcoming movies -------->
<section class="bg-grey upcoming">
    <div class="text-center mb-3"> <i class="arrows"><img src="assets/img/left-bar.png"></i>
        <h1 class="d-inline title">Upcoming Movies</h1> <i class="arrows"><img src="assets/img/right-bar.png"></i> </div>
    <div class="container">
        <div class="row upcoming-movie" data-plugin="matchHeight" data-by-row="true">
            <div class="col-md-12">
                <div class="owl-carousel" id="upcomingMovies">
                    <div class="col-xs-12 item">
                        <div class="row">
                            <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-1.jpg" alt="News" class="img-fluid"> </a>
                            <div class="card-footer">
                                <p>Qarib Qarib Singlle</p>
                                <label>Romance/comedy</label>
                                <h6>Releasing: 12 Nov 2017</h6> </div>
                        </div>
                    </div>
                    <div class="col-xs-12 item">
                        <div class="row">
                            <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-2.jpg" alt="News" class="img-fluid"> </a>
                            <div class="card-footer">
                                <p>Shaadi Mein Zaroor Aana</p>
                                <label>Romance/comedy</label>
                                <h6>Releasing: 12 Nov 2017</h6> </div>
                        </div>
                    </div>
                    <div class="col-xs-12 item">
                        <div class="row">
                            <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-3.jpg" alt="News" class="img-fluid"> </a>
                            <div class="card-footer">
                                <p>Tumhari Sulu</p>
                                <label>Romance/comedy</label>
                                <h6>Releasing: 12 Nov 2017</h6> </div>
                        </div>
                    </div>
                    <div class="col-xs-12 item">
                        <div class="row">
                            <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-4.jpg" alt="News" class="img-fluid"> </a>
                            <div class="card-footer">
                                <p>Manto</p>
                                <label>Romance/comedy</label>
                                <h6>Releasing: 12 Nov 2017</h6> </div>
                        </div>
                    </div>
                    <div class="col-xs-12 item">
                        <div class="row">
                            <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-5.jpg" alt="News" class="img-fluid"> </a>
                            <div class="card-footer">
                                <p>The House Next Door</p>
                                <label>Romance/comedy</label>
                                <h6>Releasing: 12 Nov 2017</h6> </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="button-center">
                <a href="upcoming-movies.php" class="btn btn-default">View All</a>
            </div>
        </div>
    </div>
</section>
<!---------- end Upcoming movies------->
<!-------- Reviews ---------->
<section>
    <div class="container">
        <div class="row">
            <div class="width-600">
                <div class="border-primary full-h">
                    <div class="text-center">
                        <h1  class="artist">movie reviews</h1>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-5 col-sm-12">
                            <div class="youtube_link">
                                <a href="https://www.youtube.com/watch?v=J_yb8HORges" target="_blank">
                                    <img src="assets/img/reviews-images.jpg" class="img-fluid full-wide">
                                </a>
                            </div>
                            <div class="text-center title xl-hidden">
                                <h2>Secret Superstar</h2>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-7 col-sm-12">
                            <div class="rating-view">
                                <div class="rating">
                                    <div class="star-ratings-css">
                                        <label>Bollywood Bubble Rating</label>
                                        <div class="star-ratings-sprite">
                                            <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                        </div>
                                    </div>
                                </div>
                                <h4 class="font-wt-400">Secret Superstar movie review: Take your mother along for this one. A must watch </h4>
                                <small>October 30, 2017</small>
                                <p class="pt-3 pb-3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. </p>
                                <!--<p class="pb-3">In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. </p>-->
                                <a href="movie-review-list.php">Read full review</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--<div class="right-bar-fix-width-300">
                <div class="text-center border-primary bg-white">
                    <div class="bg-primary">
                        <h2 class="sideTitle">calendar</h2>
                        <div class="calendar">
                            <select class="form-control calendar" readonly>
                                <option>Jan 2018</option>
                                <option>Dec 2017</option>
                                <option>Nov 2017</option>
                                <option>Oct 2017</option>
                                <option>Sep 2017</option>
                            </select>
                            <nav aria-label="Page navigation">
                                <ul class="pagination justify-content-center">
                                    <li class="page-item">
                                        <a class="page-link" href="#" aria-label="Previous">
                                            <span aria-hidden="true"><i class="ion-ios-arrow-left"></i> </span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                    </li>
                                    <li class="page-item"><a class="page-link" href="#calendar_slide">21st - 27th Sep</a></li>
                                    <li class="page-item">
                                        <a class="page-link" href="#" aria-label="Next">
                                            <span aria-hidden="true"><i class="ion-ios-arrow-right"></i> </span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div id="calendar_slide">
                        <img src="assets/img/boxoffice-4.jpg" alt="Image 1" class="item" />
                        <img src="assets/img/boxoffice-3.jpg" alt="Image 2" class="item" />
                        <img src="assets/img/boxoffice-2.jpg" alt="Image 3" class="item" />
                        <img src="assets/img/boxoffice-1.jpg" alt="Image 4" class="item" />
                        <img src="assets/img/boxoffice-4.jpg" alt="Image 5" class="item" />
                    </div>
                </div>
            </div>-->
            <!-------- Quiz ------------>
            <div class="right-bar-fix-width-300">
                <div class="text-center border-primary bg-white full-h">
                    <h1 class="artist bg-light-red uppercase quiz">bollywood quiz</h1>
                    <div class="col- quiz">
                        <div class="full-wide">
                            <img src="assets/img/quiz-img.jpg" class="img-fluid" alt="quiz">
                        </div>
                        <h5>Salman Khan, Shah Rukh Khan or Akshay Kumar  which superstar is your fave TV host?</h5>
                        <div class="radio-group quiz">
                            <div class="md-radio inline-radio">
                                <input type="radio" class="form-control" name="quiz" id="option-1">
                                <label for="option-1">Salman Khan</label>
                            </div>
                            <div class="md-radio inline-radio">
                                <input type="radio" class="form-control" name="quiz" id="option-2">
                                <label for="option-2">Shah Rukh Khan</label>
                            </div>
                            <div class="md-radio inline-radio">
                                <input type="radio" class="form-control" name="quiz" id="option-3">
                                <label for="option-3">Akshay Kumar</label>
                            </div>
                        </div>
                        <div class="inline-button sm-inline-b">
                            <a href="#" class="font-wt-500 fs-14 btn btn-default primary-color float-left">vote now</a>
                            <a href="#" class="links">view results</a>
                            <a href="#" class="links">take another quiz</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-------- End Reviews ---------->

<!---------- ads 970 width -------------->
<div class="ad-horizontal-970 pb-60">
    <a href="#">
        <img src="assets/img/ad-970-05.jpg" alt="ads" title="" >
    </a>
</div>
<!---------- end ads 970 width -------------->

<!-------- Box Office ------------>
<article class="bg-light-red">
    <div class="container">
        <div class="row">
            <div class="width-600">
                <h1 class="text-center full-wide">Box Office</h1>
                <div class="row" data-plugin="matchHeight" data-by-row="true">
                    <div class="col-md-12">
                        <div class="owl-carousel" id="boxoffice">
                            <div class="col-xs-12 item">
                                <div class="xs-fit-content boxOffice">
                                    <a href="#" class="full-wide"> <img src="assets/img/judua-poster.jpg" alt="poster">
                                        <label class="d-inline-b hit"><span><b>141 cr</b></span>Hit</label>
                                    </a>
                                    <h5 class="mt-2">Judwaa 2</h5>
                                    <span>Comedy</span>
                                    <div class="rating">
                                        <div class="star-ratings-css">
                                            <div class="star-ratings-sprite">
                                                <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 item">
                                <div class="xs-fit-content boxOffice">
                                    <a href="#" class="full-wide"> <img src="assets/img/toilet-poster.jpg" alt="poster">
                                        <label class="d-inline-b superHit"><span><b>141 cr</b></span>Super Hit</label>
                                    </a>
                                    <h5 class="mt-2">Toilet ek prem katha</h5>
                                    <span>Drama</span>
                                    <div class="rating">
                                        <div class="star-ratings-css">
                                            <div class="star-ratings-sprite">
                                                <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 item">
                                <div class="xs-fit-content boxOffice">
                                    <a href="#" class="full-wide"> <img src="assets/img/newton-poster.jpg" alt="poster">
                                        <label class="d-inline-b average"><span><b>141 cr</b></span>Average</label>
                                    </a>
                                    <h5 class="mt-2">Newton</h5>
                                    <span>Thriller</span>
                                    <div class="rating">
                                        <div class="star-ratings-css">
                                            <div class="star-ratings-sprite">
                                                <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="button-center">
                    <a href="box-office.php" class="btn btn-default">View All</a>
                </div>
            </div>
            <div class="right-bar-fix-width-300">
                <div class="text-center border-primary bg-white">
                    <h2 class="sideTitle">Artist of Month</h2>
                    <div class="artist-month"> <img src="assets/img/artist-month.jpg" alt="artist of month">
                        <p>These 9 Pictures Of Aishwarya Rai Prove She's Not Called The Most Beautiful Woman For Nothing!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>
<!-------- End Box Office ----------->


<article class="celebs">
    <div class="container-fluid">
        <div class="row">
            <div class="full-wide text-center bg-white">
                <div class="bollyTitle">
                    <i class="arrows"><img src="assets/img/left-bar.png"></i>
                    <h1 class="d-inline title text-center uppercase">bollywood celebs</h1>
                    <i class="arrows"><img src="assets/img/right-bar.png"></i>
                </div>
            </div>
            <div class="bubble-bg">
                <ul class="celeb-list">
                    <li>
                        <div class="wide"><a href="celebs.php"><img src="assets/img/celeb-01.jpg" alt=""><span>Salman Khan</span></a></div>
                        <div class="small"><a href="celebs.php"><img src="assets/img/celeb-02.jpg" alt=""><span>Ranveer Singh</span></a></div>
                        <div class="small"><a href="celebs.php"><img src="assets/img/celeb-03.jpg" alt=""><span>Priyanka Chopra</span></a></div>
                    </li>
                    <li>
                        <div class="wide"><a href="celebs.php"><img src="assets/img/celeb-04.jpg" alt=""><span>Aliya Bhatt</span></a></div>
                        <div class="wide"><a href="celebs.php"><img src="assets/img/celeb-05.jpg" alt=""><span>Ranbeer Kapoor</span></a></div>
                    </li>
                    <li>
                        <div class="small"><a href="celebs.php"><img src="assets/img/celeb-06.jpg" alt=""><span>Amir Khan</span></a></div>
                        <div class="small"><a href="celebs.php"><img src="assets/img/celeb-07.jpg" alt=""><span>Anushka Sharma</span></a></div>
                        <div class="wide"><a href="celebs.php"><img src="assets/img/celeb-08.jpg" alt=""><span>Shah Rukh Khan</span></a></div>
                    </li>
                    <li>
                        <div class="wide"><a href="celebs.php"><img src="assets/img/celeb-09.jpg" alt=""><span>Akishay Kumar</span></a></div>
                        <div class="wide"><a href="celebs.php"><img src="assets/img/celeb-10.jpg" alt=""><span>Dipeka Padukone</span></a></div>
                    </li>
                    <li>
                        <div class="small"><a href="celebs.php"><img src="assets/img/celeb-11.jpg" alt=""><span>Kangna Ranaut</span></a></div>
                        <div class="small"><a href="celebs.php"><img src="assets/img/celeb-12.jpg" alt=""><span>Katrina Kaif</span></a></div>
                        <div class="wide"><a href="celebs.php"><img src="assets/img/celeb-13.jpg" alt=""><span>Kareena Kapoor</span></a></div>
                    </li>
                </ul>
            </div>
            <div class="button-center">
                <a href="celebs-landing.php" class="btn btn-default">View All</a>
            </div>
        </div>

    </div>
</article>
<?php
@include 'footer.php';
?>
<script>
    var carousel;
    $(document).ready(function () {
        carousel = $("#calendar_slide").waterwheelCarousel({
            edgeFadeEnabled: true,
            speed: 500,
            flankingItems: 2
        });

        $("#calendar_slide").swipe({
            threshold:0,
            //Generic swipe handler for all directions
            swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
                if(direction == "left")
                    //$('#carousel-left').click();
                    carousel.next();
                else if(direction == "right")
                    carousel.prev();
            }
        });
    });
</script>
<?php @include 'header.php'; ?>
<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="#">Home</a>
                <a href="#" class="active">Upcoming Movies</a>
            </div>
        </div>
    </div>
</nav>

<!--------- movie slider -------->
<div class="container-fluid">
    <div class="row" data-plugin="matchHeight" data-by-row="true">
        <div class="loop owl-carousel owl-theme" id="upcoming-slider">
            <div class="slider-content item">
                <div class="poster">
                    <a href="#"> <img src="assets/img/upcoming-movie-1.jpg" class="img-fluid"> 
                    </a>
                    <div>
                        <h4>tumhari sulu</h4>
                        <label class="genreType">Comedy, drama</label>
                    </div>
                </div>
            </div>
            <div class="slider-content item">
                <div class="poster">
                    <a href="#"> <img src="assets/img/upcoming-movie-1.jpg" class="img-fluid"> </a>
                    <div>
                        <h4>tumhari sulu</h4>
                        <label class="genreType">Comedy, drama</label>
                    </div>
                </div>
            </div>
            <div class="slider-content item">
                <div class="poster">
                    <a href="#"> <img src="assets/img/upcoming-movie-1.jpg" class="img-fluid"> </a>
                    <div>
                        <h4>tumhari sulu</h4>
                        <label class="genreType">Comedy, drama</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="bg-light-black">
    <div class="container">
        <div class="row">       
            <div class="col-md-12">
                <div class="row">
                    <div class="mb-30 xs-text-center">
                        <h1 class="d-inline title uppercase">releasing year</h1> 
                        <i class="arrows"><img src="assets/img/right-bar.png"></i>
                    </div>
                    <div class="tabs-main" id="r-tabs">
                        <ul class="nav nav-tabs" id="" role="tablist">
                            <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#currentYear" role="tab" aria-controls="currentYear">2017</a> </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#nextYear" role="tab" aria-controls="nextYear">2018</a> </li>
                        </ul>
                    </div>
                    <div class="col-md-12">
                        <div class="tab-content releasingYear">
                            <div class="tab-pane active" id="currentYear" role="tabpanel">
                                <div class="row">
                                    <div class="col-xs-6 col-md-6 col-lg-3">
                                        <div class="row">
                                            <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-1.jpg" alt="News" class="img-fluid">
                                                <div class="card-footer">
                                                    <p>Qarib Qarib Singlle</p>
                                                    <label>Romance/comedy</label>
                                                    <h6>Releasing: 12 Nov 2017</h6> 
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6 col-lg-3">
                                        <div class="row">
                                            <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-2.jpg" alt="News" class="img-fluid">
                                                <div class="card-footer">
                                                    <p>Shaadi Mein Zaroor Aana</p>
                                                    <label>Romance/comedy</label>
                                                    <h6>Releasing: 12 Nov 2017</h6> 
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6 col-lg-3">
                                        <div class="row">
                                            <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-3.jpg" alt="News" class="img-fluid"> 
                                                <div class="card-footer">
                                                    <p>Tumhari Sulu</p>
                                                    <label>Romance/comedy</label>
                                                    <h6>Releasing: 12 Nov 2017</h6> 
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6 col-lg-3">
                                        <div class="row">
                                            <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-4.jpg" alt="News" class="img-fluid">
                                                <div class="card-footer">
                                                    <p>Manto</p>
                                                    <label>Romance/comedy</label>
                                                    <h6>Releasing: 12 Nov 2017</h6> 
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 col-md-6 col-lg-3">
                                        <div class="row">
                                            <a href="#" class="upcomingMovie"> <img src="assets/img/releasing-4.jpg" alt="News" class="img-fluid"> 
                                                <div class="card-footer">
                                                    <p>Jagga jasoos</p>
                                                    <label>Romance/comedy</label>
                                                    <h6>Releasing: 12 Nov 2017</h6> 
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6 col-lg-3">
                                        <div class="row">
                                            <a href="#" class="upcomingMovie"> <img src="assets/img/releasing-3.jpg" alt="News" class="img-fluid"> 
                                                <div class="card-footer">
                                                    <p>kahaani 2</p>
                                                    <label>Thriller</label>
                                                    <h6>Releasing: 12 Nov 2017</h6> 
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6 col-lg-3">
                                        <div class="row">
                                            <a href="#" class="upcomingMovie"> <img src="assets/img/releasing-2.jpg" alt="News" class="img-fluid">
                                                <div class="card-footer">
                                                    <p>Noor</p>
                                                    <label>Drama</label>
                                                    <h6>Releasing: 12 Nov 2017</h6> 
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6 col-lg-3">
                                        <div class="row">
                                            <a href="#" class="upcomingMovie"> <img src="assets/img/releasing-1.jpg" alt="News" class="img-fluid">
                                                <div class="card-footer">
                                                    <p>Juduaa 2</p>
                                                    <label>Romance/comedy</label>
                                                    <h6>Releasing: 12 Nov 2017</h6> 
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 col-md-6 col-lg-3">
                                        <div class="row">
                                            <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-1.jpg" alt="News" class="img-fluid"> 
                                                <div class="card-footer">
                                                    <p>Qarib Qarib Singlle</p>
                                                    <label>Romance/comedy</label>
                                                    <h6>Releasing: 12 Nov 2017</h6> 
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6 col-lg-3">
                                        <div class="row">
                                            <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-2.jpg" alt="News" class="img-fluid">
                                                <div class="card-footer">
                                                    <p>Shaadi Mein Zaroor Aana</p>
                                                    <label>Romance/comedy</label>
                                                    <h6>Releasing: 12 Nov 2017</h6> 
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6 col-lg-3">
                                        <div class="row">
                                            <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-3.jpg" alt="News" class="img-fluid">
                                                <div class="card-footer">
                                                    <p>Tumhari Sulu</p>
                                                    <label>Romance/comedy</label>
                                                    <h6>Releasing: 12 Nov 2017</h6> 
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6 col-lg-3">
                                        <div class="row">
                                            <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-4.jpg" alt="News" class="img-fluid"> 
                                                <div class="card-footer">
                                                    <p>Manto</p>
                                                    <label>Romance/comedy</label>
                                                    <h6>Releasing: 12 Nov 2017</h6> 
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="nextYear" role="tabpanel">
                                <div class="row">
                                    <div class="col-xs-6 col-md-6 col-lg-3">
                                        <div class="row">
                                            <a href="#" class="upcomingMovie"> <img src="assets/img/releasing-4.jpg" alt="News" class="img-fluid">
                                                <div class="card-footer">
                                                    <p>Jagga jasoos</p>
                                                    <label>Romance/comedy</label>
                                                    <h6>Releasing: 12 Nov 2017</h6> 
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6 col-lg-3">
                                        <div class="row">
                                            <a href="#" class="upcomingMovie"> <img src="assets/img/releasing-3.jpg" alt="News" class="img-fluid"> 
                                                <div class="card-footer">
                                                    <p>kahaani 2</p>
                                                    <label>Thriller</label>
                                                    <h6>Releasing: 12 Nov 2017</h6> 
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6 col-lg-3">
                                        <div class="row">
                                            <a href="#" class="upcomingMovie"> <img src="assets/img/releasing-2.jpg" alt="News" class="img-fluid"> 
                                                <div class="card-footer">
                                                    <p>Noor</p>
                                                    <label>Drama</label>
                                                    <h6>Releasing: 12 Nov 2017</h6> 
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6 col-lg-3">
                                        <div class="row">
                                            <a href="#" class="upcomingMovie"> <img src="assets/img/releasing-1.jpg" alt="News" class="img-fluid"> 
                                                <div class="card-footer">
                                                    <p>Juduaa 2</p>
                                                    <label>Romance/comedy</label>
                                                    <h6>Releasing: 12 Nov 2017</h6> 
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 col-md-6 col-lg-3">
                                        <div class="row">
                                            <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-1.jpg" alt="News" class="img-fluid">
                                                <div class="card-footer">
                                                    <p>Qarib Qarib Singlle</p>
                                                    <label>Romance/comedy</label>
                                                    <h6>Releasing: 12 Nov 2017</h6> 
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6 col-lg-3">
                                        <div class="row">
                                            <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-2.jpg" alt="News" class="img-fluid">
                                                <div class="card-footer">
                                                    <p>Shaadi Mein Zaroor Aana</p>
                                                    <label>Romance/comedy</label>
                                                    <h6>Releasing: 12 Nov 2017</h6> 
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6 col-lg-3">
                                        <div class="row">
                                            <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-3.jpg" alt="News" class="img-fluid">
                                                <div class="card-footer">
                                                    <p>Tumhari Sulu</p>
                                                    <label>Romance/comedy</label>
                                                    <h6>Releasing: 12 Nov 2017</h6> 
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6 col-lg-3">
                                        <div class="row">
                                            <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-4.jpg" alt="News" class="img-fluid">
                                                <div class="card-footer">
                                                    <p>Manto</p>
                                                    <label>Romance/comedy</label>
                                                    <h6>Releasing: 12 Nov 2017</h6> 
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php @include 'footer.php'; ?>
<script>
    $('#upcoming-slider').owlCarousel({
        center: true,
        items: 2,
        loop: true,
        lazyLoad: true,
        dots: false,
        responsive: {
            600: {
                items: 2
            },
            200: {
                items: 1
            }
        }
    });

</script>


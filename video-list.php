<?php
@include 'header.php';
?>

<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="#">Home</a>
                <a href="#" class="active">Videos-list</a>
            </div>
        </div>
    </div>
</nav>
<section>
    <div class="container">
        <div class="full-wide text-center mb-3">
            <i class="arrowsSub"><img src="assets/img/left-bar.png"></i>
            <h2 class="d-inline title text-center uppercase ">related videos</h2>
            <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
        </div>
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="related-vid">
                    <a href="#">
                        <div class="card-link">
                            <div class="video-content">
                                <img src="assets/img/video-link2.jpg" alt="BB Special" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <h3 class="text-left">Kriti Kharbanda's CANDID Rapid Fire On Deepika..</h3>
                            <small class="text-muted">October 21, 2017</small>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="related-vid">
                    <a href="#">
                        <div class="card-link">
                            <div class="video-content">
                                <img src="assets/img/video-link3.jpg" alt="BB Special" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <h3 class="text-left">Find out why would Farhan Akhtar wants to disco dance</h3>
                            <small class="text-muted">October 21, 2017</small>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="related-vid">
                    <a href="#">
                        <div class="card-link">
                            <div class="video-content">
                                <img src="assets/img/video-link4.jpg" alt="BB Special" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <h3 class="text-left">Check Out The Fantastic Behind The Scenes of 'Hawa Hawai 2.0' Song...</h3>
                            <small class="text-muted">October 21, 2017</small>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="related-vid">
                    <a href="#">
                        <div class="card-link">
                            <div class="video-content">
                                <img src="assets/img/video-link4.jpg" alt="BB Special" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <h3 class="text-left">Check Out The Fantastic Behind The Scenes of 'Hawa Hawai 2.0' Song...</h3>
                            <small class="text-muted">October 21, 2017</small>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="related-vid">
                    <a href="#">
                        <div class="card-link">
                            <div class="video-content">
                                <img src="assets/img/video-link3.jpg" alt="BB Special" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <h3 class="text-left">Find out why would Farhan Akhtar wants to disco dance</h3>
                            <small class="text-muted">October 21, 2017</small>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="related-vid">
                    <a href="#">
                        <div class="card-link">
                            <div class="video-content">
                                <img src="assets/img/video-link4.jpg" alt="BB Special" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <h3 class="text-left">Check Out The Fantastic Behind The Scenes of 'Hawa Hawai 2.0' Song...</h3>
                            <small class="text-muted">October 21, 2017</small>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="related-vid">
                    <a href="#">
                        <div class="card-link">
                            <div class="video-content">
                                <img src="assets/img/video-link4.jpg" alt="BB Special" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <h3 class="text-left">Check Out The Fantastic Behind The Scenes of 'Hawa Hawai 2.0' Song...</h3>
                            <small class="text-muted">October 21, 2017</small>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="related-vid">
                    <a href="#">
                        <div class="card-link">
                            <div class="video-content">
                                <img src="assets/img/vidya-video.jpg" alt="BB Special" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <h3 class="text-left">Padmavati: Deepika Padukone's 'ghoomar' act...</h3>
                            <small class="text-muted">October 21, 2017</small>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
@include 'footer.php';
?>

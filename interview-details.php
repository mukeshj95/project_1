<?php @include 'header.php'; ?>

<div class="shadow" id="share-article">
    <div class="container">
        <div class="row">
            <nav class="navbar navbar-inverse " id="fixTop">          
                <div class="navbar-header d-flex">
                    <div class="button  hidden-md-down">
                        <a class="btn-open" href="#"></a>
                    </div>
                    <a href="#" class="navbar-toggle" data-toggle="collapse"  onclick="openNav()">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="ion-drag"></i>
                    </a>
                    <div class="d-flex full-wide">
                        <a class="navbar-brand" href="index.php"><img src="assets/img/logo-red.png" alt="Bollywood Bubble" class="Bollywood Bubble"></a>
                        <a href="#" class="title">The SRK Interview - Part 1: 'As Long As India Makes Films,..'</a>
                        <div  class="icon-box m-auto">
                            <ul>
                                <li class="bg-fb">
                                    <a href="#">
                                        <i class="ion-social-facebook text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-twitter">
                                    <a href="#">
                                        <i class="ion-social-twitter text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-google">
                                    <a href="#">
                                        <i class="ion-social-googleplus text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-youtube">
                                    <a href="#" class="heart">
                                        <i class="ion-ios-heart-outline text-white"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
            </nav>
        </div>
    </div>
</div>
<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="#">Home</a>
                <a href="#">News</a>
                <a href="#">Interview</a>
                <a href="#" class="xs-hidden active">The SRK Interview - Part 1: 'As Long As India Makes Films, People Will Watch Shah Rukh Khan'</a>
            </div>
        </div>
    </div>
</nav>

<article>
    <div class="ad-verticle-160 ad-left-fix">
        <a href="#">
            <img src="assets/img/ad-160-01.jpg" alt="ads" title="">
        </a>
    </div>
    <div class="container">
        <div class="row">
            <div class="width-600">
                <div class="row">
                    <div class="title">
                        <h1 class="subtitle">The SRK Interview - Part 1: 'As Long As India Makes Films, People Will Watch Shah Rukh Khan'</h1>
                    </div>
                    <div class="tags full-wide pt-2">
                        <ul>
                            <li>
                                <a href="#">Lifestyle</a>
                            </li>
                            <li>
                                <a href="#">Fashion</a>
                            </li>
                            <li>
                                <a href="#">Spotted</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-50 float-left">
                        <p class="text-muted mt-1">By <span class="primary-color">Prajakta Ajgaonkar </span>on October 27 2017<i class="ion-chatbubble-working"><span class="text-muted">12</span></i></p>
                    </div>
                    <div class="col-50 editor p-0 float-right">
                        <div class="icon-box p-0 float-right xl-hidden">
                            <ul>
                                <li class="pr-3">
                                    <i class="ion-android-share-alt fs-2"></i>
                                </li>
                            </ul>
                        </div>
                        <div class="icon-box p-0 float-right xs-hidden">
                            <ul>
                                <li class="bg-fb">
                                    <a href="#">
                                        <i class="ion-social-facebook text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-twitter">
                                    <a href="#">
                                        <i class="ion-social-twitter text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-google">
                                    <a href="#">
                                        <i class="ion-social-googleplus text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-youtube">
                                    <a href="#" class="heart">
                                        <i class="ion-ios-heart-outline text-white"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="interview-text">
                    <p>Shah Rukh Khan has a problem – he can’t find his socks. <br/>And that’s right on top of his mind now. Never mind other pressing and personal issues, which the media insists on being privy to.</p>
                </div>
                <div>
                    <img src="assets/img/sahrukh-interview.jpg" class="img-fluid" alt="source">
                </div>
                <p class="source"><label>Image Source - Yogesh Shah</label></p>

                <div class="interview">
                    <p>Just a few days ago, some newspapers talked about a purported third child through surrogacy, yet another looking for that all elusive smidgen of exclusivity sniffed a prenatal gender determination test. The civic bodies, throwing urgent matters like filling potholes and sputtering gutters aside, jumped into action and decided to pay Shah Rukh Khan himself a visit.</p>
                    <p>Through it all, he remained unflappable only to break his silence one dripping evening. What is dubbed as a press statement in technical terms, for SRK, took a poetic form – a dignified and touching rebuttal to the rumour hawks. “Amidst all the noise that has been going around, the sweetest is the one made by our newborn baby, AbRam. He was born prematurely but has finally come home. Gauri and our family have been dealing with his health issues for a long time now...,�? he wrote adding, “His coming home puts to rest completely false and at times insensitive claims of sex determination and alleged illegalities... They say a baby is God’s opinion that the world should go on... I hope we all move on too.�? That’s Shah Rukh Khan. Zen like amidst the insensate hullabaloo.</p>
                </div>
                <div>
                    <img src="assets/img/sahrukh-interview-2.jpg" class="img-fluid" alt="source">
                </div>
                <p class="source"><label>Image Source - Yogesh Shah</label></p>
                <div class="interview">
                    <h5>Do you then feel victimised?</h5>
                    <p>No yaar. I don’t feel victimised. I can take it all. There are certain things I used to believe in. Earlier, I was natural and that’s what people liked about me. My naturalness is being curbed somewhere. Earlier when I’d be funny at the Filmfare Awards, it was considered okay. But now it is like ‘how the hell can he say that’. Maybe it’s right. Maybe after you become so popular it is not right to say such things. Like my friend Juhi (Chawla) tells me, ‘Shah Rukh now you can’t say it. People love you too much’. Though I was never discourteous but earlier if I disliked someone I’d say it. I didn’t give a damn. But now suddenly, I have to think whether it will be taken rightly. My other problem is that if you tell me I should do this, I’ll never do it. I’m often told, ‘Don’t think too much, do your shit’. That’s why I work so much so that I don’t</p>
                    <h5>Is it claustrophobic that a star’s life is governed by people’s perceptions?</h5>
                    <p>Along the way I have met enough people in my line of work who make me happy by telling me, ‘You’re wrong but so right and it’s alright to be like this’. One thing I teach my children and want everyone to know that even if you happen to be on a pedestal you should have the capacity to turn around and say, ‘I don’t know’. The most endearing part of all the directors who’ve worked with me is that they ask, ‘Shah Rukh, yeh kaise karein?’ I’ve asked Rohit Shetty while doing Chennai Express, ‘Kaise karoon yeh scene, I don’t know, tell me what to do’. We are scared to go wrong. Downfall is part of life. I’ve done films like, Maya Memsaab, Phir Bhi Dil Hai Hindustani, Darr, Baazigar... without thinking if the films would work or not. We just did our best and had fun.</p>
                </div>
                <div>
                    <img src="assets/img/sahrukh-interview-3.jpg" class="img-fluid" alt="source">
                </div>
                <p class="source"><label>Image Source - Yogesh Shah</label></p>   
                <div class="interview">
                    <h5>You get slammed for every commercial film you do...</h5>
                    <p>Whenever I meet offbeat film directors they say, ‘I’ll make a commercial hit with you’. No one offers me an offbeat film. I can do it. I am a trained actor. I was telling an actor, “If you’re an actor beyond the world of your film then you’re not an actor.�? For instance, if you are in a Karan Johar film then that’s the world. If you are in Chak De! India then that’s the world. If you aren’t able to capture the world you are acting in then how the hell you will capture the rest of the world? So I should look like the actor who was made to do Chennai Express. If people say that I play the same character over and over again, why do you watch them? I’ve completed 22 years in the industry. The first scene I shot for was when I walked down a pole and said something to Divya Bharti. That moment I realised that whatever I do has to be believable. A good actor never takes himself seriously. The ones who take themselves seriously are crap. If you have to think so much, plan so much, plot so much, where is the heart? Acting should begin from your mind and reach your heart. I believe perfection is boring. Maybe I find it boring be</p>
                </div>
                <div>
                    <img src="assets/img/sahrukh-interview-4.jpg" class="img-fluid" alt="source">
                </div>
                <p class="source"><label>Image Source - Yogesh Shah</label></p> 
                <div class="interview">
                    <h5>Do actors get judgmental because they are judged constantly?</h5>
                    <p>I am not judgmental. I get angry because of the banality. I’m irritated by the pettiness of thought. You know when I was younger, faster, cooler and newer, I had nothing to lose. Keeping it basic and natural has made me the star I am. </p>
                </div>
                <div class="interview-text">
                    <p>Downfall is part of life. I’ve done films like, Maya Memsaab, Phir Bhi Dil Hai Hindustani, Darr, Baazigar... without thinking if the films would work or not. We just did our best and had fun.</p>
                </div>
                <div class="interview">
                    <h5>Now that you are 47, is there a mid-life crisis?</h5>
                    <p>I was the first guy to say how old I am and that I dye my hair. I remember Juhi telling me not to reveal my age. But it’s being held against me that I am 40 and that I dye my hair. I celebrate the fact that I am 47 and I can romance a girl who’s half my age, jump from a building. As long as people don’t laugh at me it’s okay. I don’t know what a mid-life crisis is. I guess it means you have done it all and what’s there to do now? But I’ve done nothing. I’ve so much more to do. Now I have the resources but not enough time. But I want to win. It’s been years since I haven’t won any award. I have a speech ready since the past three years. I’m shameless about it. I lost this race to my son (Aryan) the other day. I used to make him run every day and one day he went running ahead of me, which was so unexpected. It’s good but the next time, I’ll beat him. My game has gone down. Even if I reach 50 I’ll beat him. My zest for life is such. Sometimes I use the ‘mid-life crisis’ thing for fun. (Laughs)Like when I am playing a game with </p>
                </div>
                <div class="ad-horizontal-674">
                    <a href="https://www.flipkart.com/" target="�?_blank�?">
                        <img src="assets/img/flipkart-ads.jpg" class="img-fluid" alt="Flipkart Ad">
                    </a>
                </div>
                <div class="comment-box">
                    <h2 class="fs-2 font-wt-800 uppercase">comments</h2>
                    <textarea class="form-control" placeholder="Add your comment" rows="4"></textarea>
                    <a href="#" class="btn btn-md btn-primary btn-round pl-5 pr-5">Post</a>
                    <a href="#" class="primary-color float-right underline">See All Comments</a>
                </div>
            </div>
            <div class="right-bar-fix-width-300 mt-3">
                <div class="ads mb-40">
                    <a href="#">
                        <img src="assets/img/mmt-ads.jpg" alt="mmt-ads" class="img-fluid">
                    </a>
                </div>
                <div class="bubble-tv-box">
                    <div class="text-center">
                        <h2 class="sideTitle text-center">bubble tv<i class="bubble-tv"><img src="assets/img/youtube-icon.png"></i></h2>
                    </div>
                    <a href="#">
                        <div class="card-link">
                            <div class="video-content">
                                <img src="assets/img/kalki.jpg" class="img-fluid full-wide" alt="video">
                            </div>
                            <div class="video-sub">
                                <h5>Kalki Koechlin visited Bollywood Bubble and this is how we welcomed her!</h5>
                                <small class="text-muted">October 21, 2017</small>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="bg-white border-primary mb-40">
                    <div class="text-center">
                        <h2 class="sideTitle uppercase">featured</h2>
                    </div>
                    <div class="video-list">
                        <a href="#">
                            <div class="card-link">
                                <div class="video-content">
                                    <img src="assets/img/salman-big.jpg" class="img-fluid full-wide" alt="video">
                                </div>
                                <div class="video-sub">
                                    <h5>Salman Khan sings his heart out as he takes the center stage at
                                        Birmingham..</h5>
                                    <small class="text-muted">October 21, 2017</small>
                                </div>
                            </div>
                        </a>
                        <a href="#">
                            <div class="card-link">
                                <div class="video-content">
                                    <img src="assets/img/maliaka.jpg" class="img-fluid full-wide" alt="video">
                                </div>
                                <div class="video-sub">
                                    <h5>Video alert! Learn to slay the monochrome look from Malaika Arora..</h5>
                                    <small class="text-muted">October 21, 2017</small>
                                </div>
                            </div>
                        </a>
                        <a href="#">
                            <div class="card-link">
                                <div class="video-content">
                                    <img src="assets/img/bb-video-3.jpg" class="img-fluid full-wide" alt="video">
                                </div>
                                <div class="video-sub">
                                    <h5>Watch: Sonakshi, Jacqueline head home after some ‘Da Bangg’iyat in UK</h5>
                                    <small class="text-muted">October 21, 2017</small>
                                </div>
                            </div>
                        </a>
                        <a href="#">
                            <div class="card-link">
                                <div class="video-content">
                                    <img src="assets/img/bb-video-4.jpg" class="img-fluid full-wide" alt="video">
                                </div>
                                <div class="video-sub">
                                    <h5>Dialogue promo: Don’t dare to hurt Haseena Parkar’s family...</h5>
                                    <small class="text-muted">October 21, 2017</small>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="bg-white border-primary mb-40">
                    <div class="text-center">
                        <h2 class="sideTitle uppercase">hot news</h2>
                    </div>
                    <div class="video-list">
                        <div class="photo-list">
                            <a href="#" class="d-block">
                                <div class="photo-link"> 
                                    <img src="assets/img/sahrukh.jpg" class="img-fluid full-wide" alt="video">
                                    <button>guess who</button>
                                </div>
                                <h5>Wow! It’s 28 million followers for Shah Rukh Khan on Twitter..</h5>
                            </a>
                            <small>October 21, 2017</small>
                        </div>
                        <div class="photo-list">
                            <a href="#" class="d-block">
                                <div class="photo-link"> 
                                    <img src="assets/img/deepika.jpg" class="img-fluid full-wide" alt="video">
                                    <button>fashion</button>
                                </div>
                                <h5>Padmavati poster No romance between Padmavati and Alauddin.. Khilji,</h5>
                            </a>
                            <small>October 21, 2017</small>
                        </div>
                        <div class="photo-list">
                            <a href="#" class="d-block">
                                <div class="photo-link"> 
                                    <img src="assets/img/hot-news-3.jpg" class="img-fluid full-wide" alt="video">
                                    <button>spotted</button>
                                </div>
                                <h5>Madhuri Dixit Nene all set to make her debut in a Marathi movie..</h5>
                            </a>
                            <small>October 21, 2017</small>
                        </div>
                        <div class="photo-list">
                            <a href="#" class="d-block">
                                <div class="photo-link"> 
                                    <img src="assets/img/virat.jpg" class="img-fluid full-wide" alt="video">
                                    <button>spotted</button>
                                </div>
                                <h5>Anushka Sharma joins Virat Kohli in a new ad of an ethnic wear brand. Pic Inside!</h5>
                            </a>
                            <small>October 21, 2017</small>
                        </div>
                    </div>
                </div>
                <div class="ads mb-50">
                    <a href="#">
                        <img src="assets/img/plural-ads.jpg" alt="mmt-ads" class="img-fluid">
                    </a>
                </div> 
            </div>
        </div>
    </div>
    <div class="ad-verticle-160 ad-right-fix">
        <a href="#">
            <img src="assets/img/ad-160-02.jpg" alt="ads" title="">
        </a>
    </div>
</article>
<div class="container-fluid">
    <div class="row border-icon"></div>
</div>
<section>
    <div class="container">
        <div class="row">
            <div class="width-600">
                <div class="row">
                    <div class="title">
                        <h1 class="subtitle">The SRK Interview - Part 1: 'As Long As India Makes Films, People Will Watch Shah Rukh Khan'</h1>
                    </div>
                    <div class="tags full-wide pt-2">
                        <ul>
                            <li>
                                <a href="#">Lifestyle</a>
                            </li>
                            <li>
                                <a href="#">Fashion</a>
                            </li>
                            <li>
                                <a href="#">Spotted</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-50 float-left">
                        <p class="text-muted mt-1">By <span class="primary-color">Prajakta Ajgaonkar </span>on October 27 2017<i class="ion-chatbubble-working"><span class="text-muted">12</span></i></p>
                    </div>
                    <div class="col-50 editor p-0 float-right">
                        <div class="icon-box p-0 float-right xl-hidden">
                            <ul>
                                <li class="pr-3">
                                    <i class="ion-android-share-alt fs-2"></i>
                                </li>
                            </ul>
                        </div>
                        <div class="icon-box p-0 float-right xs-hidden">
                            <ul>
                                <li class="bg-fb">
                                    <a href="#">
                                        <i class="ion-social-facebook text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-twitter">
                                    <a href="#">
                                        <i class="ion-social-twitter text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-google">
                                    <a href="#">
                                        <i class="ion-social-googleplus text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-youtube">
                                    <a href="#" class="heart">
                                        <i class="ion-ios-heart-outline text-white"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div>
                    <img src="assets/img/salman-interview.jpg" class="img-fluid" alt="source">
                </div>
                <p><label class="fs-14 float-right font-wt-500">Image Source - Yogesh Shah</label></p>
                <div class="interview">
                    <p>Celebrations are round the corner for Salman Khan’s fans — Tubelight on Friday and Eid just a few days later. In a freewheeling interview, the Supernova talks about stardom, remembering dialogues, colleagues, and why he wants to do more films.</p>
                    <h5>What’s your take on superstardom?</h5>
                    <p>Today, when they say (superstars) I think there is Aamir, Shah Rukh, me, and Akshay Kumar. We do fewer films — I do two films a year, Aamir does one film in three years, Akshay does four-five films a year. So you do the math. The man who works that hard and makes the maximum amount of money, and supports those many production companies, those many units, and those many directors every </p>
                    <h5>You don’t think you can do as many films as Akshay?</h5>
                    <p>I used to do that. I think Akki is the only one who understood that back in the day, char-paanch lakh ke liye itni jyaada mehnat kar rahe the, ab crores ke liye utni mehnat nahi kar rahe hain... When we were paid so little per film, we did more films. Now we get paid so much more for a film, and we are doing </p>
                </div>
                <div class="ads-toolbar">
                    <div>
                        <label>Also Read :</label>
                        <a href="#"><span class="uppercase">#ootd: </span>Bebo adds the word ‘Vulgar’ to her ah-mazing airport</a>
                    </div>
                </div>
                <div class="comment-box">
                    <h2 class="fs-2 font-wt-800 uppercase">comments</h2>
                    <textarea class="form-control" placeholder="Add your comment" rows="4"></textarea>
                    <a href="#" class="btn btn-md btn-primary btn-round pl-5 pr-5">Post</a>
                    <a href="#" class="primary-color float-right underline">See All Comments</a>
                </div>
            </div>
            <div class="right-bar-fix-width-300 mt-3">
                <div class="ads mb-40">
                    <a href="#">
                        <img src="assets/img/mmt-ads.jpg" alt="mmt-ads" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
@include 'footer.php';
?>

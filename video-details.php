<?php
@include 'header.php';
?>
<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="#">Home</a>
                <a href="#" class="active">Videos</a>
            </div>
        </div>
    </div>
</nav>

<!--- video player ---------->
<article class="video-bg height80">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mb-4">
                    <h1 class="uppercase title video-head">videos</h1>
                    <i class="arrows"><img src="assets/img/right-bar.png"></i>
                </div>
                <div class="height60">
                    <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 float-left mb-5">
                        <a href="https://www.youtube.com/" class="video-link">
                            <img src="assets/img/video-top-icon.png" class="img-fluid antenna">
                            <img src="assets/img/video-play.jpg" class="img-fluid" alt="video-player" title="player">
                        </a>
                        </div>
                </div>
            </div>
        </div>
    </div>
</article>
<section class="video-details">
    <div class="container">
        <div class="row">
            <div class="mt-3">
                <div class="col-md-11 col-xs-12">
                    <h4>Irrfan Khan plays a Bengali writer inspired by Humayun Ahmed in Mostofa Farooki's next</h4>
                </div>
                <div class="icon-box float-right col-md-3 col-sm-6 col-xs-12">
                    <ul>
                        <li>
                            <img src="assets/img/share-red.png" alt="share-icon" title="Share">
                        </li>
                    </ul>
                </div>
                <div class="d-inline-b col-lg-10 col-md-10 col-sm-12 col-xs-12">
                    <p class="text-muted d-inline-b">By <span class="primary-color font-wt-500">Prajakta Ajgaonkar </span>on October 27 2017
                        <i class="ion-eye p-3 font-22"><span class="fs-14 text-muted">1520</span></i>
                        <i class="ion-ios-heart-outline font-22 p-3"><span class="fs-14 text-muted">120</span></i>
                        <i class="p-3"><img src="assets/img/chat-icon.png" alt="chat"><span class="fs-14 text-muted">12</span></i></p>
                    <p class="fs-18 mt-3">Amidst international and Bollywood projects, Irrfan Khan has once again decided to take the unconventional route and has given his nod to Mostofa Farooki’s bilingual No Bed of Roses (in English, Doob in Bengali). Further what we hear is that Irrfan’s character will be inspired by
                        Bangladeshi writer, dramatist, filmmaker Humayun Ahmed.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="bg-light-red">
    <div class="container related-videos">
        <div class="full-wide text-center mb-3">
            <i class="arrowsSub"><img src="assets/img/left-bar.png"></i>
            <h2 class="d-inline title text-center uppercase">related videos</h2>
            <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
        </div>
        <div class="row mt-4" data-plugin="matchHeight" data-by-row="true">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="related-vid">
                    <a href="#">
                        <div class="card-link">
                            <div class="video-content">
                                <img src="assets/img/events1.jpg" alt="BB Special" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <h3 class="text-left">Dhadak: Janhvi and Ishaan soak in moments...</h3>
                            <small class="text-muted">October 21, 2017</small>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a href="#">
                    <div class="card-link">
                        <div class="video-content">
                            <img src="assets/img/events2.jpg" alt="BB Special" class="img-fluid full-wide">
                            <label class="duration">15:22</label>
                        </div>
                        <h3 class="text-left">5 unknown facts about Janhvi Kapoor that her Instagram...</h3>
                        <small class="text-muted">October 21, 2017</small>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="related-vid">
                    <a href="#">
                        <div class="card-link">
                            <div class="video-content">
                                <img src="assets/img/events3.jpg" alt="BB Special" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <h3 class="text-left">Padmavati: Deepika Padukone’s ‘ghoomar’ act...</h3>
                            <small class="text-muted">October 21, 2017</small>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a href="#">
                    <div class="card-link">
                        <div class="video-content">
                            <img src="assets/img/event4.jpg" alt="BB Special" class="img-fluid full-wide">
                            <label class="duration">15:22</label>
                        </div>
                        <h3 class="text-left">This movie would have been Chitrangda’s debut instead...</h3>
                        <small class="text-muted">October 21, 2017</small>
                    </div>
                </a>
            </div>
        </div>
        <div class="row mt-4" data-plugin="matchHeight" data-by-row="true">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="related-vid">
                    <a href="#">
                        <div class="card-link">
                            <div class="video-content">
                                <img src="assets/img/videos-link1.jpg" alt="BB Special" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <h3 class="text-left">Find out why would Farhan Akhtar wants to disco dance...</h3>
                            <small class="text-muted">October 21, 2017</small>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="related-vid">
                    <a href="#">
                        <div class="card-link">
                            <div class="video-content">
                                <img src="assets/img/alia-video.jpg" alt="BB Special" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <h3 class="text-left">Check Out The Fantastic Behind The Scenes of 'Hawa Hawai 2.0' Song...</h3>
                            <small class="text-muted">October 21, 2017</small>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="related-vid">
                    <a href="#">
                        <div class="card-link">
                            <div class="video-content">
                                <img src="assets/img/vidya-video.jpg" alt="BB Special" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <h3 class="text-left">Padmavati: Deepika Padukone's 'ghoomar' act...</h3>
                            <small class="text-muted">October 21, 2017</small>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="related-vid">
                    <a href="#">
                        <div class="card-link">
                            <div class="video-content">
                                <img src="assets/img/video-link2.jpg" alt="BB Special" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <h3 class="text-left">Kriti Kharbanda's CANDID Rapid Fire On Deepika..</h3>
                            <small class="text-muted">October 21, 2017</small>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="row mt-4" data-plugin="matchHeight" data-by-row="true">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="related-vid">
                    <a href="#">
                        <div class="card-link">
                            <div class="video-content">
                                <img src="assets/img/video-link3.jpg" alt="BB Special" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <h3 class="text-left">Find out why would Farhan Akhtar wants to disco dance</h3>
                            <small class="text-muted">October 21, 2017</small>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="related-vid">
                    <a href="#">
                        <div class="card-link">
                            <div class="video-content">
                                <img src="assets/img/video-link4.jpg" alt="BB Special" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <h3 class="text-left">Check Out The Fantastic Behind The Scenes of 'Hawa Hawai 2.0' Song...</h3>
                            <small class="text-muted">October 21, 2017</small>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="related-vid">
                    <a href="#">
                        <div class="card-link">
                            <div class="video-content">
                                <img src="assets/img/video-link4.jpg" alt="BB Special" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <h3 class="text-left">Check Out The Fantastic Behind The Scenes of 'Hawa Hawai 2.0' Song...</h3>
                            <small class="text-muted">October 21, 2017</small>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="related-vid">
                    <a href="#">
                        <div class="card-link">
                            <div class="video-content">
                                <img src="assets/img/vidya-video.jpg" alt="BB Special" class="img-fluid full-wide">
                                <label class="duration">15:22</label>
                            </div>
                            <h3 class="text-left">Padmavati: Deepika Padukone's 'ghoomar' act...</h3>
                            <small class="text-muted">October 21, 2017</small>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="text-center mt-5 mb-4">
            <a href="video-list.php" class="font-wt-500 btn-round text-capitalize btn-default">View more videos</a>
        </div>
    </div>
</section>

<?php
@include 'footer.php';
?>
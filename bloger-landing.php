<?php @include 'header.php'; ?>

<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="index.php">Home</a>
                <a href="bloger-landing.php" class="active">Blogs</a>
            </div>
        </div>
    </div>
</nav>
<article>
    <div class="container">
        <div class="row">
            <div class="news mb-30 xs-text-center">
                <h1 class="d-inline title">guest blogs</h1> 
                <i class="arrows"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <div class="owl-carousel" id="blog-slider">
                <div class="item">
                    <div class="glamour">
                        <div class="col-md-12 col-lg-8">
                            <div class="row">
                                <div class="glamour-container">
                                    <img src="assets/img/guest-blog-1.jpg" alt="Ed-Sheeran" title="Ed-Sheeran" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-4">
                            <div class="glamour-bg">
                                <div class="sidedescription">
                                    <img src="assets/img/bloger-pic-2.jpg" alt="Bloger" title="Meena Kumari">
                                    <p>Meena Kumari</p>
                                    <h5>Happy Birthday Dilip Kumar! Amitabh Bachchan to Riteish Deshmukh send out heartfelt wishes for the legendary actor</h5>
                                    <small>October 30, 2017</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="glamour">
                        <div class="col-md-12 col-lg-8">
                            <div class="row">
                                <div class="glamour-container">
                                    <img src="assets/img/guest-blog-1.jpg" alt="Ed-Sheeran" title="Ed-Sheeran" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-4">
                            <div class="glamour-bg">
                                <div class="sidedescription">
                                    <img src="assets/img/bloger-pic-2.jpg" alt="Bloger" title="Meena Kumari">
                                    <p>Meena Kumari</p>
                                    <h5>Happy Birthday Dilip Kumar! Amitabh Bachchan to Riteish Deshmukh send out heartfelt wishes for the legendary actor</h5>
                                    <small>October 30, 2017</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <div class="col-md-4 col-xs-12">
                <div class="guest-blog">
                    <div class="card">
                        <a href="#"> 
                            <img src="assets/img/priyanka.jpg" class="img-fluid full-wide" alt="bollywood-life">
                            <label class="duration"></label>
                            <div class="img-celeb">
                                <img src="assets/img/Amir-celeb.jpg" class="img-fluid" alt="Amir Khan" title="Amir Khan">
                            </div>
                        </a>
                        <div class="card-footer">
                            <label>Akshay Jain</label>
                            <h5>When Bhumi Pednekar stunned us with her simple yet significant... </h5> 
                            <small>October 30, 2017</small> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="guest-blog">
                    <div class="card">
                        <a href="#"> 
                            <img src="assets/img/jenifer.jpg" class="img-fluid full-wide" alt="bollywood-life">
                            <label class="duration"></label>
                            <div class="img-celeb">
                                <img src="assets/img/Amir-celeb.jpg" class="img-fluid" alt="Amir Khan" title="Amir Khan">
                            </div>
                        </a>
                        <div class="card-footer">
                            <label>Akshay Jain</label>
                            <h5>Jennifer Winget gets a Brigitte Bardot makeover and it's the best... </h5> 
                            <small>October 30, 2017</small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="guest-blog">
                    <div class="card">
                        <a href="#">
                            <img src="assets/img/alia-bolly.jpg" class="img-fluid full-wide" alt="bollywood-life">
                            <label class="duration"></label>
                            <div class="img-celeb">
                                <img src="assets/img/Amir-celeb.jpg" class="img-fluid" alt="Amir Khan" title="Amir Khan">
                            </div>
                        </a>
                        <div class="card-footer">
                            <label>Akshay Jain</label>
                            <h5>Alia Bhatt's ravishing black gown at the International Film Festival... </h5> 
                            <small>October 30, 2017</small> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <div class="col-md-4 col-xs-12">
                <div class="guest-blog">
                    <div class="card no-border">
                        <a href="#">
                            <img src="assets/img/deepika-air.jpg" class="img-fluid full-wide" alt="bollywood-life">
                            <label class="duration"></label>
                            <div class="img-celeb">
                                <img src="assets/img/Amir-celeb.jpg" class="img-fluid" alt="Amir Khan" title="Amir Khan">
                            </div>
                        </a>
                        <div class="card-footer">
                            <label>Akshay Jain</label>
                            <h5>Alia Bhatt's ravishing black gown at the International Film Festival... </h5> 
                            <small>October 30, 2017</small> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="guest-blog">
                    <div class="card no-border">
                        <a href="#">
                            <img src="assets/img/sara-ali.jpg" class="img-fluid full-wide" alt="bollywood-life">
                            <label class="duration"></label>
                            <div class="img-celeb">
                                <img src="assets/img/Amir-celeb.jpg" class="img-fluid" alt="Amir Khan" title="Amir Khan">
                            </div>
                        </a>
                        <div class="card-footer">
                            <label>Akshay Jain</label>
                            <h5>Airport style this week: Ranveer Singh, Sara Ali Khan, Kriti Sanon... </h5> 
                            <small>October 30, 2017</small> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="guest-blog">
                    <div class="card no-border">
                        <a href="#">
                            <img src="assets/img/shahid.jpg" class="img-fluid full-wide" alt="bollywood-life">
                            <label class="duration"></label>
                            <div class="img-celeb">
                                <img src="assets/img/Amir-celeb.jpg" class="img-fluid" alt="Amir Khan" title="Amir Khan">
                            </div>
                        </a>
                        <div class="card-footer">
                            <label>Akshay Jain</label>
                            <h5>Find out How Celebrities Are Rocking Their Airport Look, Casually </h5> 
                            <small>October 30, 2017</small> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="button-center">
            <a href="movie-landing.php" class="btn btn-default">Load more</a>
        </div>
    </div>
</article>
<?php @include 'footer.php'; ?>
<script>
    $('#blog-slider').owlCarousel({
        items: 1,
        loop: true,
        nav: true,
        lazyLoad: true,
        responsive: {
            600: {
                items: 1
            },
            200: {
                items: 1
            }
        }
    });
</script>


<?php
@include 'header.php';
?>
<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="#">Home</a>
                <a href="#">News</a>
                <a href="#" class="active">Editor's Blog</a>
            </div>
        </div>
    </div>
</nav>

<article class="bg-light-red">
    <div class="backgroung-icon left"> </div>
    <div class="container">
        <div class="row">
            <div class="bolly mb-30 xs-text-center">
                <h1 class="d-inline title text-center uppercase">Bollywood lifestyle</h1> 
                <i class="arrows"><img src="assets/img/right-bar.png"></i>
            </div>
            <div class="bollywood-life bolly" data-plugin="matchHeight" data-by-row="true">
                <div class="col-md-6 col-xs-12">
                    <div class="card no-border">
                        <a href="#">
                            <img src="assets/img/bollywood-life1.jpg" class="img-fluid full-wide" alt="bollywood-life">
                            <div class="button-link">
                                <button>fashion</button>
                            </div>
                        </a>
                        <p class="lead">Bride to be? Sagarika Ghatge’s shaadi outfits should be on your mind!</p> <small>October 30, 2017</small> 
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="card no-border">
                        <a href="#"> 
                            <img src="assets/img/bollywood-life2.jpg"  class="img-fluid full-wide" alt="bollywood-life">
                            <div class="button-link">
                                <button>fashion</button>
                            </div>
                        </a>
                        <p class="lead">Kareena Kapoor Khan spotted outside the gym, slays in a denim wear! </p> <small>October 30, 2017</small> 
                    </div>
                </div>
            </div>
        </div>
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <div class="bollywood-life sub-cat" data-plugin="matchHeight" data-by-row="true">
                <div class="col-lg-4 col-md-6 col-xs-12">
                    <div class="card no-border">
                        <a href="#"> 
                            <img src="assets/img/bollywood-life-3.jpg"  class="img-fluid full-wide" alt="bollywood-life">
                            <div class="button-link">
                                <button>fashion</button>
                            </div>
                        </a>
                        <p class="lead">Karisma Kapoor is crazy about this beauty trend! </p> <small>October 30, 2017</small> 
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-xs-12">
                    <div class="card no-border">
                        <a href="#"> 
                            <img src="assets/img/bollywood-life4.jpg"  class="img-fluid full-wide" alt="bollywood-life">
                            <div class="button-link">
                                <button>fashion</button>
                            </div>
                        </a>
                        <p class="lead">Papped: Karan Johar makes a STARRY splash at the airport </p> <small>October 30, 2017</small> 
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-xs-12">
                    <div class="card no-border">
                        <a href="#"> 
                            <img src="assets/img/bollywood-life5.jpg"  class="img-fluid full-wide" alt="bollywood-life">
                            <div class="button-link">
                                <button>fashion</button>
                            </div>
                        </a>
                        <p class="lead">Eyeing Bipasha Basu’s matching cape and clutch? Here’s how much... </p> <small>October 30, 2017</small> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="backgroung-icon right"> </div>
</article>
<article>
    <div class="container">
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <div class="full-wide text-center">
                <i class="arrowsSub"><img src="assets/img/left-bar.png"></i>
                <h2 class="text-center uppercase title d-inline-b">glamour
                </h2>
                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div class="glamour">   
            <div class="row">
                <div class="col-md-8 push-md-4">
                    <div class="row">
                        <div class="glamour-container">
                            <img src="assets/img/marushi-chhillar.jpg" alt="Marushi- Chhillar" title="Marushi- Chhillar" class="img-fluid full-wide">
                        </div>

                    </div>
                </div>
                <div class="col-md-4 pull-md-8">
                    <div class="glamour-bg">
                        <div class="sidedescription">
                            <h5>Need humour, brains in a man; I'm good-looking enough: Manushi Chhillar</h5>
                            <small>October 30, 2017</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="glamour">
            <div class="row" data-plugin="matchHeight" data-by-row="true">
                <div class="col-md-8 col-xs-12">
                    <div class="row">
                        <div class="glamour-container">
                            <img src="assets/img/ed-sheeran.jpg" alt="Ed-Sheeran" title="Ed-Sheeran" class="img-fluid full-wide">
                        </div>

                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="glamour-bg">
                        <div class="sidedescription">
                            <h5>All You Need To Know About Ed Sheeran’s Gig In Mumbai</h5>
                            <small>October 30, 2017</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="glamour">
            <div class="row" data-plugin="matchHeight" data-by-row="true">
                <div class="col-md-8 push-md-4">
                    <div class="row">
                        <div class="glamour-container">
                            <img src="assets/img/gauri-khan.jpg" alt="Gauri-khan" title="Gauri Khan" class="img-fluid full-wide">
                        </div>

                    </div>
                </div>
                <div class="col-md-4 pull-md-8">
                    <div class="glamour-bg">
                        <div class="sidedescription">
                            <h5>Get Ready For India’s Biggest Halloween Themed Bash Designed By Gauri Khan!</h5>
                            <small>October 30, 2017</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="glamour">
            <div class="row" data-plugin="matchHeight" data-by-row="true">
                <div class="col-md-8 push-md-0">
                    <div class="row">
                        <div class="glamour-container">
                            <img src="assets/img/justin-tendu.jpg" alt="Justin-Bieber" title="Justin-Bieber" class="img-fluid full-wide">
                        </div>
                    </div>
                </div>
                <div class="col-md-4 pull-md-0">
                    <div class="glamour-bg">
                        <div class="sidedescription">
                            <h5>Sachin Tendulkar’s Son Arjun Looks Freakishly Similar To Justin Bieber</h5>
                            <small>October 30, 2017</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="button-center">
            <a href="#" class="btn btn-default">View all glamour news</a>
        </div>
    </div>
</article>
<article class="bg-grey"> 
    <div class="container">
        <div class="row " data-plugin="matchHeight" data-by-row="true">
            <div class="col-md-4 col-xs-12">
                <div class="row full-h">
                    <div class="fashion-blog">
                        <div class="fashionTitle">
                            <h2 class="title">Fashion</h2>
                        </div>
                        <h5>33 Bollywood showstoppers from Lakmé Fashion Week winter/festive 2017</h5>
                        <small>October 30, 2017</small>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-xs-12">
                <div class="fashion">
                    <div class="card no-border no-shadow">
                        <img src="assets/img/fashion-1.jpg" alt="Fashion" title="Fashion" class="img-fluid full-wide">
                    </div>
                </div>
            </div>
            <div class="bollywood-life sub-cat fashion" data-plugin="matchHeight" data-by-row="true">
                <div class="col-md-4 col-xs-12">
                    <div class="card no-shadow no-border">
                        <a href="#"> 
                            <img src="assets/img/priyanka.jpg"  class="img-fluid full-wide" alt="bollywood-life">
                            <div class="button-link">
                                <button>fashion</button>
                            </div>
                        </a>
                        <p class="lead">When Bhumi Pednekar stunned us with her simple yet significant style play for Shubh Mangal </p> <small>October 30, 2017</small> 
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="card no-shadow no-border">
                        <a href="#"> 
                            <img src="assets/img/jenifer.jpg"  class="img-fluid full-wide" alt="bollywood-life">
                            <div class="button-link">
                                <button>fashion</button>
                            </div>
                        </a>
                        <p class="lead">Jennifer Winget gets a Brigitte Bardot makeover and it's the best house party look for the season </p> <small>October 30, 2017</small> 
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="card no-shadow no-border">
                        <a href="#"> 
                            <img src="assets/img/alia-bolly.jpg"  class="img-fluid full-wide" alt="bollywood-life">
                            <div class="button-link">
                                <button>fashion</button>
                            </div>
                        </a>
                        <p class="lead">Alia Bhatt's ravishing black gown at the International Film Festival of India 2017  costs Rs 82,000 </p> <small>October 30, 2017</small> 
                    </div>
                </div>
            </div>
        </div>
        <div class="button-center">
            <a href="#" class="btn btn-default">View all fashion news</a>
        </div>
    </div>
</article>
<article>
    <div class="container">
        <div class="row">
            <div class="full-wide text-center">
                <i class="arrowsSub"><img src="assets/img/left-bar.png"></i>
                <h2 class="text-center uppercase title d-inline-b">airport look
                </h2>
                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
            </div>
            <div class="bollywood-life sub-cat airport" data-plugin="matchHeight" data-by-row="true">
                <div class="col-md-4 col-xs-12">
                    <div class="card no-border">
                        <a href="#"> 
                            <img src="assets/img/deepika-air.jpg"  class="img-fluid full-wide" alt="bollywood-life">
                            <div class="button-link">
                                <button>fashion</button>
                            </div>
                        </a>
                        <p class="lead">12 Times Deepika Padukone Nailed the Airport Look #TravelStyle Nashmina Lakhani..</p> <small>October 30, 2017</small> 
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="card no-border">
                        <a href="#"> 
                            <img src="assets/img/sara-ali.jpg"  class="img-fluid full-wide" alt="bollywood-life">
                            <div class="button-link">
                                <button>fashion</button>
                            </div>
                        </a>
                        <p class="lead">Airport style this week: Ranveer Singh, Sara Ali Khan, Kriti Sanon, Ileana D’Cruz fly in style </p> <small>October 30, 2017</small> 
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="card no-border">
                        <a href="#"> 
                            <img src="assets/img/shahid.jpg"  class="img-fluid full-wide" alt="bollywood-life">
                            <div class="button-link">
                                <button>fashion</button>
                            </div>
                        </a>
                        <p class="lead">Find out How Celebrities Are Rocking Their Airport Look, Casually </p> <small>October 30, 2017</small> 
                    </div>
                </div>
            </div>
        </div>
        <div class="button-center">
            <a href="#" class="btn btn-default">View all airport look</a>
        </div>
    </div>
</article>


<?php
@include 'footer.php';
?>


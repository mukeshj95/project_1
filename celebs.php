<?php @include 'header.php' ?>

<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="#">Home</a>
                <a href="#" class="active">Sidharth Malhotra</a>
            </div>
        </div>
    </div>
</nav>

<div class="banner-bg">
    <!--            <img src="assets/img/cover-banner.jpg" alt="cover-pic" class="img-fluid full-wide">-->
</div>


<article>
    <div class="bubble-icons">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-1 col-lg-3 col-xs-12">
                <div class="profile">
                    <div class="profile-pic">
                        <img src="assets/img/Sidharth-pro.jpg" class="img-fluid full-wide" alt="Profile-pic">
                    </div>
                    <div class="icon-box">
                        <ul>
                            <li class="bg-fb">
                                <a href="#">
                                    <i class="ion-social-facebook text-white"></i>
                                </a>
                            </li>
                            <li class="bg-twitter">
                                <a href="#">
                                    <i class="ion-social-twitter text-white"></i>
                                </a>
                            </li>
                            <li class="bg-google">
                                <a href="#">
                                    <i class="ion-social-googleplus text-white"></i>
                                </a>
                            </li>
                            <li class="bg-youtube">
                                <a href="#" class="heart">
                                    <i class="ion-ios-heart-outline text-white"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-11 col-lg-8 col-xs-12">
                <div class="title">
                    <h1 class="subtitle">Sidharth Mahlotra</h1>
                </div>
                <div class="user-details">
                    <div class="col-md-4 col-xs-12">
                        <label>Full Name</label><span>Sidharth Mahlotra</span>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <label>Birth Place</label><span>Delhi</span>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <label>born</label><span>16 January 1985</span>
                    </div>
                </div>
                <div class="user-description">
                    <p>Sidharth Malhotra works in Bollywood Film Industry and established himself as the highest-paid actor.
                        He was made his on screen debut in Film My Name is Khan (2010) as Assistant director and His Lead role Debut Hindi Film Student of The Year (2012).   </p>
                </div>
            </div>
        </div>
    </div>
</article>
<article>
    <div class="container">
        <div class="row">
            <div class="tabs-main tabsUrl" id="r-tabs">
                <ul class="nav nav-tabs" id="" role="tablist">
                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#overView" role="tab" aria-controls="overView">Overview</a> </li>
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#videos1" role="tab" aria-controls="videos1">Videos</a> </li>
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#photos" role="tab" aria-controls="photos">Photos</a> </li>
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#buzz" role="tab" aria-controls="buzz">Buzz</a> </li>
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#filmography" role="tab" aria-controls="filmography">Filmography</a> </li>
                </ul>
            </div>
            <div class="tab-content col-md-12 p-0">
                <div class="tab-pane active" id="overView" role="tabpanel">
                    <article>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 mb-30 xs-text-center">
                                    <h2 class="d-inline title uppercase font-32">videos</h2>
                                    <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="card">
                                        <a href="#">
                                            <div class="card-link">
                                                <div class="video-content">
                                                    <img src="assets/img/video-celeb-1.jpg" alt="video-link" class="img-fluid">
                                                    <label class="duration">15:22</label>
                                                </div>
                                                <div class="video-sub">
                                                    <h5>Ittefaq Box Office Collection Day 4: Sonakshi Sinha And Sidharth Malho..</h5>
                                                    <small class="text-muted">October 21, 2017</small>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="card">
                                        <a href="#">
                                            <div class="card-link">
                                                <div class="video-content">
                                                    <img src="assets/img/video-celeb-2.jpg" alt="video-link" class="img-fluid">
                                                    <label class="duration">15:22</label>
                                                </div>
                                                <div class="video-sub">
                                                    <h5>Alia Bhatt And Sidharth Malhotra To Star In Sadak's Sequel: Reports</h5>
                                                    <small class="text-muted">October 21, 2017</small>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="card">
                                        <a href="#">
                                            <div class="card-link">
                                                <div class="video-content">
                                                    <img src="assets/img/video-celeb-3.jpg" alt="video-link" class="img-fluid">
                                                    <label class="duration">15:22</label>
                                                </div>
                                                <div class="video-sub">
                                                    <h5>Sidharth Malhotra Graduates To Bollywood's Big League</h5>
                                                    <small class="text-muted">October 21, 2017</small>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article>
                        <div class="container">
                            <div class="col-md-12 border-primary bg-white">
                                <div class="row">
                                    <div class="col-md-12 text-center mb-20">
                                        <h2 class="artist bg-light-red sideTitle uppercase font-32 mb-20">photos</h2>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="photo-list">
                                            <a href="#" class="d-block">
                                                <div class="photo-link">
                                                    <img src="assets/img/Sidharth-pic1.jpg" alt="BB Special" class="img-fluid full-wide">
                                                    <label for="" class="numbers">10</label>
                                                    <button>features</button>
                                                </div>
                                                <h3 class="text-left font-wt-300">Abhishek Bachchan saves Aishwarya Rai from an OOPS moments...</h3>
                                            </a>
                                            <small>October 21, 2017</small>
                                        </div>
                                        <div class="photo-list">
                                            <a href="#" class="d-block">
                                                <div class="photo-link">
                                                    <img src="assets/img/Sidharth-pic2.jpg" alt="BB Special" class="img-fluid full-wide">
                                                    <label for="" class="numbers">10</label>
                                                    <button>events</button>
                                                </div>
                                                <h3 class="text-left font-wt-300">Kareena adds an edge to the traditional look...</h3>
                                            </a>
                                            <small>October 21, 2017</small>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="photo-list">
                                            <a href="#" class="d-block">
                                                <div class="photo-link">
                                                    <img src="assets/img/Sidharth-pic5.jpg" alt="main-video" class="img-fluid full-wide">
                                                    <label for="" class="numbers">10</label>
                                                    <button>spotted</button>
                                                </div>
                                                <h2 class="font-wt-300">Deepika Padukone and Farah Khan spotted at the airport!</h2>
                                            </a>
                                            <small>October 21, 2017</small>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="right-side">
                                            <div class="photo-list">
                                                <a href="#" class="d-block">
                                                    <div class="photo-link">
                                                        <img src="assets/img/Sidharth-pic3.jpg" alt="BB Special" class="img-fluid full-wide">
                                                        <label for="" class="numbers">10</label>
                                                        <button>features</button>
                                                    </div>
                                                    <h3 class="text-left font-wt-300">Ranbir Kapoor, Aadar Jain get clicked during the usual...</h3>
                                                </a>
                                                <small>October 21, 2017</small>
                                            </div>
                                            <div class="photo-list">
                                                <a href="#" class="d-block">
                                                    <div class="photo-link">
                                                        <img src="assets/img/Sidharth-pic4.jpg" alt="BB Special" class="img-fluid full-wide">
                                                        <label for="" class="numbers">10</label>
                                                        <button>editor's blog</button>
                                                    </div>
                                                    <h3 class="text-left font-wt-300">Ranbir Kapoor, Aadar Jain get clicked during the usual...</h3>
                                                </a>
                                                <small>October 21, 2017</small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center full-wide mb-20">
                                        <a href="#" class="font-wt-500 btn-round text-capitalize btn-default">View All BB Specials</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 mb-20 xs-text-center">
                                    <h2 class="d-inline title uppercase font-32">filmography</h2>
                                    <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tabs-main" id="">
                                    <ul class="nav nav-tabs" id="filmoTabs" role="tablist">
                                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#latest" role="tab" aria-controls="latest">Latest</a> </li>
                                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#upcoming" role="tab" aria-controls="upcoming">Upcoming</a> </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane active" id="latest" role="tabpanel">
                                    <div class="owl-carousel" id="fimography">
                                        <div class="col-xs-12 item">
                                            <div class="filmo-g">
                                                <div class="filmo-img">
                                                    <img src="assets/img/filmography-1.jpg" alt="Movie-list">
                                                </div>
                                                <p>ittefaq</p>
                                                <p class="font-wt-300">Thriller</p>
                                                <div class="rating">
                                                    <div class="star-ratings-css float-left">
                                                        <div class="star-ratings-sprite">
                                                            <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                                        </div>
                                                    </div>
                                                    <div class="full-wide">
                                                        <a href="#">read more</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 item">
                                            <div class="filmo-g">
                                                <div class="filmo-img">
                                                    <img src="assets/img/filmography-2.jpg" alt="Movie-list">
                                                </div>
                                                <p>A Gentleman</p>
                                                <p class="font-wt-300">Comedy</p>
                                                <div class="rating">
                                                    <div class="star-ratings-css float-left">
                                                        <div class="star-ratings-sprite">
                                                            <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                                        </div>
                                                    </div>
                                                    <div class="full-wide">
                                                        <a href="#">read more</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 item">
                                            <div class="filmo-g">
                                                <div class="filmo-img">
                                                    <img src="assets/img/filmography-3.jpg" alt="Movie-list">
                                                </div>
                                                <p>Baar Baar Dekho</p>
                                                <p class="font-wt-300">Drama</p>
                                                <div class="rating">
                                                    <div class="star-ratings-css float-left">
                                                        <div class="star-ratings-sprite">
                                                            <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                                        </div>
                                                    </div>
                                                    <div class="full-wide">
                                                        <a href="#">read more</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 item">
                                            <div class="filmo-g">
                                                <div class="filmo-img">
                                                    <img src="assets/img/filmography-4.jpg" alt="Movie-list">
                                                </div>
                                                <p>Kapoor and Sons</p>
                                                <p class="font-wt-300">Drama</p>
                                                <div class="rating">
                                                    <div class="star-ratings-css float-left">
                                                        <div class="star-ratings-sprite">
                                                            <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                                        </div>
                                                    </div>
                                                    <div class="full-wide">
                                                        <a href="#">read more</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 item">
                                            <div class="filmo-g">
                                                <div class="filmo-img">
                                                    <img src="assets/img/filmography-5.jpg" alt="Movie-list">
                                                </div>
                                                <p>Brothers</p>
                                                <p class="font-wt-300">Drama</p>
                                                <div class="rating">
                                                    <div class="star-ratings-css float-left">
                                                        <div class="star-ratings-sprite">
                                                            <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                                        </div>
                                                    </div>
                                                    <div class="full-wide">
                                                        <a href="#">read more</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="upcoming" role="tabpanel">
                                    <div class="owl-carousel" id="upcoming-movie">
                                        <div class="col-xs-12 item">
                                            <div class="filmo-g">
                                                <div class="filmo-img">
                                                    <img src="assets/img/filmography-1.jpg" alt="Movie-list">
                                                </div>
                                                <p>ittefaq</p>
                                                <p class="font-wt-300">Drama</p>
                                                <div class="rating">
                                                    <div class="star-ratings-css float-left">
                                                        <div class="star-ratings-sprite">
                                                            <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                                        </div>
                                                    </div>
                                                    <div class="full-wide">
                                                        <a href="#">read more</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 item">
                                            <div class="filmo-g">
                                                <div class="filmo-img">
                                                    <img src="assets/img/filmography-2.jpg" alt="Movie-list">
                                                </div>
                                                <p>A Gentleman</p>
                                                <p class="font-wt-300">Drama</p>
                                                <div class="rating">
                                                    <div class="star-ratings-css float-left">
                                                        <div class="star-ratings-sprite">
                                                            <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                                        </div>
                                                    </div>
                                                    <div class="full-wide">
                                                        <a href="#">read more</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 xs-center">
                                    <i class="arrowsSub xl-hidden"><img src="assets/img/left-bar.png"></i>
                                    <h2 class="d-inline title uppercase font-32">buzz</h2>
                                    <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <div class="buzz-box">
                                        <a href="#">
                                            <div class="photo-link">
                                                <img src="assets/img/buzz-1.jpg" alt="BB Special" class="img-fluid full-wide">
                                                <label for="" class="numbers">10</label>
                                                <button>features</button>
                                            </div>
                                            <div class="details">
                                                <h5>A Gentleman Movie Review: Strong action loses its way amid ill-conceived plot
                                                </h5>
                                                <small class="font-light font-wt-400">October 21, 2017</small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="buzz-box">
                                        <a href="#">
                                            <div class="photo-link">
                                                <img src="assets/img/buzz-2.jpg" alt="BB Special" class="img-fluid full-wide">
                                                <label for="" class="numbers">10</label>
                                                <button>features</button>
                                            </div>
                                            <div class="details">
                                                <h5>Sayani Gupta is all praise for Sidharth Malhotra and Katrina Kaif
                                                </h5>
                                                <small class="font-light font-wt-400">October 21, 2017</small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="buzz-box">
                                        <a href="#">
                                            <div class="photo-link">
                                                <img src="assets/img/buzz-3.jpg" alt="BB Special" class="img-fluid full-wide">
                                                <label for="" class="numbers">10</label>
                                                <button>features</button>
                                            </div>
                                            <div class="details">
                                                <h5>Alia Bhatt ? Sidharth Malhotra romancing in Coonoor
                                                </h5>
                                                <small class="font-light font-wt-400">October 21, 2017</small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="buzz-box">
                                        <a href="#">
                                            <div class="photo-link">
                                                <img src="assets/img/buzz-4.jpg" alt="BB Special" class="img-fluid full-wide">
                                                <label for="" class="numbers">10</label>
                                                <button>blog</button>
                                            </div>
                                            <div class="details">
                                                <h5>Sidharth Malhotra?s dapper look is making us skip our heartbeat!
                                                </h5>
                                                <small class="font-light font-wt-400">October 21, 2017</small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="buzz-box">
                                        <a href="#">
                                            <div class="photo-link">
                                                <img src="assets/img/buzz-1.jpg" alt="BB Special" class="img-fluid full-wide">
                                                <label for="" class="numbers">10</label>
                                                <button>features</button>
                                            </div>
                                            <div class="details">
                                                <h5>A Gentleman Movie Review: Strong action loses its way amid ill-conceived plot
                                                </h5>
                                                <small class="font-light font-wt-400">October 21, 2017</small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="buzz-box">
                                        <a href="#">
                                            <div class="photo-link">
                                                <img src="assets/img/buzz-2.jpg" alt="BB Special" class="img-fluid full-wide">
                                                <label for="" class="numbers">10</label>
                                                <button>features</button>
                                            </div>
                                            <div class="details">
                                                <h5>Sayani Gupta is all praise for Sidharth Malhotra and Katrina Kaif
                                                </h5>
                                                <small class="font-light font-wt-400">October 21, 2017</small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="buzz-box">
                                        <a href="#">
                                            <div class="photo-link">
                                                <img src="assets/img/buzz-3.jpg" alt="BB Special" class="img-fluid full-wide">
                                                <label for="" class="numbers">10</label>
                                                <button>features</button>
                                            </div>
                                            <div class="details">
                                                <h5>Alia Bhatt ? Sidharth Malhotra romancing in Coonoor
                                                </h5>
                                                <small class="font-light font-wt-400">October 21, 2017</small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="buzz-box">
                                        <a href="#">
                                            <div class="photo-link">
                                                <img src="assets/img/buzz-4.jpg" alt="BB Special" class="img-fluid full-wide">
                                                <label for="" class="numbers">10</label>
                                                <button>features</button>
                                            </div>
                                            <div class="details">
                                                <h5>Sidharth Malhotra?s dapper look is making us skip our heartbeat!
                                                </h5>
                                                <small class="font-light font-wt-400">October 21, 2017</small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="button-center">
                                <a href="#" class="btn btn-default">load more</a>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="tab-pane" id="videos1" role="tabpanel">
                    <article>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-4 text-center">
                                        <i class="arrows"><img src="assets/img/left-bar.png"></i>
                                        <h1 class="uppercase title d-inline-b">videos</h1>
                                        <i class="arrows"><img src="assets/img/right-bar.png"></i>
                                    </div>
                                    <div class="height60">
                                        <div class="col-md-10 col-sm-12 float-left">
                                            <a href="https://www.youtube.com/" class="video-link">
                                                <img src="assets/img/video-top2.png" class="img-fluid antenna">
                                                <img src="assets/img/video-play.jpg" class="img-fluid" alt="video-player" title="player">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <section class="video-details">
                        <div class="container">
                            <div class="row">
                                <div class="mt-3">
                                    <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12">
                                        <h4>Irrfan Khan plays a Bengali writer inspired by Humayun Ahmed in Mostofa Farooki’s next</h4>
                                    </div>
                                    <div class="tags col-lg-9 col-md-9 col-sm-6 col-xs-12 float-left">
                                        <ul class="d-block">
                                            <li class="float-left mr-2">
                                                <a href="#" class="font-wt-500 p-1 primary-color d-inline-b bg-light-red fs-12">Lifestyle</a>
                                            </li>
                                            <li class="float-left mr-2">
                                                <a href="#" class="font-wt-500 p-1 primary-color d-inline-b bg-light-red fs-12">Fashion</a>
                                            </li>
                                            <li class="float-left mr-2">
                                                <a href="#" class="font-wt-500 p-1 primary-color d-inline-b bg-light-red fs-12">Spotted</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="share-buttons">
                                        <div class="icon-box float-right">
                                            <i class="ion-android-share-alt fs-2"></i>
                                        </div>
                                    </div>
                                    <div class="d-inline-b col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                        <p class="text-muted d-inline-b">By <span class="primary-color font-wt-500">Prajakta Ajgaonkar </span>on October 27 2017
                                            <i class="ion-eye p-3 font-22"><span class="fs-14 text-muted">1520</span></i>
                                            <i class="ion-ios-heart-outline font-22 p-3"><span class="fs-14 text-muted">120</span></i>
                                            <i class="ion-chatbubble-working font-22 p-3"><span class="fs-14 text-muted">12</span></i></p>
                                        <p class="fs-18 mt-3">Amidst international and Bollywood projects, Irrfan Khan has once again decided to take the unconventional route and has given his nod to Mostofa Farooki’s bilingual No Bed of Roses (in English, Doob in Bengali). Further what we hear is that Irrfan’s character will be inspired by
                                            Bangladeshi writer, dramatist, filmmaker Humayun Ahmed.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <article>
                        <div class="container">
                            <div class="full-wide text-center mb-3">
                                <i class="arrowsSub"><img src="assets/img/left-bar.png"></i>
                                <h2 class="d-inline title text-center uppercase">related videos</h2>
                                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row" data-plugin="matchHeight" data-by-row="true">
                                <div class="col-md-4 col-xs-12">
                                    <div class="card">
                                        <a href="#">
                                            <div class="card-link">
                                                <div class="video-content">
                                                    <img src="assets/img/video-celeb-1.jpg" alt="video-link" class="img-fluid full-wide">
                                                    <label class="duration">15:22</label>
                                                </div>
                                                <div class="video-sub">
                                                    <h5>Ittefaq Box Office Collection Day 4: Sonakshi Sinha And Sidharth Malhotra's...</h5>
                                                    <small class="text-muted">October 21, 2017</small>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="card">
                                        <a href="#">
                                            <div class="card-link">
                                                <div class="video-content">
                                                    <img src="assets/img/video-celeb-2.jpg" alt="video-link" class="img-fluid full-wide">
                                                    <label class="duration">15:22</label>
                                                </div>
                                                <div class="video-sub">
                                                    <h5>Alia Bhatt And Sidharth Malhotra To Star In Sadak's Sequel: Reports</h5>
                                                    <small class="text-muted">October 21, 2017</small>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="card">
                                        <a href="#">
                                            <div class="card-link">
                                                <div class="video-content">
                                                    <img src="assets/img/video-celeb-3.jpg" alt="video-link" class="img-fluid full-wide">
                                                    <label class="duration">15:22</label>
                                                </div>
                                                <div class="video-sub">
                                                    <h5>Sidharth Malhotra Graduates To Bollywood's Big League</h5>
                                                    <small class="text-muted">October 21, 2017</small>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>

                </div>
                <div class="tab-pane" id="photos" role="tabpanel">
                    <article>
                        <div class="container">
                            <div class="col-md-12 border-primary bg-white">
                                <div class="row">
                                    <div class="col-md-12 text-center mb-20">
                                        <h2 class="artist bg-light-red sideTitle uppercase font-32 mb-20">photos</h2>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="photo-list">
                                            <a href="#" class="d-block">
                                                <div class="photo-link">
                                                    <img src="assets/img/Sidharth-pic1.jpg" alt="BB Special" class="img-fluid full-wide">
                                                    <label for="" class="numbers">10</label>
                                                    <button>features</button>
                                                </div>
                                                <h3 class="text-left font-wt-300">Abhishek Bachchan saves Aishwarya Rai from an OOPS moments...</h3>
                                            </a>
                                            <small>October 21, 2017</small>
                                        </div>
                                        <div class="photo-list">
                                            <a href="#" class="d-block">
                                                <div class="photo-link">
                                                    <img src="assets/img/Sidharth-pic2.jpg" alt="BB Special" class="img-fluid full-wide">
                                                    <label for="" class="numbers">10</label>
                                                    <button>events</button>
                                                </div>
                                                <h3 class="text-left font-wt-300">Kareena adds an edge to the traditional look...</h3>
                                            </a>
                                            <small>October 21, 2017</small>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="photo-list">
                                            <a href="#" class="d-block">
                                                <div class="photo-link">
                                                    <img src="assets/img/Sidharth-pic5.jpg" alt="main-video" class="img-fluid full-wide">
                                                    <label for="" class="numbers">10</label>
                                                    <button>spotted</button>
                                                </div>
                                                <h2 class="font-wt-300">Deepika Padukone and Farah Khan spotted at the airport!</h2>
                                            </a>
                                            <small>October 21, 2017</small>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="right-side">
                                            <div class="photo-list">
                                                <a href="#" class="d-block">
                                                    <div class="photo-link">
                                                        <img src="assets/img/Sidharth-pic3.jpg" alt="BB Special" class="img-fluid full-wide">
                                                        <label for="" class="numbers">10</label>
                                                        <button>features</button>
                                                    </div>
                                                    <h3 class="text-left font-wt-300">Ranbir Kapoor, Aadar Jain get clicked during the usual...</h3>
                                                </a>
                                                <small>October 21, 2017</small>
                                            </div>
                                            <div class="photo-list">
                                                <a href="#" class="d-block">
                                                    <div class="photo-link">
                                                        <img src="assets/img/Sidharth-pic4.jpg" alt="BB Special" class="img-fluid full-wide">
                                                        <label for="" class="numbers">10</label>
                                                        <button>editor's blog</button>
                                                    </div>
                                                    <h3 class="text-left font-wt-300">Ranbir Kapoor, Aadar Jain get clicked during the usual...</h3>
                                                </a>
                                                <small>October 21, 2017</small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="button-center">
                                        <a href="#" class="btn btn-default">Load more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 mb-30 xs-center">
                                    <i class="arrowsSub xl-hidden"><img src="assets/img/left-bar.png"></i>
                                    <h2 class="d-inline title uppercase font-32">related</h2>
                                    <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                                </div>
                            </div>
                            <div class="row mb-30 xs-mb-0" data-plugin="matchHeight" data-by-row="true">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 float-left">
                                    <div class="card">
                                        <a href="#" class="p-relative">
                                            <img src="assets/img/interview-.jpg" alt="video-link" class="img-fluid full-wide">
                                            <label for="" class="numbers">10</label>
                                        </a>
                                        <div class="card-title text-center">
                                            <h3 class="font-22 font-wt-300 font-black mb-10">Photo: Sushant Singh Rajput will make you blush with his charm</h3>
                                            <small class="font-light font-wt-400">October 21, 2017</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 float-left">
                                    <div class="card">
                                        <a href="#" class="p-relative">
                                            <img src="assets/img/interview-1.jpg" alt="video-link" class="img-fluid full-wide">
                                            <label for="" class="numbers">10</label>
                                        </a>
                                        <div class="card-title text-center">
                                            <h3 class="font-22 font-wt-300 font-black mb-10">Arjun Kapoor and Parineeti Chopra start shooting Sandeep Aur Pinky Faraar in New Delhi!</h3>
                                            <small class="font-light font-wt-400">October 21, 2017</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 float-left">
                                    <div class="card">
                                        <a href="#" class="p-relative">
                                            <img src="assets/img/interview-2.jpg" alt="video-link" class="img-fluid full-wide">
                                            <label for="" class="numbers">10</label>
                                        </a>
                                        <div class="card-title text-center">
                                            <h3 class="font-22 font-wt-300 font-black mb-10">Who is Amir Khan? The boxing world champ hoping to knock out the... </h3>
                                            <small class="font-light font-wt-400">October 21, 2017</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-30 xs-mb-0" data-plugin="matchHeight" data-by-row="true">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 float-left">
                                    <div class="card">
                                        <a href="#" class="p-relative">
                                            <img src="assets/img/interview-3.jpg" alt="video-link" class="img-fluid full-wide">
                                            <label for="" class="numbers">10</label>
                                        </a>
                                        <div class="card-title text-center">
                                            <h3 class="font-22 font-wt-300 font-black mb-10">WOW! Zaira Wasim receives National Child Award from President...</h3>
                                            <small class="font-light font-wt-400">October 21, 2017</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 float-left">
                                    <div class="card">
                                        <a href="#" class="p-relative">
                                            <img src="assets/img/interview-5-.jpg" alt="video-link" class="img-fluid full-wide">
                                            <label for="" class="numbers">10</label>
                                        </a>
                                        <div class="card-title text-center">
                                            <h3 class="font-22 font-wt-300 font-black mb-10">Irrfan Khan plays a Bengali writer inspired by Humayun Ahmed in Mostofa Farooki’s next</h3>
                                            <small class="font-light font-wt-400">October 21, 2017</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 float-left">
                                    <div class="card">
                                        <a href="#" class="p-relative">
                                            <img src="assets/img/interview-6.jpg" alt="video-link" class="img-fluid full-wide">
                                            <label for="" class="numbers">10</label>
                                        </a>
                                        <div class="card-title text-center">
                                            <h3 class="font-22 font-wt-300 font-black mb-10">Aditya Roy Kapur, Shraddha Kapoor, Gurmeet Choudhary spotted after </h3>
                                            <small class="font-light font-wt-400">October 21, 2017</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="button-center">
                                <a href="#" class="btn btn-default">Load more news</a>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="tab-pane" id="buzz" role="tabpanel">
                    <article>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 xs-center">
                                    <i class="arrowsSub xl-hidden"><img src="assets/img/left-bar.png"></i>
                                    <h2 class="d-inline title uppercase font-32">buzz</h2>
                                    <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <div class="buzz-box">
                                        <a href="#" >
                                            <div class="photo-link">
                                                <img src="assets/img/buzz-1.jpg" alt="BB Special" class="img-fluid full-wide">
                                                <label for="" class="numbers">10</label>
                                                <button>features</button>
                                            </div>
                                            <div class="details">
                                                <h5>A Gentleman Movie Review: Strong action loses its way amid ill-conceived plot
                                                </h5>
                                                <small class="font-light font-wt-400">October 21, 2017</small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="buzz-box">
                                        <a href="#" >
                                            <div class="photo-link">
                                                <img src="assets/img/buzz-2.jpg" alt="BB Special" class="img-fluid full-wide">
                                                <label for="" class="numbers">10</label>
                                                <button>features</button>
                                            </div>
                                            <div class="details">
                                                <h5>Sayani Gupta is all praise for Sidharth Malhotra and Katrina Kaif
                                                </h5>
                                                <small class="font-light font-wt-400">October 21, 2017</small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="buzz-box">
                                        <a href="#" >
                                            <div class="photo-link">
                                                <img src="assets/img/buzz-3.jpg" alt="BB Special" class="img-fluid full-wide">
                                                <label for="" class="numbers">10</label>
                                                <button>features</button>
                                            </div>
                                            <div class="details">
                                                <h5>Alia Bhatt � Sidharth Malhotra romancing in Coonoor
                                                </h5>
                                                <small class="font-light font-wt-400">October 21, 2017</small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="buzz-box">
                                        <a href="#" >
                                            <div class="photo-link">
                                                <img src="assets/img/buzz-4.jpg" alt="BB Special" class="img-fluid full-wide">
                                                <label for="" class="numbers">10</label>
                                                <button>features</button>
                                            </div>
                                            <div class="details">
                                                <h5>Sidharth Malhotra�s dapper look is making us skip our heartbeat!
                                                </h5>
                                                <small class="font-light font-wt-400">October 21, 2017</small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="buzz-box">
                                        <a href="#" >
                                            <div class="photo-link">
                                                <img src="assets/img/buzz-1.jpg" alt="BB Special" class="img-fluid full-wide">
                                                <label for="" class="numbers">10</label>
                                                <button>features</button>
                                            </div>
                                            <div class="details">
                                                <h5>A Gentleman Movie Review: Strong action loses its way amid ill-conceived plot
                                                </h5>
                                                <small class="font-light font-wt-400">October 21, 2017</small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="buzz-box">
                                        <a href="#" >
                                            <div class="photo-link">
                                                <img src="assets/img/buzz-2.jpg" alt="BB Special" class="img-fluid full-wide">
                                                <label for="" class="numbers">10</label>
                                                <button>features</button>
                                            </div>
                                            <div class="details">
                                                <h5>Sayani Gupta is all praise for Sidharth Malhotra and Katrina Kaif
                                                </h5>
                                                <small class="font-light font-wt-400">October 21, 2017</small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="buzz-box">
                                        <a href="#" >
                                            <div class="photo-link">
                                                <img src="assets/img/buzz-3.jpg" alt="BB Special" class="img-fluid full-wide">
                                                <label for="" class="numbers">10</label>
                                                <button>features</button>
                                            </div>
                                            <div class="details">
                                                <h5>Alia Bhatt � Sidharth Malhotra romancing in Coonoor
                                                </h5>
                                                <small class="font-light font-wt-400">October 21, 2017</small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="buzz-box">
                                        <a href="#" >
                                            <div class="photo-link">
                                                <img src="assets/img/buzz-4.jpg" alt="BB Special" class="img-fluid full-wide">
                                                <label for="" class="numbers">10</label>
                                                <button>features</button>
                                            </div>
                                            <div class="details">
                                                <h5>Sidharth Malhotra�s dapper look is making us skip our heartbeat!
                                                </h5>
                                                <small class="font-light font-wt-400">October 21, 2017</small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <div class="button-center">
                                    <a href="#" class="btn btn-default">Load more news</a>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="tab-pane" id="filmography" role="tabpanel">
                    <article>
                        <div class="container">
                            <div class="full-wide xs-text-center mb-3">
                                <i class="arrowsSub xl-hidden"><img src="assets/img/left-bar.png"></i>
                                <h2 class="d-inline title uppercase">movies</h2>
                                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select class="form-control filter">
                                            <option>Filter by</option>
                                            <option>This Week</option>
                                            <option>This Year</option>
                                            <option>Last 3 Months</option>
                                            <option>Last 6 Months</option>
                                            <option>All of 2017</option>
                                            <option>2016</option>
                                            <option>2015</option>
                                            <option>2014</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="table text-capitalize shadow">
                                        <table id="DataTable" class="table table-hover table-bordered shadow full-wide" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <td>film</td>
                                                    <td>year</td>
                                                    <td>role</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr><td>ittefaq <a href="#">watch trailer</a></td>
                                                    <td>15 Nov 2017</td>
                                                    <td>actor</td>
                                                </tr>
                                                <tr><td>A Gentleman <a href="#">watch trailer</a></td>
                                                    <td>15 Nov 2017</td>
                                                    <td>actor</td>
                                                </tr>
                                                <tr><td> My Name Is Khan <a href="#">watch trailer</a></td>
                                                    <td>15 Nov 2017</td>
                                                    <td>actor</td>
                                                </tr>
                                                <tr><td>Qarib Qarib Singlle <a href="#">watch trailer</a></td>
                                                    <td>15 Nov 2017</td>
                                                    <td>actor</td>
                                                </tr>
                                                <tr><td> Kapoor & Sons <a href="#">watch trailer</a></td>
                                                    <td>15 Nov 2017</td>
                                                    <td>actor</td>
                                                </tr>
                                                <tr><td> Brothers <a href="#">watch trailer</a></td>
                                                    <td>15 Nov 2017</td>
                                                    <td>actor</td>
                                                </tr>
                                                <tr><td> Student of the Year <a href="#">watch trailer</a></td>
                                                    <td>15 Nov 2017</td>
                                                    <td>actor</td>
                                                </tr>
                                                <tr><td> Kapoor & Sons <a href="#">watch trailer</a></td>
                                                    <td>15 Nov 2017</td>
                                                    <td>actor</td>
                                                </tr>
                                                <tr><td>ittefaq <a href="#">watch trailer</a></td>
                                                    <td>15 Nov 2017</td>
                                                    <td>actor</td>
                                                </tr>
                                                <tr><td>ittefaq <a href="#">watch trailer</a></td>
                                                    <td>15 Nov 2017</td>
                                                    <td>actor</td>
                                                </tr>
                                                <tr><td>ittefaq <a href="#">watch trailer</a></td>
                                                    <td>15 Nov 2017</td>
                                                    <td>actor</td>
                                                </tr>
                                                <tr><td>ittefaq <a href="#">watch trailer</a></td>
                                                    <td>15 Nov 2017</td>
                                                    <td>actor</td>
                                                </tr>
                                                <tr><td>ittefaq <a href="#">watch trailer</a></td>
                                                    <td>15 Nov 2017</td>
                                                    <td>actor</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </article>
                </div>
            </div>
        </div>
    </div>

</article>


<?php @include 'footer.php' ?>
<script>
    $('#fimography').owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        dots: false,
        //autoplay: true,
        //autoplayTimeout: 5000,

        responsive: {
            100: {
                items: 1
            },
            576: {
                items: 1
            },
            769: {
                items: 3
            },
            990: {
                items: 5
            }
        }
    });
    $('#upcoming-movie').owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        dots: false,
        //autoplay: true,
        //autoplayTimeout: 5000,
        animateOut: 'fadeOut',

        responsive: {
            100: {
                items: 1
            },
            576: {
                items: 1
            },
            769: {
                items: 3
            },
            990: {
                items: 5
            }
        }
    });
</script>
<?php @include 'header.php' ?>

<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="#">Home</a>
                <a href="#" class="active">top movies</a>
            </div>
        </div>
    </div>
</nav>
<article>
    <div class="container">
        <div class="row">
            <div class="news mb-30 xs-text-center">
                <h1 class="d-inline title text-center uppercase">trending movies</h1> 
                <i class="arrows"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
    </div>

    <!---------Star Slider ------->

    <div class="container-fluid">
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <div class="owl-carousel" id="top-movies-slider">
                <div class="slider-content item">
                    <div class="poster">
                        <a href="#">
                            <img src="assets/img/top-movie-01.jpg" class="img-fluid">
                            <div class="tag">1</div>
                            <div class="movie-ratings">
                                <h5>Judwaa 2</h5>
                                <p>Comedy</p>
                                <div class="rating">
                                    <div class="star-ratings-css">
                                        <div class="star-ratings-sprite">
                                            <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                        </div>
                                    </div>
                                </div>
                                <label class="hit"><span>141 cr</span><small>Hit</small></label>
                            </div>
                        </a> 
                    </div>                
                </div>
                <div class="slider-content item">
                    <div class="poster">
                        <a href="#"> 
                            <img src="assets/img/top-movie-02.jpg" class="img-fluid"> 
                            <div class="tag">2</div>
                            <div class="movie-ratings">
                                <h5>Toilet: Ek Prem K..</h5>
                                <p>Comedy</p>
                                <div class="rating">
                                    <div class="star-ratings-css">
                                        <div class="star-ratings-sprite">
                                            <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                        </div>
                                    </div>
                                </div>
                                <label class="d-inline-b superHit"><span>241 cr</span><small>Super Hit</small></label>
                            </div>
                        </a> 
                    </div>                
                </div>
                <div class="slider-content item">
                    <div class="poster">
                        <a href="#"> 
                            <img src="assets/img/top-movie-03.jpg" class="img-fluid"> 
                            <div class="tag">3</div>
                            <div class="movie-ratings">
                                <h5>Newton </h5>
                                <p>Comedy</p>
                                <div class="rating">
                                    <div class="star-ratings-css">
                                        <div class="star-ratings-sprite">
                                            <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                        </div>
                                    </div>
                                </div>
                                <label class="d-inline-b average"><span>41 cr</span><small>Average</small></label>
                            </div>
                        </a> 
                    </div>             
                </div>
                <div class="slider-content item">
                    <div class="poster">
                        <a href="#"> 
                            <img src="assets/img/top-movie-04.jpg" class="img-fluid"> 
                            <div class="tag">4</div>  
                            <div class="movie-ratings">
                                <h5>Secret Superstar</h5>
                                <p>Comedy</p>
                                <div class="rating">
                                    <div class="star-ratings-css">
                                        <div class="star-ratings-sprite">
                                            <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                        </div>
                                    </div>
                                </div> 
                                <label class="d-inline-b average"><span>41 cr</span><small>Average</small></label>
                            </div>
                        </a> 
                    </div>                
                </div>
                <div class="slider-content item">
                    <div class="poster">
                        <a href="#"> 
                            <img src="assets/img/top-movie-05.jpg" class="img-fluid">
                            <div class="tag">5</div>
                            <div class="movie-ratings">
                                <h5>Noor</h5>
                                <p>Comedy</p>
                                <div class="rating">
                                    <div class="star-ratings-css">
                                        <div class="star-ratings-sprite">
                                            <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                        </div>
                                    </div>
                                </div>     
                                <label class="hit"><span>141 cr</span><small>Hit</small></label>
                            </div>
                        </a> 
                    </div>                
                </div>
            </div>
        </div>
    </div>
</article>
<!---------End Slider ------->

<article>
    <div class="container">
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <div class="col-lg-3 col-md-4 col-sm-6 ">
                <div class="boxOffice">
                    <a href="#" class="full-wide"> <img src="assets/img/top-movie-06.jpg" alt="Movie-list">
                        <div class="p-relative">
                            <label class="hit"><span>141 cr</span><small>Hit</small></label>
                        </div>
                        <h5>Newton</h5>
                        <span>Thriller</span>
                        <div class="rating">
                            <div class="star-ratings-css">
                                <div class="star-ratings-sprite">
                                    <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="boxOffice">
                    <a href="#" class="full-wide"> <img src="assets/img/top-movie-07.jpg" alt="Movie-list">
                        <div class="p-relative">
                            <label class="d-inline-b average"><span>41 cr</span><small>Average</small></label>
                        </div>
                        <h5>Newton</h5>
                        <span>Thriller</span>
                        <div class="rating">
                            <div class="star-ratings-css">
                                <div class="star-ratings-sprite">
                                    <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="boxOffice">
                    <a href="#" class="full-wide"> <img src="assets/img/top-movie-08.jpg" alt="Movie-list">
                        <div class="p-relative">
                            <label class="d-inline-b superHit"><span>241 cr</span><small>Super Hit</small></label>
                        </div>
                        <h5>Newton</h5>
                        <span>Thriller</span>
                        <div class="rating">
                            <div class="star-ratings-css">
                                <div class="star-ratings-sprite">
                                    <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="boxOffice">
                    <a href="#" class="full-wide"> <img src="assets/img/top-movie-09.jpg" alt="Movie-list">
                        <div class="p-relative">
                            <label class="d-inline-b average"><span>41 cr</span><small>Average</small></label>
                        </div>
                        <h5>Newton</h5>
                        <span>Thriller</span>
                        <div class="rating">
                            <div class="star-ratings-css">
                                <div class="star-ratings-sprite">
                                    <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="boxOffice">
                    <a href="#" class="full-wide"> <img src="assets/img/top-movie-10.jpg" alt="Movie-list">
                        <div class="p-relative">
                            <label class="d-inline-b superHit"><span>241 cr</span><small>Super Hit</small></label>
                        </div>
                        <h5>Newton</h5>
                        <span>Thriller</span>
                        <div class="rating">
                            <div class="star-ratings-css">
                                <div class="star-ratings-sprite">
                                    <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="boxOffice">
                    <a href="#" class="full-wide"> <img src="assets/img/top-movie-11.jpg" alt="Movie-list">
                        <div class="p-relative">
                            <label class="d-inline-b average"><span>41 cr</span><small>Average</small></label>
                        </div>
                        <h5>Newton</h5>
                        <span>Thriller</span>
                        <div class="rating">
                            <div class="star-ratings-css">
                                <div class="star-ratings-sprite">
                                    <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="boxOffice">
                    <a href="#" class="full-wide"> <img src="assets/img/top-movie-12.jpg" alt="Movie-list">
                        <div class="p-relative">
                            <label class="d-inline-b superHit"><span>241 cr</span><small>Super Hit</small></label>
                        </div>
                        <h5>Newton</h5>
                        <span>Thriller</span>
                        <div class="rating">
                            <div class="star-ratings-css">
                                <div class="star-ratings-sprite">
                                    <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="boxOffice">
                    <a href="#" class="full-wide"> <img src="assets/img/top-movie-13.jpg" alt="Movie-list">
                        <div class="p-relative">
                            <label class="d-inline-b average"><span>41 cr</span><small>Average</small></label>
                        </div>
                        <h5 class="mt-2">Newton</h5>
                        <div class="rating">
                            <div class="star-ratings-css">
                                <div class="star-ratings-sprite">
                                    <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="boxOffice">
                    <a href="#" class="full-wide"> <img src="assets/img/top-movie-14.jpg" alt="Movie-list">
                        <div class="p-relative">
                            <label class="d-inline-b superHit"><span>241 cr</span><small>Super Hit</small></label>
                        </div>
                        <h5>Newton</h5>
                        <span>Thriller</span>
                        <div class="rating">
                            <div class="star-ratings-css">
                                <div class="star-ratings-sprite">
                                    <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="boxOffice">
                    <a href="#" class="full-wide"> <img src="assets/img/top-movie-15.jpg" alt="Movie-list">
                        <div class="p-relative">
                            <label class="hit"><span>141 cr</span><small>Hit</small></label>
                        </div>
                        <h5>Newton</h5>
                        <span>Thriller</span>
                        <div class="rating">
                            <div class="star-ratings-css">
                                <div class="star-ratings-sprite">
                                    <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="boxOffice">
                    <a href="#" class="full-wide"> <img src="assets/img/top-movie-16.jpg" alt="Movie-list">
                        <div class="p-relative">
                            <label class="d-inline-b superHit"><span>241 cr</span><small>Super Hit</small></label>
                        </div>
                        <h5>Newton</h5>
                        <span>Thriller</span>
                        <div class="rating">
                            <div class="star-ratings-css">
                                <div class="star-ratings-sprite">
                                    <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="boxOffice">
                    <a href="#" class="full-wide"> <img src="assets/img/top-movie-17.jpg" alt="Movie-list">
                        <div class="p-relative">
                            <label class="hit"><span>141 cr</span><small>Hit</small></label>
                        </div>
                        <h5>Newton</h5>
                        <span>Thriller</span>
                        <div class="rating">
                            <div class="star-ratings-css">
                                <div class="star-ratings-sprite">
                                    <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="boxOffice">
                    <a href="#" class="full-wide"> <img src="assets/img/top-movie-18.jpg" alt="Movie-list">
                        <div class="p-relative">
                            <label class="hit"><span>141 cr</span><small>Hit</small></label>
                        </div>
                        <h5>Newton</h5>
                        <span>Thriller</span>
                        <div class="rating">
                            <div class="star-ratings-css">
                                <div class="star-ratings-sprite">
                                    <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="xs-fit-content boxOffice">
                    <a href="#" class="full-wide"> <img src="assets/img/top-movie-19.jpg" alt="Movie-list">
                        <div class="p-relative">
                            <label class="d-inline-b average"><span>41 cr</span><small>Average</small></label>
                        </div>
                        <h5>Newton</h5>
                        <span>Thriller</span>
                        <div class="rating">
                            <div class="star-ratings-css">
                                <div class="star-ratings-sprite">
                                    <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="xs-fit-content boxOffice">
                    <a href="#" class="full-wide"> <img src="assets/img/top-movie-20.jpg" alt="Movie-list">
                        <div class="p-relative">
                            <label class="hit"><span>141 cr</span><small>Hit</small></label>
                        </div>
                        <h5>Newton</h5>
                        <span>Thriller</span>
                        <div class="rating">
                            <div class="star-ratings-css">
                                <div class="star-ratings-sprite">
                                    <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                </div>
                            </div>
                        </div>   
                    </a>
                </div>
            </div> 
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="xs-fit-content boxOffice">
                    <a href="#" class="full-wide"> <img src="assets/img/top-movie-21.jpg" alt="Movie-list">
                        <div class="p-relative">
                            <label class="d-inline-b superHit"><span>241 cr</span><small>Super Hit</small></label>
                        </div>
                        <h5>Newton</h5>
                        <span>Thriller</span>
                        <div class="rating">
                            <div class="star-ratings-css">
                                <div class="star-ratings-sprite">
                                    <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>                    
        </div>
        <div class="button-center">
            <a href="#" class="btn btn-default">Load More</a>
        </div>
    </div>
</article>

<?php @include 'footer.php' ?>



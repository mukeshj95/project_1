<?php @include 'header.php'; ?>
<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="index.php">Home</a>
                <a href="my-account.php" class="active">My Account</a>
            </div>
        </div>
    </div>
</nav>

<div class="author-ac">
    <div class="container">
        <div class="row">
            <div class="account-info">
                <div>
                <div class="xs-text-center float-left">
                    <h1 class="d-inline title text-center uppercase text-white">my account</h1> 
                    <i class="arrows"><img src="assets/img/right-bar.png"></i>
                </div>
                <div class="float-right p-2">
                    <a href="#" data-toggle="modal" data-target="#settingModal"><i class="ion-ios-gear-outline"></i><span>Settings</span></a>
                </div>
                </div>
                <div class="blog-imgae">
                    <img src="assets/img/author-pic.jpg" alt="Author" title="Rohit Sharma">
                </div>
                <h2>Rohit Sharma</h2>
                <a href="#" data-toggle="modal" data-target="#blogModal" class="btn btn-blog" data-keyboard="false">Write your own Blog</a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="about-me">
                    <div class="card cast-bg">
                        <div class="full-wide text-center mb-10">
                            <i class="arrowsSub"><img src="assets/img/left-bar.png"></i>
                            <h2 class="d-inline title text-center">favourite celebs <span>(6)</span></h2>
                            <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                        </div>
                        <div class="row" data-plugin="matchHeight" data-by-row="true">
                            <div class="owl-carousel col-md-12 CrewList" id="CrewList2">
                                <div class="item">
                                    <a href="#">
                                        <div class="image">
                                            <img src="assets/img/cast-1.jpg" alt="Cast" title="Ajay" class="img-fluid">
                                        </div>
                                        <h3>Ajay Devgan</h3>.
                                        <label>Musician</label>
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="#">
                                        <div class="image">
                                            <img src="assets/img/cast-2.jpg" alt="Cast" title="Ajay" class="img-fluid">
                                        </div>
                                        <h3>Parineeti Chopra</h3>.
                                        <label>Director</label>
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="#">
                                        <div class="image">
                                            <img src="assets/img/cast-3.jpg" alt="Cast" title="Ajay" class="img-fluid">
                                        </div>
                                        <h3>Kunal Khemu</h3>.
                                        <label>Actor</label>
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="#">
                                        <div class="image">
                                            <img src="assets/img/cast-4.jpg" alt="Cast" title="Ajay" class="img-fluid">
                                        </div>
                                        <h3>Ajay Devgon</h3>.
                                        <label>Actor</label>
                                    </a>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="bg-light-red">
    <div class="container">
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <div class="full-wide text-center">
                <i class="arrowsSub"><img src="assets/img/left-bar.png"></i>
                <h2 class="text-center uppercase title d-inline-b">favourite movies</h2>
                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div class="row upcoming-movie" data-plugin="matchHeight" data-by-row="true">
            <div class="col-md-12 owl-carousel" id="favMovies">
                <div class="col-xs-12 item">
                    <div class="row">
                        <a href="#" class="upcomingMovie"> <img src="assets/img/fav-movie-1.jpg" alt="News" class="img-fluid"> </a>
                        <div class="card-footer">
                            <p>Lunchbox</p>
                            <label>Romance/comedy</label>
                            <h6>Releasing: 12 Nov 2017</h6> </div>
                    </div>
                </div>
                <div class="col-xs-12 item">
                    <div class="row">
                        <a href="#" class="upcomingMovie"> <img src="assets/img/masan.jpg" alt="News" class="img-fluid"> </a>
                        <div class="card-footer">
                            <p>Masaan</p>
                            <label>Romance/comedy</label>
                            <h6>Releasing: 12 Nov 2017</h6> </div>
                    </div>
                </div>
                <div class="col-xs-12 item">
                    <div class="row">
                        <a href="#" class="upcomingMovie"> <img src="assets/img/fav-movie-3.jpg" alt="News" class="img-fluid"> </a>
                        <div class="card-footer">
                            <p>Queen</p>
                            <label>Drama</label>
                            <h6>Releasing: 12 Nov 2017</h6> </div>
                    </div>
                </div>
                <div class="col-xs-12 item ">
                    <div class="row">
                        <a href="#" class="upcomingMovie"> <img src="assets/img/fav-movie-4.jpg" alt="News" class="img-fluid"> </a>
                        <div class="card-footer">
                            <p>Dear Zindagi</p>
                            <label>Romance/comedy</label>
                            <h6>Releasing: 12 Nov 2017</h6> </div>
                    </div>
                </div>
                <div class="col-xs-12 item">
                    <div class="row">
                        <a href="#" class="upcomingMovie"> <img src="assets/img/upcoming-5.jpg" alt="News" class="img-fluid"> </a>
                        <div class="card-footer">
                            <p>The House Next Door</p>
                            <label>Romance/comedy</label>
                            <h6>Releasing: 12 Nov 2017</h6> </div>
                    </div>
                </div>
            </div>
            <div class="button-center">
                <a href="upcoming-movies.php" class="btn btn-default">View All movies</a>
            </div>
        </div>
    </div>
</div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="full-wide text-center">
                <i class="arrowsSub"><img src="assets/img/left-bar.png"></i>
                <h2 class="text-center uppercase title d-inline-b">favourite photos</h2>
                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 float-left">
                <div class="card">
                    <a href="#" class="p-relative">
                        <img src="assets/img/fav-photo-1.jpg" alt="video-link" class="img-fluid">
                        <label for="" class="numbers">10</label>
                    </a>
                    <div class="card-title">
                        <h3 class="font-22 font-wt-400 font-black mb-10">Photo: Sushant Singh Rajput will make you blush with his charm</h3>
                        <small class="font-light font-wt-400">October 21, 2017</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 float-left">
                <div class="card photo">
                    <a href="#" class="p-relative">
                        <img src="assets/img/fav-photo-2.jpg" alt="video-link" class="img-fluid">
                        <label for="" class="numbers">10</label>
                    </a>
                    <div class="card-title">
                        <h3 class="font-22 font-wt-400 font-black mb-10">Arjun Kapoor and Parineeti Chopra start shooting...</h3>
                        <small class="font-light font-wt-400">October 21, 2017</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 float-left">
                <div class="card photo">
                    <a href="#" class="p-relative">
                        <img src="assets/img/spotted-3.jpg" alt="video-link" class="img-fluid">
                        <label for="" class="numbers">10</label>
                    </a>
                    <div class="card-title">
                        <h3 class="font-22 font-wt-400 font-black mb-10">Who is Amir Khan? The boxing world champ... </h3>
                        <small class="font-light font-wt-400">October 21, 2017</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="row xs-hidden" data-plugin="matchHeight" data-by-row="true">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 float-left">
                <div class="card photo">
                    <a href="#" class="p-relative">
                        <img src="assets/img/fav-photo-4.jpg" alt="video-link" class="img-fluid">
                        <label for="" class="numbers">10</label>
                    </a>
                    <div class="card-title">
                        <h3 class="font-22 font-wt-400 font-black mb-10">WOW! Zaira Wasim receives National Child Award from President...</h3>
                        <small class="font-light font-wt-400">October 21, 2017</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 float-left">
                <div class="card photo">
                    <a href="#" class="p-relative">
                        <img src="assets/img/fav-photo-5.jpg" alt="video-link" class="img-fluid">
                        <label for="" class="numbers">10</label>
                    </a>
                    <div class="card-title">
                        <h3 class="font-22 font-wt-400 font-black mb-10">Irrfan Khan plays a Bengali writer inspired by Humayun Ahmed in Mostofa Farooki�s next</h3>
                        <small class="font-light font-wt-400">October 21, 2017</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 float-left">
                <div class="card photo">
                    <a href="#" class="p-relative">
                        <img src="assets/img/fav-photo-6.jpg" alt="video-link" class="img-fluid">
                        <label for="" class="numbers">10</label>
                    </a>
                    <div class="card-title">
                        <h3 class="font-22 font-wt-400 font-black mb-10">Aditya Roy Kapur, Shraddha Kapoor, Gurmeet Choudhary spotted after </h3>
                        <small class="font-light font-wt-400">October 21, 2017</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="button-center">
            <a href="#" class="btn btn-default">View all photos</a>
        </div>
    </div>
</section>
<section class="bg-grey">
    <div class="container">
        <div class="row">
            <div class="full-wide text-center">
                <i class="arrowsSub"><img src="assets/img/left-bar.png"></i>
                <h2 class="text-center uppercase title d-inline-b">favourite videos</h2>
                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#">
                    <div class="card-link mb-30">
                        <div class="video-content">
                            <img src="assets/img/fav-video-1.jpg" alt="BB Special" class="img-fluid full-wide">
                            <label class="duration">15:22</label>
                        </div>
                        <h3>Dhadak: Janhvi and Ishaan soak in moments...</h3>
                        <small class="text-muted">October 21, 2017</small>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#">
                    <div class="card-link mb-30">
                        <div class="video-content">
                            <img src="assets/img/fav-video-2.jpg" alt="BB Special" class="img-fluid full-wide">
                            <label class="duration">15:22</label>
                        </div>
                        <h3>5 unknown facts about Janhvi Kapoor that her Instagram...</h3>
                        <small class="text-muted">October 21, 2017</small>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#">
                    <div class="card-link mb-30">
                        <div class="video-content">
                            <img src="assets/img/fav-video-3.jpg" alt="BB Special" class="img-fluid full-wide">
                            <label class="duration">15:22</label>
                        </div>
                        <h3>Padmavati: Deepika Padukone�s �ghoomar� act...</h3>
                        <small class="text-muted">October 21, 2017</small>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#">
                    <div class="card-link mb-30">
                        <div class="video-content">
                            <img src="assets/img/fav-video-4.jpg" alt="BB Special" class="img-fluid full-wide">
                            <label class="duration">15:22</label>
                        </div>
                        <h3>THIS movie would have been Chitrangda�s debut instead...</h3>
                        <small class="text-muted">October 21, 2017</small>
                    </div>
                </a>
            </div>
        </div>
        <div class="button-center">
            <a href="#" class="btn btn-default">View all videos</a>
        </div>
    </div>
</div>
</section>
<section>
    <div class="container">
        <div class="col-md-12 border-primary bg-white">
            <div class="row">
                <div class="full-wide text-center">
                    <h2 class="artist bg-light-red sideTitle special uppercase">favourite news</h2>
                </div>
            </div>
            <div class="row" data-plugin="matchHeight" data-by-row="true">
                <div class="col-md-4 col-xs-12">
                    <div class="bb-dairies">
                        <a href="#" class="d-block">
                            <div class="photo-link">
                                <img src="assets/img/fav-news-1.jpg" class="img-fluid full-wide">
                                <button>featured</button>
                            </div>
                            <h5>Inside Kareena Kapoor's Grand Birthday Party </h5>
                        </a>
                        <small>October 21, 2017</small>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="bb-dairies">
                        <a href="#" class="d-block">
                            <div class="photo-link">
                                <img src="assets/img/fav-news-2.jpg" class="img-fluid full-wide">
                                <button>stories</button>
                            </div>
                            <h5>Things that brought Salman-Shah Rukh Khan together</h5>
                        </a>
                        <small>October 21, 2017</small>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="bb-dairies">
                        <a href="#" class="d-block">
                            <div class="photo-link">
                                <img src="assets/img/fav-news.jpg" class="img-fluid full-wide">
                                <button>featured</button>
                            </div>
                            <h5>Ayushmann Khurana And The Art Of Effortless Charm</h5>
                        </a>
                        <small>October 21, 2017</small>
                    </div>
                </div>
            </div>
            <div class="button-center">
                <a href="#" class="btn btn-default">View All news</a>
            </div>
        </div>
    </div>
</section>

<?php @include 'footer.php'; ?>
<script>
    $('#CrewList2').owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        dots: false,
        //autoplay: true,
        autoplayTimeout: 2000,

        responsive: {
            100: {
                items: 1
            },
            576: {
                items: 2
            },
            769: {
                items: 3
            },
            990: {
                items: 4
            }
        }
    });

    $('#favMovies').owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        dots: false,
        //autoplay: true,
        autoplayTimeout: 2000,

        responsive: {
            100: {
                items: 1
            },
            576: {
                items: 3
            },
            769: {
                items: 3
            },
            990: {
                items: 4
            }
        }
    });
</script>


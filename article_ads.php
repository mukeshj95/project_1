<?php @include 'header.php'; ?>

<div class="shadow" id="share-article">
    <div class="container">
        <div class="row">
            <nav class="navbar navbar-inverse " id="fixTop">          
                <div class="navbar-header d-flex">
                    <div class="button  hidden-md-down">
                        <a class="btn-open" href="#"></a>
                    </div>
                    <a href="#" class="navbar-toggle" data-toggle="collapse"  onclick="openNav()">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="ion-drag"></i>
                    </a>
                    <div class="d-flex full-wide">
                        <a class="navbar-brand" href="index.php"><img src="assets/img/logo-red.png" alt="Bollywood Bubble" class="Bollywood Bubble"></a>
                        <a href="#" class="title">Check Out: Flight fashion: Best and worst dressed celebs..</a>
                        <div  class="icon-box m-auto">
                            <ul>
                                <li class="bg-fb">
                                    <a href="#">
                                        <i class="ion-social-facebook text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-twitter">
                                    <a href="#">
                                        <i class="ion-social-twitter text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-google">
                                    <a href="#">
                                        <i class="ion-social-googleplus text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-youtube">
                                    <a href="#">
                                        <i class="ion-ios-heart-outline text-white"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="banner">
            <a href="#">
                <img src="assets/img/being-human-top.jpg" class="img-fluid" alt="Being Human" title="Being Human">
            </a>
        </div>
    </div>
</div>
<nav class="nav bg-grey">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="index.php">Home</a>
                <a href="#" class="active">Check Out: Flight fashion: Best and worst dressed celebs..</a>
            </div>
        </div>
    </div>
</nav>

<article class="bg-type">
    <div class="ad-verticle-160 ad-left-fix">
        <a href="#">
            <img src="assets/img/being-human-l.png" alt="ads" title="">
        </a>
    </div>
    <div class="container bg-black">
        <div class="row">
            <div class="width-600">
                <div class="row">
                    <div class="title">
                        <h1 class="subtitle">Salman Khan & Katrina Kaif Combine To Form The DEADLIEST Duo In This Fantastic Promo Of Tiger Zinda Hai Khan Starrer</h1>
                    </div>
                    <div class="tags full-wide pt-2">
                        <ul>
                            <li>
                                <a href="#">Lifestyle</a>
                            </li>
                            <li>
                                <a href="#">Fashion</a>
                            </li>
                            <li>
                                <a href="#">Spotted</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-50 float-left">
                        <p class="text-muted mt-1">By <span class="primary-color">Prajakta Ajgaonkar </span>on October 27 2017<i class="ion-chatbubble-working"><span class="text-muted">12</span></i></p>
                    </div>
                    <div class="col-50 editor p-0 float-right">
                        <div class="icon-box p-0 float-right xl-hidden">
                            <ul>
                                <li class="pr-3">
                                    <i class="ion-android-share-alt fs-2"></i>
                                </li>
                            </ul>
                        </div>
                        <div class="icon-box p-0 float-right xs-hidden">
                            <ul>
                                <li class="bg-fb">
                                    <a href="#">
                                        <i class="ion-social-facebook text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-twitter">
                                    <a href="#">
                                        <i class="ion-social-twitter text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-google">
                                    <a href="#">
                                        <i class="ion-social-googleplus text-white"></i>
                                    </a>
                                </li>
                                <li class="bg-youtube">
                                    <a href="#">
                                        <i class="ion-ios-heart-outline text-white"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="mt-3">
                    <img src="assets/img/tiger-pic.jpg" class="img-fluid full-wide m-auto" alt="source">
                    <h5 class="mt-2 font-wt-500">Jacqueline Fernandez at Airport</h5>
                    <hr>
                    <p class="source"><label>Image Source - Yogesh Shah</label></p>
                </div>
                <div class="interview">
                    <p>Movie promotions, brand endorsements and vacays! Bollywood celebs are always on their toes and trying to catch up with the pace of their buzzing glamour life. They have a brigade of fashion stylists at their disposal even when they are shuttling between airport terminals, so it’s not just their choice of ensembles but also travel essentials like hats, backpacks, shades and shoes that make us go ‘wowie’.</p>
                    <p>As fashion aficionados, we bring to you some of the week’s best and worst-dressed celebs whose breezy travel wear features high on our travel wish list. This week, we’ve spotted most celebrities giving preference to comfort over style; few others preferred style. While Kangana Ranaut, Kareena Kapoor Khan, Anushka Sharma. Ranveer Singh upped the much-needed travel quo, we were disappointed with Vidya Balan</p>
                </div>
            </div>
            <div class="right-bar-fix-width-300 mt-3">
                <div class="ads mb-40">
                    <a href="#">
                        <img src="assets/img/mmt-ads.jpg" alt="mmt-ads" class="img-fluid">
                    </a>
                </div>
                <div class="bubble-tv-box">
                    <div class="text-center">
                        <h2 class="uppercase sideTitle text-center">bubble tv<i class="bubble-tv"><img src="assets/img/youtube-icon.png"></i></h2>
                    </div>
                    <a href="#">
                        <div class="card-link">
                            <div class="video-content">
                                <img src="assets/img/kalki.jpg" class="img-fluid full-wide" alt="video">
                            </div>
                            <div class="video-sub">
                                <h5>Kalki Koechlin visited Bollywood Bubble and this is how we welcomed her!</h5>
                                <small class="text-muted">October 21, 2017</small>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="bg-white border-primary mb-40">
                    <div class="text-center">
                        <h2 class="sideTitle uppercase">featured</h2>
                    </div>
                    <div class="video-list">
                        <a href="#">
                            <div class="card-link">
                                <div class="video-content">
                                    <img src="assets/img/salman-big.jpg" class="img-fluid full-wide" alt="video">
                                </div>
                                <div class="video-sub">
                                    <h5>Salman Khan sings his heart out as he takes the center stage at
                                        Birmingham..</h5>
                                    <small class="text-muted">October 21, 2017</small>
                                </div>
                            </div>
                        </a>
                        <a href="#">
                            <div class="card-link">
                                <div class="video-content">
                                    <img src="assets/img/maliaka.jpg" class="img-fluid full-wide" alt="video">
                                </div>
                                <div class="video-sub">
                                    <h5>Video alert! Learn to slay the monochrome look from Malaika Arora..</h5>
                                    <small class="text-muted">October 21, 2017</small>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="ad-horizontal-970 bg-type">
            <a href="#">
                <img src="assets/img/being-human-b.png" alt="ads" title="" >
            </a>
        </div>
    </div>
    <div class="ad-verticle-160 ad-right-fix">
        <a href="#">
            <img src="assets/img/being-human-r.png" alt="ads" title="">
        </a>
    </div>
</article>


<?php
@include 'footer.php';
?>

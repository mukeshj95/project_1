<?php @include 'header.php' ?>

<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="#">Home</a>
                <a href="#">News</a>
                <a href="#" class="active">Bubble speaks</a>
            </div>
        </div>
    </div>
</nav>

<section class="bg-bottom">
    <div class="container">
        <div class="row">
            <div class="news mb-30 xs-text-center">
                <h1 class="d-inline title text-center uppercase">Bubble dairies</h1> 
                <i class="arrows"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div class="news-list">
                    <a href="#" class="d-block">
                        <div class="photo-link">
                            <img src="assets/img/sanjay-leela.jpg" class="img-fluid full-wide">
                            <button>spotted</button>
                        </div>
                        <h2>Dear Sanjay Leela Bhansali, no more clarifications PLEASE!</h2>
                    </a>
                    <small>October 21, 2017</small>
                    <p>Dear Sanjay Leela Bhansali, <br />In the last two decades you devoted to the Indian film industry, you have made some outstanding films with concepts many filmmakers would not even like to touch. You made ‘Khamoshi: The Musical’ in which two...</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <div class="news-list">
                            <a href="#" class="d-block">
                                <div class="photo-link">
                                    <img src="assets/img/golmal-blog.jpg" class="img-fluid full-wide">
                                    <button>spotted</button>
                                </div>
                                <h5>Horror-comedy to become the new flavour of the season replacing adult comedies?</h5>
                            </a>
                            <small>October 21, 2017</small>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="news-list">
                            <a href="#" class="d-block">
                                <div class="photo-link">
                                    <img src="assets/img/blog-1.jpg" class="img-fluid full-wide">
                                    <button>spotted</button>
                                </div>
                                <h5>Here’s everything about ‘Judwaa 2’ that is NOT funny</h5>
                            </a>
                            <small>October 21, 2017</small>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="news-list">
                            <a href="#" class="d-block">
                                <div class="photo-link">
                                    <img src="assets/img/blog-2.jpg" class="img-fluid full-wide">
                                    <button>spotted</button>
                                </div>
                                <h5>‘Chef’: Will this Saif-starrer break the bust of stale Bollywood stereotypes?</h5>
                            </a>
                            <small>October 21, 2017</small>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="news-list">
                            <a href="#" class="d-block">
                                <div class="photo-link">
                                    <img src="assets/img/blog-3.jpg" class="img-fluid full-wide">
                                    <button>spotted</button>
                                </div>
                                <h5>Dear Mahira, thank you for uncovering the misogyny once more</h5>
                            </a>
                            <small>October 21, 2017</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-------- Nostalgia start -------->
<article class="bg-grey">
    <div class="container">
        <div class="row">
            <div class="full-wide text-center mb-30">
                <i class="arrowsSub"><img src="assets/img/left-bar.png"></i>
                <h2 class="d-inline title text-center uppercase">NOSTALGIA</h2>
                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div class="nostalgia-grey">
            <div class="border-1-grey">
                <div class="row" data-plugin="matchHeight" data-by-row="true">
                    <div class="col-md-6 col-xs-12">
                        <div class="nostalgia-list">
                            <div>
                                <img src="assets/img/nostalgia-1.jpg" alt="Nostalgia" title="Nostalgia" class="img-fluid full-wide">
                            </div>
                            <div class="card">
                                <h5>When a little Kareena would hide and watch her sister Karisma cry</h5>
                                <small>October 21, 2017</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="nostalgia-list">
                            <div>
                                <img src="assets/img/nostalgia-2.jpg" alt="Nostalgia" title="Nostalgia" class="img-fluid full-wide">
                            </div>
                            <div class="card">
                                <h5>Salman, Madhuri and Shah Rukh Khan: A casting coup that could have...</h5>
                                <small>October 21, 2017</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" data-plugin="matchHeight" data-by-row="true">
                    <div class="col-md-4 col-xs-12">
                        <div class="nostalgia-list">
                            <div>
                                <img src="assets/img/priyanka-nos.jpg" alt="Nostalgia" title="Nostalgia" class="img-fluid full-wide">
                            </div>
                            <div class="card">
                                <h5>The stage was being set: When Priyanka was unaware of her photos..</h5>
                                <small>October 21, 2017</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="nostalgia-list">
                            <div>
                                <img src="assets/img/karishma-nos.jpg" alt="Nostalgia" title="Nostalgia" class="img-fluid full-wide">
                            </div>
                            <div class="card">
                                <h5>When Karisma rejected ‘Dil To Pagal Hai’, but destiny had other plans</h5>
                                <small>October 21, 2017</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="nostalgia-list">
                            <div>
                                <img src="assets/img/amir-nos.jpg" alt="Nostalgia" title="Nostalgia" class="img-fluid full-wide">
                            </div>
                            <div class="card">
                                <h5>When a teetotaller Aamir Khan ended up drinking a bottle of vodka</h5>
                                <small>October 21, 2017</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" data-plugin="matchHeight" data-by-row="true">
                    <div class="col-md-6 col-xs-12">
                        <div class="nostalgia-list">
                            <div>
                                <img src="assets/img/nostalgia-1.jpg" alt="Nostalgia" title="Nostalgia" class="img-fluid full-wide">
                            </div>
                            <div class="card">
                                <h5>When a little Kareena would hide and watch her sister Karisma cry</h5>
                                <small>October 21, 2017</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="nostalgia-list">
                            <div>
                                <img src="assets/img/nostalgia-2.jpg" alt="Nostalgia" title="Nostalgia" class="img-fluid full-wide">
                            </div>
                            <div class="card">
                                <h5>Salman, Madhuri and Shah Rukh Khan: A casting coup that could have...</h5>
                                <small>October 21, 2017</small>
                            </div>
                        </div>
                    </div>
                    <div class="button-center">
                        <a href="#" class="btn btn-default">View all</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>
<!--------end Nostalgia start -------->

<section class="bg-bottom-speak">
    <div class="container">
        <div class="row">
            <div class="full-wide text-center mb-30">
                <i class="arrowsSub"><img src="assets/img/left-bar.png"></i>
                <h2 class="d-inline title text-center uppercase">BUBBLE SPEAKs</h2>
                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-xs-6">
                <div class="row">
                    <div class="bubble-s">
                        <a href="#"> 
                            <img src="assets/img/bubble-speak1.jpg" class="img-fluid full-wide" alt="bollywood-life">
                            <div class="button-link">
                                <button class="font-wt-700">fashion</button>
                            </div>
                        </a>
                        <h5>Can we please stop obsessing over the obviously-morphed picture of Deepika?</h5>
                        <small>October 30, 2017</small> 
                    </div>
                </div>  
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="row">
                    <div class="bubble-s">
                        <a href="#"> 
                            <img src="assets/img/bubble-speak2.jpg" class="img-fluid full-wide" alt="bollywood-life">
                            <div class="button-link">
                                <button class="font-wt-700">fashion</button>
                            </div>
                        </a>
                        <h5>‘Shab’, ‘Lipstick Under My Burkha’: Why do we refuse progression and concede to </h5> 
                        <small>October 30, 2017</small> 
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="row">
                    <div class="bubble-s">
                        <a href="#"> 
                            <img src="assets/img/bubble-speak3.jpg" class="img-fluid full-wide" alt="bollywood-life">
                            <div class="button-link">
                                <button class="font-wt-700">spotted</button>
                            </div>
                        </a>
                        <h5>A note to Sunny Leone’s daughter: Know that you’re in strength</h5> 
                        <small>October 30, 2017</small> 
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="row">
                    <div class="bubble-s">
                        <a href="#"> 
                            <img src="assets/img/bubble-speak4.jpg" class="img-fluid full-wide" alt="bollywood-life">
                            <div class="button-link">
                                <button class="font-wt-700">blog</button>
                            </div>
                        </a>
                        <h5>Not just the actor, but the larger-than-life phenomenon that is Shah Rukh Khan</h5> 
                        <small>October 30, 2017</small> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="bg-light-red">
    <div class="container">
        <div class="row">
            <div class="full-wide text-center mb-30">
                <i class="arrowsSub"><img src="assets/img/left-bar.png"></i>
                <h2 class="d-inline title text-center uppercase">guest blog</h2>
                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <div class="col-md-4 col-xs-12">
                <div class="guest-blog">
                    <div class="card">
                        <a href="#"> 
                            <img src="assets/img/priyanka.jpg" class="img-fluid full-wide" alt="bollywood-life">
                            <label class="duration"></label>
                            <div class="img-celeb">
                                <img src="assets/img/Amir-celeb.jpg" class="img-fluid" alt="Amir Khan" title="Amir Khan">
                            </div>
                        </a>

                        <div class="card-footer">
                            <label>Akshay Jain</label>
                            <h5>When Bhumi Pednekar stunned us with her simple yet significant style play for Shubh Mangal </h5> 
                            <small>October 30, 2017</small> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="guest-blog">
                    <div class="card">
                        <a href="#"> 
                            <img src="assets/img/jenifer.jpg" class="img-fluid full-wide" alt="bollywood-life">
                            <label class="duration"></label>
                            <div class="img-celeb">
                                <img src="assets/img/Amir-celeb.jpg" class="img-fluid" alt="Amir Khan" title="Amir Khan">
                            </div>
                        </a>

                        <div class="card-footer">
                            <label>Akshay Jain</label>
                            <h5>Jennifer Winget gets a Brigitte Bardot makeover and it's the best house party look for the season </h5> 
                            <small>October 30, 2017</small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="guest-blog">
                    <div class="card">
                        <a href="#">
                            <img src="assets/img/alia-bolly.jpg" class="img-fluid full-wide" alt="bollywood-life">
                            <label class="duration"></label>
                            <div class="img-celeb">
                                <img src="assets/img/Amir-celeb.jpg" class="img-fluid" alt="Amir Khan" title="Amir Khan">
                            </div>
                        </a>

                        <div class="card-footer">
                            <label>Akshay Jain</label>
                            <h5>Alia Bhatt's ravishing black gown at the International Film Festival of India 2017  costs Rs 82,000 </h5> 
                            <small>October 30, 2017</small> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="button-center">
                <a href="#" class="btn btn-default">View all blogs</a>
            </div>
        </div>
    </div>
</section>


<?php @include 'footer.php' ?>


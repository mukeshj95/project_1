<?php @include 'header.php'; ?>

<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="#">Home</a>
                <a href="#" class="active">Box Office List View</a>
            </div>
        </div>
    </div>
</nav>
<article class="box-office-list">
    <div class="container">
        <div class="row">
            <div class="mb-30 xs-text-center">
                <h1 class="d-inline title color-white">box office list</h1> 
                <i class="arrows"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div class="ad-horizontal-674">
            <a href="https://www.flipkart.com/" target="�?_blank�?">
                <img src="assets/img/flipkart-ads.jpg" class="img-fluid" alt="Flipkart Ad">
            </a>
        </div>
    </div>
</article>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12 table-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                          <select class="form-control filter">
                            <option>Filter by</option>
                            <option>This Week</option>
                            <option>This Year</option>
                            <option>Last 3 Months</option>
                            <option>Last 6 Months</option>
                            <option>All of 2017</option>
                            <option>2016</option>
                            <option>2015</option>
                            <option>2014</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <div class="table text-capitalize">
                            <table id="DataTable" class="table table-hover table-bordered shadow full-wide" cellspacing="0">
                                <thead>
                                    <tr>
                                        <td>film</td>
                                        <td>release date</td>
                                        <td>total collection</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                       <td><img src="assets/img/box-office-list-1.jpg">ittefaq</td>
                                        <td>15 Nov 2017</td>
                                        <td>141 cr <a href="#">watch trailer</a></td>
                                    </tr>
                                    <tr>
                                       <td><img src="assets/img/box-office-list-2.jpg">A Gentleman </td>
                                        <td>15 Nov 2017</td>
                                        <td>140 cr <a href="#">watch trailer</a></td>
                                    </tr>
                                    <tr>
                                       <td><img src="assets/img/box-office-list-1.jpg"> My Name Is Khan</td>
                                        <td>15 dec 2017</td>
                                        <td>145 cr <a href="#">watch trailer</a></td>
                                    </tr>
                                    <tr>
                                       <td><img src="assets/img/box-office-list-2.jpg">Qarib Qarib Singlle</td>
                                        <td>15 oct 2017</td>
                                        <td>75 cr <a href="#">watch trailer</a></td>
                                    </tr>
                                    <tr>
                                       <td><img src="assets/img/box-office-list-1.jpg"> Kapoor & Sons </td>
                                        <td>15 jan 2017</td>
                                        <td>50 cr <a href="#">watch trailer</a></td>
                                    </tr>
                                    <tr>
                                       <td> <img src="assets/img/box-office-list-2.jpg">Brothers</td>
                                        <td>15 Nov 2017</td>
                                        <td>45 cr <a href="#">watch trailer</a></td>
                                    </tr>
                                    <tr>
                                       <td> <img src="assets/img/box-office-list-1.jpg">Student of the Year </td>
                                        <td>15 Nov 2017</td>
                                        <td>89 cr <a href="#">watch trailer</a></td>
                                    </tr>
                                    <tr>
                                       <td><img src="assets/img/box-office-list-2.jpg"> Kapoor & Sons </td>
                                        <td>15 Nov 2017</td>
                                        <td>65 cr <a href="#">watch trailer</a></td>
                                    </tr>
                                    <tr>
                                       <td><img src="assets/img/box-office-list-1.jpg">ittefaq</td>
                                        <td>15 Nov 2017</td>
                                        <td>150 cr <a href="#">watch trailer</a></td>
                                    </tr>
                                    <tr>
                                       <td><img src="assets/img/box-office-list-2.jpg">raees</td>
                                        <td>15 Nov 2017</td>
                                        <td>96 cr <a href="#">watch trailer</a></td>
                                    </tr>
                                    <tr>
                                       <td><img src="assets/img/box-office-list-1.jpg">ittefaq</td>
                                        <td>15 Nov 2017</td>
                                        <td>85 cr <a href="#">watch trailer</a></td>
                                    </tr>
                                    <tr>
                                       <td><img src="assets/img/box-office-list-2.jpg">brothers</td>
                                        <td>15 Nov 2017</td>
                                        <td>110 cr <a href="#">watch trailer</a></td>
                                    </tr>
                                    <tr>
                                       <td><img src="assets/img/box-office-list-1.jpg">ittefaq</td>
                                        <td>15 Nov 2017</td>
                                        <td>150 cr<a href="#">watch trailer</a></td>
                                    </tr>
                                    <tr>
                                       <td><img src="assets/img/box-office-list-2.jpg">ittefaq</td>
                                        <td>15 Nov 2017</td>
                                        <td>87 cr <a href="#">watch trailer</a></td>
                                    </tr>
                                    <tr>
                                       <td><img src="assets/img/box-office-list-1.jpg">A Gentleman </td>
                                        <td>15 Nov 2017</td>
                                        <td>200 cr <a href="#">watch trailer</a></td>
                                    </tr>
                                    <tr>
                                       <td><img src="assets/img/box-office-list-2.jpg"> My Name Is Khan</td>
                                        <td>15 Nov 2017</td>
                                        <td>114 cr <a href="#">watch trailer</a></td>
                                    </tr>
                                    <tr>
                                        <td><img src="assets/img/box-office-list-1.jpg">Qarib Qarib Singlle</td>
                                        <td>15 Nov 2017</td>
                                        <td>65 cr <a href="#">watch trailer</a></td>
                                    </tr>
                                </tbody>
                            </table>                            
                        </div>
                        <div class="clear"></div>
                        <p class="mt-20">Disclaimer : Bollywood bubble does not claim these numbers to be accurate, this has been accumulated from other sources. </p>
                    </div>
                </div>
                <div class="bottom-btn">
                    <a href="box-office.php" class="btn-round text-capitalize btn-default">Back to Box Office Report</a>
                </div>                
            </div>
        </div>
    </div>
</section>


<?php @include 'footer.php'; ?>



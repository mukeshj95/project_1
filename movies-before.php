<?php @include 'header.php'; ?>

<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="#">Movie</a>
                <a href="#" class="active">Golmal Again</a>
            </div>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="banner-movie">
            <a href="#">
                <i class="ion-play"></i>
            </a>
            <div class="movie-img"></div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-3 col-xs-12">
            <div class="movie-image">
                <img src="assets/img/golmal-movie.jpg" alt="Golmal" title="Golmal" class="img-fluid full-wide">
            </div>
        </div>
        <div class="col-md-12 col-lg-9 col-xs-12">
            <div class="movie-details">
                <h1>Golmaal Again</h1>
                <p><span>U/A</span>Hindi</p>
                <p><span><i class="ion-clock"></i></span>2 hr 50 mins</p>
                <p><span><i class="ion-ios-calendar-outline"></i></span>19th May</p>

                <p><span><i class="icon"><img src="assets/img/genre-icon.png" class="img-fluid" alt="genre-icon"></i></span>Comedy drama</p>
                <div class="icon-box float-right xs-hidden">
                    <ul>
                        <li class="bg-fb">
                            <a href="#">
                                <i class="ion-social-facebook text-white"></i>
                            </a>
                        </li>
                        <li class="bg-twitter">
                            <a href="#">
                                <i class="ion-social-twitter text-white"></i>
                            </a>
                        </li>
                        <li class="bg-google">
                            <a href="#">
                                <i class="ion-social-googleplus text-white"></i>
                            </a>
                        </li>
                        <li class="bg-youtube">
                            <a href="#" class="heart">
                                <i class="ion-ios-heart-outline text-white"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="icon-box float-right md-hide">
                    <ul>
                        <li>
                            <i class="ion-android-share-alt fs-2"></i>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="ratings">
                <div class="col-md-5 col-sm-12 d-flex">
                    <div class="rating">
                        <div id="rateYo"></div>
                        <!--                        <label id="value">3.4</label>-->
                        <span>Your Rating</span>
                    </div>
                    <div class="rating">
                        <div id="rateYo2"></div>
                        <!--                        <label id="value2">3.4</label>-->
                        <span>BB Rating</span>
                    </div>
                </div>
                <div class="col-md-7 col-sm-12">
                    <!--<div>
                    <a href="#" class="btn btn-round btn-dislike" title="Won't Watch"><i class="ion-thumbsdown"></i>won't watch <span>(4)</span></a>
                    <a href="#" class="btn btn-round btn-like" title="Will Watch"><i class="ion-thumbsup"></i>will watch <span>(50)</span></a>
                    <a href="#" class="like-icon"><i class="ion-thumbsup"></i><label>90%</label></a>
                    </div>-->

                    <!--  --------   user case 1  ------------->
                    <div class="prediction">
                        <div class="float-left">
                            <h6>Predictometer
                            </h6>
                            <div id="slider-range-min">
                                <div class="ui-slider-handle"><span id="custom-handle" class="shadow"></span></div><small>10cr</small><small>150cr</small><small>300cr</small><small>350+cr</small></div>
                        </div>
                        <div class="float-right">
                            <div class="button-right" id="submit_result">
                                <label>Thank you</label><br />
                                <label>Avg <span>250 cr</span></label>
                            </div>
                            <div class="button-right">
                                <button type="submit" class="btn-submit btn" onsubmit=""  id="submit_pre">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<article class="pb-0">
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="tabs-main tabsUrl col-lg-12" id="r-tabs">
                        <ul class="nav nav-tabs" id="movies-tabs" role="tablist">
                            <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#movieOverview" role="tab" aria-controls="movieOverview">Plot</a> </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#castCrewList" role="tab" aria-controls="castCrewList">Cast & Crew</a> </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#moviePhoto" role="tab" aria-controls="moviePhoto">Photos</a> </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#movieVideos" role="tab" aria-controls="movieVideos">Videos</a> </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#movieReview" role="tab" aria-controls="movieReview">Movie Review</a> </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#movieBoxoffice" role="tab" aria-controls="movieBoxoffice">Box Office</a> </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="tab-content col-md-12 p-0">
                <div class="tab-pane active" id="movieOverview" role="tabpanel">
                    <article>
                        <div class="container">
                            <div class="row">
                                <div class="mb-20 col-md-12 xs-text-center">
                                    <h2 class="d-inline title uppercase">synopsis</h2>
                                    <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                                </div>
                                <div class="interview col-md-12">
                                    <p>Golmaal Again is a fun filled ride about two gangs who are unable to stand each other since their childhood and how they repulse each other even after they grow up. Golmaal Again is just another hilarious adventure with its fair share of thrills & action that are sure to...</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="bg-light-red">
                        <div class="container">
                            <div class="mb-20 xs-text-center">
                                <h2 class="d-inline title uppercase">videos</h2>
                                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                            </div>
                            <div class="row" data-plugin="matchHeight" data-by-row="true">
                                <div class="col-md-4 col-xs-12">
                                    <div class="card">
                                        <a href="#">
                                            <div class="card-link">
                                                <div class="video-content">
                                                    <img src="assets/img/movie-video-1.jpg" alt="video-link" class="img-fluid">
                                                    <label class="duration">15:22</label>
                                                </div>
                                                <div class="video-sub">
                                                    <h5>The Golmaal Gang Pranks Parineeti</h5>
                                                    <small class="text-muted">October 21, 2017</small>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="card">
                                        <a href="#">
                                            <div class="card-link">
                                                <div class="video-content">
                                                    <img src="assets/img/movie-video.jpg" alt="video-link" class="img-fluid">
                                                    <label class="duration">15:22</label>
                                                </div>
                                                <div class="video-sub">
                                                    <h5>Title song of Golmaal Again was shot in 40 degrees</h5>
                                                    <small class="text-muted">October 21, 2017</small>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="card">
                                        <a href="#">
                                            <div class="card-link">
                                                <div class="video-content">
                                                    <img src="assets/img/movie-video-3.jpg" alt="video-link" class="img-fluid">
                                                    <label class="duration">15:22</label>
                                                </div>
                                                <div class="video-sub">
                                                    <h5>Golmaal Again | Trailer decode</h5>
                                                    <small class="text-muted">October 21, 2017</small>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article>
                        <div class="container">
                            <div class="box-office-details movies full-wide">
                                <div class="row" data-plugin="matchHeight" data-by-row="true">

                                    <div class="col-md-6">
                                        <div class="row collections">
                                            <div class="full-wide">
                                                <h2 class="title">Box Office</h2><i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                                            </div>
                                            <div class="dayCollection">
                                                <h3><span><img src="assets/img/rupee-icon.png" alt="Rupees" title="Rs" class="img-fluid"></span>9.450 Cr</h3>
                                                <label>first day collection</label>
                                            </div>
                                            <div class="dayCollection">
                                                <h3><span><img src="assets/img/rupee-icon.png" alt="Rupees" title="Rs" class="img-fluid"></span>92.450 Cr</h3>
                                                <label>total collection</label>
                                            </div>
                                            <div class="dayCollection collections text-center">
                                                <div class="button">
                                                    <a href="#" class="btn btn-sm btn-success">Hit</a>
                                                    <label>Box Office Verdict</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 offset-md-2">
                                        <div class="totalCollection banner">
                                            <div class="collection-img">
                                                <img src="assets/img/golmal-movie-c.jpg" alt="Movie" title="Golmaal" class="img-fluid">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article>
                        <div class="container">
                            <div class="col-md-12 border-primary bg-white">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <h2 class="artist bg-light-red sideTitle uppercase font-32 mb-20">photos</h2>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-12 col-xs-6" id="order2">
                                        <div class="photo-list">
                                            <a href="#" class="d-block">
                                                <div class="photo-link">
                                                    <img src="assets/img/movie-e.jpg" alt="BB Special" class="img-fluid">
                                                    <label for="" class="numbers">10</label>
                                                    <button>features</button>
                                                </div>
                                                <h3 class="text-left">Parineeti Chopra walks on fire during the  promotions..</h3>
                                            </a>
                                            <small>October 21, 2017</small>
                                        </div>
                                        <div class="photo-list">
                                            <a href="#" class="d-block">
                                                <div class="photo-link">
                                                    <img src="assets/img/movie-e-2.jpg" alt="BB Special" class="img-fluid">
                                                    <label for="" class="numbers">10</label>
                                                    <button>events</button>
                                                </div>
                                                <h3 class="text-left">Ajay Devgn, team promote 'Golmaal Again' in ekta kap...</h3>
                                            </a>
                                            <small>October 21, 2017</small>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12" id="order1">
                                        <div class="photo-list xs-text-center">
                                            <a href="#" class="d-block">
                                                <div class="photo-link">
                                                    <img src="assets/img/movie-e-1.jpg" alt="main-video" class="img-fluid">
                                                    <label for="" class="numbers">10</label>
                                                    <button>spotted</button>
                                                </div>
                                                <h2 class="font-wt-300 pt-3">Arshad Warsi Holds A Special Screening Of Golmaal Again For KIDS</h2>
                                            </a>
                                            <small>October 21, 2017</small>
                                            <h6 class="xs-hidden">Shraddha Kapoor will next be seen in Apoorva Lakhia’s ‘Haseena Parkar’ which is slated to hit the screens on September 22, 2017. Recently, a special screening of the film took place which was attended by Shraddha...</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-6" id="order3">
                                        <div class="right-side">
                                            <div class="photo-list">
                                                <a href="#" class="d-block">
                                                    <div class="photo-link">
                                                        <img src="assets/img/movie-e-3.jpg" alt="BB Special" class="img-fluid">
                                                        <label for="" class="numbers">10</label>
                                                        <button>spotted</button>
                                                    </div>
                                                    <h3 class="text-left">Golmaal team In The Great Indian Laughter Challenge...</h3>
                                                </a>
                                                <small>October 21, 2017</small>
                                            </div>
                                            <div class="photo-list">
                                                <a href="#" class="d-block">
                                                    <div class="photo-link">
                                                        <img src="assets/img/movie-e-4.jpg" alt="BB Special" class="img-fluid">
                                                        <label for="" class="numbers">10</label>
                                                        <button>spotted</button>
                                                    </div>
                                                    <h3 class="text-left">Ranbir Kapoor, Aadar Jain get clicked during...</h3>
                                                </a>
                                                <small>October 21, 2017</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="cast-bg">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-20 xs-text-center">
                                        <h2 class="d-inline title uppercase">cast & crew</h2>
                                        <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="row" data-plugin="matchHeight" data-by-row="true">
                                <div class="col-md-12">
                                    <div class="owl-carousel" id="castCrew">
                                        <div class="item">
                                            <a href="#">
                                                <div class="image">
                                                    <img src="assets/img/cast-1.jpg" alt="Cast" title="Ajay" class="img-fluid">
                                                </div>
                                                <h3>Ajay Devgan</h3>
                                                <label>Actor</label>
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="#">
                                                <div class="image">
                                                    <img src="assets/img/cast-2.jpg" alt="Cast" title="Ajay" class="img-fluid">
                                                </div>
                                                <h3>Parineeti Chopra</h3>
                                                <label>Actor</label>
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="#">
                                                <div class="image">
                                                    <img src="assets/img/cast-3.jpg" alt="Cast" title="Ajay" class="img-fluid">
                                                </div>
                                                <h3>Kunal Khemu</h3>
                                                <label>Actor</label>
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="#">
                                                <div class="image">
                                                    <img src="assets/img/cast-4.jpg" alt="Cast" title="Ajay" class="img-fluid">
                                                </div>
                                                <h3>Ajay Devgon</h3>
                                                <label>Actor</label>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <section>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-20 xs-text-center">
                                        <h2 class="d-inline title uppercase">calendar</h2>
                                        <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="calendarBox">
                                <div class="row">
                                    <div class="col-md-12 col-lg-4">
                                        <input type="text" id="datepicker" class="form-control" value="17 December">
                                        <i class="ion-ios-arrow-down"></i>
                                        <label for="datepicker"></label>
                                    </div>
                                    <div class="col-md-12 col-lg-8">
                                        <div class="tabs-main calendar-days" id="calendar-days">
                                            <ul class="nav nav-tabs" id="" role="tablist">
                                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#day1" role="tab" aria-controls="day1">17 <br /> wed</a> </li>
                                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#day2" role="tab" aria-controls="day2">18 <br /> thu</a> </li>
                                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#day3" role="tab" aria-controls="day3">19 <br /> fri</a> </li>
                                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#day4" role="tab" aria-controls="day4">20 <br /> sat</a> </li>
                                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#day5" role="tab" aria-controls="day5">21 <br /> sun</a> </li>
                                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#day6" role="tab" aria-controls="day6">22 <br /> mon</a> </li>
                                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#day7" role="tab" aria-controls="day7">22 <br /> tue</a> </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="day1" role="tabpanel">
                                            <div class="row" data-plugin="matchHeight" data-by-row="true">
                                                <div class="col-md-6 col-lg-4 col-xs-12">
                                                    <div class="card birthday">
                                                        <div class="header">
                                                            <h2 class="title">todays birthday</h2>
                                                            <span><a href="#" title="set as reminder" ><i class="ion-android-notifications-none"></i></a></span>
                                                        </div>
                                                        <div class="list">
                                                            <p><img src="assets/img/birthday-1.jpg" alt="Birthday" title="Ritesh"><label>Ritesh Deshmukh</label></p>
                                                            <p><img src="assets/img/birthday-2.jpg" alt="Birthday" title="John"><label>John Abraham</label></p>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-4 col-xs-12">
                                                    <div class="card photo-list">
                                                        <a href="#">
                                                            <div class="card-link">
                                                                <div class="photo-link">
                                                                    <img src="assets/img/calendar-1.jpg" alt="video-link" class="img-fluid">
                                                                    <label for="">11</label>
                                                                    <button>stories</button>
                                                                </div>
                                                                <div class="video-sub">
                                                                    <h5>The Golmaal Gang Pranks Parineeti</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-4 col-xs-12">
                                                    <div class="card">
                                                        <a href="#">
                                                            <div class="card-link">
                                                                <div class="video-content">
                                                                    <img src="assets/img/calendar-2.jpg" alt="video-link" class="img-fluid">
                                                                    <label class="duration">15:22</label>
                                                                </div>
                                                                <div class="video-sub">
                                                                    <h5>The Golmaal Gang Pranks Parineeti</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-4 col-xs-12">
                                                    <div class="card photo-list">
                                                        <a href="#">
                                                            <div class="card-link">
                                                                <div class="photo-link">
                                                                    <img src="assets/img/calendar-3.jpg" alt="video-link" class="img-fluid">
                                                                    <label for="">11</label>
                                                                    <button>stories</button>
                                                                </div>
                                                                <div class="video-sub">
                                                                    <h5>The Golmaal Gang Pranks Parineeti</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-4 col-xs-12">
                                                    <div class="card photo-list">
                                                        <a href="#">
                                                            <div class="card-link">
                                                                <div class="photo-link">
                                                                    <img src="assets/img/calendar-4.jpg" alt="video-link" class="img-fluid">
                                                                    <label for="">11</label>
                                                                    <button>stories</button>
                                                                </div>
                                                                <div class="video-sub">
                                                                    <h5>The Golmaal Gang Pranks Parineeti</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-4 col-xs-12">
                                                    <div class="card">
                                                        <a href="#">
                                                            <div class="card-link">
                                                                <div class="video-content">
                                                                    <img src="assets/img/calendar-2.jpg" alt="video-link" class="img-fluid">
                                                                    <label class="duration">15:22</label>
                                                                </div>
                                                                <div class="video-sub">
                                                                    <h5>The Golmaal Gang Pranks Parineeti</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="day2" role="tabpanel">
                                            <div class="row" data-plugin="matchHeight" data-by-row="true">
                                                <div class="col-md-6 col-lg-4 col-xs-12">
                                                    <div class="card">
                                                        <a href="#">
                                                            <div class="card-link">
                                                                <div class="video-content">
                                                                    <img src="assets/img/movie-video-1.jpg" alt="video-link" class="img-fluid">
                                                                    <label class="duration">15:22</label>
                                                                </div>
                                                                <div class="video-sub">
                                                                    <h5>The Golmaal Gang Pranks Parineeti</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-4 col-xs-12">
                                                    <div class="card photo-list">
                                                        <a href="#">
                                                            <div class="card-link">
                                                                <div class="photo-link">
                                                                    <img src="assets/img/calendar-3.jpg" alt="video-link" class="img-fluid">
                                                                    <label for="">11</label>
                                                                    <button>stories</button>
                                                                </div>
                                                                <div class="video-sub">
                                                                    <h5>The Golmaal Gang Pranks Parineeti</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-4 col-xs-12">
                                                    <div class="card">
                                                        <a href="#">
                                                            <div class="card-link">
                                                                <div class="video-content">
                                                                    <img src="assets/img/movie-video-1.jpg" alt="video-link" class="img-fluid">
                                                                    <label class="duration">15:22</label>
                                                                </div>
                                                                <div class="video-sub">
                                                                    <h5>The Golmaal Gang Pranks Parineeti</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="day3" role="tabpanel">
                                            <div class="row" data-plugin="matchHeight" data-by-row="true">
                                                <div class="col-md-6 col-lg-4 col-xs-12">
                                                    <div class="card">
                                                        <a href="#">
                                                            <div class="card-link">
                                                                <div class="video-content">
                                                                    <img src="assets/img/movie-video-1.jpg" alt="video-link" class="img-fluid">
                                                                    <label class="duration">15:22</label>
                                                                </div>
                                                                <div class="video-sub">
                                                                    <h5>The Golmaal Gang Pranks Parineeti</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-4 col-xs-12">
                                                    <div class="card photo-list">
                                                        <a href="#">
                                                            <div class="card-link">
                                                                <div class="photo-link">
                                                                    <img src="assets/img/calendar-1.jpg" alt="video-link" class="img-fluid">
                                                                    <label for="">11</label>
                                                                    <button>stories</button>
                                                                </div>
                                                                <div class="video-sub">
                                                                    <h5>The Golmaal Gang Pranks Parineeti</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-4 col-xs-12">
                                                    <div class="card">
                                                        <a href="#">
                                                            <div class="card-link">
                                                                <div class="video-content">
                                                                    <img src="assets/img/movie-video-1.jpg" alt="video-link" class="img-fluid">
                                                                    <label class="duration">15:22</label>
                                                                </div>
                                                                <div class="video-sub">
                                                                    <h5>The Golmaal Gang Pranks Parineeti</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="day4" role="tabpanel">
                                            <div class="row" data-plugin="matchHeight" data-by-row="true">
                                                <div class="col-md-6 col-lg-4 col-xs-12">
                                                    <div class="card photo-list">
                                                        <a href="#">
                                                            <div class="card-link">
                                                                <div class="photo-link">
                                                                    <img src="assets/img/calendar-1.jpg" alt="video-link" class="img-fluid">
                                                                    <label for="">11</label>
                                                                    <button>stories</button>
                                                                </div>
                                                                <div class="video-sub">
                                                                    <h5>The Golmaal Gang Pranks Parineeti</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-4 col-xs-12">
                                                    <div class="card">
                                                        <a href="#">
                                                            <div class="card-link">
                                                                <div class="video-content">
                                                                    <img src="assets/img/calendar-3.jpg" alt="video-link" class="img-fluid">
                                                                    <label class="duration">15:22</label>
                                                                </div>
                                                                <div class="video-sub">
                                                                    <h5>The Golmaal Gang Pranks Parineeti</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-4 col-xs-12">
                                                    <div class="card photo-list">
                                                        <a href="#">
                                                            <div class="card-link">
                                                                <div class="photo-link">
                                                                    <img src="assets/img/calendar-1.jpg" alt="video-link" class="img-fluid">
                                                                    <label for="">11</label>
                                                                    <button>stories</button>
                                                                </div>
                                                                <div class="video-sub">
                                                                    <h5>The Golmaal Gang Pranks Parineeti</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="day5" role="tabpanel">
                                            <div class="row" data-plugin="matchHeight" data-by-row="true">
                                                <div class="col-md-6 col-lg-4 col-xs-12">
                                                    <div class="card">
                                                        <a href="#">
                                                            <div class="card-link">
                                                                <div class="video-content">
                                                                    <img src="assets/img/calendar-2.jpg" alt="video-link" class="img-fluid">
                                                                    <label class="duration">15:22</label>
                                                                </div>
                                                                <div class="video-sub">
                                                                    <h5>The Golmaal Gang Pranks Parineeti</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-4 col-xs-12">
                                                    <div class="card photo-list">
                                                        <a href="#">
                                                            <div class="card-link">
                                                                <div class="photo-link">
                                                                    <img src="assets/img/calendar-4.jpg" alt="video-link" class="img-fluid">
                                                                    <label for="">11</label>
                                                                    <button>stories</button>
                                                                </div>
                                                                <div class="video-sub">
                                                                    <h5>The Golmaal Gang Pranks Parineeti</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-4 col-xs-12">
                                                    <div class="card">
                                                        <a href="#">
                                                            <div class="card-link">
                                                                <div class="video-content">
                                                                    <img src="assets/img/calendar-1.jpg" alt="video-link" class="img-fluid">
                                                                    <label class="duration">15:22</label>
                                                                </div>
                                                                <div class="video-sub">
                                                                    <h5>The Golmaal Gang Pranks Parineeti</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="day6" role="tabpanel">
                                            <div class="row" data-plugin="matchHeight" data-by-row="true">
                                                <div class="col-md-6 col-lg-4 col-xs-12">
                                                    <div class="card">
                                                        <a href="#">
                                                            <div class="card-link">
                                                                <div class="video-content">
                                                                    <img src="assets/img/calendar-3.jpg" alt="video-link" class="img-fluid">
                                                                    <label class="duration">15:22</label>
                                                                </div>
                                                                <div class="video-sub">
                                                                    <h5>The Golmaal Gang Pranks Parineeti</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-4 col-xs-12">
                                                    <div class="card photo-list">
                                                        <a href="#">
                                                            <div class="card-link">
                                                                <div class="photo-link">
                                                                    <img src="assets/img/calendar-3.jpg" alt="video-link" class="img-fluid">
                                                                    <label for="">11</label>
                                                                    <button>stories</button>
                                                                </div>
                                                                <div class="video-sub">
                                                                    <h5>The Golmaal Gang Pranks Parineeti</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-4 col-xs-12">
                                                    <div class="card">
                                                        <a href="#">
                                                            <div class="card-link">
                                                                <div class="video-content">
                                                                    <img src="assets/img/calendar-3.jpg" alt="video-link" class="img-fluid">
                                                                    <label class="duration">15:22</label>
                                                                </div>
                                                                <div class="video-sub">
                                                                    <h5>The Golmaal Gang Pranks Parineeti</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="day7" role="tabpanel">
                                            <div class="row" data-plugin="matchHeight" data-by-row="true">
                                                <div class="col-md-6 col-lg-4 col-xs-12">
                                                    <div class="card">
                                                        <a href="#">
                                                            <div class="card-link">
                                                                <div class="video-content">
                                                                    <img src="assets/img/movie-video-1.jpg" alt="video-link" class="img-fluid">
                                                                    <label class="duration">15:22</label>
                                                                </div>
                                                                <div class="video-sub">
                                                                    <h5>The Golmaal Gang Pranks Parineeti</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-4 col-xs-12">
                                                    <div class="card photo-list">
                                                        <a href="#">
                                                            <div class="card-link">
                                                                <div class="photo-link">
                                                                    <img src="assets/img/calendar-4.jpg" alt="video-link" class="img-fluid">
                                                                    <label for="">11</label>
                                                                    <button>stories</button>
                                                                </div>
                                                                <div class="video-sub">
                                                                    <h5>The Golmaal Gang Pranks Parineeti</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-4 col-xs-12">
                                                    <div class="card">
                                                        <a href="#">
                                                            <div class="card-link">
                                                                <div class="video-content">
                                                                    <img src="assets/img/movie-video-1.jpg" alt="video-link" class="img-fluid">
                                                                    <label class="duration">15:22</label>
                                                                </div>
                                                                <div class="video-sub">
                                                                    <h5>The Golmaal Gang Pranks Parineeti</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="tab-pane" id="castCrewList" role="tabpanel">
                    <article class="cast-bg">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-20 xs-text-center">
                                        <h2 class="d-inline title uppercase">cast</h2>
                                        <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="row" data-plugin="matchHeight" data-by-row="true">
                                <div class="col-md-12">
                                    <div class="owl-carousel CrewList" id="CastList">
                                        <div class="item">
                                            <a href="#">
                                                <div class="image">
                                                    <img src="assets/img/cast-1.jpg" alt="Cast" title="Ajay" class="img-fluid">
                                                </div>
                                                <h3>Ajay Devgan</h3>
                                                <label>Actor</label>
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="#">
                                                <div class="image">
                                                    <img src="assets/img/cast-2.jpg" alt="Cast" title="Ajay" class="img-fluid">
                                                </div>
                                                <h3>Parineeti Chopra</h3>
                                                <label>Actor</label>
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="#">
                                                <div class="image">
                                                    <img src="assets/img/cast-3.jpg" alt="Cast" title="Ajay" class="img-fluid">
                                                </div>
                                                <h3>Kunal Khemu</h3>
                                                <label>Actor</label>
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="#">
                                                <div class="image">
                                                    <img src="assets/img/cast-4.jpg" alt="Cast" title="Ajay" class="img-fluid">
                                                </div>
                                                <h3>Ajay Devgon</h3>
                                                <label>Actor</label>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <section>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-20 xs-text-center">
                                        <h2 class="d-inline title uppercase">crew</h2>
                                        <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="row" data-plugin="matchHeight" data-by-row="true">
                                <div class="col-md-12">
                                    <div class="owl-carousel CrewList" id="CrewList">
                                        <div class="item">
                                            <a href="#">
                                                <div class="image">
                                                    <img src="assets/img/cast-1.jpg" alt="Cast" title="Ajay" class="img-fluid">
                                                </div>
                                                <h3>Ajay Devgan</h3>
                                                <label>Musician</label>
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="#">
                                                <div class="image">
                                                    <img src="assets/img/cast-2.jpg" alt="Cast" title="Ajay" class="img-fluid">
                                                </div>
                                                <h3>Parineeti Chopra</h3>
                                                <label>Director</label>
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="#">
                                                <div class="image">
                                                    <img src="assets/img/cast-3.jpg" alt="Cast" title="Ajay" class="img-fluid">
                                                </div>
                                                <h3>Kunal Khemu</h3>
                                                <label>Actor</label>
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="#">
                                                <div class="image">
                                                    <img src="assets/img/cast-4.jpg" alt="Cast" title="Ajay" class="img-fluid">
                                                </div>
                                                <h3>Ajay Devgon</h3>
                                                <label>Actor</label>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="tab-pane" id="moviePhoto" role="tabpanel">
                    <article>
                        <div class="container">
                            <div class="col-md-12 border-primary bg-white">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <h2 class="artist bg-light-red sideTitle uppercase font-32 mb-20">photos</h2>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-12 col-xs-6" id="order2">
                                        <div class="photo-list">
                                            <a href="#" class="d-block">
                                                <div class="photo-link">
                                                    <img src="assets/img/movie-e.jpg" alt="BB Special" class="img-fluid">
                                                    <label for="" class="numbers">10</label>
                                                    <button>features</button>
                                                </div>
                                                <h3 class="text-left">Parineeti Chopra walks on fire during the  promotions..</h3>
                                            </a>
                                            <small>October 21, 2017</small>
                                        </div>
                                        <div class="photo-list">
                                            <a href="#" class="d-block">
                                                <div class="photo-link">
                                                    <img src="assets/img/movie-e-2.jpg" alt="BB Special" class="img-fluid">
                                                    <label for="" class="numbers">10</label>
                                                    <button>events</button>
                                                </div>
                                                <h3 class="text-left">Ajay Devgn, team promote 'Golmaal Again' in ekta kap...</h3>
                                            </a>
                                            <small>October 21, 2017</small>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12" id="order1">
                                        <div class="photo-list xs-text-center">
                                            <a href="#" class="d-block">
                                                <div class="photo-link">
                                                    <img src="assets/img/movie-e-1.jpg" alt="main-video" class="img-fluid">
                                                    <label for="" class="numbers">10</label>
                                                    <button>spotted</button>
                                                </div>
                                                <h2 class="font-wt-300 pt-3">Arshad Warsi Holds A Special Screening Of Golmaal Again For KIDS</h2>
                                            </a>
                                            <small>October 21, 2017</small>
                                            <h6 class="xs-hidden">Shraddha Kapoor will next be seen in Apoorva Lakhia’s ‘Haseena Parkar’ which is slated to hit the screens on September 22, 2017. Recently, a special screening of the film took place which was attended by Shraddha...</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-6" id="order3">
                                        <div class="right-side">
                                            <div class="photo-list">
                                                <a href="#" class="d-block">
                                                    <div class="photo-link">
                                                        <img src="assets/img/movie-e-3.jpg" alt="BB Special" class="img-fluid">
                                                        <label for="" class="numbers">10</label>
                                                        <button>spotted</button>
                                                    </div>
                                                    <h3 class="text-left">Golmaal team In The Great Indian Laughter Challenge...</h3>
                                                </a>
                                                <small>October 21, 2017</small>
                                            </div>
                                            <div class="photo-list">
                                                <a href="#" class="d-block">
                                                    <div class="photo-link">
                                                        <img src="assets/img/movie-e-4.jpg" alt="BB Special" class="img-fluid">
                                                        <label for="" class="numbers">10</label>
                                                        <button>spotted</button>
                                                    </div>
                                                    <h3 class="text-left">Ranbir Kapoor, Aadar Jain get clicked during...</h3>
                                                </a>
                                                <small>October 21, 2017</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <section class="bg-grey">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 mb-30 xs-text-center">
                                    <h2 class="d-inline title uppercase font-32">spotted</h2>
                                    <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                                </div>
                            </div>
                            <div class="row" data-plugin="matchHeight" data-by-row="true">
                                <div class="col-md-4 col-xs-12 float-left">
                                    <div class="card">
                                        <a href="#" class="p-relative">
                                            <img src="assets/img/movie-photo-1.jpg" alt="video-link" class="img-fluid">
                                            <label for="" class="numbers">10</label>
                                        </a>
                                        <div class="card-title text-center">
                                            <h3 class="font-22 font-wt-400 font-black mb-10">Photo: Sushant Singh Rajput will make you blush with his charm</h3>
                                            <small class="font-light font-wt-400">October 21, 2017</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12 float-left">
                                    <div class="card photo">
                                        <a href="#" class="p-relative">
                                            <img src="assets/img/movie-photo-2.jpg" alt="video-link" class="img-fluid">
                                            <label for="" class="numbers">10</label>
                                        </a>
                                        <div class="card-title text-center">
                                            <h3 class="font-22 font-wt-400 font-black mb-10">Arjun Kapoor and Parineeti Chopra start shooting...</h3>
                                            <small class="font-light font-wt-400">October 21, 2017</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12 float-left">
                                    <div class="card photo">
                                        <a href="#" class="p-relative">
                                            <img src="assets/img/movie-photo-3.jpg" alt="video-link" class="img-fluid">
                                            <label for="" class="numbers">10</label>
                                        </a>
                                        <div class="card-title text-center">
                                            <h3 class="font-22 font-wt-400 font-black mb-10">Who is Amir Khan? The boxing world champ... </h3>
                                            <small class="font-light font-wt-400">October 21, 2017</small>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row xs-hidden" data-plugin="matchHeight" data-by-row="true">
                                <div class="col-md-4 col-xs-12 float-left">
                                    <div class="card photo">
                                        <a href="#" class="p-relative">
                                            <img src="assets/img/movie-photo-4.jpg" alt="video-link" class="img-fluid">
                                            <label for="" class="numbers">10</label>
                                        </a>
                                        <div class="card-title text-center">
                                            <h3 class="font-22 font-wt-400 font-black mb-10">WOW! Zaira Wasim receives National Child Award from President...</h3>
                                            <small class="font-light font-wt-400">October 21, 2017</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12 float-left">
                                    <div class="card photo">
                                        <a href="#" class="p-relative">
                                            <img src="assets/img/movie-photo-5.jpg" alt="video-link" class="img-fluid">
                                            <label for="" class="numbers">10</label>
                                        </a>
                                        <div class="card-title text-center">
                                            <h3 class="font-22 font-wt-400 font-black mb-10">Irrfan Khan plays a Bengali writer inspired by Humayun Ahmed in Mostofa Farooki’s next</h3>
                                            <small class="font-light font-wt-400">October 21, 2017</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12 float-left">
                                    <div class="card photo">
                                        <a href="#" class="p-relative">
                                            <img src="assets/img/movie-photo-6.jpg" alt="video-link" class="img-fluid">
                                            <label for="" class="numbers">10</label>
                                        </a>
                                        <div class="card-title text-center">
                                            <h3 class="font-22 font-wt-400 font-black mb-10">Aditya Roy Kapur, Shraddha Kapoor, Gurmeet Choudhary spotted after </h3>
                                            <small class="font-light font-wt-400">October 21, 2017</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="button-center">
                                <a href="#" class="btn btn-default">View all bb specials</a>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="tab-pane" id="movieVideos" role="tabpanel">
                    <article>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-4 text-center">
                                        <i class="arrowsSub"><img src="assets/img/left-bar.png"></i>
                                        <h1 class="uppercase title d-inline-b">videos</h1>
                                        <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                                    </div>
                                    <div class="height60">
                                        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 float-left">
                                            <a href="https://www.youtube.com/" class="video-link">
                                                <img src="assets/img/video-top-icon.png" class="img-fluid antenna">
                                                <img src="assets/img/video-play.jpg" class="img-fluid" alt="video-player" title="player">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <section class="video-details">
                        <div class="container">
                            <div class="row">
                                <div class="mt-3">
                                    <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12">
                                        <h4>Irrfan Khan plays a Bengali writer inspired by Humayun Ahmed in Mostofa Farooki’s next</h4>
                                    </div>
                                    <div class="tags col-lg-9 col-md-9 col-sm-6 col-xs-12 float-left">
                                        <ul class="d-block">
                                            <li class="float-left mr-2">
                                                <a href="#" class="font-wt-500 p-1 primary-color d-inline-b bg-light-red fs-12">Lifestyle</a>
                                            </li>
                                            <li class="float-left mr-2">
                                                <a href="#" class="font-wt-500 p-1 primary-color d-inline-b bg-light-red fs-12">Fashion</a>
                                            </li>
                                            <li class="float-left mr-2">
                                                <a href="#" class="font-wt-500 p-1 primary-color d-inline-b bg-light-red fs-12">Spotted</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="share-buttons">
                                        <div class="icon-box float-right">
                                            <i class="ion-android-share-alt fs-2"></i>
                                        </div>
                                    </div>
                                    <div class="d-inline-b col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                        <p class="text-muted d-inline-b">By <span class="primary-color font-wt-500">Prajakta Ajgaonkar </span>on October 27 2017
                                            <i class="ion-eye p-3 font-22"><span class="fs-14 text-muted">1520</span></i>
                                            <i class="ion-ios-heart-outline font-22 p-3"><span class="fs-14 text-muted">120</span></i>
                                            <i class="p-3 ion-chatbubble-working"><span class="fs-14 text-muted">12</span></i></p>
                                        <p class="fs-18 mt-3">Amidst international and Bollywood projects, Irrfan Khan has once again decided to take the unconventional route and has given his nod to Mostofa Farooki’s bilingual No Bed of Roses (in English, Doob in Bengali). Further what we hear is that Irrfan’s character will be inspired by
                                            Bangladeshi writer, dramatist, filmmaker Humayun Ahmed.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <article>
                        <div class="container">
                            <div class="full-wide text-center mb-3">
                                <i class="arrowsSub"><img src="assets/img/left-bar.png"></i>
                                <h2 class="d-inline title text-center uppercase">related videos</h2>
                                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row" data-plugin="matchHeight" data-by-row="true">
                                <div class="col-md-4 col-xs-12">
                                    <div class="card">
                                        <a href="#">
                                            <div class="card-link">
                                                <div class="video-content">
                                                    <img src="assets/img/video-celeb-1.jpg" alt="video-link" class="img-fluid full-wide">
                                                    <label class="duration">15:22</label>
                                                </div>
                                                <div class="video-sub">
                                                    <h5>Ittefaq Box Office Collection Day 4: Sonakshi Sinha And Sidharth Malhotra's...</h5>
                                                    <small class="text-muted">October 21, 2017</small>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="card">
                                        <a href="#">
                                            <div class="card-link">
                                                <div class="video-content">
                                                    <img src="assets/img/video-celeb-2.jpg" alt="video-link" class="img-fluid full-wide">
                                                    <label class="duration">15:22</label>
                                                </div>
                                                <div class="video-sub">
                                                    <h5>Alia Bhatt And Sidharth Malhotra To Star In Sadak's Sequel: Reports</h5>
                                                    <small class="text-muted">October 21, 2017</small>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="card">
                                        <a href="#">
                                            <div class="card-link">
                                                <div class="video-content">
                                                    <img src="assets/img/video-celeb-3.jpg" alt="video-link" class="img-fluid full-wide">
                                                    <label class="duration">15:22</label>
                                                </div>
                                                <div class="video-sub">
                                                    <h5>Sidharth Malhotra Graduates To Bollywood's Big League</h5>
                                                    <small class="text-muted">October 21, 2017</small>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="tab-pane" id="movieReview" role="tabpanel">
                    <article>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-20 xs-text-center">
                                        <h2 class="d-inline title uppercase">Bollywood Bubble Review</h2>
                                        <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                                    </div>

                                    <div class="rating border">
                                        <div class="star-ratings-css">
                                            <label>Bollywood Bubble Rating</label>
                                            <div class="star-ratings-sprite">
                                                <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="review-des">
                                        <h3>laugh and be merry</h3>
                                        <p>Golmaal is the screwball comedy franchise that has been kept alive for the last eleven years. Reruns on satellite channels always make you chuckle, no matter from which point you catch the film. Like its previous instalments, this one too has moments of unadulterated fun. The gags tnate alternate between being truly funny and averagely routine but you find yourself laughing
                                            uncontrollably because each of the actors here is a delight to watch. Whether it's the lisping Lakshman (Shreyas) or the bully Gopal, who is petrified of ghosts, everyone is so proficient, you can't help but smile at their antics. The dialogue is pedestrian but witty. </p>
                                        <p>Of course then you have the additional horror angle thrown in. Drawing inspiration from the South film   Kanchana, where a ghost is out to seek revenge, these parts too are treated
                                            lightheartedly making you guffaw at situations that you may have seen many times over. However, in the second half, when the actual plot is introduced, proceedings get slightly dull.</p>
                                        <p>Here one must say, it is to Rohit Shetty's credit that he never lets you get too fidgety. Gags, fights, songs, giggles, ghosts, here is a buffet you can overdose on. At times, you actually
                                            admonish yourself for being stupid but you can't help but guffaw. As far as performances go, Tabu as the 'exorcist' lends credence to a part, which could have otherwise looked ridiculous. The add-on members � Vasuli Bhai (Mukesh) and Pappi (Johny Lever) have their moments too but Shreyas is the scene-stealer. Parineeti (Khushi/Damini) is just </p>
                                        <p>The add-on members � Vasuli Bhai (Mukesh) and Pappi (Johny Lever) have their moments too but Shreyas is the scene-stealer. Parineeti (Khushi/Damini) is just </p>
                                        <div class="read-more">
                                            <label>Also Read :</label> <br>
                                            <a href="#">'Golmaal Again' box-office collection Day 2</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <div class="xs-text-center">
                                        <h2 class="d-inline title uppercase">user review</h2>
                                        <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="button-right">
                                        <a href="#" class="btn btn-primary">Post Your Review</a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-11">
                                    <div class="float-left">
                                        <p><span class="pr-2"><i class="ion-chatbubble-working"></i></span>5 Reviews</p>
                                    </div>
                                    <div class="float-right">
                                        <div class="form-group fs-14">
                                            <label class="p-2" for="sorting">Sort by</label>
                                            <select class="form-control filter" id="sorting">
                                                <option>Newest</option>
                                                <option>Oldest</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-11 col-xs-12">
                                    <div class="reviews">
                                        <div class="review-image">
                                            <img src="assets/img/comment-1.jpg" alt="User" title="User">
                                        </div>
                                        <div>
                                            <label>Rajat Sharma</label>
                                            <span><i class="ion-ios-clock-outline"></i>Sept 09, 12:50pm</span>
                                            <p>It’s Ajay-Rohit combo Again makes everyone happy perfect Diwali gift Ajay and Golmaal rest Gang rock too with there respective Roles it’s must watch movie.</p>
                                            <div class="rating">
                                                <div class="star-ratings-css">
                                                    <div class="star-ratings-sprite">
                                                        <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="reviews">
                                        <div class="review-image">
                                            <img src="assets/img/comment-2.jpg" alt="User" title="User">
                                        </div>
                                        <div>
                                            <label>Rajat Sharma</label>
                                            <span><i class="ion-ios-clock-outline"></i>Sept 09, 12:50pm</span>
                                            <p>super de bhi uper uper de bhi super duper hit ajey devgan is best in Bollywood industry's gollmaal again is best movies in Diwali super hit super hit.</p>
                                            <div class="rating">
                                                <div class="star-ratings-css">
                                                    <div class="star-ratings-sprite">
                                                        <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="reviews">
                                        <div class="review-image">
                                            <img src="assets/img/comment-2.jpg" alt="User" title="User">
                                        </div>
                                        <div>
                                            <label>Rajat Sharma</label>
                                            <span><i class="ion-ios-clock-outline"></i>Sept 09, 12:50pm</span>
                                            <p>best movie for Diwali with family entertainment watch paid preview fantastic enjoy the movie with Diwali festival Parineeti and Ajay Tushar once again rock</p>
                                            <div class="rating">
                                                <div class="star-ratings-css">
                                                    <div class="star-ratings-sprite">
                                                        <span style="width:68%" class="star-ratings-sprite-rating"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="tab-pane" id="movieBoxoffice" role="tabpanel">
                    <article>
                        <div class="container">
                            <div class="row">
                                <div class="xs-text-center col-md-12">
                                    <h2 class="box uppercase title d-inline-b">box office
                                    </h2>
                                    <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                                </div>
                            </div>
                            <div class="box-office-details">
                                <div class="row" data-plugin="matchHeight" data-by-row="true">
                                    <div class="col-md-12 col-lg-2">
                                        <div class="totalCollection">
                                            <div class="total">
                                                <img src="assets/img/rupee-icon.png" alt="Rupees" title="Rs" class="img-fluid">
                                                <h2>92.450 Cr</h2>
                                                <label>total collection</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-7">
                                        <div class="row collections">
                                            <div class="dayCollection">
                                                <h3><span><img src="assets/img/rupee-icon.png" alt="Rupees" title="Rs" class="img-fluid"></span>92.450 Cr</h3>
                                                <label>first day collection</label>
                                            </div>
                                            <div class="dayCollection">
                                                <h3><span><img src="assets/img/rupee-icon.png" alt="Rupees" title="Rs" class="img-fluid"></span>92.450 Cr</h3>
                                                <label>second day collection</label>
                                            </div>
                                            <div class="dayCollection">
                                                <h3><span><img src="assets/img/rupee-icon.png" alt="Rupees" title="Rs" class="img-fluid"></span>92.450 Cr</h3>
                                                <label>third day collection</label>
                                            </div>
                                        </div>
                                        <div class="row collections">
                                            <div class="dayCollection">
                                                <h3><span><img src="assets/img/rupee-icon.png" alt="Rupees" title="Rs" class="img-fluid"></span>92.450 Cr</h3>
                                                <label>fourth day collection</label>
                                            </div>
                                            <div class="dayCollection">
                                                <h3><span><img src="assets/img/rupee-icon.png" alt="Rupees" title="Rs" class="img-fluid"></span>92.450 Cr</h3>
                                                <label>fifth day collection</label>
                                            </div>
                                            <div class="dayCollection">
                                                <h3><span><img src="assets/img/rupee-icon.png" alt="Rupees" title="Rs" class="img-fluid"></span>92.450 Cr</h3>
                                                <label>sixth day collection</label>
                                            </div>

                                        </div>
                                        <div class="collections text-center">
                                            <div class="button mt-3">
                                                <label class="btn btn-sm btn-success">Hit</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-3">
                                        <div class="totalCollection banner">
                                            <div class="collection-img">
                                                <img src="assets/img/collection-bg.jpg" alt="Movie" title="Golmaal" class="img-fluid">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <section class="bg-light-red">
                        <div class="container">
                            <div class="row">
                                <div class="xs-text-center col-md-12">
                                    <i class="arrowsSub xl-hidden"><img src="assets/img/left-bar.png"></i>
                                    <h2 class="box uppercase title d-inline-b">movie tracker
                                    </h2>
                                    <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                                </div>
                            </div>
                            <div id="containerBox" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                        </div>

                    </section>
                </div>
            </div>
        </div>
    </div>
</article>


<?php @include 'footer.php'; ?>


<script>
    $('#castCrew').owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        dots: false,
        //autoplay: true,
        autoplayTimeout: 2000,

        responsive: {
            100: {
                items: 1
            },
            576: {
                items: 2
            },
            769: {
                items: 3
            },
            990: {
                items: 4
            }
        }
    });
    $('#CastList').owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        dots: false,
        //autoplay: true,
        autoplayTimeout: 2000,

        responsive: {
            100: {
                items: 1
            },
            576: {
                items: 2
            },
            769: {
                items: 3
            },
            990: {
                items: 4
            }
        }
    });
    $('#CrewList').owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        dots: false,
        //autoplay: true,
        autoplayTimeout: 2000,

        responsive: {
            100: {
                items: 1
            },
            576: {
                items: 2
            },
            769: {
                items: 3
            },
            990: {
                items: 4
            }
        }
    });
    $(function () {
        $("#datepicker").datepicker();
        $("#anim").on("change", function () {
            $("#datepicker").datepicker("option", "showAnim", $(this).val());
        });
    });
    var date = $('#datepicker').datepicker({dateFormat: 'dd/mm/yy'}).val();



</script>
<script>
    Highcharts.chart('containerBox', {
        chart: {
            type: 'area'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            allowDecimals: false,
            labels: {
                formatter: function () {
                    return this.value + ' Week'; // clean, unformatted number for year
                }

            }
        },
        yAxis: {
            title: {
                text: ''
            },
            labels: {
                formatter: function () {
                    return this.value * 10 + 'Cr';
                }
            }
        },
        tooltip: {
            pointFormat: '{point.x} Cr'
        },
        plotOptions: {
            area: {
                pointStart: 1,
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: '',
            data: [null, null, 2, 5, 9, 15, 17, 23, 25, 20, 17, 23, 22, 20, 13, 5, 9, 15, 20, 13, 8]
        }]
    });
    $(function () {
        $("#rateYo").rateYo({
            starWidth: "18px",
            normalFill: "#dddddd",
            ratedFill: "#ed1c24",
            spacing: "3px",
            //maxValue: 1,
            precision: 1,
            border: 1,
            halfStar: true,
            rating: 3.5,
            readOnly: false,
//        onSet: function (rating, rateYoInstance) {
//            $('#value').text(rating);
//    }
//            onChange: function (rating, rateYoInstance) {
//
//                $(this).next().text(rating);
//            }
        });
    });
    $(function () {
        $("#rateYo2").rateYo({
            starWidth: "18px",
            normalFill: "#dddddd",
            ratedFill: "#ed1c24",
            spacing: "3px",
            //maxValue: 1,
            precision: 1,
            rating: 3.5,
            halfStar: true,
            readOnly: true
        });
    });
    $(function () {
        $('nav a[href^="/' + location.pathname.split("/")[1] + '"]').addClass('active');
    });
    // ------min slider ------//
    $( function() {
        var handle = $( "#custom-handle" );
        $( "#slider-range-min" ).slider({
            range: "min",
            min: 1,
            max: 500,
            create: function() {
                handle.text( $( this ).slider( "value" )+ "Cr" );
            },
            slide: function( event, ui ) {
                handle.text( ui.value + "Cr" );
            }
        });
    });

    //----range slider -------//
    $( function() {
        $( "#slider-range" ).slider({
            range: true,
            min: 1,
            max: 500,
            values: [ 75, 300 ],
            slide: function( event, ui ) {
                $( "#amount" ).val( ui.values[ 0 ]+ "Cr" + " - " + ui.values[ 1 ] + "Cr" );
            }
        });
        $( "#amount" ).val($( "#slider-range" ).slider( "values", 0 )+ "Cr" +
            " - " + $( "#slider-range" ).slider( "values", 1 ) + "Cr");
    });

    $('#submit_pre').on('click', function (){
        $('#submit_result').show();
        $('#submit_pre').hide();
    });

    $('[data-toggle="tooltip"]').tooltip();
</script>

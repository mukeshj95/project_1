<?php @include 'header.php';?>
<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="index.php">Home</a>
                <a href="timeline.php" class="active">News Lists</a>
            </div>
        </div>
    </div>
</nav>

<article>
    <div class="container">
        <div class="row">
            <div class="news mb-30 xs-text-center">
                <h1 class="d-inline title text-center uppercase">news listings</h1>
                <i class="arrows"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div class="row xs-hidden" data-plugin="matchHeight" data-by-row="true" id="order4">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="news-lists">
                    <a href="#" class="d-block">
                        <div class="photo-link">
                            <img src="assets/img/events1.jpg" alt="BB Special" class="img-fluid">
                            <button>features</button>
                        </div>
                        <h3 class="text-left">Dhadak: Janhvi and Ishaan soak in moments...</h3>
                    </a>
                    <small>October 21, 2017</small>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="news-lists">
                    <a href="#" class="d-block">
                        <div class="photo-link">
                            <img src="assets/img/events2.jpg" alt="BB Special" class="img-fluid">
                            <button>features</button>
                        </div>
                        <h3 class="text-left ">5 unknown facts about Janhvi Kapoor that her...</h3>
                    </a>
                    <small>October 21, 2017</small>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="news-lists">
                    <a href="#" class="d-block">
                        <div class="photo-link">
                            <img src="assets/img/events3.jpg" alt="BB Special" class="img-fluid">
                            <button>features</button>
                        </div>
                        <h3 class="text-left">Padmavati: Deepika Padukone’s ‘ghoomar’ act...</h3>
                    </a>
                    <small>October 21, 2017</small>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="news-lists">
                    <a href="#" class="d-block">
                        <div class="photo-link">
                            <img src="assets/img/event4.jpg" alt="BB Special" class="img-fluid">
                            <button>features</button>
                        </div>
                        <h3 class="text-left">THIS movie would have been Chitrangda’s debut...</h3>
                    </a>
                    <small>October 21, 2017</small>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="news-lists">
                    <a href="#" class="d-block">
                        <div class="photo-link">
                            <img src="assets/img/events1.jpg" alt="BB Special" class="img-fluid">
                            <button>features</button>
                        </div>
                        <h3 class="text-left">Dhadak: Janhvi and Ishaan soak in moments...</h3>
                    </a>
                    <small>October 21, 2017</small>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="news-lists">
                    <a href="#" class="d-block">
                        <div class="photo-link">
                            <img src="assets/img/events2.jpg" alt="BB Special" class="img-fluid">
                            <button>features</button>
                        </div>
                        <h3 class="text-left ">5 unknown facts about Janhvi Kapoor that her...</h3>
                    </a>
                    <small>October 21, 2017</small>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="news-lists">
                    <a href="#" class="d-block">
                        <div class="photo-link">
                            <img src="assets/img/events3.jpg" alt="BB Special" class="img-fluid">
                            <button>features</button>
                        </div>
                        <h3 class="text-left">Padmavati: Deepika Padukone’s ‘ghoomar’ act...</h3>
                    </a>
                    <small>October 21, 2017</small>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="news-lists">
                    <a href="#" class="d-block">
                        <div class="photo-link">
                            <img src="assets/img/event4.jpg" alt="BB Special" class="img-fluid">
                            <button>features</button>
                        </div>
                        <h3 class="text-left">THIS movie would have been Chitrangda’s debut...</h3>
                    </a>
                    <small>October 21, 2017</small>
                </div>
            </div>
        </div>
    </div>
</article>

<?php @include 'footer.php';?>


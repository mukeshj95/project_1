<?php @include 'header.php'; ?>

<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="movies.php">Movie</a>
                <a href="#" class="active">Golmal Again</a>
            </div>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="banner-movie">
            <a href="#">
                <img src="assets/img/play-movie.png" alt="player" title="Play" class="img-fluid">
            </a>
            <div class="movie-img"></div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-3 col-xs-12">
            <div class="movie-image">
                <img src="assets/img/golmal-movie.jpg" alt="Golmal" title="Golmal" class="img-fluid full-wide">
            </div>
        </div>
        <div class="col-md-9 col-xs-12">
            <div class="movie-details">
                <h1>Golmaal Again</h1>
                <p><span>U/A</span>Hindi</p>
                <p><span><i class="ion-ios-calendar-outline"></i></span>19th May</p>
                <p><span><i class="ion-clock"></i></span>2 hr 50 mins</p>
                <p><span><i class="icon"><img src="assets/img/genre-icon.png" class="img-fluid" alt="genre-icon"></i></span>Comedy drama</p>
                <div class="share-buttons">
                    <div class="icon-box float-right">
                        <img src="assets/img/share-red.png" alt="Share" title="Share">
                    </div>
                </div>
            </div>
            <div class="rating border">
                <div class="star-ratings-css">
                    <label>Bollywood Bubble Rating</label>
                    <div class="star-ratings-sprite">
                        <span style="width:68%" class="star-ratings-sprite-rating"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<article>
    <div class="container">
        <div class="row">
            <div class="xs-text-center col-md-12">
                <h2 class="box uppercase title d-inline-b">box office
                </h2>
                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div class="box-office-details">
            <div class="row" data-plugin="matchHeight" data-by-row="true">
                <div class="col-md-12 col-lg-2">
                    <div class="totalCollection">
                        <div class="total">
                            <img src="assets/img/rupee-icon.png" alt="Rupees" title="Rs" class="img-fluid">
                            <h2>92.450 Cr</h2>
                            <label>total collection</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-7">
                    <div class="row collections">
                        <div class="dayCollection">
                            <h3><span><img src="assets/img/rupee-icon.png" alt="Rupees" title="Rs" class="img-fluid"></span>92.450 Cr</h3>
                            <label>first day collection</label>
                        </div>
                        <div class="dayCollection">
                            <h3><span><img src="assets/img/rupee-icon.png" alt="Rupees" title="Rs" class="img-fluid"></span>92.450 Cr</h3>
                            <label>second day collection</label>
                        </div>
                        <div class="dayCollection">
                            <h3><span><img src="assets/img/rupee-icon.png" alt="Rupees" title="Rs" class="img-fluid"></span>92.450 Cr</h3>
                            <label>third day collection</label>
                        </div>
                    </div>
                    <div class="row collections">
                        <div class="dayCollection">
                            <h3><span><img src="assets/img/rupee-icon.png" alt="Rupees" title="Rs" class="img-fluid"></span>92.450 Cr</h3>
                            <label>fourth day collection</label>
                        </div>
                        <div class="dayCollection">
                            <h3><span><img src="assets/img/rupee-icon.png" alt="Rupees" title="Rs" class="img-fluid"></span>92.450 Cr</h3>
                            <label>fifth day collection</label>
                        </div>
                        <div class="dayCollection">
                            <h3><span><img src="assets/img/rupee-icon.png" alt="Rupees" title="Rs" class="img-fluid"></span>92.450 Cr</h3>
                            <label>sixth day collection</label>
                        </div>

                    </div>
                    <div class="collections text-center">
                        <div class="button mt-3">
                            <label class="btn btn-sm btn-success">Hit</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-3">
                    <div class="totalCollection banner">
                        <div class="collection-img">
                            <img src="assets/img/collection-bg.jpg" alt="Movie" title="Golmaal" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</article>
<section class="bg-light-red">
    <div class="container">
        <div class="row">
            <div class="xs-text-center col-md-12">
                <h2 class="box uppercase title d-inline-b">movie tracker
                </h2>
                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>

</section>


<?php @include 'footer.php'; ?>
<script>
    Highcharts.chart('container', {
        chart: {
            type: 'area'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            allowDecimals: false,
            labels: {
                formatter: function () {
                    return this.value + ' Week'; // clean, unformatted number for year
                }

            }
        },
        yAxis: {
            title: {
                text: ''
            },
            labels: {
                formatter: function () {
                    return this.value * 10 + 'Cr';
                }
            }
        },
        tooltip: {
            pointFormat: '{point.x} Cr'
        },
        plotOptions: {
            area: {
                pointStart: 1,
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
                name: '',
                data: [null, null, 2, 5, 9, 15, 17, 23, 25, 20, 17, 23, 22, 20, 13, 5, 9, 15, 20, 13, 8]
            }]
    });
</script>


<?php
/**
 * Created by PhpStorm.
 * User: HIRA
 * Date: 11/9/2017
 * Time: 6:07 PM
 */
?>
<footer>
    <div class="container">
        <a href="#" id="scrollTop"> <i class="ion-ios-arrow-thin-up"></i> </a>
        <div class="row top-footer">
            <div class="col-md-4">
                <h5 class="font-wt-700">Our Presence</h5>
                <div class="icon-box">
                    <ul>
                        <li class="bg-fb">
                            <a href="#"> <i class="ion-social-facebook text-white"></i> </a>
                        </li>
                        <li class="bg-twitter">
                            <a href="#"> <i class="ion-social-twitter text-white"></i> </a>
                        </li>
                        <li class="bg-primary">
                            <a href="#"> <i class="ion-social-instagram text-white"></i> </a>
                        </li>
                        <li class="bg-youtube">
                            <a href="#"> <i class="ion-social-youtube text-white"></i> </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 mb-20">
                <div class="row">
                    <div class="col-md-12">
                        <h5 class="font-wt-700">Sitemap</h5>
                    </div>
                    <div class="col-xs-6">
                        <ul>
                            <li> <a href="news.php">News</a> </li>
                            <li> <a href="celebs-landing.php">Celebrities</a> </li>
                            <li> <a href="movie-landing.php">Movies</a> </li>
                        </ul>
                    </div>
                    <div class="col-xs-6">
                        <ul>
                            <li> <a href="photo-landing.php">Photos</a> </li>
                            <li> <a href="video.php">Videos</a> </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12">
                        <h5 class="font-wt-700">Others</h5>
                    </div>
                    <div class="col-xs-6">
                        <ul>
                            <li> <a href="#">About Us</a> </li>
                            <li> <a href="#">Privacy Policy</a> </li>
                        </ul>
                    </div>
                    <div class="col-xs-6">
                        <ul>
                            <li> <a href="#">Terms & Conditions</a> </li>
                            <li> <a href="#">Contact Us</a> </li>
                        </ul>
                    </div>
                </div>
            </div>            
        </div>
    </div>
    <hr>
    <div class="container">
        <div class="row bottom-footer">
            <div class="col-md-12 col-xs-12 text-center">
                <ul class="d-inline-b pt-1">
                    <li>Copyright@2017<span class="pipe2">|</span> </li>
                    <li>www.bollywoodbubble.com<span class="pipe2">|</span> </li>
                    <li>All Rights Reserved </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<div id="overlay"></div>
<div id="mySidenav" class="sidenav">
    <div class="inner">
        <div class="nav-head"><a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><i class="ion-ios-close-empty"></i></a>
            <div class="profile">
                <div class="profile-img"><img src="assets/img/profile-pic.png" alt=""></div>
                <p>Hello Guest!</p>
                <a href="#" data-toggle="modal" data-target="#login" class="font-wt-500 btn-round link-red">Login</a>
            </div>
        </div>
        <ul class="nav flex-column">            
            <li class="nav-item"><a class="nav-link" href="ind">Home</a></li>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#news" data-toggle="collapse" data-target="#news">News</a>
                <div class="collapse" id="news" aria-expanded="false">
                    <ul class="flex-column pl-2 nav">
                        <li class="nav-item"><a class="nav-link" href="">Gossip</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Bubble Diaries</a></li>
                        <li class="nav-item">
                            <a class="nav-link collapsed" href="#submenu1sub1" data-toggle="collapse" data-target="#submenu1sub1">Customers</a>
                            <div class="collapse small" id="submenu1sub1" aria-expanded="false">
                                <ul class="flex-column nav pl-4">
                                    <li class="nav-item">
                                        <a class="nav-link p-0" href="">
                                            <i class="fa fa-fw fa-clock-o"></i> Bubble Speaks
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link p-0" href="">
                                            <i class="fa fa-fw fa-dashboard"></i> Nostalgia
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link p-0" href="">
                                            <i class="fa fa-fw fa-bar-chart"></i> Time Travel
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="">Guest Blogger</a></li>
                    </ul>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#celeb" data-toggle="collapse" data-target="#celeb">Celebrities</a>
                <div class="collapse" id="celeb" aria-expanded="false">
                    <ul class="flex-column pl-2 nav">
                        <li class="nav-item"><a class="nav-link" href="">Interviews</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Lifestyle</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Top Trending</a></li>
                    </ul>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#movies" data-toggle="collapse" data-target="#movies">Movies</a>
                <div class="collapse" id="movies" aria-expanded="false">
                    <ul class="flex-column pl-2 nav">
                        <li class="nav-item"><a class="nav-link" href="">Reviews</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Box Office</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Upcoming Movies</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Top Trending</a></li>
                    </ul>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#photos" data-toggle="collapse" data-target="#photos">Photos</a>
                <div class="collapse" id="photos" aria-expanded="false">
                    <ul class="flex-column pl-2 nav">
                        <li class="nav-item"><a class="nav-link" href="">Caught on Camera</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Features</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Events</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Gym Spottings</a></li>
                    </ul>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#video" data-toggle="collapse" data-target="#video">Bubble TV</a>
                <div class="collapse" id="video" aria-expanded="false">
                    <ul class="flex-column pl-2 nav">
                        <li class="nav-item"><a class="nav-link" href="">BB Specials</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Price Tag</a></li>
                        <li class="nav-item"><a class="nav-link" href="">SHort Talk</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Break Time</a></li>
                        <li class="nav-item"><a class="nav-link" href="">B-Town Backpackers</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Bubble Snoop</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Bollywood Sass</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Bollywood Spotting </a></li>
                        <li class="nav-item"><a class="nav-link" href="">Bubble Bytes</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Petstagram</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Road to Fitness</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Postal Address</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Time Travel</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Bollywood FAM JAM</a></li>
                    </ul>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#video" data-toggle="collapse" data-target="#video">Lifestyle</a>
                <div class="collapse" id="video" aria-expanded="false">
                    <ul class="flex-column pl-2 nav">
                        <li class="nav-item">
                            <a class="nav-link collapsed py-0" href="#fitness" data-toggle="collapse" data-target="#fitness">Fitness</a>
                            <div class="collapse small" id="fitness" aria-expanded="false">
                                <ul class="flex-column nav pl-4">
                                    <li class="nav-item">
                                        <a class="nav-link p-0" href="">
                                            <i class="fa fa-fw fa-clock-o"></i> Road to Fitness
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link p-0" href="">
                                            <i class="fa fa-fw fa-dashboard"></i> Gym Spottings
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link p-0" href="">
                                            <i class="fa fa-fw fa-bar-chart"></i> Gym Videos
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="">B-Town Juniors</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Postal Address</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Sportstainment</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Exclusives</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Couple Goals</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Bollywood FAM JAM</a></li>
                        <li class="nav-item"><a class="nav-link" href="">OOTD</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Wonders & Blunders</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Trendsetters</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Price Tag</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Clone the Look</a></li>
                        <li class="nav-item"><a class="nav-link" href="">B-Town Backpackers</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Petstagram</a></li>
                    </ul>
                </div>
            </li>            
            <!--<li class="nav-item"><a class="nav-link" href="#" data-toggle="modal" data-target="#login">Login</a></li>-->      
        </ul>
        <div class="icon-box pt-20">
            <ul>
                <li class="bg-fb">
                    <a href="#">
                        <i class="ion-social-facebook text-white"></i>
                    </a>
                </li>
                <li class="bg-twitter">
                    <a href="#">
                        <i class="ion-social-twitter text-white"></i>
                    </a>
                </li>
                <li class="bg-google">
                    <a href="#">
                        <i class="ion-social-googleplus text-white"></i>
                    </a>
                </li>
                <li class="bg-youtube">
                    <a href="#">
                        <i class="ion-social-youtube text-white"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>

<!-- LOGIN/SIGNUP -->
<div class="modal fade" tabindex="-1" id="login" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"> &nbsp;
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ion-ios-close-empty"></i> 
                </button>
            </div>
            <div class="modal-body">
                <div class="tab-pane active" id="login-tab" role="tabpanel">
                    <h3>Login to your Account</h3>
                    <ul class="login-social">
                        <li><a href="#" class="btn fb-btn"><i class="ion-social-facebook"></i> Facebook</a></li>
                        <li><a href="#" class="btn google-btn"><i class="ion-social-googleplus"></i> Google+</a></li>
                    </ul>                    
                </div>
            </div>
        </div>
    </div>
</div>

<!-- MOBILE FLOATING ELEMENT -->
<div class="floating-nav">
    <div class="nav1"><a href="news.php">News</a></div>
    <div class="nav2"><a href="celebs-landing.php">Celebs</a></div>
    <div class="nav3"><a href="movie-landing.php">Movies</a></div>
    <div class="nav4"><a href="photo-landing.php">Photos</a></div>
    <div class="mask"><i class="ion-android-star"></i></div>  
</div>

<!---------- Search modal -------------->
<div class="modal fade search-modal" role="dialog" aria-labelledby="searchModal" aria-hidden="true" id="searchModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content ">
            <div class="row bg-bubble">
                <div class="modal-header container">
                    <div class="filter-box">
                        <ul class="filter-list">
                            <li class="init"><a href="#"><span></span>All</a></li>
                            <li data-value="value 1" class="news"><a href="#"><span></span>News</a></li>
                            <li data-value="value 2" class="celeb"><a href="#"><span></span>Celebrities</a></li>
                            <li data-value="value 3" class="movies"><a href="#"><span></span>Movies</a></li>
                            <li data-value="value 4" class="photo"><a href="#"><span></span>Photos</a></li>
                            <li data-value="value 5" class="video"><a href="#"><span></span>Videos</a></li>
                        </ul>
                    </div>
                    <div class="search-box">
                        <input type="search" placeholder="Search news, videos, photos, movies, celebrities" class="form-control">
                        <span class="icon"><i class="ion-ios-search"></i></span>
                    </div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">    
                    <i class="ion-ios-close-empty"></i>
                </button>
            </div>
            <div class="modal-body">
                <article>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="fs-2">Search result for <b>"Salman Khan"</b> <span>( found 75 results )</span></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8 col-xs-12">
                                <div class="d-flex">
                                    <div class="result-image">
                                        <img src="assets/img/salman-search-result.jpg" class="img-fluid" alt="Salman Khan" title="Salman Khan">
                                    </div>
                                    <div class="title xs-text-center">
                                        <h1 class="subtitle mb-0">Salman Khan</h1>
                                        <label class="mb-20">India film actor, Singer, Producer</label>
                                        <div class="icon-box">
                                            <ul>
                                                <li class="bg-fb">
                                                    <a href="#">
                                                        <i class="ion-social-facebook text-white"></i>
                                                    </a>
                                                </li>
                                                <li class="bg-twitter">
                                                    <a href="#">
                                                        <i class="ion-social-twitter text-white"></i>
                                                    </a>
                                                </li>
                                                <li class="bg-google">
                                                    <a href="#">
                                                        <i class="ion-social-googleplus text-white"></i>
                                                    </a>
                                                </li>
                                                <li class="bg-youtube">
                                                    <a href="#">
                                                        <i class="ion-bookmark text-white"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                <section class="recent-search">
                    <div class="container">
                        <div class="">
                            <h3>Your Recent Searches</h3>
                            <div class="alert alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert"><i class="ion-ios-close-empty"></i></button>
                                Tiger zinda hai
                            </div>
                            <div class="alert alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert"><i class="ion-ios-close-empty"></i></button>
                                Recent Articles 
                            </div>
                            <div class="alert alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert"><i class="ion-ios-close-empty"></i></button>
                                Shahrukh Khan
                            </div>
                            <div class="alert alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert"><i class="ion-ios-close-empty"></i></button>
                                Virat & Anushka Wedding
                            </div>
                            <button type="button" class="clear-btn">Clear all</button>
                        </div>
                    </div>
                </section>
                <section class="bg-grey">
                    <div class="container">
                        <div class="row">
                            <div class="xs-text-center col-md-12">
                                <i class="arrowsSub xl-hidden"><img src="assets/img/left-bar.png"></i>
                                <h2 class="box uppercase title d-inline-b">popular
                                </h2>
                                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                            </div>
                            <div class="events">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <a href="#" class="d-block">
                                        <div class="popular-link">
                                            <div class="photo">
                                                <img src="assets/img/events1.jpg" alt="BB Special" class="img-fluid full-wide">
                                            </div>
                                            <h3>Dhadak: Janhvi and Ishaan soak in moments...</h3>
                                            <small class="text-muted">October 21, 2017</small>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <a href="#" class="d-block">
                                        <div class="popular-link">
                                            <div class="photo">
                                                <img src="assets/img/events2.jpg" alt="BB Special" class="img-fluid full-wide">
                                            </div>
                                            <h3>5 unknown facts about Janhvi Kapoor that her Instagram...</h3>
                                            <small class="text-muted">October 21, 2017</small>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <a href="#" class="d-block">
                                        <div class="popular-link">
                                            <div class="photo">
                                                <img src="assets/img/events3.jpg" alt="BB Special" class="img-fluid full-wide">
                                            </div>
                                            <h3>Padmavati: Deepika Padukone�s �ghoomar� act...</h3>
                                            <small class="text-muted">October 21, 2017</small>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <a href="#" class="d-block">
                                        <div class="popular-link">
                                            <div class="photo">
                                                <img src="assets/img/event4.jpg" alt="BB Special" class="img-fluid full-wide">
                                            </div>
                                            <h3>THIS movie would have been Chitrangda�s debut instead...</h3>
                                            <small class="text-muted">October 21, 2017</small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="xs-hidden">
                    <div class="container">
                        <div class="row">
                            <div class="xs-text-center col-md-12">
                                <i class="arrowsSub xl-hidden"><img src="assets/img/left-bar.png"></i>
                                <h2 class="box uppercase title d-inline-b">trending
                                </h2>
                                <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                            </div>
                            <div class="events">
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 sm-mb-3">
                                    <a href="#" class="d-block">
                                        <div class="card-link">
                                            <div class="video-content">
                                                <img src="assets/img/events1.jpg" alt="BB Special" class="img-fluid full-wide">
                                                <label class="duration">15:22</label>
                                            </div>
                                            <h3>Dhadak: Janhvi and Ishaan soak in moments...</h3>
                                            <small class="text-muted">October 21, 2017</small>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 sm-mb-3">
                                    <a href="#" class="d-block">
                                        <div class="card-link">
                                            <div class="video-content">
                                                <img src="assets/img/events2.jpg" alt="BB Special" class="img-fluid full-wide">
                                                <label class="duration">15:22</label>
                                            </div>
                                            <h3>5 unknown facts about Janhvi Kapoor that her Instagram...</h3>
                                            <small class="text-muted">October 21, 2017</small>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 sm-mb-3">
                                    <a href="#" class="d-block">
                                        <div class="card-link">
                                            <div class="video-content">
                                                <img src="assets/img/events3.jpg" alt="BB Special" class="img-fluid full-wide">
                                                <label class="duration">15:22</label>
                                            </div>
                                            <h3>Padmavati: Deepika Padukone�s �ghoomar� act...</h3>
                                            <small class="text-muted">October 21, 2017</small>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 sm-mb-3">
                                    <a href="#" class="d-block">
                                        <div class="card-link">
                                            <div class="video-content">
                                                <img src="assets/img/event4.jpg" alt="BB Special" class="img-fluid full-wide">
                                                <label class="duration">15:22</label>
                                            </div>
                                            <h3>THIS movie would have been Chitrangda�s debut instead...</h3>
                                            <small class="text-muted">October 21, 2017</small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-30">
                            <div class="col-md-3 col-xs-12">
                                <div class="photo-block">
                                    <a href="#" class="p-relative">
                                        <img src="assets/img/interview-.jpg" alt="video-link" class="img-fluid full-wide">
                                        <label for="" class="numbers">10</label>
                                    </a>
                                    <h3 class="font-22 font-wt-300 font-black mb-10" id="title-limit1">Photo: Sushant Singh Rajput will make you blush with his cha</h3>
                                    <small class="font-light font-wt-400">October 21, 2017</small>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <div class="photo-block">
                                    <a href="#" class="p-relative">
                                        <img src="assets/img/interview-1.jpg" alt="video-link" class="img-fluid full-wide">
                                        <label for="" class="numbers">10</label>
                                    </a>
                                    <h3 class="font-22 font-wt-300 font-black mb-10" id="title-limit1">Photo: Sushant Singh Rajput will make you blush with his cha</h3>
                                    <small class="font-light font-wt-400">October 21, 2017</small>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <div class="photo-block">
                                    <a href="#" class="p-relative">
                                        <img src="assets/img/interview-3.jpg" alt="video-link" class="img-fluid full-wide">
                                        <label for="" class="numbers">10</label>
                                    </a>
                                    <h3 class="font-22 font-wt-300 font-black mb-10" id="title-limit1">Photo: Sushant Singh Rajput will make you blush with his cha</h3>
                                    <small class="font-light font-wt-400">October 21, 2017</small>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <div class="photo-block">
                                    <a href="#" class="p-relative">
                                        <img src="assets/img/interview-2.jpg" alt="video-link" class="img-fluid full-wide">
                                        <label for="" class="numbers">10</label>
                                    </a>
                                    <h3 class="font-22 font-wt-300 font-black mb-10" id="title-limit1">Photo: Sushant Singh Rajput will make you blush with his cha</h3>
                                    <small class="font-light font-wt-400">October 21, 2017</small>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </section>                
            </div>

        </div>
    </div>
</div>

<!---------- My Account Setting modal ----------->
<div class="modal fade" tabindex="-1" id="settingModal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="full-wide text-center mb-10">
                    <i class="arrowsSub"><img src="assets/img/left-bar.png"></i>
                    <h2 class="d-inline title uppercase">profile information</h2>
                    <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ion-ios-close-empty"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="blog-imgae">
                    <img src="assets/img/author-pic.jpg" alt="Author" title="Rohit Sharma">
                </div>
                <form>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" placeholder="Tell us about yourself" rows="4"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Email Id">
                    </div>
                    <div class="form-group text-center">
                        <a href="#" data-toggle="modal" data-target="#forgotPass" id="password-btn">Change Password</a>
                    </div>
                    <div class="button-center">
                        <button type="submit" class="btn btn-primary btn-round">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!---------- Forgot password modal ----------->
<div class="modal fade" tabindex="-1" id="forgotPass" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="full-wide text-center mb-10">
                    <i class="arrowsSub"><img src="assets/img/left-bar.png"></i>
                    <h2 class="d-inline title uppercase">change password</h2>
                    <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ion-ios-close-empty"></i>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Current Paswword">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="New Password">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Confirm Password">
                    </div>
                    <div class="button-center">
                        <button type="submit" class="btn btn-primary btn-round">Change password</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!---------- write blog modal ----------->
<div class="modal fade" tabindex="-1" id="blogModal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="full-wide text-center mb-10">
                    <i class="arrowsSub"><img src="assets/img/left-bar.png"></i>
                    <h2 class="d-inline title uppercase">write your blog</h2>
                    <i class="arrowsSub"><img src="assets/img/right-bar.png"></i>
                </div>
                <button type="button" class="close" data-toggle="modal" aria-label="Close" data-target="#confirmation">
                    <i class="ion-ios-close-empty"></i>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group title">
                        <div><img src="assets/img/bloger-img.png" alt="Profile" title="Bloger"></div>
                        <textarea rows="1" class="form-control" placeholder="Title"></textarea>
                        <label class="float-right">max 100 characters</label>
                    </div>
                    <div class="form-group">
                        <textarea rows="6" class="form-control" placeholder="Tell your story"></textarea>
                        <label class="float-right">max 1000 characters</label>
                    </div>
                    <div class="form-group">
                        <label class="fileContainer"><i class="ion-paperclip"></i>Attach your file
                            <input type="file"/>
                            <span>e.g. photos (.jpg, .png, .svg)</span>
                        </label>
                    </div>
                    <div class="button-center">
<!--                        <button type="submit" class="btn btn-default btn-round mr-2">Cancel</button>-->
                        <button type="submit" class="btn btn-primary btn-round">Publish</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-------- confirmation modal ----------->

<div class="modal fade" tabindex="-1" id="confirmation" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="full-wide text-center mb-10">
                    <h3 class="d-inline title uppercase">Are You Sure want Exit</h3>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ion-ios-close-empty"></i>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="button-center">
                        <button type="submit" class="btn btn-primary btn-round">Yes</button>
                        <button type="submit" class="btn btn-primary btn-round">No</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="assets/js/jquery-3.2.1.min.js"></script>
<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/plugins/js/owl.carousel.js"></script>
<script src="assets/plugins/js/select2.min.js"></script>
<script src="assets/plugins/js/jquery.waterwheelCarousel.min.js"></script>
<script src="assets/plugins/js/jquery.rateyo.js"></script>
<script src="assets/plugins/js/jquery.touchSwipe.min.js"></script>
<script src="assets/plugins/js/jquery.matchHeight.js" type="text/javascript"></script>
<script src="assets/js/main.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<!-- Go to www.addthis.com/dashboard to customize your tools --> 
<script src="assets/plugins/js/jquery.waterwheelCarousel.min.js"></script>

<!-- Data Table -->
<script src="assets/plugins/js/jquery.dataTables.min.js"></script>
  <script>
    $(function(){
      var hash = window.location.hash;
      hash && $('ul.nav a[href="' + hash + '"]').tab('show');

      $('.tabsUrl a').click(function (e) {
        $(this).tab('show');
        //var scrollmem = $('body').scrollTop();
        //window.location.hash = this.hash;
        if(history.pushState) {
            history.pushState(null, null, this.hash);
        }
        else {
            location.hash = this.hash;
        }
        //$('html,body').scrollTop(scrollmem);
      });
        return false; 
    });
  </script>
<script>
            window.ga = function () {
                ga.q.push(arguments)
            };
            ga.q = [];
            ga.l = +new Date;
            ga('create', 'UA-XXXXX-Y', 'auto');
            ga('send', 'pageview')
</script>
<script src="https://www.google-analytics.com/analytics.js" async defer></script>

<script>
            function openNav() {
                document.getElementById("mySidenav").style.width = "300px";
                document.getElementById("overlay").style.display = "block";
                //document.getElementById("main").style.marginLeft = "300px";
                //document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
                document.body.style.overflow = "hidden"
            }

            function closeNav() {
                document.getElementById("mySidenav").style.width = "0";
                //document.getElementById("main").style.marginLeft= "0";
                //document.body.style.backgroundColor = "white";
                document.body.style.overflow = "scroll"
                document.getElementById("overlay").style.display = "none";
            }

            $(document).ready(function () {
                $(".dropdown").hover(
                        function () {
                            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true, true).slideDown("1500");
                            $(this).toggleClass('open');
                        },
                        function () {

                            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true, true).slideUp("1500");
                            $(this).toggleClass('open');
                        }
                );
            });
            function scrollUp() {
                $('#scrollTop').click(function () {
                    $("html, body").animate({scrollTop: 1}, 1500);
                    return true;
                });
            }
            scrollUp();

            $('#boxoffice').owlCarousel({
                loop: true,
                margin: 0,
                nav: false,
                dots: false,
                //autoplay: true,
                autoplayTimeout: 3000,

                responsive: {
                    100: {
                        items: 1
                    },
                    576: {
                        items: 1
                    },
                    770: {
                        items: 2
                    },
                    990: {
                        items: 3
                    }
                }
            });

            $("ul.filter-list").on("click", ".init", function () {
                $(this).closest("ul.filter-list").children('li:not(.init)').slideDown();
            });

            var allOptions = $("ul.filter-list").children('li:not(.init)');
            $("ul.filter-list").on("click", "li:not(.init)", function () {
                allOptions.removeClass('selected');
                $(this).addClass('selected');
                $("ul.filter-list").children('.init').html($(this).html());
                allOptions.slideUp();
            });
</script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#DataTable').DataTable();
    });
    $(document).ready(function () {
    $('#password-btn').click(function(){
        $('#settingModal').css('display', 'none');
    });
        
    });
</script>
<script>
    $(document).ready(function () {
        var headerTop = $('header').offset().top;
        var headerBottom = headerTop + 120; // Sub-menu should appear after this distance from top.
        $(window).scroll(function () {
            var scrollTop = $(window).scrollTop(); // Current vertical scroll position from the top
            if (scrollTop > headerBottom) { // Check to see if we have scrolled more than headerBottom
                if (($("#share-article").is(":visible") === false)) {
                    $('#share-article').fadeIn('slow');
                }
            } else {
                if ($("#share-article").is(":visible")) {
                    $('#share-article').hide();
                }
            }
        });
    });
</script>
<script>
    $(document).ready(function () {

        var active1 = false;
        var active2 = false;
        var active3 = false;
        var active4 = false;

        $('.floating-nav').on('hover touchstart', function () {

            if (!active1)
                $(this).find('.nav1').css({'transform': 'translate(14px,-96px)'});
            else
                $(this).find('.nav1').css({'transform': 'none'});
            if (!active2)
                $(this).find('.nav2').css({'transform': 'translate(-43px,-83px)'});
            else
                $(this).find('.nav2').css({'transform': 'none'});
            if (!active3)
                $(this).find('.nav3').css({'transform': 'translate(-85px,-43px)'});
            else
                $(this).find('.nav3').css({'transform': 'none'});
            if (!active4)
                $(this).find('.nav4').css({'transform': 'translate(-102px,13px)'});
            else
                $(this).find('.nav4').css({'transform': 'none'});
            active1 = !active1;
            active2 = !active2;
            active3 = !active3;
            active4 = !active4;

        });
    });
</script>
<script>
    $(document).ready(function () {
        $(".button a").click(function () {
            $(".overlay").fadeToggle(200);
            $(this).toggleClass('btn-open').toggleClass('btn-close');
        });
    });
    $('.overlay').on('click', function () {
        $(".overlay").fadeToggle(200);
        $(".button a").toggleClass('btn-open').toggleClass('btn-close');
        open = false;
    });

    $('.heart i').hover(
       function(){ $(this).removeClass('ion-ios-heart-outline') },
       function(){ $(this).addClass('ion-ios-heart') }
)
    $('.heart i').mouseleave(
       function(){ $(this).removeClass('ion-ios-heart') },
       function(){ $(this).addClass('ion-ios-heart-outline') }
)
</script>


</body>
</html>

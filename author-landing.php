<?php @include 'header.php'; ?>

<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="index.php">Home</a>
                <a href="author-landing.php" class="active">BB Authors</a>
            </div>
        </div>
    </div>
</nav>

<section class="author-bg">
    <div class="container">
        <div class="row">
            <div class="news xs-text-center">
                <h1 class="d-inline title">bb authors</h1> 
                <i class="arrows"><img src="assets/img/right-bar.png"></i>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <div class="bb-authors">
                    <div class="blog-imgae">
                        <img src="assets/img/author-1.jpg" alt="Blogger" title="Mina Kumari">
                    </div>
                    <div class="card-footer shadow">
                        <h4>Dhirendra Kumar</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi non quis exercitationem culpa nesciunt nihil</p>
                        <label>number of blogs: <span>12</span></label>
                        <div class="button-center">
                            <a href="#" class="btn btn-default">follow me</a>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="bb-authors">
                    <div class="blog-imgae">
                        <img src="assets/img/author-2.jpg" alt="Blogger" title="Mina Kumari">
                    </div>
                    <div class="card-footer shadow">
                        <h4>Vishakha Nishad</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi non quis exercitationem culpa nesciunt nihil</p>
                        <label>number of blogs: <span>12</span></label>
                        <div class="button-center">
                            <a href="#" class="btn btn-default">follow me</a>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="bb-authors">
                    <div class="blog-imgae">
                        <img src="assets/img/author-3.jpg" alt="Blogger" title="Mina Kumari">
                    </div>
                    <div class="card-footer shadow">
                        <h4>Anchal Nishad</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi non quis exercitationem culpa nesciunt nihil</p>
                        <label>number of blogs: <span>12</span></label>
                        <div class="button-center">
                            <a href="#" class="btn btn-default">follow me</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <div class="bb-authors">
                    <div class="blog-imgae">
                        <img src="assets/img/author-1.jpg" alt="Blogger" title="Mina Kumari">
                    </div>
                    <div class="card-footer shadow">
                        <h4>Dhirendra Kumar</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi non quis exercitationem culpa nesciunt nihil</p>
                        <label>number of blogs: <span>12</span></label>
                        <div class="button-center">
                            <a href="#" class="btn btn-default">follow me</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="bb-authors">
                    <div class="blog-imgae">
                        <img src="assets/img/author-2.jpg" alt="Blogger" title="Mina Kumari">
                    </div>
                    <div class="card-footer shadow">
                        <h4>Vishakha Nishad</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi non quis exercitationem culpa nesciunt nihil</p>
                        <label>number of blogs: <span>12</span></label>
                        <div class="button-center">
                            <a href="#" class="btn btn-default">follow me</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="bb-authors">
                    <div class="blog-imgae">
                        <img src="assets/img/author-3.jpg" alt="Blogger" title="Mina Kumari">
                    </div>
                    <div class="card-footer shadow">
                        <h4>Anchal Nishad</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi non quis exercitationem culpa nesciunt nihil</p>
                        <label>number of blogs: <span>12</span></label>
                        <div class="button-center">
                            <a href="#" class="btn btn-default">follow me</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <div class="bb-authors">
                    <div class="blog-imgae">
                        <img src="assets/img/author-1.jpg" alt="Blogger" title="Mina Kumari">
                    </div>
                    <div class="card-footer shadow">
                        <h4>Dhirendra Kumar</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi non quis exercitationem culpa nesciunt nihil</p>
                        <label>number of blogs: <span>12</span></label>
                        <div class="button-center">
                            <a href="#" class="btn btn-default">follow me</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="bb-authors">
                    <div class="blog-imgae">
                        <img src="assets/img/author-2.jpg" alt="Blogger" title="Mina Kumari">
                    </div>
                    <div class="card-footer shadow">
                        <h4>Vishakha Nishad</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi non quis exercitationem culpa nesciunt nihil</p>
                        <label>number of blogs: <span>12</span></label>
                        <div class="button-center">
                            <a href="#" class="btn btn-default">follow me</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="bb-authors">
                    <div class="blog-imgae">
                        <img src="assets/img/author-3.jpg" alt="Blogger" title="Mina Kumari">
                    </div>
                    <div class="card-footer shadow">
                        <h4>Anchal Nishad</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi non quis exercitationem culpa nesciunt nihil</p>
                        <label>number of blogs: <span>12</span></label>
                        <div class="button-center">
                            <a href="#" class="btn btn-default">follow me</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php @include 'footer.php'; ?>
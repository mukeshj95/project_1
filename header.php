<?php /**
 * Created by PhpStorm.
 * User: HIRA
 * Date: 11/9/2017
 * Time: 6:04 PM
 */ ?>
<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Bollywood Bubble</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="assets/img/bubble-logo.png" type="image/gif" sizes="16x16">
        <link rel="manifest" href="site.webmanifest">
        <link rel="apple-touch-icon" href="icon.png">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/main.css">
        <link rel="stylesheet" href="assets/plugins/css/select2.min.css">
        <link rel="stylesheet" href="assets/plugins/css/ionicons.min.css">
        <link href="https://fonts.googleapis.com/css?family=Passion+One:400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Alegreya+Sans:300,400,500,700,800" rel="stylesheet">
        <link rel="stylesheet" href="assets/plugins/css/owl.carousel.min.css">
        <link rel="stylesheet" href="assets/plugins/css/owl.theme.default.min.css">
        <link rel="stylesheet" href="assets/css/custom.css">
        <link rel="stylesheet" href="assets/css/responsive.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
        
        <!-- Data Table -->
        <link rel="stylesheet" href="assets/plugins/css/dataTables.bootstrap4.min.css">
    </head>
    <body>
       <!-- PRELOADER -->
       <div class="loader-wrapper">
           <div class="loader"><img src="assets/img/loader.png" class="spinn-right" alt=""></div>
       </div>
       <!-- END: PRELOADER -->
        <header>
            <nav class="navbar navbar-inverse fixed-top">
                <div class="container">                    
                    <div class="navbar-header">
                        <div class="button  hidden-md-down">
                            <a class="btn-open" href="#"></a>
                        </div>
                        <a href="#" class="navbar-toggle" data-toggle="collapse"  onclick="openNav()">
                            <span class="sr-only">Toggle navigation</span>
                            <i class="ion-navicon"></i>
                        </a>
                        <a class="navbar-brand" href="index.php"><img src="assets/img/logo-white.png" alt="Bollywood Bubble" class="Bollywood Bubble"></a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav hidden-md-down">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="news.php">News</a></li>
                            <li><a href="celebs-landing.php"> Celebrities</a></li>
                            <li><a href="movie-landing.php">Movies</a></li>
                            <li><a href="photo-landing.php">Photos</a></li>
                            <li><a href="video.php">Videos</a></li>
                            <li><a href="lifestyle.php">Lifestyle</a></li>
                            <li><a href="photo-details.php">Spotted</a></li>
                        </ul>
                        <ul class="float-right right-nav">
                            <li class="nav-item float-left">
                                <form class="form-inline">
                                    <a class="search-bar-lg" data-toggle="modal" href="#searchModal" data-toggle="modal" data-target="#searchModal">
                                        <span class="icon"><i class="ion-ios-search"></i></span>
                                    </a>
                                </form>
                            </li>
                            <li class="nav-item float-left border-l-r">
                                <a href="index-hindi.php">
                                    <span class="p-10"><img src="assets/img/language-icon.png" class="img-fluid" title="Change Language" alt="Language"></span>
                                </a>
                            </li>
                            <li class="nav-item float-left mr-10 hidden-md-down">
                                <a href="#"  data-toggle="modal" data-target="#login">
                                    <span class="icon">
                                        <i class="ion-ios-person-outline fs"></i>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div><!-- /.nav-collapse -->
                </div>
            </nav>
        </header>

        <div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="wrap">
                            <ul class="wrap-nav">
                                <li><span>News</span>
                                    <ul>
                                        <li><a href="#">Gossip</a></li>
                                        <li><a href="#">Bubble Diaries</a>
                                            <ul>
                                                <li><a href="#">Bubble Speaks</a></li>
                                                <li><a href="#">Nostalgia</a></li>
                                                <li><a href="#">Time Travel</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Guest Blogger</a></li>
                                    </ul>
                                </li>
                                <li><span>Celebrities</span>
                                    <ul>
                                        <li><a href="#">Interviews</a></li>
                                        <li><a href="#">Lifestyle</a></li>
                                        <li><a href="#">Top Trending</a></li>
                                    </ul>
                                </li>
                                <li><span>Movies</span>
                                    <ul>
                                        <li><a href="#">Reviews</a></li>
                                        <li><a href="#">Box Office</a></li>
                                        <li><a href="#">Upcoming Movies</a></li>
                                        <li><a href="#">Top Trending</a></li>
                                    </ul>
                                </li>
                                <li><span>Photos</span>
                                    <ul>
                                        <li><a href="#">Caught on Camera</a></li>
                                        <li><a href="#">Features</a></li>
                                        <li><a href="#">Events</a></li>
                                        <li><a href="#">Gym Spottings</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="wrap-nav">
                                <li><span>Bubble TV</span>
                                    <ul>
                                        <li><a href="#">BB Specials</a></li>
                                        <li><a href="#">Price Tag</a></li>
                                        <li><a href="#">SHort Talk</a></li>
                                        <li><a href="#">Break Time</a></li>
                                        <li><a href="#">B-Town Backpackers</a></li>
                                        <li><a href="#">Bubble Snoop</a></li>
                                        <li><a href="#">Bollywood Sass</a></li>
                                    </ul>
                                </li>
                                <li><span class="no-bg">&nbsp;</span>
                                    <ul>
                                        <li><a href="#">Bollywood Spotting </a></li>
                                        <li><a href="#">Bubble Bytes</a></li>
                                        <li><a href="#">Petstagram</a></li>
                                        <li><a href="#">Road to Fitness</a></li>
                                        <li><a href="#">Postal Address</a></li>
                                        <li><a href="#">Time Travel</a></li>
                                        <li><a href="#">Bollywood FAM JAM</a></li>
                                    </ul>
                                </li>
                                <li><span>Lifestyle</span>
                                    <ul>
                                        <li><a href="#">Fitness</a>
                                            <ul>
                                                <li><a href="#">Road to Fitness</a></li>
                                                <li><a href="#">Gym Spottings</a></li>
                                                <li><a href="#">Gym Videos</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">B-Town Juniors</a></li>
                                        <li><a href="#">Postal Address</a></li>
                                        <li><a href="#">Sportstainment</a></li>
                                        <li><a href="#">Exclusives</a></li>
                                        <li><a href="#">Couple Goals</a></li>
                                    </ul>
                                </li>
                                <li><span class="no-bg">&nbsp;</span>
                                    <ul>
                                        <li><a href="#">Bollywood FAM JAM</a></li>
                                        <li><a href="#">OOTD</a></li>
                                        <li><a href="#">Wonders & Blunders</a></li>
                                        <li><a href="#">Trendsetters</a></li>
                                        <li><a href="#">Price Tag</a></li>
                                        <li><a href="#">Clone the Look</a></li>
                                        <li><a href="#">B-Town Backpackers</a></li>
                                        <li><a href="#">Petstagram</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>


<?php @include 'header.php'; ?>
<nav class="nav shadow-bottom">
    <div class="container full-wide">
        <div class="row">
            <div class="breadcrumb">
                <a href="index.php">Home</a>
                <a href="photo-listing.php" class="active">Photo Lists</a>
            </div>
        </div>
    </div>
</nav>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="text-center mb-30">
                    <i class="arrows"><img src="assets/img/left-bar.png"></i>
                    <h1 class="d-inline title text-center uppercase">photo lists</h1>
                    <i class="arrows"><img src="assets/img/right-bar.png"></i>
                </div>
            </div>
        </div>
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 float-left">
                <div class="card">
                    <a href="#" class="p-relative">
                        <img src="assets/img/spotted-1.jpg" alt="video-link" class="img-fluid">
                        <label for="" class="numbers">10</label>
                    </a>
                    <div class="card-title text-center">
                        <h3 class="font-22 font-wt-400 font-black mb-10">Photo: Sushant Singh Rajput will make you blush with his charm</h3>
                        <small class="font-light font-wt-400">October 21, 2017</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 float-left">
                <div class="card photo">
                    <a href="#" class="p-relative">
                        <img src="assets/img/spotted-2.jpg" alt="video-link" class="img-fluid">
                        <label for="" class="numbers">10</label>
                    </a>
                    <div class="card-title text-center">
                        <h3 class="font-22 font-wt-400 font-black mb-10">Arjun Kapoor and Parineeti Chopra start shooting...</h3>
                        <small class="font-light font-wt-400">October 21, 2017</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 float-left">
                <div class="card photo">
                    <a href="#" class="p-relative">
                        <img src="assets/img/spotted-3.jpg" alt="video-link" class="img-fluid">
                        <label for="" class="numbers">10</label>
                    </a>
                    <div class="card-title text-center">
                        <h3 class="font-22 font-wt-400 font-black mb-10">Who is Amir Khan? The boxing world champ... </h3>
                        <small class="font-light font-wt-400">October 21, 2017</small>
                    </div>
                </div>
            </div>
        </div>

        <div class="row xs-hidden" data-plugin="matchHeight" data-by-row="true">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 float-left">
                <div class="card photo">
                    <a href="#" class="p-relative">
                        <img src="assets/img/spotted-4.jpg" alt="video-link" class="img-fluid">
                        <label for="" class="numbers">10</label>
                    </a>
                    <div class="card-title text-center">
                        <h3 class="font-22 font-wt-400 font-black mb-10">WOW! Zaira Wasim receives National Child Award from..</h3>
                        <small class="font-light font-wt-400">October 21, 2017</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 float-left">
                <div class="card photo">
                    <a href="#" class="p-relative">
                        <img src="assets/img/spotted-5.jpg" alt="video-link" class="img-fluid">
                        <label for="" class="numbers">10</label>
                    </a>
                    <div class="card-title text-center">
                        <h3 class="font-22 font-wt-400 font-black mb-10">Irrfan Khan plays a Bengali writer inspired by Humayun Ahmed..</h3>
                        <small class="font-light font-wt-400">October 21, 2017</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 float-left">
                <div class="card photo">
                    <a href="#" class="p-relative">
                        <img src="assets/img/spotted-6.jpg" alt="video-link" class="img-fluid">
                        <label for="" class="numbers">10</label>
                    </a>
                    <div class="card-title text-center">
                        <h3 class="font-22 font-wt-400 font-black mb-10">Aditya Roy Kapur, Shraddha Kapoor, Gurmeet Choudhary spotted after </h3>
                        <small class="font-light font-wt-400">October 21, 2017</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="row xs-hidden" data-plugin="matchHeight" data-by-row="true">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 float-left">
                <div class="card photo">
                    <a href="#" class="p-relative">
                        <img src="assets/img/gossips-1.jpg" alt="video-link" class="img-fluid">
                        <label for="" class="numbers">10</label>
                    </a>
                    <div class="card-title text-center">
                        <h3 class="font-22 font-wt-400 font-black mb-10">WOW! Zaira Wasim receives National Child Award from..</h3>
                        <small class="font-light font-wt-400">October 21, 2017</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 float-left">
                <div class="card photo">
                    <a href="#" class="p-relative">
                        <img src="assets/img/gossips-2.jpg" alt="video-link" class="img-fluid">
                        <label for="" class="numbers">10</label>
                    </a>
                    <div class="card-title text-center">
                        <h3 class="font-22 font-wt-400 font-black mb-10">Irrfan Khan plays a Bengali writer inspired by Humayun Ahmed..</h3>
                        <small class="font-light font-wt-400">October 21, 2017</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 float-left">
                <div class="card photo">
                    <a href="#" class="p-relative">
                        <img src="assets/img/gossips-3.jpg" alt="video-link" class="img-fluid">
                        <label for="" class="numbers">10</label>
                    </a>
                    <div class="card-title text-center">
                        <h3 class="font-22 font-wt-400 font-black mb-10">Aditya Roy Kapur, Shraddha Kapoor, Gurmeet Choudhary spotted after </h3>
                        <small class="font-light font-wt-400">October 21, 2017</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="button-center">
            <a href="#" class="btn btn-default">Load more</a>
        </div>
    </div>
</section>

<?php @include 'footer.php'; ?>
